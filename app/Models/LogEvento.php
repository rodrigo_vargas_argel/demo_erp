<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogEvento extends Model
{
  //  public $timestamps = false;
    use SoftDeletes;

    protected $fillable = ['tabla','id_tabla','usuarios_id','tipo_evento','detalle'];

    public function usuarios(){
        return $this->belongsTo(\App\User::class, 'usuarios_id', 'id');
    }
    public function servicio(){
        return $this->belongsTo(Servicio::class, 'id_tabla', 'Id');
    }
}