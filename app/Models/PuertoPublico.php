<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PuertoPublico extends Model
{
    protected $table = 'mensualidad_puertos_publicos';
    protected $primaryKey = 'IdMensualidadPuertosPublicos';
    public $timestamps = false;

    protected $fillable = ['IdServicio','PuertoTCPUDP','Descripcion'];

    public function servicios(){
        return $this->hasOne(Servicio::class, 'IdServicio', 'Id');
    }

}