<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';
    public $timestamps = false;

    protected $fillable = ['nombre','provincia_id'];


    public function provincias(){
        return $this->belongsTo(Provincia::class, 'provincia_id');
    }

    public function clientes(){
        return $this->hasMany(Cliente::class, 'ciudad' );
    }
    
    
}