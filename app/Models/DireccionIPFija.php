<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DireccionIPFija extends Model
{
    protected $table = 'mensualidad_direccion_ip_fija';
    protected $primaryKey = 'IdMensualidadDireccionIPFija';
    public $timestamps = false;

    protected $fillable = ['IdServicio','DireccionIPFija','Descripcion'];

    public function servicios(){
        return $this->hasOne(Servicio::class, 'IdServicio', 'Id');
    }

}