<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Correo_Factibilidad extends Model
{
    protected $table = 'correo_factibilidades';
    public $timestamps = true;

    protected $fillable = ['asunto','comentarios_id'];

    public function comentario()
    {
        return $this->belongsTo(Comentario::class, 'comentarios_id');
    }

    public function destinatarios()
    {
        return $this->belongsToMany(User::class, 'correo_factibilidades_has_destinatarios', 'correo_factibilidades_id', 'usuarios_id');
    }
}
