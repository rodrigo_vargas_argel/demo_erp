<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = 'provincias';
    public $timestamps = false;

    protected $fillable = ['nombre','region_id'];

    public function regiones(){
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function ciudades(){
        return $this->hasMany(Ciudad::class, 'provincia_id' );
    }
    


}