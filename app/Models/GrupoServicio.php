<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoServicio extends Model
{
    protected $table = 'grupo_servicio';
    protected $primaryKey = 'IdGrupo';
    public $timestamps = false;

    protected $fillable = ['Nombre','EsOC'];

    public function servicios(){
        return $this->hasMany(Servicio::class, 'Grupo');
    }
}