<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketOtEquipo extends Model
{
    protected $table = 'ticket_ot_equipos';
    public $timestamps = true;

    protected $fillable = ['ticket_ot_id', 'equipo_id', 'cantidad', 'usuario_id', 'entregado', 'usuario_entrega'];

    public function ticket_ot() {
        return $this->belongsTo(TicketOt::class, 'ticket_ot_id', 'id');
    }

    public function equipos(){
        return $this->belongsTo(\App\producto::class, 'equipo_id', 'id');
    }

    public function usuarios() {
        return $this->belongsTo(\App\User::class, 'usuario_id', 'id');
    }

    public function usuario_entrega() {
        return $this->belongsTo(\App\User::class, 'usuario_entrega', 'id');
    }

}
