<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TraficoGenerado extends Model
{
    protected $table = 'trafico_generado';	//Telefonía IP
    protected $primaryKey = 'IdTraficoGenerado';
    public $timestamps = false;

    protected $fillable = ['IdServicio','LineaTelefonica','Descripcion'];

    public function servicios(){
        return $this->hasOne(Servicio::class, 'IdServicio', 'Id');
    }

}