<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contacto extends Model
{
    protected $table = 'contactos';
    public $timestamps = false;
    use SoftDeletes;

    protected $fillable = ['contacto','tipo_contacto','correo','telefono','rut'];

    public function tipos_contacto(){
        return $this->belongsTo(MantenedorTipoContacto::class, 'tipo_contacto');
    }
}