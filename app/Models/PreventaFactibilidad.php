<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreventaFactibilidad extends Model
{
    protected $table = 'preventa_factibilidades';

    protected $fillable = ['nombre'];
}
