<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketPrioridad extends Model
{
    protected $table = 'ticket_prioridad';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['id', 'nombre','TiempoHora'];

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'prioridad_id', 'id');
    }
}
