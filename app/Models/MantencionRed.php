<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MantencionRed extends Model
{
    protected $table = 'mantencion_red';
    protected $primaryKey = 'IdMantencionRed';
    public $timestamps = false;

    protected $fillable = ['IdServicio','ComentarioDatosAdicionales','Descripcion'];

    public function servicios(){
        return $this->hasOne(Servicio::class, 'IdServicio', 'Id');
    }

}