<?php

namespace App\Models;

use Factura;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';
    protected $primaryKey = 'Id';
    public $timestamps = false;


    protected $fillable = [
        'Rut', 'Grupo', 'TipoFactura', 'Valor', 'Descuento', 'IdServicio', 'Codigo', 'Descripcion', 'EstatusInstalacion', 'FechaInstalacion', 'InstaladoPor', 'Comentario', 'UsuarioPppoe', 'Conexion', 'IdUsuarioSession', 'Direccion', 'Latitud', 'Longitud', 'Referencia', 'Contacto', 'Fono', 'FechaComprometidaInstalacion', 'PosibleEstacion', 'Equipamiento', 'SenalTeorica','IdUsuarioAsignado','SenalFinal', 'EstacionFinal', 'EstatusFacturacion', 'CostoInstalacion', 'FacturarSinInstalacion', 'CostoInstalacionDescuento', 'UsuarioPppoeTeorico', 'FechaFacturacion', 'FechaInicioDesactivacion', 'FechaFinalDesactivacion', 'FechaUltimoCobro', 'NombreServicioExtra', 'EstatusServicio', 'tipo_moneda', 'create_at', 'update_at', 'CostoInstalacionBackup', 'descuentoTemporal', 'UrlGraficos'
    ];

    public function getPlanIdAttribute()
    {
        if ($this->IdServicio == 1) {
            return @$this->arriendo_equipos->planes_id;
        } elseif ($this->IdServicio == 2) {
            return @$this->servicio_internet->planes_id;
        } else {
            return null;
        }

    }

    public function getPlanNombreAttribute()
    {
        if ($this->IdServicio == 1) {
            if (isset($this->arriendo_equipos->planes->nombre)) {
                return @$this->arriendo_equipos->planes->nombre." ( ".@$this->arriendo_equipos->planes->velocidad_subida."x".@$this->arriendo_equipos->planes->velocidad_bajada." )" ;
            } else {
                return @$this->arriendo_equipos->Plan." ( ".@$this->arriendo_equipos->Velocidad." )" ;
            }
        } elseif ($this->IdServicio == 2) {
            if (isset($this->servicio_internet->planes->nombre)) {
                return @$this->servicio_internet->planes->nombre." ( ".@$this->servicio_internet->planes->velocidad_subida."x".@$this->servicio_internet->planes->velocidad_bajada." )";
            } else {
                return @$this->servicio_internet->Plan." ( ".@$this->servicio_internet->Velocidad." )" ;
            }
        } else {
            return null;
        }

    }

    public function servicios_equipos(){
        return $this->hasMany(Cliente::class, 'Id', 'servicios_id' );
    }
    public function servicios(){
        return $this->belongsTo(GrupoServicio::class, 'Grupo', 'IdGrupo');
    }
    public function tipo_servicios(){
        return $this->belongsTo(TipoServicio::class, 'IdServicio', 'IdServicio');
    }
    public function tipo_facturas(){
        return $this->belongsTo(TipoFactura::class, 'TipoFactura', 'id');
    }
    public function estado_servicios(){
        return $this->belongsTo(EstadoServicio::class, 'EstatusServicio', 'id');
    }
    public function clientes(){
        return $this->belongsTo(Cliente::class, 'Rut', 'rut');
    }
    public function tecnico_asignado(){
        return $this->belongsTo(\App\User::class, 'IdUsuarioAsignado', 'id');
    }
    public function tecnico_instalador(){
        return $this->belongsTo(\App\User::class, 'InstaladoPor', 'id');
    }
    public function estaciones(){
        return $this->belongsTo(\App\mantenedor_site::class, 'EstacionFinal', 'id');
    }
    public function arriendo_equipos(){   // 1
        return $this->hasOne(ArriendoEquiposDato::class, 'IdServicio', 'Id');
    }
    public function servicio_internet(){  // 2
        return $this->hasOne(ServicioInternet::class, 'IdServicio', 'Id');
    }
    public function ip_fija(){     // 4
        return $this->hasOne(DireccionIPFija::class, 'IdServicio', 'Id');
    }
    public function puerto_publico(){   // 3
        return $this->hasOne(PuertoPublico::class, 'IdServicio', 'Id');
    }
    public function mantencion_red(){   // 5
        return $this->hasOne(MantencionRed::class, 'IdServicio', 'Id');
    }
    public function trafico_generado(){  // 6
        return $this->hasOne(TraficoGenerado::class, 'IdServicio', 'Id');
    }
    public function tickets()  {
        return $this->hasMany(Ticket::class, 'servicios_id', 'Id');
    }
    public function ultima_factura(){
        //return $this->hasOne(\Factura::class, 'Rut','Rut')->latest();
        return $this->hasOne(Factura::class, 'Rut','Rut')->where('EstatusFacturacion','!=',0)->latest();
       // return $this->hasOne(\App\factura::getConnection,'Rut','Rut')->orderBy('create_at', 'desc')->first();
      //  return factura::query()->where('Rut','=',$this->Rut)->orderBy('create_at', 'desc')->first();
    }


}
