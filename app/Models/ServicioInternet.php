<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicioInternet extends Model
{
    protected $table = 'servicio_internet';
    protected $primaryKey = 'IdServInternet';
    public $timestamps = false;

    protected $fillable = ['Velocidad', 'Plan','IdServicio','planes_id','IdOrigen','IdProducto','TipoDestino'];

    public function servicios(){
        return $this->hasOne(Servicio::class, 'Id', 'IdServicio');
    }

    public function planes(){
        return $this->hasOne(Plan::class, 'id', 'planes_id');
    }

}