<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario_Factibilidad extends Model
{
    protected $table = 'comentarios_factibilidades';
    public $timestamps = true;

    protected $fillable = [
        'comentario'
    ];

    public function contacto_factibilidades()
    {
        return $this->hasMany(Contacto_Factibilidad::class, 'comentarios_factibilidades_id', 'id');
    }
}
