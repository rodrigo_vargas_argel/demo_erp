<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\mantenedor_site;

class Ticket extends Model
{
    protected $table = 'tickets';
    public $timestamps = true;

    protected $fillable = ['origen_id', 'tipo_id', 'area_id', 'nivel_id','prioridad_id','cliente_id','servicios_id','estacion_id','tecnico_id','fecha_visita','hora_visita','estado_id','descripcion','usuarios_id','fecha_evento','hora_evento'];

    public function ticket_ot() {
        return $this->hasMany(TicketOt::class);
    }

    public function ticket_tipo() {
        return $this->belongsTo(TicketTipo::class, 'tipo_id', 'id');
    }

    public function ticket_area() {
        return $this->belongsTo(TicketArea::class, 'area_id', 'id');
    }

    public function ticket_estado() {
        return $this->belongsTo(TicketEstado::class,'estado_id');
    }

    public function clientes() {
        return $this->belongsTo(Cliente::class, 'cliente_id', 'id');
    }

    public function servicios() {
        return $this->belongsTo(Servicio::class, 'servicios_id', 'Id');
    }

    public function ticket_origen()
    {
        return $this->belongsTo(TicketOrigen::class, 'origen_id');
    }

    public function ticket_nivel()
    {
        return $this->belongsTo(TicketNivel::class, 'nivel_id', 'id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarios_id','id');
    }

    public function tecnico()
    {
        return $this->belongsTo(User::class, 'tecnico_id','id');
    }

    public function ticket_prioridad()
    {
        return $this->belongsTo(TicketPrioridad::class, 'prioridad_id', 'id');
    }

    public function ticket_auditorias()
    {
        return $this->hasMany(TicketAuditoria::class, 'tickets_id', 'id');
    }
    public function estacion()
    {
        return $this->belongsTo(mantenedor_site::class, 'estacion_id');
    }

    public function comentarios_tickets()
    {
        return $this->hasMany(TicketComentario::class, 'tickets_id', 'id');
    }

    public function ticket_cierre()
    {
        return $this->hasOne(TicketCierre::class, 'id', 'ticket_id');
    }

}
