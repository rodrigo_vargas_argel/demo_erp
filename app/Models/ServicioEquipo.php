<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicioEquipo extends Model
{
    protected $table = 'servicios_equipos';
    use SoftDeletes;

    protected $fillable = ['servicios_id','producto_id'];

    public function servicios(){
        return $this->belongsTo(Region::class, 'servicios_id', 'Id');
    }

    public function productos(){
        return $this->belongsTo(\App\producto::class, 'producto_id', 'id');
    }



}