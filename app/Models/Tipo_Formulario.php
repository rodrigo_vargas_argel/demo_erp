<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo_Formulario extends Model
{
    protected $table = 'tipo_formulario';
    public $timestamps = true;

    protected $fillable = ['formulario'];

    public function contacto_factibilidades()
    {
        return $this->hasMany(Contacto_Factibilidad::class, 'tipo_formulario_id', 'id');
    }
}
