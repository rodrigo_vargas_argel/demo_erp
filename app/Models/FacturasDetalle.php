<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacturasDetalle extends Model
{
    protected $table = 'facturas_detalle';
    protected $primaryKey = 'Id';
    public $timestamps = false;

    protected $fillable = [
        'FacturaId', 'Concepto', 'Valor', 'Descuento', 'Cantidad', 'Total', 'IdServicio', 'Codigo', 'documentDetailIdBsale', 'create_at', 'update_at', 'delete_at', 'FacturaIdTmp'
    ];

}
