<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Giro extends Model
{
    protected $table = 'giros';
    public $timestamps = false;

    protected $fillable = ['nombre'];

}