<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoCorreoUsuario extends Model
{
    protected $table = 'motivo_correo_usuarios';
    protected $fillable = ['motivo_correos_id', 'usuarios_id'];
    
    use SoftDeletes;

    public function motivo_correos(){
        return $this->belongsTo(MotivoCorreo::class);
    }
    public function usuarios(){
        return $this->belongsTo(\App\User::class);
    }
}