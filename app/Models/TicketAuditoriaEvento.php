<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketAuditoriaEvento extends Model
{
    protected $table = 'ticket_auditoria_evento';
    protected $fillable = ['nombre'];

    public function ticket_auditorias()
    {
        return $this->hasMany(TicketAuditoria::class, 'evento_id', 'id');
    }

}
