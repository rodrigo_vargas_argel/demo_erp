<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Comentario extends Model
{
    protected $table = 'comentarios';
    public $timestamps = true;

    protected $fillable = [
        'comentario','contacto_factibilidades_id','usuarios_id'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarios_id');
    }

    public function contacto_factibilidad()
    {
        return $this->belongsTo(contacto_factibilidad::class, 'contacto_factibilidades_id');
    }

    public function correo()
    {
        return $this->hasOne(Correo_Factibilidad::class, 'comentarios_id', 'id');
    }
}
