<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketTipo extends Model
{
    protected $table = 'ticket_tipo';
    public $timestamps = true;

    protected $fillable = ['nombre', 'es_ticket', 'es_ot'];

    public function tickets() {
        return $this->hasMany(Ticket::class, 'tipo_id', 'id');
    }


}