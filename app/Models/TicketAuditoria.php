<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class TicketAuditoria extends Model
{
    protected $table = 'ticket_auditoria';
    protected $fillable = ['comentario','evento_id','usuarios_id','tickets_id'];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'tickets_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarios_id');
    }

    public function ticket_auditoria_evento()
    {
        return $this->belongsTo(TicketAuditoriaEvento::class, 'evento_id');
    }

    //llamarlo así xd    TicketAuditoria::grabaAuditoria(1, $ticket, "bla bla bla" );
    public static function grabaAuditoria($evento, $ticket, $comentario){
        $ticket_auditoria = TicketAuditoria::create([
                    'comentario'  => $comentario,
                    'evento_id'   => $evento, // 1=Crear   -   2=Editar   -  3=ELiminar
                    'tickets_id'  => $ticket,
                    'usuarios_id' => Auth::user()->id
            ]);
    }

}
