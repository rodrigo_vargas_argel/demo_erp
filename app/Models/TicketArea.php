<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketArea extends Model
{
    protected $table = 'ticket_area';
    public $timestamps = true;

    protected $fillable = ['nombre'];

    public function tickets() {
        return $this->hasMany(Ticket::class, 'area_id', 'id');
    }

}