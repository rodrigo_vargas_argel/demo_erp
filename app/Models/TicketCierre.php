<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketCierre extends Model
{
    protected $table = 'ticket_cierre';
    public $timestamps = true;

    protected $fillable = ['ticket_id', 'fecha_salida', 'hora_salida', 'fecha_llegada', 'hora_llegada', 'observaciones', 'usuario_id'];

    public function tickets() {
        return $this->hasOne(Ticket::class, 'ticket_id', 'id');
    }


}