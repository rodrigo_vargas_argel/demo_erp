<?php
/**
 * Created by PhpStorm.
 * User: GeekStore
 * Date: 28/8/2020
 * Time: 05:23
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Factura_Pago extends Model
{
    protected $table = 'facturas_pagos';
    public $timestamps = false;
    use SoftDeletes;

    public function factura()    {

        return $this->belongsTo(Factu::class,'FacturaId', 'Id');
    }

    public function getIdUsuarioSessionAttribute($value)
    {
        $usuario= User::where('id', $value)->first();
        return $usuario->nombre;

    }

    public function usuario(){
        return $this->belongsTo(User::class,'IdUsuarioSession', 'id');

    }

    public function tipo_pago(){
        return $this->belongsTo(mantenedor_tipo_pago::class,'TipoPago', 'id');

    }

    public function getNombrePagoAttribute(){

        $tipo_pago= mantenedor_tipo_pago::query()->where('id', $this->TipoPago)->first();
        return $tipo_pago->nombre;

    }


}