<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estados';
    public $timestamps = true;

    protected $fillable = ['estado'];

    public function contacto_factibilidades()
    {
        return $this->hasMany(Contacto_Factibilidad::class, 'estados_id', 'id');
    }
}
