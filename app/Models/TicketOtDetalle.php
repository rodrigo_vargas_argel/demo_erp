<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketOtDetalle extends Model
{
    protected $table = 'ticket_ot_detalle';
    public $timestamps = true;

    protected $fillable = ['ticket_ot_id', 'nombre', 'estado', 'usuario_id'];

    public function ticket_ot() {
        return $this->belongsTo(TicketOt::class, 'ticket_ot_id', 'id');
    }

    public function usuarios() {
        return $this->belongsTo(\App\User::class, 'usuario_id', 'id');
    }


}
