<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\mantenedor_site;
use App\mantenedor_tipo_cliente;

class Contacto_Factibilidad extends Model
{
    use SoftDeletes;
 
    
    protected $table = 'contacto_factibilidades';
    public $timestamps = true;

    protected $fillable = ['rut','dv','fecha_cotizacion','nombre','email','telefono','mensaje','ubicacion','latitud','longitud','distancia',
    'ip','tipo_formulario_id','estados_id','planes_id','mantenedor_site_id','mantenedor_tipo_cliente_id','comentarios_factibilidades_id','estaciones_de_prueba',
    'tipo_cliente_factibilidades_id','preventa_factibilidades_id','estado_contrato_factibilidades_id','nro_servicio','plan_inicial','habilitacion',
    'nro_documentos','estados_id_2','fecha_instalacion','fecha_activacion','fecha_respuesta'];

    protected $appends = ['rut_completo','nombre_estado','edicion','cantidad_comentarios','fecha_solicitud'];

    protected $dates = ['deleted_at'];


    public function tipo_formulario()
    {
        return $this->belongsTo(Tipo_Formulario::class,'tipo_formulario_id');
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class,'estados_id');
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class,'planes_id');
    }

    public function torre()
    {
        return $this->belongsTo(mantenedor_site::class,'mantenedor_site_id');
    }

    public function tipo_cliente()
    {
        return $this->belongsTo(mantenedor_tipo_cliente::class,'mantenedor_tipo_cliente_id');
    }

    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'contacto_factibilidades_id', 'id');
    }

    public function comentario_factibilidad()
    {
        return $this->belongsTo(Comentario_Factibilidad::class, 'comentarios_factibilidades_id');
    }

    public function tipo_cliente_factibilidad()
    {
        return $this->belongsTo(TipoClienteFactibilidad::class, 'tipo_cliente_factibilidades_id');
    }

    public function preventa_factibilidad()
    {
        return $this->belongsTo(PreventaFactibilidad::class, 'preventa_factibilidades_id');
    }

    public function estado_contrato()
    {
        return $this->belongsTo(EstadoContrato::class, 'estado_contrato_factibilidades_id');
    }

    public function estado_2()
    {
        return $this->belongsTo(Estado::class,'estados_id_2');
    }

    public function getRutCompletoAttribute(){
        $valor = "";
        if($this->rut && $this->dv){
            $valor = $this->rut . '-' . $this->dv;
        }
        return $valor;
    }

    public function getNombreEstadoAttribute(){
        $valor = "";
        if($this->estados_id){
            $valor = $this->estado->estado;
        }
        return $valor;
    }

    public function getEdicionAttribute(){
        return false;
    }

    public function getCantidadComentariosAttribute(){
        return count($this->comentarios);
    }

    public function getFechaSolicitudAttribute(){
        if($this->created_at){
            return $this->created_at->format("d/m/Y H:i");
        }else{
            return null;
        }
    }
}
