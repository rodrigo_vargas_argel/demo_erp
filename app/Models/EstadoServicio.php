<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoServicio extends Model
{
    protected $table = 'estado_servicios';
    public $timestamps = false;
    protected $fillable = ['id', 'nombre'];
    
    public function compras(){
        return $this->hasMany(Servicio::class,'EstatusServicio');
    }
    
}