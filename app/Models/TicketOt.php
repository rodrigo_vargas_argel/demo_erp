<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketOt extends Model
{
    protected $table = 'ticket_ot';
    public $timestamps = true;
    public $comentario;

    protected $fillable = ['ticket_id', 'usuario_id', 'path', 'estado_id', 'descripcion'];

    public function ticket_ot_detalle() {
        return $this->hasMany(TicketOtDetalle::class, 'ticket_ot_id', 'id');
    }

    public function ticket_ot_equipo() {
        return $this->hasMany(TicketOtEquipo::class, 'ticket_ot_id', 'id');
    }

    public function tickets() {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }

    public function usuarios() {
        return $this->belongsTo(\App\User::class, 'usuario_id', 'id');
    }

    public function ticket_estado() {
        return $this->belongsTo(TicketEstado::class, 'estado_id', 'id');
    }


}
