<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoCorreo extends Model
{
    protected $table = 'motivo_correos';
    protected $fillable = ['nombre', 'texto'];
    
    use SoftDeletes;

    public function correo_usuarios(){
        return $this->hasMany(MotivoCorreoUsuario::class, 'motivo_correos_id');
    }
}