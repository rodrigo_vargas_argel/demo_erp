<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoContrato extends Model
{
    protected $table = 'estado_contrato_factibilidades';

    protected $fillable = ['estado'];
}
