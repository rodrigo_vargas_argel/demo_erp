<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'personaempresa';
    public $timestamps = false;

    protected $fillable = [
        'rut', 'dv', 'nombre', 'giro', 'direccion', 'correo', 'contacto', 'comentario', 'telefono', 'region', 'ciudad', 'alias', 'tipo_cliente', 'id_usuario_sistema', 'clase_cliente', 'cliente_id_bsale', 'tipo_pago_bsale_id', 'posee_pac', 'state', 'fecha_creacion', 'fecha_actualizacion', 'href', 'firstName', 'lastName', 'hasCredit', 'maxCredit', 'city', 'companyOrPerson', 'accumulatePoints', 'points', 'pointsUpdated', 'sendDte', 'isForeigner', 'posee_prefactura'
    ];

    public function servicios(){
        return $this->hasMany(Servicio::class, 'Rut', 'rut');
    }
    public function tipoClientes(){
        return $this->belongsTo(\App\mantenedor_tipo_cliente::class, 'tipo_cliente');
    }

    public function tipoPagos(){
        return $this->belongsTo(MantenedorTipoPagoBsale::class, 'tipo_pago_bsale_id');
    }

    public function claseClientes(){
        return $this->belongsTo(\App\clase_cliente::class, 'clase_cliente');
    }

    public function regiones(){
        return $this->belongsTo(Region::class, 'region');
    }

    public function ciudades(){
        return $this->belongsTo(Ciudad::class, 'ciudad');
    }
    public function notaVenta(){
        return $this->hasMany(\App\nota_de_venta::class, 'rut', 'rut');
    }

    public function factura(){
        return $this->hasMany(\App\factura::class, 'Rut', 'rut');
    }

    public function tickets(){
        return $this->hasMany(\App\Models\Ticket::class, 'cliente_id', 'rut');
    }
    
    public function ultima_factura(){
        return $this->hasOne(Factura::class, 'Rut','rut')->where('EstatusFacturacion','!=',0)->latest();
        // return $this->hasOne(\App\factura::getConnection,'Rut','Rut')->orderBy('create_at', 'desc')->first();
        //  return factura::query()->where('Rut','=',$this->Rut)->orderBy('create_at', 'desc')->first();
    }


    public function contacto(){
        return $this->hasMany(Contacto::class, 'rut', 'rut');
    }


}
