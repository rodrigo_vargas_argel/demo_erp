<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketEstado extends Model
{
    protected $table = 'ticket_estado';
    public $timestamps = true;

    protected $fillable = ['nombre', 'es_ticket', 'es_ot'];

    public function ticket_ot() {
        return $this->hasMany(TicketOt::class, 'estado_id', 'id');
    }

    public function tickets() {
        return $this->hasMany(Ticket::class, 'estado_id', 'id');
    }

}