<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoFacturacion extends Model
{
    protected $table = 'mantenedor_tipo_facturacion';
    public $timestamps = false;


    protected $fillable = ['nombre']; 

    public function tipos_factura(){
        return $this->hasMany(TipoFactura::class, 'tipo_facturacion');
    }

    
}