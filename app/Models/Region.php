<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regiones';
    public $timestamps = false;

    protected $fillable = ['nombre','region_ordinal'];

    public function provincias(){
        return $this->hasMany(Provincia::class, 'region_id' );
    }

    public function clientes(){
        return $this->hasMany(Cliente::class, 'region' );
    }

}