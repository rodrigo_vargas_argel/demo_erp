<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoClienteFactibilidad extends Model
{
    protected $table = 'tipo_cliente_factibilidades';

    protected $fillable = ['nombre'];
}
