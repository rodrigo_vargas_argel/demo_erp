<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class TicketComentario extends Model
{
    protected $table = 'comentarios_tickets';
    protected $fillable = ['comentario','tickets_id','usuarios_id'];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'tickets_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarios_id');
    }
}
