<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{

    protected $table = 'planes';
    
    protected $fillable = ['nombre', 'velocidad_subida', 'velocidad_bajada', 'valor_referencia', 'tipo_moneda'];
    
    use SoftDeletes;


    public function arriendos(){   // 1
        return $this->hasOne(ArriendoEquiposDato::class, 'planes_id');
    }

    public function servicio_internet(){  // 2
        return $this->hasOne(ServicioInternet::class, 'planes_id');
    }
}

