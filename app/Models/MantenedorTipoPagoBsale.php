<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MantenedorTipoPagoBsale extends Model
{
    protected $table = 'mantenedor_tipo_pago_bsale';
    public $timestamps = false;

    

    public function clientes(){
        return $this->hasMany(Cliente::class, 'tipo_pago_bsale_id' );
    }
}
