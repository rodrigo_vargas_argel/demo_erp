<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MantenedorTipoContacto extends Model
{
    protected $table = 'mantenedor_tipo_contacto';
    public $timestamps = false;

    protected $fillable = ['nombre'];

    public function contactos(){
        return $this->hasMany(Contacto::class, 'tipo_contacto' );
    }
    
}