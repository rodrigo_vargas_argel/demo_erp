<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoFactura extends Model
{
    protected $table = 'mantenedor_tipo_factura';
    public $timestamps = false;

    protected $fillable = ['codigo', 'descripcion', 'tipo_facturacion', 'tipo_documento']; 

    public function tipos_facturacion(){
        return $this->belongsTo(\App\Models\TipoFacturacion::class, 'tipo_facturacion', 'id');
    }

    public function servicios(){
        return $this->hasMany(Servicio::class, 'IdServicio');
    }

    

    
}