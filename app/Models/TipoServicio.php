<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoServicio extends Model
{
    protected $table = 'mantenedor_servicios';
    protected $primaryKey = 'IdServicio';
    public $timestamps = false;


    protected $fillable = ['servicio']; //IdServicio

    public function servicios(){
        return $this->hasMany(Servicio::class, 'IdServicio', 'IdServicio');
    }
}