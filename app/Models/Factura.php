<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    protected $table = 'facturas';

    public $timestamps = false;

    protected $fillable = [
        'Rut', 'Grupo', 'TipoFactura', 'EstatusFacturacion','DocumentoIdBsale', 'NumeroDocumento', 'UrlPdfBsale', 'informedSiiBsale','responseMsgSiiBsale', 'FechaFacturacion', 'HoraFacturacion', 'TipoDocumento','FechaVencimiento', 'IVA', 'NumeroOC', 'FechaOC','Referencia',
    ];

    const CREATED_AT = 'create_at';

    public function pagos(){
    return  $this->hasMany(Factura_Pago::class, 'FacturaId', 'Id');
}
    public function pagos2(){
        return  $this->hasMany(\App\factura_pago::class, 'FacturaId', 'Id');
    }





}
