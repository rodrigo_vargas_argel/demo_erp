<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketNivel extends Model
{
    protected $table = 'ticket_nivel';
    protected $fillable = ['nombre'];

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'nivel_id', 'id');
    }
}
