<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketOrigen extends Model
{
    protected $table = 'ticket_origen';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['id', 'nombre'];

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'origen_id', 'id');
    }
}
