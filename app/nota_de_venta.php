<?php

namespace App;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class nota_de_venta extends Model
{
    protected $table = 'nota_venta';

    use SoftDeletes;

    public function nota_venta_detalle(){
       return  $this->hasMany(nota_venta_detalle::class, 'nota_venta_id');
    }

    public function responsable(){
        return  $this->belongsTo(User::class, 'solicitado_por');
    }

    public function cliente(){
        return  $this->belongsTo(Cliente::class, 'rut','rut');
    }

    public function factura(){
        return  $this->belongsTo(factura::class, 'factura_id','Id');
    }
    public function  hes(){
        return $this->hasMany(nota_venta_hes::class, 'nota_venta_id');
    }

    public function detalle(){
        return $this->hasMany(nota_venta_detalle::class, 'nota_venta_id');
    }
}
