<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_modelo_marca extends Model
{
    protected $table = 'mantenedor_modelos_marca';
    protected $fillable = ['mantenedor_marca_id','nombre'];
    //public $timestamps = false;
    use SoftDeletes;

//    public function marca(){
//        return $this->belongsTo(mantenedor_marca::class);
//    }
    public function marca(){
        return $this->belongsTo(mantenedor_marca::class, 'mantenedor_marca_id');
    }
    public function productos(){
        return $this->hasMany(producto::class);
    }
}
