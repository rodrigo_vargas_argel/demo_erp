<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class compra_ingreso_detalle extends Model
{
    protected $table = 'compras_detalle';
    // public $timestamps = false;
    use SoftDeletes;

    public function compra(){
        return $this->belongsTo(compra_ingreso::class,'compras_ingresos_id');
    }
    public function producto(){
        return $this->belongsTo(producto::class,'producto_id');
    }
    public function detalle_mac(){
        return $this->hasMany(compra_detalle_mac::class, 'compra_detalle_id');
    }

}
