<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class morosidades extends Model
{
    protected $table = 'morosidades';
    public $timestamps = false;
}
