<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existencia_rma extends Model
{
    protected $table = 'existencias_rma';
    public $timestamps = false;

    public function proveedor(){
        return $this->belongsTo(mantenedor_proveedor::class,'proveedor_id');
    }
    public function producto(){
        return $this->belongsTo(producto::class, 'producto_id');
    }
    public function  usuario(){
        return $this->belongsTo(User::class, 'usuario_salida');
    }
    public function  usuario2(){
        return $this->belongsTo(User::class, 'usuario_llegada');
    }
}
