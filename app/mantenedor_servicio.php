<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mantenedor_servicio extends Model
{
    protected $table = 'mantenedor_servicios';

    public function servicios(){
        return  $this->hasMany(servicio::class,'IdServicio','IdServicio');
    }
}
