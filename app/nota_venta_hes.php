<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nota_venta_hes extends Model
{
    protected $table = 'nota_venta_hes';

    public function nota_venta(){
        return  $this->belongsTo(nota_de_venta::class, 'nota_venta_id');
    }
}
