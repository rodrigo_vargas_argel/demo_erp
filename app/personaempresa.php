<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personaempresa extends Model
{
    protected $table = 'personaempresa';
    public $timestamps = false;
}
