<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class devoluciones extends Model
{
    protected $table = 'devoluciones';
    public $timestamps = false;



    public function factura()    {

        return $this->belongsTo(factura::class,'FacturaId', 'Id');
    }

    public function anulaciones(){
        return $this->hasOne(anulaciones::class,'DevolucionId','Id' );
    }
}
