<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Comentario;
use App\Models\Correo_Factibilidad;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'usuario', 'nombre', 'email', 'clave',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'clave', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function bodegas(){
        return $this->hasMany(mantenedor_bodega::class,'personal_id');
    }

    public function costos(){
        return $this->hasMany(mantenedor_costo::class,'personal_id');
    }
    public function sites(){
        return $this->hasMany(mantenedor_site::class,'personal_id');
    }
    public function traslados(){
        return $this->hasMany(existencias_traslados::class, 'usuario_id');
    }

    public function nota_venta(){
        return $this->hasMany(nota_de_venta::class, 'solicitado_por');
    }

    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'usuarios_id', 'id');
    }



    public function asignado_servicio(){
        return $this->hasMany(\App\Models\Servicio::class, 'IdUsuarioAsignado');
    }
    public function instalador_servicio()
    {
        return $this->hasMany(\App\Models\Servicio::class, 'InstaladoPor');
    }


    public function correo_factibilidades()
    {
        return $this->belongsToMany(Correo_Factibilidad::class, 'correo_factibilidades_has_destinatarios', 'usuarios_id', 'correo_factibilidades_id');

    }
    public function log_eventos(){
        return $this->hasMany(\App\Models\LogEvento::class, 'usuarios_id');

    }

    public function getAuthPassword()
    {
        return $this->clave;
    }


}
