<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class servicio extends Model
{
    protected $table = 'servicios';

    public function mantenedor_servicios(){
        return  $this->belongsTo(mantenedor_servicio::class,'IdServicio','IdServicio');
    }

    public function cliente(){
        return  $this->belongsTo(personaempresa::class,'Rut','rut');
    }

}
