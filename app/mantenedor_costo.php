<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_costo extends Model
{
    protected $table = 'mantenedor_costos';
    public $timestamps = false;
    use SoftDeletes;


    public function responsable()
    {

        //return $this->belongsTo('App\User', 'foreign_key', 'other_key');
        return $this->belongsTo(User::class,'personal_id', 'id');

    }
}
