<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto_archivo extends Model
{
    protected $table = 'productos_archivos';

    public function producto(){
        return $this->belongsTo(producto::class,'producto_id');
    }

}
