<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class compra_detalle_mac extends Model
{
    protected $table = 'compras_detalle_mac';
    // protected $fillable=['centro_costo_id'];
    // public $timestamps = false;
    use SoftDeletes;

    public function producto(){
        return $this->belongsTo(producto::class, 'producto_id');
    }

    public function compra(){
        return $this->belongsTo(compra_ingreso::class,'compra_id');
    }

    public function detalle_compra(){
        return $this->belongsTo(compra_ingreso_detalle::class,'compra_detalle_id');
    }
}
