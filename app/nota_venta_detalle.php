<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nota_venta_detalle extends Model
{
    protected $table = 'nota_venta_detalle';

    public function nota_venta(){
        $this->belongsTo(nota_de_venta::class, 'nota_venta_id');
    }
}
