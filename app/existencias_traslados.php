<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existencias_traslados extends Model
{
    protected $table = 'existencias_traslados';

    public function usuario(){
        return $this->belongsTo(User::class,'usuario_id');
    }

    public function detalle(){

        return $this->hasMany(existencias_traslado_detalle::class, 'existencia_traslado_id' );
    }
    public function bodega_origen(){
        return $this->belongsTo(mantenedor_bodega::class, 'bodega_origen_id');
    }

    public function bodega_destino(){
        return $this->belongsTo(mantenedor_bodega::class, 'bodega_destino_id');
    }
}
