<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log_login extends Model
{
    protected $table = 'log_login';
    public $timestamps = false;
}
