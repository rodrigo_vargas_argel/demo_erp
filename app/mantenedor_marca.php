<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_marca extends Model
{
    protected $table = 'mantenedor_marcas';
    protected $fillable = ['nombre'];
    // public $timestamps = false;
    use SoftDeletes;

    public function modelo()    {
        return $this->hasMany(mantenedor_modelo_marca::class,'mantenedor_marca_id');
    }

    public function marca(){
        return $this->hasMany(producto::class, 'mantenedor_marca_id');
    }

    public function producto(){
        return $this->hasMany(producto::class,'mantenedor_marca_id');
    }
}