<?php

namespace App\Mail;

use App\Models\Ticket;
use App\Models\TicketOt;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailAsignaTicket extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $ticket;
    public $tipo;
    public $modificacion;
    public $arr_titulo = array( 
                                1 => "Asignación Ticket de Soporte N° ", 
                                2 => "Ásignación Orden de Trabajo para Ticket N° ",
                                3 => "Modificación Ticket N° ", 
                            );

    public function __construct(Ticket $ticket, $tipo, $modificacion = " ")
    {
        $this->ticket = $ticket;
        $this->tipo = $tipo;
        $this->modificacion = $modificacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Asignación ticket '.$this->ticket->id)
        ->markdown('emails.asigna-ticket')
        ->with([
            'ticket' => $this->ticket,
            'tipo' => $this->tipo,
            'modificacion' => $this->modificacion,
            'titulo' => $this->arr_titulo[$this->tipo].$this->ticket->id
        ]);
    }
}