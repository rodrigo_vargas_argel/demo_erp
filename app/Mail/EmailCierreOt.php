<?php

namespace App\Mail;

use App\Models\Ticket;
use App\Models\TicketOt;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailCierreOt extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $ticket;
    public $tipo;
    public $arr_titulo = array( 
                                1 => "Cierre de Ticket N° ",
                                2 => "Cierre de OT y ticket N° ", 
                            );

    public function __construct(Ticket $ticket, $tipo)
    {
        $this->ticket = $ticket;
        $this->tipo = $tipo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->arr_titulo[$this->tipo].$this->ticket->id)
        ->markdown('emails.cierra-ot')
        ->with([
                    'ticket' => $this->ticket,
                    'tipo' => $this->tipo,
                    'titulo' => $this->arr_titulo[$this->tipo].$this->ticket->id
        ]);
    }
}