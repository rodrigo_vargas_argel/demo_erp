<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class envia_nc extends Mailable
{
    use Queueable, SerializesModels;

    public $email_content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_content)
    {
        $this->email_content = $email_content;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('teledatadte@teledata.cl')->view('mails.envia_nc')->subject('Teledata DTE NC');
        // return $this->from('teledatadte@teledata.cl')->view('emails.mail-servicios')->subject('Servicio Nuevo');
    }
}

?>
