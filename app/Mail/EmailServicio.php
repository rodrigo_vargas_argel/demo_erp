<?php

namespace App\Mail;

use App\Models\Servicio;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailServicio extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $servicio;
    public $tipo;
    public $arr_titulo = array( 
                                1 => "Creación de nuevo servicio", 
                                2 => "Ásignación de Instalación de Servicio",
                                3 => "Instalación de Servicio (activación)",
                                4 => "Modificación de Servicio "
                            );

    public function __construct(Servicio $servicio, $tipo)
    {
        $this->servicio = $servicio;
        $this->tipo = $tipo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Servicio código '.$this->servicio->Codigo)
        ->markdown('emails.mail-servicios')
        ->with([
            'servicio' => $this->servicio,
            'tipo' => $this->tipo,
            'titulo' => $this->arr_titulo[$this->tipo]
        ]);
    }
}