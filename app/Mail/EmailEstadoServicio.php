<?php

namespace App\Mail;

use App\Models\Servicio;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailEstadoServicio extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $servicio;
    public $tipo;
    public $arr_titulo = array( 
                                1 => "Cambio Estado Servicio"
                            );

    public function __construct(Servicio $servicio, $tipo)
    {
        $this->servicio = $servicio;
        $this->tipo = $tipo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Servicio código '.$this->servicio->Codigo)
        ->markdown('emails.estado-servicio')
        ->with([
            'servicio' => $this->servicio,
            'tipo' => $this->tipo,
            'titulo' => $this->arr_titulo[$this->tipo]
        ]);
    }
}