<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existencia_movimiento extends Model
{
    protected $table = 'existencias_movimientos';
    //public $timestamps = false;
    //protected $fillable=['nombre','personal_id'];
    //use SoftDeletes;

    public function existencia(){
        return $this->hasOne(existencia::class,'movimiento_id');
    }

    public function producto(){
        return $this->belongsTo(producto::class, 'producto_id');
    }
    public function bodega_origen(){
        return $this->belongsTo(mantenedor_bodega::class, 'bodega_origen_id');
    }

    public function bodega_destino(){
        return $this->belongsTo(mantenedor_bodega::class, 'bodega_destino_id');
    }


}
