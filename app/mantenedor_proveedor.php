<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_proveedor extends Model
{
    protected $table = 'mantenedor_proveedores';
    public $timestamps = false;
    protected $fillable=['nombre','rut','dv','direccion'];

    use SoftDeletes;

    public function productos(){
        return $this->hasMany(producto::class,'mantenedor_proveedores_id','id');
    }
}
