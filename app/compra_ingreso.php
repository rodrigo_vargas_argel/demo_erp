<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class compra_ingreso extends Model
{
    protected $table = 'compras_ingresos';
    protected $fillable=['centro_costo_id'];
    // public $timestamps = false;
    use SoftDeletes;

    public function tipo_documento(){
        return $this->belongsTo(mantenedor_tipo_proveedor::class,'tipo_documento_id');
    }

    public function proveedor(){
        return $this->belongsTo(mantenedor_proveedor::class,'proveedor_id');

    }

    public function centro_costo(){
        return $this->belongsTo(mantenedor_costo::class,'centro_costo_id');
    }

    public function bodega(){
        return $this->belongsTo(mantenedor_bodega::class,'mantenedor_bodegas_id');
    }

    public function detalle_compra(){
        return $this->hasMany(compra_ingreso_detalle::class,'compras_ingresos_id');
    }

    public function estado(){
        return $this->belongsTo(compras_estado::class,'estado_id');
    }

    public function detalle_mac(){
        return $this->hasMany(compra_detalle_mac::class,'compra_id');
    }
    public function archivos(){
        return $this->hasMany(compra_archivo::class, 'compra_id');
    }
}
