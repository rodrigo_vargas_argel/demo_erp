<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class compras_estado extends Model
{
    protected $table = 'compras_estado';
    protected $fillable=['nombre'];

    public function compras(){
        return $this->hasMany(compra_ingreso::class,'estado_id');
    }
}
