<?php
namespace App\Helpers;

use Box\Spout\Common\Type;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Common\Entity\Style\Border;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

use App\Models\Contacto_Factibilidad;

class FactibilidadExcel
{
    public static function Generar($des,$has){

        $desde = $des.' 00:00:00';
        $hasta = $has.' 23:59:59';

        $factibilidades = Contacto_Factibilidad::with(['tipo_cliente','torre','estado','comentario_factibilidad'])
        ->whereBetween('created_at', [$desde, $hasta])
        ->orderBy('id','desc')
        ->get();

        // dd($factibilidades);


        $fileName = "factibilidades_".$des."_".$has.".xlsx";
        $filePath = public_path()."/excels/factibilidades/".$fileName;

		$defaultStyle = (new StyleBuilder())
						    ->setFontSize(10)
						    ->build();

		$writer = WriterEntityFactory::createXLSXWriter();
		$writer->setDefaultRowStyle($defaultStyle)
                        ->openToFile($filePath);

        // $sheet = $writer->addNewSheetAndMakeItCurrent();
        $sheet = $writer->getCurrentSheet();
        $sheet->setName('Factibilidades');

        // $cells = [
        //     WriterEntityFactory::createCell(''),
        //     WriterEntityFactory::createCell(''),
        //     WriterEntityFactory::createCell(''),
        // ];
        $filavacia = WriterEntityFactory::createRow([]);
        $writer->addRow($filavacia);
        $writer->addRow($filavacia);

        $border = (new BorderBuilder())
                    ->setBorderTop(Color::rgb(64, 64, 64), Border::WIDTH_THIN, Border::STYLE_SOLID)
                    ->setBorderBottom(Color::rgb(64, 64, 64), Border::WIDTH_THIN, Border::STYLE_SOLID)
                    ->setBorderLeft(Color::rgb(64, 64, 64), Border::WIDTH_THIN, Border::STYLE_SOLID)
                    ->setBorderRight(Color::rgb(64, 64, 64), Border::WIDTH_THIN, Border::STYLE_SOLID)
                    ->build();

        $style = (new StyleBuilder())
                    ->setBorder($border)
                    ->setCellAlignment(CellAlignment::CENTER)
                    ->build();
        $negrita = (new StyleBuilder())
                    ->setFontBold()
                    ->setFontSize(10)
                    ->setBackgroundColor(Color::rgb(0, 137, 123))
                    ->setFontColor(Color::WHITE)
                    ->setBorder($border)
                    ->setCellAlignment(CellAlignment::CENTER)
                    ->build();

        $titulos = [
            WriterEntityFactory::createCell(''),
            WriterEntityFactory::createCell('N° Factibilidad',$negrita),
            WriterEntityFactory::createCell('Fecha Solicitud',$negrita),
            WriterEntityFactory::createCell('Fecha Cotización',$negrita),
            WriterEntityFactory::createCell('Nombre',$negrita),
            WriterEntityFactory::createCell('Rut',$negrita),
            WriterEntityFactory::createCell('Email',$negrita),
            WriterEntityFactory::createCell('Teléfono',$negrita),
            WriterEntityFactory::createCell('Pre Venta',$negrita),
            WriterEntityFactory::createCell('Ubicación',$negrita),
            WriterEntityFactory::createCell('Latitud',$negrita),
            WriterEntityFactory::createCell('Longitud',$negrita),
            WriterEntityFactory::createCell('Estaciones de prueba',$negrita),
            WriterEntityFactory::createCell('Distancia',$negrita),
            WriterEntityFactory::createCell('Tipo Cliente',$negrita),
            WriterEntityFactory::createCell('Torre',$negrita),
            WriterEntityFactory::createCell('Estado 1',$negrita),
            WriterEntityFactory::createCell('Fecha de Respuesta',$negrita),

            WriterEntityFactory::createCell('Comentario',$negrita),
            WriterEntityFactory::createCell('Contrato',$negrita),
            WriterEntityFactory::createCell('N° Servicio',$negrita),
            WriterEntityFactory::createCell('Plan Inicial',$negrita),
            WriterEntityFactory::createCell('Habilitación',$negrita),
            WriterEntityFactory::createCell('N° Documentos',$negrita),
            WriterEntityFactory::createCell('Estado 2',$negrita),
            WriterEntityFactory::createCell('Fecha Instalación',$negrita),
            WriterEntityFactory::createCell('Fecha Activación',$negrita),

        ];

        $singleRow = WriterEntityFactory::createRow($titulos);
        $writer->addRow($singleRow);

        $verde = (new StyleBuilder())
           ->setFontColor(Color::BLACK)
           ->setBorder($border)
           ->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::rgb(200,230,201))
           ->build();

        $plomo = (new StyleBuilder())
           ->setFontColor(Color::BLACK)
           ->setBorder($border)
           ->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::rgb(176,190,197))
           ->build();

        $azul = (new StyleBuilder())
           ->setFontColor(Color::BLACK)
           ->setBorder($border)
           ->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::rgb(144,202,249))
           ->build();

        $naranjo = (new StyleBuilder())
        ->setFontColor(Color::BLACK)
        ->setBorder($border)
        ->setCellAlignment(CellAlignment::CENTER)
        ->setBackgroundColor(Color::rgb(255, 204, 128))
        ->build();

        foreach ($factibilidades as $key => $value) {
            $estilo_celda = $style;
            if($value->estados_id==1){
                $estilo_celda = $azul;
            }
            if($value->estados_id==4){
                $estilo_celda = $verde;
            }
            if($value->estados_id==2 || $value->estados_id==6 || $value->estados_id==7){
                $estilo_celda = $plomo;
            }
            if($value->estados_id==3 || $value->estados_id==5){
                $estilo_celda = $naranjo;
            }

            if($value->fecha_cotizacion!=null){
                $fecha_cotizacion=date_create($value->fecha_cotizacion);
                $fecha_cotizacion = $fecha_cotizacion->format('d/m/Y');
            }else{
                $fecha_cotizacion= '';
            }

            if($value->fecha_respuesta!=null){
                $fecha_respuesta=date_create($value->fecha_respuesta);
                $fecha_respuesta = $fecha_respuesta->format('d/m/Y');
            }else{
                $fecha_respuesta= '';
            }

            if($value->fecha_instalacion!=null){
                $fecha_instalacion=date_create($value->fecha_instalacion);
                $fecha_instalacion = $fecha_instalacion->format('d/m/Y');
            }else{
                $fecha_instalacion= '';
            }

            if($value->fecha_activacion!=null){
                $fecha_activacion=date_create($value->fecha_activacion);
                $fecha_activacion = $fecha_activacion->format('d/m/Y');
            }else{
                $fecha_activacion= '';
            }
            // $fecha_cotizacion = ($value->fecha_cotizacion!=null) ? $value->fecha_cotizacion->format('d/m/Y') : '';
            $cells = [
                WriterEntityFactory::createCell(''),
                WriterEntityFactory::createCell($value->id,$estilo_celda),
                WriterEntityFactory::createCell($value->created_at->format('d/m/Y H:i'),$estilo_celda),
                WriterEntityFactory::createCell($fecha_cotizacion,$estilo_celda),
                WriterEntityFactory::createCell($value->nombre,$estilo_celda),
                WriterEntityFactory::createCell($value->rut."-".$value->dv,$estilo_celda),
                WriterEntityFactory::createCell($value->email,$estilo_celda),
                WriterEntityFactory::createCell($value->telefono,$estilo_celda),
                WriterEntityFactory::createCell(@$value->preventa_factibilidad->nombre,$estilo_celda),
                WriterEntityFactory::createCell($value->ubicacion,$estilo_celda),
                WriterEntityFactory::createCell($value->latitud,$estilo_celda),
                
                WriterEntityFactory::createCell($value->longitud,$estilo_celda),
                WriterEntityFactory::createCell($value->estaciones_de_prueba,$estilo_celda),
                WriterEntityFactory::createCell($value->distancia,$estilo_celda),
                WriterEntityFactory::createCell(@$value->tipo_cliente->nombre,$estilo_celda),
                WriterEntityFactory::createCell(@$value->torre->nombre,$estilo_celda),
                WriterEntityFactory::createCell(@$value->estado->estado,$estilo_celda),
                WriterEntityFactory::createCell($fecha_respuesta,$estilo_celda),

                
                WriterEntityFactory::createCell(@$value->comentario_factibilidad->comentario,$estilo_celda),
                WriterEntityFactory::createCell(@$value->estado_contrato->estado,$estilo_celda),
                WriterEntityFactory::createCell($value->nro_servicio,$estilo_celda),

                WriterEntityFactory::createCell($value->plan_inicial,$estilo_celda),
                WriterEntityFactory::createCell($value->habilitacion,$estilo_celda),
                WriterEntityFactory::createCell($value->nro_documentos,$estilo_celda),
                WriterEntityFactory::createCell(@$value->estado_2->estado,$estilo_celda),

                WriterEntityFactory::createCell($fecha_instalacion,$estilo_celda),
                WriterEntityFactory::createCell($fecha_activacion,$estilo_celda)

            ];
            
            /** add a row at a time */
            

            $singleRow = WriterEntityFactory::createRow($cells);
            $writer->addRow($singleRow);
        }
        $writer->close();
        // dd($factibilidades);
        return url("/excels/factibilidades/".$fileName);
    }
}

?>