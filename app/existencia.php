<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class existencia extends Model
{
    protected $table = 'existencias';
    //public $timestamps = false;
    //protected $fillable=['nombre','personal_id'];
    use SoftDeletes;

    public function bodega(){
        return $this->belongsTo(mantenedor_bodega::class, 'bodega_id');
    }

    public function producto(){
        return $this->belongsTo(producto::class, 'producto_id');
    }

    public function movimiento(){
        return $this->belongsTo(existencia_movimiento::class, 'movimiento_id');
    }

    public function estadoo(){ //para q no se confunda con el campo estado de la tabla existencias
        return $this->belongsTo(existencias_estado::class, 'estado');
    }

    public function servicio(){
        return $this->belongsTo(servicio::class, 'servicio_id','Id');
    }

}
