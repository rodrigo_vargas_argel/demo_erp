<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_bodega extends Model
{
    protected $table = 'mantenedor_bodegas';
    public $timestamps = false;
    protected $fillable=['nombre','personal_id'];
    use SoftDeletes;


    // metodo usado para las relaciones
    public function responsable()    {

        return $this->belongsTo(User::class,'personal_id', 'id');
    }
    public function compras(){
        return $this->hasMany(compra_ingreso::class,'mantenedor_bodegas_id');
    }

    public function existencias(){
        return $this->hasMany(existencia::class,'bodega_id');
    }
     public function movimiento_origen(){
        return $this->hasMany(existencia_movimiento::class,'bodega_origen_id');
     }

    public function movimiento_destino(){
        return $this->hasMany(existencia_movimiento::class,'bodega_destino_id');
    }

    public function traslados_origen(){
        return $this->hasMany(existencias_traslados::class,'bodega_origen_id');
    }
    public function traslados_destino(){
        return $this->hasMany(existencias_traslados::class,'bodega_destino_id');
    }




}
