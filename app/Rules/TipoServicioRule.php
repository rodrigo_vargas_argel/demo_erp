<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TipoServicioRule implements Rule
{
    private $req;
    private $msge;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($req)
    {
        $this->req = $req;
        $this->msge = "";
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (($value == 1) || ($value == 2)) { // Arriendo de Equipos de Datos // Servicio de Internet 

            $requeridos = ['planes_id'=>'Plan','Contacto'=>'Contacto','Fono'=>'Fono','Longitud'=>'Longitud','Latitud'=>'Latitud','FechaComprometidaInstalacion'=>'Fecha Comprometida de Instalación','PosibleEstacion'=>'Posible Estación','UsuarioPppoeTeorico'=>'Usuario Pppoe Teórico','SenalTeorica'=>'Senal Teórica'];

            foreach ($requeridos as $key => $value) {
                if ($this->req[$key] == "") {
                    $this->msge = "Falta indicar el ".$value;
                    return false;
                }
            }
        } elseif ($value == 3) { // Servicio de Puertos Públicos
            //la info lo llenan los técnicos
            return true;
        } elseif ($value == 4) { // Servicio de IP Pública
            //la info lo llenan los técnicos
            return true;
        } elseif ($value == 5) { // Servicio de Mantención de Red
            if ($this->req['descripcion'] == "") {
                $this->msge = "Falta indicar descripcion de la mantención";
                return false;
            }
        } elseif ($value == 6) { // Arriendo de equipos de Telefonía IP
            if ($this->req['linea'] == "") {
                $this->msge = "Falta indicar linea telefónica";
                return false;
            }
        } elseif ($value == 7) { // Otros Servicios
            if ($this->req['NombreServicioExtra'] == "") {
                $this->msge = "Falta Nombre del Servicio";
                return false;
            }
        } else {
            $this->msge = "Tipo de Servicio no existe";
            return false;
        }
        if (($this->req['BooleanCostoInstalacion'] == "1") && (($this->req['CostoInstalacion'] <= 0 ) || ($this->req['CostoInstalacion'] == ""))) {
            $this->msge = "Si hay Facturación, costo de instalación debe ser mayor que cero ";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msge;
    }
}
