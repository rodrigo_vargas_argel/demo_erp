<?php

namespace App\Http\Controllers;

use App\factura_detalle;
use App\Models\Servicio;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UpdateMasivaController extends Controller
{

   public function update_detalles(){
       $fecha1='2020-04-25';
       $fecha2='2020-04-26 00:00:01';

       // UF 28-04 2020
              $uf=28694;

       $detalles=factura_detalle::query()->whereDate('create_at','=',$fecha1)->orderBy('Id', 'desc')->get();
       //dd($detalle[0]->Id);
       foreach ($detalles as $detalle){
           $ValorServicio=Servicio::query()->where('Id','=',$detalle->IdServicio)->first();
           if ($ValorServicio)
           {
            $ValorPesos=$ValorServicio->Valor*$uf;
            $iva=$ValorPesos*0.19;
            $Total=$ValorPesos+$iva;
           $Total=round( $Total, 0, PHP_ROUND_HALF_UP);


               factura_detalle::where('Id', '=', $detalle->Id)
                   ->update(['Valor' => $ValorPesos, 'Total'=>$Total]);


           }

               //dd('id: '.$detalle->Id.' Valor:'. $ValorServicio->Valor . 'valor pesos: '.$ValorPesos. 'valor en doc: '.$sql->Valor . 'total: '.$Total);
            //else
            //    dd('no se pudo');
       }

       dd('Listo!');

   }

}
