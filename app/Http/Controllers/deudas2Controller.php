<?php

namespace App\Http\Controllers;

use App\deudas2;
use App\factura;
use App\Models\Cliente;
use Illuminate\Http\Request;

class deudas2Controller extends Controller
{
    public function show(){
        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        //$deudas=deudas2::query()->where('Deuda','>',1)->get();
        $morosidades=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('FechaVencimiento','<=',now())->orderBy('Id','Asc')->get();
        //dd($morosidades);
        return view('facturacion.deudas',['facturas'=>$morosidades,'clientes'=>$clientes]);
    }
}
