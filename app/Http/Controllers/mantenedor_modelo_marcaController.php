<?php

namespace App\Http\Controllers;

use App\producto;
use Illuminate\Http\Request;
use App\mantenedor_marca;
use App\mantenedor_modelo_marca;
use PHPUnit\Exception;

class mantenedor_modelo_marcaController extends Controller
{
    public function show()
    {
        $consulta=mantenedor_modelo_marca::all();
        // dd($consulta->proveedor->nombre);
        //  dd($consulta);
        return view('mantenedores.modelos.modelos', ['consulta'=>$consulta]);
    }

    public function nuevo(){
        $marcas=mantenedor_marca::all();
        $modelos=mantenedor_modelo_marca::all();
        return view('mantenedores.modelos.modelo_nuevo',['marcas'=>$marcas,'modelos'=>$modelos]);
    }

    public function guardar(Request $request){
        $request->validate([
            'marca'=>'required',
            'modelo'=>'required'
        ]);

        $sql=new mantenedor_modelo_marca;

        $sql->mantenedor_marca_id=$request->marca;
        $sql->nombre=$request->modelo;
        $sql->descripcion=$request->descripcion;

        $sql->save();

        return redirect()->route('mantenedores/modelos');

    }

    public function editar($id_modelo){
        $consulta=mantenedor_modelo_marca::query()->where('id','=',$id_modelo)->get();
//        dd($consulta);
        return view('mantenedores.modelos.modelo_editar', ['consulta'=>$consulta]);
    }

    public function modificar(Request $request){
        $id_modelo=$request->id;
        $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required'

        ]);

        $sql=mantenedor_modelo_marca::find($id_modelo);
        $sql->nombre = $request->nombre;
        $sql->descripcion = $request->descripcion;
        $sql->save();

        return redirect()->route('mantenedores/modelos');
    }

    public function eliminar($id_modelo){
//        dd($id_modelo);
        $sql=mantenedor_modelo_marca::find($id_modelo);
        $sql->delete();
        return redirect()->route('mantenedores/modelos');
    }
}
