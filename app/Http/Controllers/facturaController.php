<?php

namespace App\Http\Controllers;

use App\anulaciones;
use App\devoluciones;
use App\factura;
use App\factura_detalle;
use App\factura_pago;
use App\Mail\envia_nc;
use App\Mail\TestAmazonSes;
use App\mantenedor_tipo_factura;
use App\mantenedor_tipo_pago;
use App\Models\Cliente;
use App\Models\Contacto;
use App\Models\Servicio;
use App\morosidades;
use App\nota_de_venta;
use App\personaempresa;
use App\variables_globales;
use Carbon\Carbon;
use DateTimeZone;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Helpers\uf;
use Illuminate\Support\Facades\Auth;

use App\Mail\DemoEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use stdClass;
use Illuminate\Support\Collection as RV_coleccion;



class facturaController extends Controller
{

    public function show(){
        set_time_limit(0);
       // $date = Carbon::now();
       // $endDate = $date->subdays(15); //resto 61 dias a la fecha actual
        //$sesenta_dias_antes=$endDate->toDateString();
        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        $tipo_pago= mantenedor_tipo_pago::query()->get();
       //25
       $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->orderBy('Id','Desc')->take(25)->get();
       //todas
        //$facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('FechaFacturacion','>=','2020-01-01')->orderBy('Id','Desc')->get();

        //dd($facturas[0]->pagos[0]->nombre_pago);
        return view('facturacion.facturas',['facturas'=>$facturas,'clientes'=>$clientes, 'tipo_pago'=>$tipo_pago]);

    }
    public function lista_filtro(Request $request){

        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('Rut','=',$request->cliente)->orderBy('Id','Desc')->get();
        $tipo_pago= mantenedor_tipo_pago::query()->get();
        $cliente_old=$request->cliente;
        if (($request->num_doc != null) && ($request->num_doc != "")){
            $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('NumeroDocumento','=',$request->num_doc)->orderBy('Id','Desc')->get();
            $cliente_old=null;
        }

            // dd($facturas);
        return view('facturacion.facturas',['facturas'=>$facturas,'clientes'=>$clientes, 'tipo_pago'=>$tipo_pago,'cliente_old'=>$cliente_old]);

    }
    public function lista_filtro2($rut_cliente){

        if (($rut_cliente =="") || ($rut_cliente=="0"))
            $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->orderBy('Id','Desc')->take(25)->get();
        else
            $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('Rut','=',$rut_cliente)->orderBy('Id','Desc')->get();


        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        $tipo_pago= mantenedor_tipo_pago::query()->get();
        $cliente_old=$rut_cliente;
        // dd($facturas);
        return view('facturacion.facturas',['facturas'=>$facturas,'clientes'=>$clientes, 'tipo_pago'=>$tipo_pago,'cliente_old'=>$cliente_old]);

    }
    public function masiva_index(){

        $UfClass = new Uf();
        $FechaUF = date('Y/m/d');
        //sumo 1 mes
        //$FechaUF = date("Y/m/01",strtotime($FechaUF."+ 1 month"));
       // $FechaUfApi = $run->fechaApiSbif($FechaUF);
        $FechaFacturacion = explode('/', $FechaUF);
        $FechaFacturacion = $FechaFacturacion[0].'/'.$FechaFacturacion[1].'/dias/'.$FechaFacturacion[2];
       // $UF = $UfClass->getValue($FechaFacturacion);
       $UF = $UfClass->getValue(date('Y/m/d'));
        $tipo_facturacion=mantenedor_tipo_factura::query()->orderBy('codigo')->get();
        $consulta = "No hay registros";
        $clientes=Cliente::all();
        return view('facturacion.masiva',['mantenedor_tipo_factura'=>$tipo_facturacion,'consulta'=>$consulta,'uf'=>$UF,'fecha_uf'=>$FechaUF, 'clientes'=>$clientes]);
    }

    public function masiva_get(Request $request){
        $tipo_factura=$request->tipo_factura;
        $UfClass = new Uf();
        $FechaUF = date('Y/m/d');
        //sumo 1 mes
       // $FechaUF = date("Y/m/01",strtotime($FechaUF."+ 1 month"));
        // $FechaUfApi = $run->fechaApiSbif($FechaUF);
        $FechaFacturacion = explode('/', $FechaUF);
        $FechaFacturacion = $FechaFacturacion[0].'/'.$FechaFacturacion[1].'/dias/'.$FechaFacturacion[2];
       // $UF = $UfClass->getValue($FechaFacturacion);
       $UF = $UfClass->getValue(date('Y/m/d'));

        $tipo_facturacion=mantenedor_tipo_factura::query()->orderBy('codigo')->get();

        if ($tipo_factura== "todos"){
            $clientes=Cliente::query()->with('ultima_factura')->with(['servicios' => function ($q) use ($tipo_factura) {
                $q->with('estado_servicios')->with('tipo_facturas');
                $q->where(function ($query) {
                        $query->where('EstatusServicio', 1)
                            ->orWhere('EstatusServicio', 3)
                            ->orWhere('EstatusServicio', 5);
                    });



            }])->get();

        }else{
            $clientes=Cliente::query()->with('ultima_factura')->with(['servicios' => function ($q) use ($tipo_factura) {
                $q->with('estado_servicios')->with('tipo_facturas');
                $q->where('TipoFactura',$tipo_factura)
                    ->where(function ($query) {
                        $query->where('EstatusServicio', 1)
                            ->orWhere('EstatusServicio', 3)
                            ->orWhere('EstatusServicio', 5);
                    });



            }])->get();

        }


        $tipo_factura_old=$request->tipo_factura;
       //$servicios=$clientes;
        $consulta = $clientes;
        return view('facturacion.masiva',['mantenedor_tipo_factura'=>$tipo_facturacion,'consulta'=>$consulta,'uf'=>$UF,'fecha_uf'=>$FechaUF,'clientes'=>$clientes, 'tipo_factura_old'=>$tipo_factura_old]);
    }


    public function facturar_masiva(Request $request){
        $modo=2;
        $rv_estado=0;
        Carbon::setLocale('es');
        $date = Carbon::now();
        $mes_actual= $date->format('F'); // July
        $mes_anterior= $date->subMonth()->format('F'); // June
        // dd($request->factura['rut']);
        // $servicio=Servicio::query()->with('clientes')->where('id','=',$servicio)->first();
        //dd($servicio);
        $Cliente=$request->factura;
        //dd($Cliente);
        $getRegCity=Cliente::query()->with('regiones')->with('ciudades')->where('rut','=',$request->factura['rut'])->first();

        $Tipo=$Cliente['tipo_cliente'];
        $ufi = new Uf;
        $UF = $ufi->getValue();
        $Detalles=$request->factura['servicios'];
        //dd($Detalles);
        $FacturaBsale = $this->sendFacturaBsaleFromMasiva($Cliente,$Detalles,$UF,$Tipo,$modo,$getRegCity);
        $FacturaBsale['urlPdf'] .= '&123456';
        $urlPdf = $FacturaBsale['urlPdf'];
        $UrlPdf = $FacturaBsale['urlPdf'];
        $DocumentoId = $FacturaBsale['id'];
        $informedSii = $FacturaBsale['informedSii'];
        $responseMsgSii = $FacturaBsale['responseMsgSii'];
        $NumeroDocumento = $FacturaBsale['number'];
        // $expirationDate = date('Y-m-d', $FacturaBsale['expirationDate']);
        $expirationDate = gmdate("Y-m-d", $FacturaBsale['expirationDate']);

        if ($modo==1){
            //  Insertar Factura
            $sql=new factura();
            $sql->Rut=$Cliente['rut'];;
            $sql->Grupo=1;
            $sql->TipoFactura=$Cliente['tipo_cliente']; //en el ERP antiguo este llega como parametro, no se en que se diferencia con  TipoDocumento
            $sql->EstatusFacturacion=1;
            $sql->DocumentoIdBsale=$DocumentoId;
            $sql->NumeroDocumento=$NumeroDocumento;
            $sql->UrlPdfBsale=$UrlPdf;
            $sql->informedSiiBsale=$informedSii;
            $sql->responseMsgSiiBsale=$responseMsgSii;
            $sql->FechaFacturacion=NOW();
            $sql->HoraFacturacion=NOW();
            $sql->TipoDocumento=$Tipo;//en el ERP sntiguo este llega del array cliente[tipo_cliente]
            $sql->FechaVencimiento=$expirationDate;
            $sql->IVA=0.19;
            if(isset($Cliente['oc'])){
                $sql->NumeroOC=$Cliente['oc'][0]['num_oc'];
                $sql->FechaOC=$Cliente['oc'][0]['fecha_oc'];
            }else{
                $sql->NumeroOC=null;
                $sql->FechaOC=null;
            }

            $sql->Referencia=null;
            $sql->deleted_at=null;
            $sql->IdUsuarioSession= Auth::user()->id;
            $sql->CountDTE=0;
            $sql->create_at=NOW();
            $sql->update_at=NOW();
            $sql->NumeroHES=null;
            $sql->FechaHES=null;

            if ($sql->save()){

                foreach($Detalles as $Detalle) {

                    $Detalle['tipo_pago_bsale']=15; //20 dias
                    $Concepto = $Detalle["Codigo"].' '.$Detalle["Conexion"].' '.$Detalle["Descripcion"]. " ".$mes_anterior;
                    $Cantidad = 1;
                    $Descuento = $Detalle['Descuento'];
                    $Valor = $Descuento*$UF;
                    $Descuento_real=$Valor*$Descuento;
                    $sql_detalle = new factura_detalle();
                    //$iva=($Detalle['total']*0.19);
                    $total_con_iva=($Detalle['Valor']*0.19)-$Descuento;
                    $sql_detalle->FacturaId=$sql->id;
                    $sql_detalle->Concepto=$Concepto;

                    $sql_detalle->Valor=$Valor;
                    $sql_detalle->Descuento=$Descuento;
                    $sql_detalle->Cantidad=$Cantidad;
                    $sql_detalle->Total=$total_con_iva;
                    $sql_detalle->IdServicio=$Detalle["Id"];
                    $sql_detalle->Codigo=$Detalle["Codigo"];
                    $sql_detalle->documentDetailIdBsale=null;
                    $sql_detalle->create_at=NOW();
                    $sql_detalle->update_at=NOW();
                    $sql_detalle->delete_at=null;
                    $sql_detalle->FacturaIdTmp=null;

                    $sql_detalle->save();




                }


                $rv_estado=1; //se emitio documento real




            }
        }
        // dd($urlPdf);

        if ($urlPdf != ""){
            $mensaje="Factura Emitida";
            return response()
                ->json(["success"=>true,'msg'=>$mensaje,'url'=>$urlPdf]);
        }
        else{
            $mensaje="Factura Fallida";
            return response()
                ->json(["success"=>false,'msg'=>$mensaje ]);
        }
        //dd($FacturaBsale);

    }


    public function sendFacturaBsaleFromMasiva($Cliente,$Detalles,$UF,$Tipo,$TipoToken,$getRegCity){
        // si se quiere enviar datos de prueba a la api usar el token 2 para pruebas
        Carbon::setLocale('es');
        $date = Carbon::now();
        $mes_actual= $date->format('F'); // July
        $mes_anterior= $date->subMonth()->format('F'); // June

        $TipoToken = $TipoToken;
        // $run = new Method;
        //dd($Detalles);
        $token=variables_globales::query()->first();
        if($TipoToken == 1){

            $access_token =$token->token_produccion ;
        }else{
            $access_token = $token->token_prueba;
        }

        $clientId = null;

        if($getRegCity['regiones']){
            $Provincia = $getRegCity['regiones']['nombre'];
        }else{
            $Provincia = 'Llanquihue';
        }

        if($getRegCity['ciudades']){
            $Ciudad = $getRegCity['ciudades']['nombre'];
        }else{
            $Ciudad = 'Puerto Varas';
        }

        $clientId = null;
        if($Cliente['tipo_cliente'] == "1"){
            $Cliente['contacto'] = $Cliente['nombre'];
        }
        $client = array(
            "code"          => $Cliente['rut'].'-'.$Cliente['dv'],
            "firstName"     => $Cliente['contacto'],
            "lastName"      => "",
            "email"         => $Cliente['correo'],
            "phone"         => $Cliente['telefono'],
            "address"       => $Cliente['direccion'],
            "company"       => $Cliente['nombre'],
            "city"          => $Provincia,
            "municipality"  => $Ciudad,
            "activity"      => $Cliente['giro']
        );
        //     }
        // }

        //CREACION DE LA FACTURA

        $url='https://api.bsale.cl/v1/documents.json';

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // Activa SSL
        // curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // Configura cabeceras
        $headers = array(
            'access_token: ' . $access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);

        //CONSTRUCCION DEL ARRAY DE DETALLE

        $details = array();
        $Total = 0;
        $Detalle="";
        foreach ($Detalles as $Detalle){
            //  $Detalle = $Detalles;
            // dd($Detalles);
            $Detalle['tipo_pago_bsale']=15; //20 dias
            $Concepto = $Detalle["Codigo"].' '.$Detalle["Conexion"].' '.$Detalle["Descripcion"]. "Mes ".$mes_anterior;
            $Cantidad = 1;
            $Descuento = $Detalle['Descuento'];
            $Valor = $Detalle["Valor"]*$UF;

            $detail = array("netUnitValue" => $Valor, "quantity" => $Cantidad, "taxId" => "[1]", "comment" => $Concepto, "discount" => $Descuento);

            array_push($details,$detail);
            $Total += $Valor;
        }
        $Detalle = $Detalles;
        if(isset($Detalle['Referencia'])){
            $Referencia = $Detalle['Referencia'];
            if($Referencia){
                $last_index = count($details) - 1;
                $details[$last_index]['comment'] .= PHP_EOL . ' ' . $Referencia;
            }
        }

        $payments = array();
        $payment = array("paymentTypeId" => 15, "amount" => $Total, "recordDate" => time());
        array_push($payments,$payment);

        $references = array();



        if(isset($Cliente['oc'])){
            $OC = $Cliente['oc'];
            foreach ($Cliente['oc'] as $oc) {
                $idOC = $oc['id'];
                if ($idOC==$Cliente["id"]) {
                    $NumeroOC = $oc['num_oc'];
                    $FechaOC = $oc['fecha_oc'];
                    $fecha1 = Carbon::parse($FechaOC);
                    $FechaOC = $fecha1->format('U');

                    $reference = array("number" => $NumeroOC, "referenceDate" => $FechaOC, "reason" => "Orden de Compra " . $NumeroOC, "codeSii" => 801);
                    array_push($references, $reference);
                }
            }
        }
        if(isset($Cliente['hes'])){

            foreach ($Cliente['hes'] as $hes)
            {
                $idges=$hes["id"];

                if($idges==$Cliente['id']){
                    $NumeroHES = $hes['num_hes'];
                    $FechaHES = $hes['fecha_hes'];
                    $fecha1=Carbon::parse($FechaHES);
                    $FechaHES=$fecha1->format('U');
                    //$FechaHES = $FechaHES->format('U');
                    // $FechaHES = $dateTime->format('U');
                    $reference = array("number" => $NumeroHES, "referenceDate" => $FechaHES, "reason" => "Hoja Entrada de servicio " . $NumeroHES, "codeSii" => "HES");
                    array_push($references,$reference);
                }
            }
        }




        if($Cliente['tipo_cliente'] == "2"){
            $documentTypeId = 5;
        }else{
            $documentTypeId = 22;
        }

        $expirationDate = time() + (20 * 24 * 60 * 60);


        $array = array(
            "documentTypeId"    => $documentTypeId,
            // "priceListId"       => 18,
            "emissionDate"      => time(),
            "expirationDate"    => $expirationDate,
            "declareSii"        => 1,
            "details"           => $details,
            "payments"          => $payments
        );

        if(isset($references)){
            $array['references'] = $references;
        }

        if($clientId){
            $array['clientId'] = $clientId;
        }else{
            $array['client'] = $client;
        }
        // Parsea a JSON
        $data = json_encode($array);

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);

        // // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando
        $FacturaBsale = json_decode($response, true);

        $UrlPdf = isset($FacturaBsale['urlPublicViewOriginal']) ? trim($FacturaBsale['urlPublicViewOriginal']) : "";
        if($UrlPdf){
            $FacturaBsale['status'] = 1;
        }else{
            $Message = $FacturaBsale['error'];
            $FacturaBsale = array();
            $FacturaBsale['Message'] = $Message;
            $FacturaBsale['status'] = 0;
        }
        // print_r($FacturaBsale); exit;
        return $FacturaBsale;
    }

    public function facturar_nota_venta($id,$modo){
        $hoy = date('Y-m-d H:i:s');
        $rv_estado=0;
        $nota_venta=nota_de_venta::query()->with('cliente')->with('detalle')->with('hes')->where('id','=',$id)->first();
        $Cliente=$nota_venta->cliente;
        $getRegCity=Cliente::query()->with('regiones')->with('ciudades')->where('id','=',$Cliente->id)->first();

        $Tipo=$Cliente['tipo_cliente'];
        $ufi = new Uf;
        $UF = $ufi->getValue();
        $Detalles=$nota_venta;
        $FacturaBsale = $this->sendFacturaBsale($Cliente,$Detalles,$UF,$Tipo,$modo,$getRegCity);
        //dd($FacturaBsale);
        if($FacturaBsale['status'] == 1){

            //1=boleta 2=factura, les voy a setear un grupo tal como lo hace el erp antiguo en la tabla facturas
            if ($Tipo==1)
                $Grupo=1000;
            else
                $Grupo=1;
            $FacturaBsale['urlPdf'] .= '&123456';
            $urlPdf = $FacturaBsale['urlPdf'];

            $UrlPdf = $FacturaBsale['urlPdf'];
            $DocumentoId = $FacturaBsale['id'];
            $informedSii = $FacturaBsale['informedSii'];
            $responseMsgSii = $FacturaBsale['responseMsgSii'];
            $NumeroDocumento = $FacturaBsale['number'];
            // $expirationDate = date('Y-m-d', $FacturaBsale['expirationDate']);
            $expirationDate = gmdate("Y-m-d", $FacturaBsale['expirationDate']);
            if ($modo==1){
                //  Insertar Factura
                $sql=new factura();
                $sql->Rut=$Cliente['rut'];;
                $sql->Grupo=$Grupo;
                $sql->TipoFactura=$Tipo; //en el ERP antiguo este llega como parametro, no se en que se diferencia con  TipoDocumento
                $sql->EstatusFacturacion=1;
                $sql->DocumentoIdBsale=$DocumentoId;
                $sql->NumeroDocumento=$NumeroDocumento;
                $sql->UrlPdfBsale=$UrlPdf;
                $sql->informedSiiBsale=$informedSii;
                $sql->responseMsgSiiBsale=$responseMsgSii;
                $sql->FechaFacturacion=NOW();
                $sql->HoraFacturacion=NOW();
                $sql->TipoDocumento=$Tipo;//en el ERP sntiguo este llega del array cliente[tipo_cliente]
                $sql->FechaVencimiento=$expirationDate;
                $sql->IVA=0.19;
                $sql->NumeroOC=$Detalles['numero_oc'];
                $sql->FechaOC=$Detalles['fecha_oc'];
                $sql->Referencia=$Detalles['referencia'];
                $sql->deleted_at=null;
                $sql->IdUsuarioSession= Auth::user()->id;
                $sql->CountDTE=0;
                $sql->create_at=NOW();
                $sql->update_at=NOW();
                $sql->NumeroHES=$Detalles['numero_hes'];
                $sql->FechaHES=$Detalles['fecha_hes'];

                if ($sql->save()){

                    foreach($Detalles->detalle as $Detalle) {

                        $sql_detalle = new factura_detalle();
                        //$iva=($Detalle['total']*0.19);
                        $total_con_iva=$Detalle['total'];
                        $sql_detalle->FacturaId=$sql->id;
                        $sql_detalle->Concepto=$Detalle["concepto"];
                        if(($Detalle["valor"]==null) || ($Detalle["valor"]=="")) // este if es necesario xq aun emiten notas desde el erp antiguo, y desde ahi valor es mulo y poner el valor en el precio (11/4/2020)
                            $sql_detalle->Valor=$Detalle["precio"];
                        else
                            $sql_detalle->Valor=$Detalle["valor"];
                        $sql_detalle->Descuento=$Detalle["fdescuento"];
                        $sql_detalle->Cantidad=$Detalle['cantidad'];
                        $sql_detalle->Total=$total_con_iva;
                        $sql_detalle->IdServicio;
                        $sql_detalle->Codigo=null;
                        $sql_detalle->documentDetailIdBsale=null;
                        $sql_detalle->create_at=NOW();
                        $sql_detalle->update_at=NOW();
                        $sql_detalle->delete_at=null;
                        $sql_detalle->FacturaIdTmp=null;

                        $sql_detalle->save();

                    }

                    $nota_venta->estatus_facturacion=1;
                    $nota_venta->factura_id=$sql->id;
                    $nota_venta->save();
                    $rv_estado=1; //se emitio documento real

                }
            }
            else{
                $rv_estado=0; //se emite documento preview

            }

            $response_array['NombrePdf'] = $urlPdf;
            $response_array['status'] = 1;
            $success=true;


        }else{
            $response_array['Message'] = $FacturaBsale['Message'];
            // $response_array['error']=$FacturaBsale['error'];
            $response_array['status'] = 0;
            $success=false;
            $urlPdf="";
        }

        return response()
            ->json(["success"=>$success,'arr'=>$response_array ,'url'=>$urlPdf, 'rv_estado'=>$rv_estado, 'numdoc'=>$NumeroDocumento]);

    }

    public function enviar_documento($id_factura){
        $factura=factura::query()->with('detalle')->with('cliente')->with('cliente_contacto')->where('Id',$id_factura)->first();


        if($factura->TipoDocumento == 1){
            $TipoDocumento = 'Boleta';
        }else{
            $TipoDocumento = 'Factura';
        }
        $factura["TipoDocumento"]=$TipoDocumento;


        $Asunto = $TipoDocumento . ' #' . $factura->NumeroDocumento . ' Teledata';
        $espacios2 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $espacios = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $factura["espacios"]=$espacios;
        $factura["espacios2"]=$espacios2;
        $usermail=Auth::user()->email;
        $cc="teledatadte@teledata.cl";

        if (($usermail=="")||($usermail==null))
            $correo_destino2='teledatadte@teledata.cl';
        else
            $correo_destino2=$usermail;

        if (($factura->cliente->correo=="")||($factura->cliente->correo==null))
            $correo_destino='fpezzuto@teledata.cl';
        else
            $correo_destino=$factura->cliente->correo; //. ','.$factura->cliente->correo;

        $contactos=Contacto::query()->where('rut', $factura->cliente->rut)->get();
        if ($contactos){
            $contactList = [];
            $i=0;

            // Fill the array element
            foreach($contactos as $contact){
                $contactList[$i] = $contact->correo;
                $i++;
            }

            array_push($contactList, "teledatadte@teledata.cl");

            $cc=$contactList;

        }

        // $correo_destino="varas.argel@gmail.com";

        Mail::to($correo_destino)->cc($cc)->bcc($correo_destino2)->send(new TestAmazonSes($factura));
        // Mail::to("rvargas@teledata.cl")->send(new DemoEmail($objDemo));
        // dd($factura->cliente_contacto);

    }


    public function sendFacturaBsale($Cliente,$Detalles,$UF,$Tipo,$TipoToken,$getRegCity){
        // si se quiere enviar datos de prueba a la api usar el token 2 para pruebas
        $TipoToken = $TipoToken;
        // $run = new Method;
        $token=variables_globales::query()->first();
        if($TipoToken == 1){

            $access_token =$token->token_produccion ;
        }else{
            $access_token = $token->token_prueba;
        }

        $clientId = null;

        if($getRegCity['regiones']){
            $Provincia = $getRegCity['regiones']['nombre'];
        }else{
            $Provincia = 'Llanquihue';
        }

        if($getRegCity['ciudades']){
            $Ciudad = $getRegCity['ciudades']['nombre'];
        }else{
            $Ciudad = 'Puerto Varas';
        }

        $clientId = null;
        if($Cliente['tipo_cliente'] == "1"){
            $Cliente['contacto'] = $Cliente['nombre'];
        }
        $client = array(
            "code"          => $Cliente['rut'].'-'.$Cliente['dv'],
            "firstName"     => $Cliente['contacto'],
            "lastName"      => "",
            "email"         => $Cliente['correo'],
            "phone"         => $Cliente['telefono'],
            "address"       => $Cliente['direccion'],
            "company"       => $Cliente['nombre'],
            "city"          => $Provincia,
            "municipality"  => $Ciudad,
            "activity"      => $Cliente['giro']
        );
        //     }
        // }

        //CREACION DE LA FACTURA

        $url='https://api.bsale.cl/v1/documents.json';

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // Activa SSL
        // curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // Configura cabeceras
        $headers = array(
            'access_token: ' . $access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);

        //CONSTRUCCION DEL ARRAY DE DETALLE

        $details = array();
        $Total = 0;

        foreach($Detalles->detalle as $Detalle){

            $Concepto = $Detalle["concepto"];
            $Cantidad = $Detalle['cantidad'];
            $Descuento = $Detalle['fdescuento'];
            $Valor = $Detalle["valor"];
            if (($Valor==null)||($Valor=="")) // en caso de que hayan creado nota venta desde el erp antiguo,yo ahora el en precio pongo valorUnit * Q
                $Valor=$Detalle["precio"];




            $detail = array("netUnitValue" => $Valor, "quantity" => $Cantidad, "taxId" => "[1]", "comment" => $Concepto, "discount" => $Descuento);

            array_push($details,$detail);
            $Total += $Valor;
        }
        $Detalle = $Detalles;
        if(isset($Detalle['Referencia'])){
            $Referencia = $Detalle['Referencia'];
            if($Referencia){
                $last_index = count($details) - 1;
                $details[$last_index]['comment'] .= PHP_EOL . ' ' . $Referencia;
            }
        }

        $payments = array();
        $payment = array("paymentTypeId" => $Detalle['tipo_pago_bsale'], "amount" => $Total, "recordDate" => time());
        array_push($payments,$payment);

        $references = array();

        if(isset($Detalle['numero_oc'])){
            $NumeroOC = $Detalle['numero_oc'];
            if($NumeroOC){
                $FechaOC = $Detalle['fecha_oc'];
                $fecha1=Carbon::parse($FechaOC);
                $FechaOC=$fecha1->format('U');


                $reference = array("number" => $NumeroOC, "referenceDate" => $FechaOC, "reason" => "Orden de Compra " . $NumeroOC, "codeSii" => 801);
                array_push($references,$reference);
            }
        }
        if($Detalle['numero_hes']>0){

            foreach ($Detalle->hes as $hes)
            {

                $NumeroHES = $hes->num_hes;
                if($NumeroHES){
                    $FechaHES = $hes->fecha_hes;
                    $fecha1=Carbon::parse($FechaHES);
                    $FechaHES=$fecha1->format('U');
                    //$FechaHES = $FechaHES->format('U');
                    // $FechaHES = $dateTime->format('U');
                    $reference = array("number" => $NumeroHES, "referenceDate" => $FechaHES, "reason" => "Hoja Entrada de servicio " . $NumeroHES, "codeSii" => "HES");
                    array_push($references,$reference);
                }
            }
        }
        // para generar la fecha de vencimiento
        // tipo_pago trae por ejemplo 20 dias
        // El explode agarra el 20
        // Y lo multiplica por todos esos valores para generar un timestamp
        // Ese timestamp se le suma al time actual
        // Y genera una fecha de 20 días después pero en formato timestamñ
        // osea que si se esta facturando hoy, y el tipo de pago es 20 días, la fecha de vencimiento va a ser el 5 de diciembre
        //Eso solo se trabaja si el primer explode es un numero, sino es un numero, la fecha de vencimiento es el día de hoy
        // Eso es para los tipo de pago efectivo, débito, etc
        $tipo_pago = $Detalle['tipo_pago_bsale'];
        $Explode = explode(' ',$tipo_pago);
        if ($tipo_pago==1)
            $expirationDate = time();
        if ($tipo_pago==11)
            $expirationDate = time() + (30 * 24 * 60 * 60);
        if ($tipo_pago==12)
            $expirationDate = time() + (30 * 24 * 60 * 60);
        if ($tipo_pago==15)
            $expirationDate = time() + (20 * 24 * 60 * 60);
        if ($tipo_pago==16)
            $expirationDate = time() + (14 * 24 * 60 * 60);
        if ($tipo_pago==17)
            $expirationDate = time() + (7 * 24 * 60 * 60);

        // ctype_digit — Chequear posibles caracteres numéricos
        // if(ctype_digit($Explode[0])){
        // $expirationDate = time() + (20 * 24 * 60 * 60);
        //  }else{
        //daria el dia actual
        // $expirationDate = time();
        //  }

        // $expirationDate = 1543186789; //2018/11/25


        //FACTURA

        //Demo
        // "documentTypeId"    => 5

        //Producción
        // "documentTypeId"    => 5

        //BOLETA

        //Demo
        // "documentTypeId"    => 22

        //Producción
        // "documentTypeId"    => 22

        if($Cliente['tipo_cliente'] == "2"){
            $documentTypeId = 5;
        }else{
            $documentTypeId = 22;
        }

        $array = array(
            "documentTypeId"    => $documentTypeId,
            // "priceListId"       => 18,
            "emissionDate"      => time(),
            "expirationDate"    => $expirationDate,
            "declareSii"        => 1,
            "details"           => $details,
            "payments"          => $payments
        );

        if(isset($references)){
            $array['references'] = $references;
        }

        if($clientId){
            $array['clientId'] = $clientId;
        }else{
            $array['client'] = $client;
        }
        // Parsea a JSON
        $data = json_encode($array);

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);

        // // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando
        $FacturaBsale = json_decode($response, true);

        $UrlPdf = isset($FacturaBsale['urlPublicViewOriginal']) ? trim($FacturaBsale['urlPublicViewOriginal']) : "";
        if($UrlPdf){
            $FacturaBsale['status'] = 1;
        }else{
            $Message = $FacturaBsale['error'];
            $FacturaBsale = array();
            $FacturaBsale['Message'] = $Message;
            $FacturaBsale['status'] = 0;
        }
        // print_r($FacturaBsale); exit;
        return $FacturaBsale;
    }

    public function rpt_pagos(){
        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
       // $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('facturas_pagos.Monto','!=',null)->orderBy('Id','Desc')->get();
        //dd($facturas);
        return view('facturacion.pagos',['clientes'=>$clientes,'facturas'=>'Seleccione Cliente en la lista de arriba a la derecha...' , 'pagos'=>'na']);
    }

    public function rpt_pagos_filtro(Request $request){

        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        if ($request->cliente == -1){
            //$facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('FechaFacturacion','>=','2020-09-01')->orderBy('Id','Desc')->get();
            $facturas="Seleccione Cliente en la lista de arriba a la derecha...";
            set_time_limit(0);
            $pagos=factura_pago::query()->with('factura')->where('FechaPago','>=','2020-01-01')->get();
        }
            else{
            $pagos="na";
                $facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('Rut','=',$request->cliente)->orderBy('Id','Desc')->get();
            }

        $cliente_old=$request->cliente;
        // dd($facturas);
        return view('facturacion.pagos',['facturas'=>$facturas,'clientes'=>$clientes, 'cliente_old'=>$cliente_old, 'pagos'=>$pagos]);
    }

    public function guardar_pago($id_factura, $monto, $fecha_pago, $tipo_pago){
        $hoy = date('Y-m-d H:i:s');

        $sql=new factura_pago;
        $sql->FacturaId=$id_factura;
        $sql->FechaPago=$fecha_pago;
        $sql->TipoPago=$tipo_pago;
        $sql->Monto=$monto;
        $sql->IdUsuarioSession=Auth::user()->id;

        if($sql->save()){
            $mensaje="Se ha Guardao el pago exitosamente.";
            $success=true;
        }
        else{

            $mensaje="Error al intentar guardar el pago!";
            $success=false;

        };

        return response()
            ->json(["success"=>$success,'msg'=>$mensaje ]);

    }

    public function eliminar_pago($id_pago){
        $hoy = date('Y-m-d H:i:s');

        if(factura_pago::where('Id',$id_pago)->delete()){
            $mensaje="Se ha eliminado el pago exitosamente.";
            $success=true;
        }
        else{

            $mensaje="Error al intentar eliminar el pago!";
            $success=false;

        };

        return response()
            ->json(["success"=>$success,'msg'=>$mensaje ]);

    }


    /// ********* NC devolucionesss **********************************
    ///  NOTA DE CREDITO
    public function guardar_nc(Request $request){

          // echo 'Dentro de sendDevolucionBsale DetallesSeleccionados es '.$DetallesSeleccionados; echo "\n";
          $DevolucionBsale="";
          $rut=$request->rut_cliente;
          $motive=$request->motive;
          $referenceDocumentId=$request->referenceDocumentId;
          $TipoToken=1;
          $tipoNotaCredito = $request->tipoNotaCredito;
          $DetallesSeleccionados=false;
          $Cliente=Cliente::query()->where('rut','=',$rut)->first();

                  $details = array();
                  $cont=0;
              if($tipoNotaCredito==2){


                  foreach($request->nuevo_valor as $nuevo_monto){

                          $detail = array(
                              "documentDetailId" => intval($request->documentDetailIdBsale[$cont]),
                              "unitValue"        => floatval($nuevo_monto),
                              "quantity"         => floatval($request->quantity[$cont]));
                          array_push($details,$detail);
                          $cont=$cont+1;
                      }
              }

        $response_array = array();

              $Factura = factura::query()->where('Id','=',$request->fid)->first();

              if($Factura)
              {

                  $Rut = $Factura['Rut'];
                  $DocumentoIdBsale = $Factura['DocumentoIdBsale'];


                  if($Cliente){
                      // echo 'Dentro de storeDevolucion DetallesSeleccionados es '.$DetallesSeleccionados; echo "\n";
                      $DevolucionBsale = $this->sendDevolucionBsale($Cliente,$DocumentoIdBsale,$request->motive,1, $tipoNotaCredito, $details);

                      if($DevolucionBsale['status'] == 1){
                          $DevolucionIdBsale = $DevolucionBsale['id'];
                          $priceAdjustment = $DevolucionBsale['priceAdjustment'];
                          $editTexts = $DevolucionBsale['editTexts'];
                          $DevolucionAmount = (double)$DevolucionBsale['amount'];
                          $NotaCredito = $DevolucionBsale['credit_note'];
                          $DocumentoIdBsale = $NotaCredito['id'];

                          $DocumentoBsale = $this->getDocumentoBsale($DocumentoIdBsale,1);
                          if($DocumentoBsale['status'] == 1){
                              $UrlPdf = $DocumentoBsale['urlPdf'];
                              $NumeroDocumento = $DocumentoBsale['number'];
                              $response_array['status'] = 1;
                              $response_array['Message'] ="Nota de Credito se Generó con Exito : ";

                              $up_factura = DB::table('facturas')
                                  ->where('Id', $request->fid)
                                  ->update(['EstatusFacturacion' => 2]);

                              if ($up_factura)
                                  $response_array['Message']=$response_array['Message']. "Cambió EstatusFacturacion =2 ";
                              else
                                  $response_array['Message']=$response_array['Message']. "No se Cambió EstatusFacturacion";
                          }else{
                              $response_array['Message'] = 'se hizo el proceso pero no tengo nada para mostrar '. $DocumentoBsale['Message'];
                              $response_array['status'] = 'Documento';
                              //dd(json_encode($response_array));
                              //return;
                          }


                          ////***********************+INSERT ***********************************

                          $devoluciones=new devoluciones;
                          //    ('".$Id."', '".."', '".."', '".."','".."', , ,'".."', '0', '".."', '".."', '".."' )";
                          $devoluciones->FacturaId=$request->fid;
                          $devoluciones->DevolucionIdBsale=$DevolucionIdBsale;
                          $devoluciones->DocumentoIdBsale=$DocumentoIdBsale;
                          $devoluciones->Motivo=$request->motive;
                          $devoluciones->FechaDevolucion=NOW();
                          $devoluciones->HoraDevolucion=NOW();
                          $devoluciones->UrlPdfBsale=$UrlPdf;
                          $devoluciones->NumeroDocumento=$NumeroDocumento;
                          $devoluciones->DevolucionAnulada=0;
                          $devoluciones->create_at=NOW();
                          $devoluciones->update_at=NOW();
                          $devoluciones->DevolucionAmount=$DevolucionAmount;
                          $devoluciones->priceAdjustment=$priceAdjustment;
                          $devoluciones->editTexts=$editTexts;
                          $devoluciones->save();

                          $LastInsertId = $devoluciones->Id;



                              $espacios2 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                              $espacios = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                              $Factura["espacios"]=$espacios;
                              $Factura["espacios2"]=$espacios2;
                              $usermail=Auth::user()->email;
                              $cc="varas.argel@gmail.com";
                              $Factura["nombreusuario"]=Auth::user()->nombre;
                              $Factura["NumeroDocumento"]=$NumeroDocumento;
                              $Factura["UrlPdfBsale"]=$UrlPdf;

                              Mail::to("rvargas@teledata.cl")->cc($cc)->bcc($usermail)->send(new envia_nc($Factura));



                      }else{
                          $response_array['Message'] = "Error de Bsale, ".$DevolucionBsale['Message'];
                          $response_array['tipo'] = 'Devolucion';
                          $response_array['status'] = 0;
                          //dd (json_encode($response_array));
                          return response()
                              ->json(["response_array"=>$response_array ]);
                      }


                  }else{
                      $response_array['status'] = 4;
                  }
              }else{
                  $response_array['status'] = 3;
              }




        return response()
            ->json(["response_array"=>$response_array, "nc_documento"=>$DevolucionBsale]);


    }

    public function sendDevolucionBsale($Cliente,$DocumentoIdBsale,$Motivo,$TipoToken, $tipoNotaCredito , $DetallesSeleccionados = false) {

       //dd($DetallesSeleccionados);
        $TipoToken=1;

        $variables_globales=variables_globales::query()->get();
        if($TipoToken == 1){

            $access_token = $variables_globales[0]['token_produccion'];
        }else{

            $access_token = $variables_globales[0]['token_prueba'];
        }


        if($tipoNotaCredito != 2){
            $details = $this->getDetallesDocumentoBsale($DocumentoIdBsale,$access_token, $tipoNotaCredito, $DetallesSeleccionados);
        }else{
            $details = $this->getDetallesDocumentoBsale($DocumentoIdBsale,$access_token, $tipoNotaCredito, $DetallesSeleccionados);
        }

        //dd($details);
        if($Cliente['provincia']){
            $Provincia = $Cliente['provincia'];
        }else{
            $Provincia = 'Llanquihue';
        }

        if($Cliente['ciudad']){
            $Ciudad = $Cliente['ciudad'];
        }else{
            $Ciudad = 'Puerto Varas';
        }

       // dd($details);
        $client = array(
            "code"          => $Cliente['rut'].'-'.$Cliente['dv'],
            "firstName"     => $Cliente['contacto'],
            "lastName"      => "",
            "email"         => $Cliente['correo'],
            "phone"         => $Cliente['telefono'],
            "address"       => $Cliente['direccion'],
            "company"       => $Cliente['nombre'],
            "city"          => $Provincia,
            "municipality"  => $Ciudad,
            "activity"      => $Cliente['giro']
        );

        //CREACION DE LA DEVOLUCION

        $url='https://api.bsale.cl/v1/returns.json';

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // Activa SSL
        // curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // $rvtoken=$access_token;
        $rvtoken="e0b1793372fd0a5477794e167dd72a1824a95d2c"; //para cosultar el detalle del documento el token siempre es produccion.
        // Configura cabeceras
        $headers = array(
            'access_token: ' . $rvtoken,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);
        $priceAdjustment = 0;
        $editTexts = 0;
        if($tipoNotaCredito == 2){
            $priceAdjustment = 1;
        }
        if($tipoNotaCredito == 3){
            $editTexts = 1;
        }
        //CONSTRUCCION DEL ARRAY DE DEVOLUCION

        $array = array(
            "documentTypeId"        => 2,
            "officeId"              => 1,
            "referenceDocumentId"   => $DocumentoIdBsale,
            "emissionDate"          => time(),
            "expirationDate"        => time(),
            "motive"                => $Motivo,
            "declareSii"            => 1,
            "priceAdjustment"       => $priceAdjustment,
            "editTexts"             => $editTexts,
            "type"                  => 0,
            "details"               => $details,
            "client"                => $client
        );
        // Parsea a JSON
        $data = json_encode($array);
        //dd($data);
        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);
        //dd($response);
        // // Cierra la sesión cURL
        curl_close($session);

          //Esto es sólo para poder visualizar lo que se está retornando
          $DevolucionBsale = json_decode($response, true);
          $DocumentoId = isset($DevolucionBsale['id']) ? trim($DevolucionBsale['id']) : "";
          if($DocumentoId){
              $DevolucionBsale['status'] = 1;
          }else{
              $Message = $DevolucionBsale['error'];
              $DevolucionBsale = array();
              $DevolucionBsale['Message'] = $Message;
              $DevolucionBsale['status'] = 0;
          }

         // dd($DevolucionBsale);
          return $DevolucionBsale;

          //********* get documento url
            /*$DevolucionIdBsale =$DocumentoId;
            dd($DevolucionIdBsale);
             $DocumentoBsale = $this->getDocumentoBsale($DevolucionIdBsale,$TipoToken);

         if($DocumentoBsale['status'] == 1){
              $UrlPdf = $DocumentoBsale['urlPdf'];
              $NumeroDocumento = $DocumentoBsale['number'];
          }else{
              $response_array['Message'] = $DocumentoBsale['Message'];
              $response_array['status'] = 'Documento';
              dd($response_array);
             // return;
          }
          if($UrlPdf){
            dd($UrlPdf);
          }
        dd($DevolucionBsale); */
    }

    public function getDetallesDocumentoBsale($referenceDocumentId,$access_token, $type, $DetallesSeleccionados){
        //dd($referenceDocumentId);
        $url='https://api.bsale.cl/v1/documents/'.$referenceDocumentId.'/details.json';
        // echo 'Dentro de getDetallesDocumentoBsale getDetallesDocumentoBsale es '.print_r($getDetallesDocumentoBsale); echo "\n"; exit;
        // Inicia cURL

       // dd('holaa '.$url);
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Configura cabeceras
       // $rvtoken=$access_token;
        $rvtoken="e0b1793372fd0a5477794e167dd72a1824a95d2c"; //para cosultar el detalle del documento el token siempre es produccion.
        $headers = array(
            'access_token: ' . $rvtoken,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Ejecuta cURL
        $response = curl_exec($session);
       // dd('hello' . $response);
        // Cierra la sesión cURL
        curl_close($session);
        $DetallesBsale = json_decode($response, true);
        $details = array();


        if(isset($DetallesBsale['items'])){
            $contDet=0;
            foreach($DetallesBsale['items'] as $Detalle){
                $documentDetailId = $Detalle['id'];
                if($type == 2 OR $type == 3){
                    $unitValue = 0;
                    if ($type==2)
                        $unitValue=$DetallesSeleccionados[$contDet]['unitValue'];
                }else{
                    // devolución para ajustar el precio de los productos
                    $unitValue = $Detalle['netUnitValue'];

                }

                if($type == 2 OR $type == 3){
                    $quantity = 0;
                    if ($type==2)
                        $quantity=$DetallesSeleccionados[$contDet]['quantity'];
                }else{
                   $quantity = $Detalle['quantity'];

                }


                    // echo "Entro en otro tipo"; "\n";
                    $detail = array("documentDetailId" => $documentDetailId, "unitValue" => $unitValue, "quantity" => $quantity);
                    array_push($details,$detail);

                $contDet++;
            }
        }

       // dd($details);
        return $details;
    }
    public function getDocumentoBsale($Id,$TipoToken){

        $variables_globales=variables_globales::query()->get();
        if($TipoToken == 1){

            $access_token = $variables_globales[0]['token_produccion'];
        }else{

            $access_token = $variables_globales[0]['token_prueba'];
        }


        $url='https://api.bsale.cl/v1/documents/'.$Id.'.json?expand=[details]';

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Configura cabeceras
        $headers = array(
            'access_token: ' . $access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Ejecuta cURL
        $response = curl_exec($session);

        // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando
        $DocumentoBsale = json_decode($response, true);
        $DocumentoId = isset($DocumentoBsale['id']) ? trim($DocumentoBsale['id']) : "";
        if($DocumentoId){
            $DocumentoBsale['status'] = 1;
        }else{
            $Message = $DocumentoBsale['error'];
            $DocumentoBsale = array();
            $DocumentoBsale['Message'] = $Message;
            $DocumentoBsale['status'] = 0;
        }
        return $DocumentoBsale;
    }

    function encontrarEnArray($Value, $findme){
        if (!in_array($findme, $Value)) {
            $findme = false;
        }
        return $findme;
    }

    ///// ********* ANULACIONES ***********************************
    /// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public function guardar_nd($Id){

        if(in_array  ('curl', get_loaded_extensions())) {

            $response_array = array();

            $Devoluciones = devoluciones::query()->where('Id','=',$Id)->first();
           //dd($Devoluciones->FacturaId);
           $rvfacturaId=$Devoluciones->FacturaId;
            if($Devoluciones){

                $Devolucion = $Devoluciones;
                $DevolucionIdBsale = $Devolucion['DevolucionIdBsale'];
                $DocumentoIdBsale = $Devolucion['DocumentoIdBsale'];
                $AnulacionBsale = $this->sendAnulacionBsale($DevolucionIdBsale,$DocumentoIdBsale,1);

                if($AnulacionBsale['status'] == 1){
                    $AnulacionIdBsale = $AnulacionBsale['id'];
                    $NotaDebito = $AnulacionBsale['debit_note'];
                    $DocumentoIdBsale = $NotaDebito['id'];

                    $DocumentoBsale = $this->getDocumentoBsale($DocumentoIdBsale,1);
                    if($DocumentoBsale['status'] == 1){
                        $UrlPdf = $DocumentoBsale['urlPdf'];
                        $NumeroDocumento = $DocumentoBsale['number'];

                        $DevolucionId= new anulaciones();
                        $DevolucionId->DevolucionId =$Id;
                        $DevolucionId->AnulacionIdBsale =$AnulacionIdBsale;
                        $DevolucionId->DocumentoIdBsale =$DocumentoIdBsale;
                        $DevolucionId->UrlPdfBsale =$UrlPdf;
                        $DevolucionId->FechaAnulacion =NOW();
                        $DevolucionId->HoraAnulacion =NOW();
                        $DevolucionId->NumeroDocumento =$NumeroDocumento;
                        $DevolucionId->save();

                        $up_factura = DB::table('facturas')
                            ->where('Id', $rvfacturaId)
                            ->update(['EstatusFactu$requestracion' => 2]);

                        if ($up_factura)
                            $response_array['Message']=$response_array['Message']. "Cambió EstatusFacturacion =3 ";
                        else
                            $response_array['Message']=$response_array['Message']. "No se Cambió EstatusFacturacion";

                        $rvdevoluciones= DB::table('devoluciones')
                            ->where('Id','=',$Id)
                            ->update(['DevolucionAnulada' => 1 , 'FechaDevolucion' => NOW()]);

                        if ($rvdevoluciones)
                            $response_array['Message']=$response_array['Message']. "Cambió Devolucion anulada =1 ";
                        else
                            $response_array['Message']=$response_array['Message']. "No se Cambió Devolucion anulada";



                        $response_array['Id'] = $Id;
                        $response_array['UrlPdf'] = $UrlPdf;
                        $response_array['status'] = 1;



                    }else{
                            $response_array['Message'] = $DocumentoBsale['Message'];
                            $response_array['status'] = 'Documento';
                            return response()
                                ->json(["response_array"=>$response_array ]);
                        }




                }else{
                    $response_array['Message'] = $AnulacionBsale['Message']."  Error Proviene de Send Bsale";
                    $response_array['tipo'] = 'Anulacion';
                    $response_array['status'] = 0;
                    return response()
                        ->json(["response_array"=>$response_array ]);
                }


            }else{
                $response_array['status'] = 3;
            }
        }else{
            $response_array['status'] = 99;
            return response()
                ->json(["response_array"=>$response_array ]);
        }

        return response()
            ->json(["response_array"=>$response_array ]);
    }

    public function sendAnulacionBsale($DevolucionId,$referenceDocumentId,$TipoToken){
        //$TipoToken=1;
        //dd('Devolucionid: '.$DevolucionId. '-> Referencedocumentid: '.$referenceDocumentId);
        $variables_globales=variables_globales::query()->get();
        if($TipoToken == 1){

            $access_token = $variables_globales[0]['token_produccion'];
        }else{

            $access_token = $variables_globales[0]['token_prueba'];
        }

        //CREACION DE LA ANULACION

        $url='https://api.bsale.cl/v1/returns/'.$DevolucionId.'/annulments.json';

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // Activa SSL
        // curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // Configura cabeceras
        $headers = array(
            'access_token: ' . $access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);

        //CONSTRUCCION DEL ARRAY DE DEVOLUCION

        $array = array(
            "documentTypeId"        => 17,
            "referenceDocumentId"   => $referenceDocumentId,
            "emissionDate"          => time(),
            "expirationDate"        => time(),
            "declareSii"            => 1
        );

        // Parsea a JSON
        $data = json_encode($array);

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);

        // // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando
        $AnulacionBsale = json_decode($response, true);
        $DocumentoId = isset($AnulacionBsale['id']) ? trim($AnulacionBsale['id']) : "";
        if($DocumentoId){
            $AnulacionBsale['status'] = 1;
        }else{
            $Message = $AnulacionBsale['error'];
            $AnulacionBsale = array();
            $AnulacionBsale['Message'] = $Message;
            $AnulacionBsale['status'] = 0;
        }
        return $AnulacionBsale;
    }



    public function morosidades(){
        // $morosidades= morosidades::query()->where('Deuda','>',1)->get();
        $morosidades=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->where('FechaVencimiento','<=',now())->orderBy('Id','Asc')->get();

        ///**** esta no es la funcion... la real está e deudas2Controller
        dd($morosidades);
    }


}
