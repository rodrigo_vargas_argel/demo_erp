<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\mantenedor_costo;

class mantenedor_costoController extends Controller
{
    public function show(){
        $consulta=mantenedor_costo::query()->get();
        //$consulta=mantenedor_bodegaController::first();
        // dd($consulta[1]->responsable->nombre);
        return view('mantenedores.costos.costos',['consulta'=>$consulta]);
    }

    public function nuevo(){
//        $consulta=mantenedor_bodegaController::query()->get();
        $responsables=User::all();
        return view('mantenedores.costos.costos_nuevo',['responsables'=>$responsables]);
    }

    public function guardar(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'personal_id'=>'required|numeric|min:1',
            'codigo_cuenta'=>'required|numeric',

        ]);

        $sql = new mantenedor_costo();
        $sql->nombre = $request->nombre;
        $sql->personal_id = $request->personal_id;
        $sql->codigo_cuenta = $request->codigo_cuenta;
        $sql->save();

        return redirect()->route('mantenedores/costos');
    }




    public function editar($id_){
        $consulta=mantenedor_costo::query()->where('id','=',$id_)->get();
        $responsables=User::all();
//        dd($consulta);
        return view('mantenedores.costos.costos_editar', ['consulta'=>$consulta,'responsables'=>$responsables]);

    }

    public function modificar(Request $request){
        $id_=$request->id;


        $request->validate([
            'nombre'=>'required',
            'personal_id'=>'required|numeric',
            'codigo_cuenta'=>'required|numeric',

        ]);

        $sql=mantenedor_costo::find($id_);
        $sql->nombre=$request->nombre;
        $sql->personal_id=$request->personal_id;
        $sql->codigo_cuenta = $request->codigo_cuenta;
        $sql->save();

        return redirect()->route('mantenedores/costos');

    }



    public function eliminar($id_){
        $hoy = date('Y-m-d H:i:s');
        $sql=mantenedor_costo::find($id_);
        $sql->deleted_at=$hoy;
        $sql->save();

        return response()
            ->json(["Success"=>true]);
        //return redirect()->route('mantenedores/bodegas');





    }
}
