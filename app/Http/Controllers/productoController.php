<?php

namespace App\Http\Controllers;

use App\mantenedor_marca;
use App\mantenedor_modelo_marca;
use App\mantenedor_proveedor;
use App\mantenedor_tipo_producto;
use App\producto;
use App\producto_archivo;
use Illuminate\Http\Request;
use PHPUnit\Exception;


class productoController extends Controller
{
    public function show()
    {
        $consulta=producto::query()->orderBy('id','desc')->get();
       // dd($consulta->proveedor->nombre);
      //  dd($consulta);
        return view('mantenedores.productos.productos', ['consulta'=>$consulta]);
    }
    public function nuevo(){
        $tipos=mantenedor_tipo_producto::query()->orderBy('nombre','asc')->get();
        $marcas=mantenedor_marca::query()->orderBy('nombre','asc')->get();
        $modelos=mantenedor_modelo_marca::query()->orderBy('nombre','asc')->get();
        $proveedores=mantenedor_proveedor::query()->orderBy('nombre','asc')->get();
        return view('mantenedores.productos.producto_nuevo',['tipos'=>$tipos,'marcas'=>$marcas,'modelos'=>$modelos,'proveedores'=>$proveedores]);
    }
    public function guardar(Request $request){
        $request->validate([
            'tipo_producto'=>'required',
            'marca'=>'required',
            'modelo'=>'required',
            'codigo_barra'=>'required',
            'proveedor'=>'required',
            'unico'=>'required',
        ]);

        $sql=new producto;

        $sql->mantenedor_tipo_producto_id=$request->tipo_producto;
        $sql->mantenedor_marca_id=$request->marca;
        $sql->mantenedor_modelo_id=$request->modelo;
        $sql->codigo_barra=$request->codigo_barra;
        $sql->unico=$request->unico;
        $sql->descripcion=$request->descripcion;
        $sql->mantenedor_proveedores_id=$request->proveedor;

        $sql->save();

        return redirect()->route('mantenedores/productos');

    }

    public function editar($id_){
        $consulta=producto::query()->where('id','=',$id_)->first();
        $tipos=mantenedor_tipo_producto::all();
        $marcas=mantenedor_marca::all();
        $modelos=mantenedor_modelo_marca::where('mantenedor_marca_id',$consulta->mantenedor_marca_id)->get(); //asi cargo solo los modelos de la marca seleccionada
        $proveedores=mantenedor_proveedor::all();
        return view('mantenedores.productos.producto_editar',['consulta'=>$consulta,'tipos'=>$tipos,'marcas'=>$marcas,'modelos'=>$modelos,'proveedores'=>$proveedores]);
    }

    public  function modificar(Request $request){
        $request->validate([
            'tipo_producto'=>'required',
            'marca'=>'required',
            'modelo'=>'required',
            'codigo_barra'=>'required',
            'proveedor'=>'required',
            'unico'=>'required',
        ]);

        $sql= producto::find($request->id_);

        $sql->mantenedor_tipo_producto_id=$request->tipo_producto;
        $sql->mantenedor_marca_id=$request->marca;
        $sql->mantenedor_modelo_id=$request->modelo;
        $sql->codigo_barra=$request->codigo_barra;
        $sql->unico=$request->unico;
        $sql->descripcion=$request->descripcion;
        $sql->mantenedor_proveedores_id=$request->proveedor;

        $sql->update();

        return redirect()->route('mantenedores/productos');
    }

    public function cargar_marcas_modelos($id_){
        $data=mantenedor_modelo_marca::where('mantenedor_marca_id',$id_)->get();

        return response()
            ->json(["data"=>$data]);
    }

    public function nuevo_tipo_producto($nuevo_tipo){

            $sql= mantenedor_tipo_producto::create(['nombre' => $nuevo_tipo]);
            return response()
                ->json(["sql"=>$sql->id]);

    }

    public function nueva_marca_producto($nueva_marca){

        $sql= mantenedor_marca::create(['nombre' => $nueva_marca]);
        return response()
            ->json(["sql"=>$sql->id]);

    }
    public function nuevo_modelo_producto($id_marca, $nuevo_modelo){

        $sql= mantenedor_modelo_marca::create(['mantenedor_marca_id'=>$id_marca,'nombre' => $nuevo_modelo]);
        return response()
            ->json(["sql"=>$sql->id]);

    }

    public function  subir_archivos(Request $request)
    {
        // dd('hola');
        $request->validate([
            //  'archivo'=>'required',
            'id_report' => 'required',
        ]);

        $id_report = $request->id_report;
        $hoy = date('Y-m-d H:i:s');
        // dd($request->archivo);
        if (($request->archivo != null) || ($request->archivo != "")) {
            foreach ($request->archivo as $photo) {
                $filename = $photo->store('public/archivos');
                $path = str_replace('public/', '/', $filename);
                $sql2 = New producto_archivo();
                $sql2->producto_id = $id_report;
                $sql2->path = $path;
                $sql2->save();

            }
            return response()
                ->json(["Success" => "True"]);

        } else {
            return response()
                ->json(["Success" => "False"]);
        }
    }
}
