<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mantenedor_proveedor;
class mantenedor_proveedorController extends Controller
{
    public function show(){
        $consulta=mantenedor_proveedor::query()->get();
        return view('mantenedores.proveedores.proveedores',['consulta'=>$consulta]);
    }

    public function nuevo(){
        return view('mantenedores.proveedores.proveedores_nuevo');
    }

    public function guardar(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric|min:9',
            'contacto'=>'required',
            'correo'=>'required|email',
            'rut'=>'required',
            'dv'=>'required'

        ]);

        $sql = new mantenedor_proveedor;
        $sql->nombre = $request->nombre;
        $sql->direccion = $request->direccion;
        $sql->telefono = $request->telefono;
        $sql->contacto = $request->contacto;
        $sql->correo = $request->correo;
        $sql->rut = $request->rut;
        $sql->dv = $request->dv;
        $sql->save();

        return redirect()->route('mantenedores/proveedores');
    }

    public function editar($id_proveedor){
        $consulta=mantenedor_proveedor::query()->where('id','=',$id_proveedor)->get();
//        dd($consulta);
        return view('mantenedores.proveedores.proveedores_editar', ['consulta'=>$consulta]);
    }

    public function modificar(Request $request){
        $id_proveedor=$request->id;
        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric|min:9',
            'contacto'=>'required',
            'correo'=>'required|email',
            'rut'=>'required',
            'dv'=>'required'

        ]);

        $sql=mantenedor_proveedor::find($id_proveedor);
        $sql->nombre = $request->nombre;
        $sql->direccion = $request->direccion;
        $sql->telefono = $request->telefono;
        $sql->contacto = $request->contacto;
        $sql->correo = $request->correo;
        $sql->rut = $request->rut;
        $sql->dv = $request->dv;
        $sql->save();

        return redirect()->route('mantenedores/proveedores');
    }

    public function eliminar($id_proveedor){
//        dd($id_proveedor);
        $sql=mantenedor_proveedor::find($id_proveedor);
        $sql->delete();
        return redirect()->route('mantenedores/proveedores');
    }
}
