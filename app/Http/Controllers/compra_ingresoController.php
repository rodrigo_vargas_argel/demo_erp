<?php

namespace App\Http\Controllers;

use App\compra_archivo;
use App\compra_ingreso;
use App\existencia;
use App\existencia_movimiento;
use App\mantenedor_bodega;
use App\mantenedor_costo;
use App\mantenedor_proveedor;
use App\mantenedor_tipo_proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class compra_ingresoController extends Controller
{
    public function show(){
        $consulta=compra_ingreso::query()->with('detalle_compra')->orderBy('id','desc')->get();
       // dd($consulta[0]->centro_costo);
        return view('inventarios.compras',['consulta'=>$consulta]);
    }
    public function nuevo(){
        $proveedores=mantenedor_proveedor::all();
        $costos=mantenedor_costo::all();
        $bodegas=mantenedor_bodega::all();
        $tipos_doc=mantenedor_tipo_proveedor::all();

        return view('inventarios.compras_nuevo',['proveedores'=>$proveedores,'costos'=>$costos,'bodegas'=>$bodegas,'tipos_doc'=>$tipos_doc]);

    }

    public function guardar(Request $request){
        session_start();
        $request->validate([
            'tipo_doc'=>'required',
            'num_doc'=>'required|numeric',
            'fecha_emision'=>'required|date',
            'fecha_venc'=>'date',
            'total_neto'=>'required|numeric',
            'proveedor'=>'required|numeric',
            'bodega'=>'required|numeric',
        ]);

        $sql=new compra_ingreso;
        $sql->numero_documento=$request->num_doc;
        $sql->fecha_emision=$request->fecha_emision;
        $sql->fecha_vencimiento=$request->fecha_venc;
        $sql->detalle=$request->obs;
        $sql->proveedor_id=$request->proveedor;
        $sql->centro_costo_id=$request->costo;
        $sql->tipo_documento_id=$request->tipo_doc;
        $sql->mantenedor_bodegas_id=$request->bodega;
        $sql->total_documento=$request->total_neto;
        $sql->estado_id=1;
        $sql->save();
        $_SESSION['id_compra']=$sql->id;

        //return redirect()->route('inventarios/compras');
        return redirect()->route('detalle_nuevo');
    }

    public function editar($id){
        $consulta=compra_ingreso::query()->where('id','=',$id)->first();
        $proveedores=mantenedor_proveedor::all();
        $costos=mantenedor_costo::all();
        $bodegas=mantenedor_bodega::all();
        $tipos_doc=mantenedor_tipo_proveedor::all();

        return view('inventarios.compras_editar',['consulta'=>$consulta,'proveedores'=>$proveedores,'costos'=>$costos,'bodegas'=>$bodegas,'tipos_doc'=>$tipos_doc]);

    }
    public function modificar(Request $request){

        $request->validate([
            'tipo_doc'=>'required',
            'num_doc'=>'required|numeric',
            'fecha_emision'=>'required|date',
            'fecha_venc'=>'date',
            'total_neto'=>'required|numeric',
            'proveedor'=>'required|numeric',
            'bodega'=>'required|numeric',
        ]);

        $sql=compra_ingreso::find($request->id_compra);
        $sql->numero_documento=$request->num_doc;
        $sql->fecha_emision=$request->fecha_emision;
        $sql->fecha_vencimiento=$request->fecha_venc;
        $sql->detalle=$request->obs;
        $sql->proveedor_id=$request->proveedor;
        $sql->centro_costo_id=$request->costo;
        $sql->tipo_documento_id=$request->tipo_doc;
        $sql->mantenedor_bodegas_id=$request->bodega;
        $sql->total_documento=$request->total_neto;
       // $sql->estado_id=1;
        $sql->save();

        return redirect()->route('/inventarios/compras');
    }
    public function  finalizar($id_compra){
        $hoy = date('Y-m-d H:i:s');

        $compra=compra_ingreso::query()->where('id','=',$id_compra)->first();
        //$stock=existencia::query()->where('producto_id','=',$compra->producto_id)->first();
        //dd($stock);

        DB::beginTransaction();

        try {
                for($i=0; $i<$compra->detalle_compra->count();$i++)
                {
                    //dd($compra->detalle_compra[$i]->producto->unico);
                    if ($compra->detalle_compra[$i]->producto->unico==0)
                    {
                     $mac=$compra->detalle_compra[$i]->detalle_mac;

                      for($x=0;$x<$mac->count();$x++){
                         // dd($mac[$x]->producto_id);

                          $id_mac = DB::table('existencias_movimientos')->insertGetId(
                              ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $mac[$x]->producto_id, 'mac_serie' => $mac[$x]->mac,'cantidad' =>  1, 'bodega_origen_id' => 0,'bodega_destino_id' => $compra->mantenedor_bodegas_id, 'num_doc' => $compra->numero_documento,'tipo_doc' => 0, 'tipo_movimiento' => 'Compra','responsable' => 0]
                          );


                          DB::table('existencias')->insertGetId(
                              ['bodega_id' => $compra->mantenedor_bodegas_id, 'producto_id' => $mac[$x]->producto_id, 'mac_serie' => $mac[$x]->mac,'cantidad' =>  1,  'cantidad_old' => 0,'movimiento_id' => $id_mac]
                          );
                      }


                    }
                    else
                    {
                        $detalle=$compra->detalle_compra[$i];
                        //dd($detalle->total_neto);

                        $id_mov = DB::table('existencias_movimientos')->insertGetId(
                            ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $detalle->producto_id, 'mac_serie' => '0','cantidad' =>  $detalle->cantidad, 'bodega_origen_id' => 0,'bodega_destino_id' => $compra->mantenedor_bodegas_id, 'num_doc' => $compra->numero_documento,'tipo_doc' =>0 , 'tipo_movimiento' => 'Compra','responsable' => 0,'obs' => 0]
                        );

                        $stock=existencia::query()->where('producto_id','=',$detalle->producto_id)->where('bodega_id','=',$compra->mantenedor_bodegas_id)->first();
                        if ($stock){
                            $nueva_cantidad=$stock->cantidad+ $detalle->cantidad;
                            DB::table('existencias')
                                ->where('id','=',$stock->id)
                                ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $detalle->cantidad,'movimiento_id' => $id_mov]);
                        }
                        else{
                            //dd( $detalle->cantidad);
                            DB::table('existencias')->insertGetId(
                                ['bodega_id' => $compra->mantenedor_bodegas_id, 'producto_id' => $detalle->producto_id, 'mac_serie' => 0,'cantidad' =>  $detalle->cantidad,  'cantidad_old' => 0,'movimiento_id' => $id_mov]
                            );
                        }
                        //dd($detalle->total_neto);
                    }

                }
            $affected = DB::table('compras_ingresos')
                ->where('id', $id_compra)
                ->update(['estado_id' => 2]);

            DB::commit();

            return response()
                ->json(["Success"=>true, 'filas_afectadas'=>$affected]);

        }

        catch (\Throwable $e) {
            DB::rollback();
            //throw $e;
            return response()
                ->json(["Success"=>false, 'filas_afectadas'=>$e]);
        }


    }
    public function  subir_archivos(Request $request){
        // dd('hola');
        $request->validate([
            //  'archivo'=>'required',
            'id_report'=>'required',
        ]);

        $id_report=$request->id_report;
        $hoy = date('Y-m-d H:i:s');
        // dd($request->archivo);
        if (($request->archivo !=null) ||($request->archivo !="")){
            foreach ($request->archivo as $photo) {
                $filename = $photo->store('public/archivos');
                $path = str_replace('public/', '/', $filename);
                $sql2 = New compra_archivo();
                $sql2->compra_id = $id_report;
                $sql2->path = $path;
                $sql2->save();

            }
            return response()
                ->json(["Success"=>"True"]);

        }
        else{
            return response()
                ->json(["Success"=>"False"]);
        }
    }



}
