DB::beginTransaction();

try {
//********primero la cabecita **************

$id_nota = DB::table('nota_venta')->insertGetId(
['rut' => $request->cabecera[0]['cliente'], 'fecha' => $request->cabecera[0]['fecha_emision'], 'numero_oc' => $request->cabecera[0]['numero_oc'], 'fecha_oc' => $request->cabecera[0]['fecha_oc'], 'solicitado_por' => Auth::user()->id, 'estatus_facturacion' => 0, 'factura_id' => null, 'numero_hes' => $request->cabecera[0]['cont_hes'], 'fecha_hes' => null, 'tipo_pago_bsale'=>$request->cabecera[0]['tipo_pago_bsale'],'neto'=>$request->pie[0]['neto'],'iva'=>$request->pie[0]['iva'],'total'=>$request->pie[0]['total'],'created_at'=>$hoy]
);

//******* ahora el cuerpo *********************

if ($id_nota) {

for ($i = 0; $i < $contador_detalle; $i++) {
if (isset($request->detalle[0]['detalle'][$i]['descripcion'])) {
DB::table('nota_venta_detalle')->insertGetId(

['nota_venta_id' => $id_nota, 'concepto' => $request->detalle[0]['detalle'][$i]['descripcion'], 'valor' => $request->detalle[0]['detalle'][$i]['valor'], 'cantidad' => $request->detalle[0]['detalle'][$i]['cantidad'], 'precio' => $request->detalle[0]['detalle'][$i]['precio'], 'fdescuento' => $request->detalle[0]['detalle'][$i]['fdescuento'], 'descuento' => $request->detalle[0]['detalle'][$i]['descuento'], 'subtotal' => $request->detalle[0]['detalle'][$i]['subtotal'], 'iva' => $request->detalle[0]['detalle'][$i]['iva'], 'total' => $request->detalle[0]['detalle'][$i]['total']]
);
}
}

//ahora inserto los hes
for ($i = 0; $i < $cont_hes; $i++) {
// $num_hes = $request->hes[0]['hes'][$i]['num_hes'];
// dd($num_hes);
if (isset($request->hes[0]['hes'][$i]['num_hes'])) {
DB::table('nota_venta_hes')->insertGetId(

['nota_venta_id' => $id_nota, 'num_hes' => $request->hes[0]['hes'][$i]['num_hes'], 'fecha_hes' => $request->hes[0]['hes'][$i]['fecha_hes']]
);
}
}
DB::commit();
}


} catch (\Throwable $e) {
DB::rollback();
//throw $e;
return response()
->json(["Success" => false, 'msg' => $e->getMessage()]);
}