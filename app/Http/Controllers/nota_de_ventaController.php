<?php

namespace App\Http\Controllers;

//use App\Cliente;
use App\factura;
use App\mantenedor_tipo_pago_bsale;
use App\Models\Cliente;
use App\nota_de_venta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use App\Helpers\uf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class nota_de_ventaController extends Controller
{
    public function show()
    {

        $date = Carbon::now();
        $endDate = $date->subdays(15); //resto 61 dias a la fecha actual
        $sesenta_dias_antes=$endDate->toDateString();
        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        //$tipo_pago= mantenedor_tipo_pago::query()->get();
        $consulta = nota_de_venta::query()->with('nota_venta_detalle')->with('responsable')->with('cliente')->with('hes')->with('factura')->where('fecha','>=',$sesenta_dias_antes)->orderBy('id', 'desc')->get();

        //  dd($consulta[0]);
        return view('facturacion.nota_de_venta', ['consulta' => $consulta,'clientes'=>$clientes]);

    }

    public function lista_filtro(Request $request){

        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        //$facturas=factura::query()->with('detalle')->with('cliente')->with('pagos')->with('devoluciones')->where('EstatusFacturacion','!=',0)->->orderBy('Id','Desc')->get();
        $consulta = nota_de_venta::query()->with('nota_venta_detalle')->with('responsable')->with('cliente')->with('hes')->with('factura')->where('rut','=',$request->cliente)->orderBy('id', 'desc')->get();


        return view('facturacion.nota_de_venta', ['consulta' => $consulta,'clientes'=>$clientes]);

    }

    public function nuevo()
    {
        $ufi = new Uf;
        $uf = $ufi->getValue();
       // $uf=$ufi->getValue();
       // $uf=28688;
        $clientes = Cliente::query()->orderBy('nombre', 'asc')->get();
        $tipo_pago_bsale = mantenedor_tipo_pago_bsale::query()->where('activo', '=', 1)->get();
        return view('facturacion.nota_de_venta_nuevo', ['clientes' => $clientes, 'tipo_pago_bsale' => $tipo_pago_bsale, 'uf' => $uf]);

    }

    public function guardar(Request $request)
    {
        $hoy = date('Y-m-d H:i:s');
        $cont_hes= $request->cabecera[0]['cont_hes'];
       // dd($request->hes);
       // dd($request->detalle[0]['detalle'][0]['descripcion']); esta wea de array de detalles me quedo asi cuando lo armo en el front
        $contador_detalle=count($request->detalle[0]['detalle']);


        DB::beginTransaction();

        try {
            //********primero la cabecita **************

            $id_nota = DB::table('nota_venta')->insertGetId(
                ['rut' => $request->cabecera[0]['cliente'], 'fecha' => $request->cabecera[0]['fecha_emision'], 'numero_oc' => $request->cabecera[0]['numero_oc'], 'fecha_oc' => $request->cabecera[0]['fecha_oc'], 'solicitado_por' => Auth::user()->id, 'estatus_facturacion' => 0, 'factura_id' => null, 'numero_hes' => $request->cabecera[0]['cont_hes'], 'fecha_hes' => null, 'tipo_pago_bsale'=>$request->cabecera[0]['tipo_pago_bsale'],'neto'=>$request->pie[0]['neto'],'iva'=>$request->pie[0]['iva'],'total'=>$request->pie[0]['total'],'created_at'=>$hoy]
            );

            //******* ahora el cuerpo *********************

            if ($id_nota) {

                for ($i = 0; $i < $contador_detalle; $i++) {
                    if (isset($request->detalle[0]['detalle'][$i]['descripcion'])) {
                        DB::table('nota_venta_detalle')->insertGetId(

                            ['nota_venta_id' => $id_nota, 'concepto' => $request->detalle[0]['detalle'][$i]['descripcion'], 'valor' => $request->detalle[0]['detalle'][$i]['valor'], 'cantidad' => $request->detalle[0]['detalle'][$i]['cantidad'], 'precio' => $request->detalle[0]['detalle'][$i]['precio'], 'fdescuento' => $request->detalle[0]['detalle'][$i]['fdescuento'], 'descuento' => $request->detalle[0]['detalle'][$i]['descuento'], 'subtotal' => $request->detalle[0]['detalle'][$i]['subtotal'], 'iva' => $request->detalle[0]['detalle'][$i]['iva'], 'total' => $request->detalle[0]['detalle'][$i]['total'], 'servicio_id'=>$request->detalle[0]['detalle'][$i]['servicio_id']]
                        );
                    }
                }

                //ahora inserto los hes
                for ($i = 0; $i < $cont_hes; $i++) {
                   // $num_hes = $request->hes[0]['hes'][$i]['num_hes'];
                   // dd($num_hes);
                    if (isset($request->hes[0]['hes'][$i]['num_hes'])) {
                        DB::table('nota_venta_hes')->insertGetId(

                            ['nota_venta_id' => $id_nota, 'num_hes' => $request->hes[0]['hes'][$i]['num_hes'], 'fecha_hes' => $request->hes[0]['hes'][$i]['fecha_hes']]
                        );
                    }
                }
                DB::commit();
            }


        } catch (\Throwable $e) {
            DB::rollback();
            //throw $e;
            return response()
                ->json(["Success" => false, 'msg' => $e->getMessage()]);
        }

        return response()
            ->json(["Success"=>true, 'msg'=>"Nota de Venta Guardada con exito"]);



    }

    public function eliminar_nv($id_nv){
        $hoy = date('Y-m-d H:i:s');
        $sql=nota_de_venta::find($id_nv);
        $sql->deleted_at=$hoy;
        $sql->updated_user=Auth::user()->id;
        if($sql->save()){
            return response()
                ->json(["Success"=>true]);
        }
        else{
            return response()
                ->json(["Success"=>false]);

        }




    }

}
