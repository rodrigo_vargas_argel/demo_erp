<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Servicio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Contacto_Factibilidad;
use App\Models\Comentario;

use App\Helpers\FactibilidadExcel;
use Illuminate\Support\Facades\DB;

class Contacto_FactibilidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factibilidades = Contacto_Factibilidad::with(['estado','plan','torre','tipo_cliente','tipo_formulario','comentario_factibilidad','preventa_factibilidad','estado_contrato','estado_2'])
        ->orderBy('id','desc')
        ->get();
        return response()->json([
            'data'=>$factibilidades->toArray()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //por defecto el tipo de formulario es el 1 (formulario de factibilidad)
        $input['tipo_formulario_id'] = 1;
        //por defecto el estado es 8 (Sin Estado)
        $input['estados_id'] = 8;
        $factibilidad = Contacto_Factibilidad::create($input);
        $factibilidad->save();
        return response()->json([
            'data' => $factibilidad
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factibilidad = Contacto_Factibilidad::with(['estado','plan','torre','tipo_cliente','tipo_formulario','comentarios','comentarios.usuario'])->find($id);
        return response()->json([
            'data'=>$factibilidad->toArray()
        ], 200);
    }

    /**
     * Retorna las factibilidades por rango de fecha
     */
    public function cargar_factibilidades($desde,$hasta){
        $desde = $desde.' 00:00:00';
        $hasta = $hasta.' 23:59:59';
        $factibilidades = Contacto_Factibilidad::with(['estado','plan','torre','tipo_cliente','tipo_formulario','comentario_factibilidad','preventa_factibilidad','estado_contrato','estado_2'])
        ->whereBetween('created_at',[$desde,$hasta])
        ->orderBy('id','desc')
        ->get();
        //dd($factibilidades);
        return response()->json([
            'data'=>$factibilidades->toArray()
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $contacto = Contacto_Factibilidad::find($id);
        $contacto->fecha_cotizacion = $input['fecha_cotizacion'];
        $contacto->nombre = $input['nombre'];
        $contacto->rut = $input['rut'];
        $contacto->dv = $input['dv'];
        $contacto->email = $input['email'];
        $contacto->telefono = $input['telefono'];
        $contacto->latitud = $input['latitud'];
        $contacto->longitud = $input['longitud'];
        $contacto->ubicacion = $input['ubicacion'];
        $contacto->distancia = $input['distancia'];
        $contacto->mensaje = $input['mensaje'];
        $contacto->estados_id = $input['estados_id'];
        $contacto->planes_id = $input['planes_id'];
        $contacto->mantenedor_site_id = $input['mantenedor_site_id'];
        $contacto->mantenedor_tipo_cliente_id = $input['mantenedor_tipo_cliente_id'];
        $contacto->comentarios_factibilidades_id = $input['comentarios_factibilidades_id'];
        /**campos nuevos */
        $contacto->preventa_factibilidades_id = $input['preventa_factibilidades_id'];
        $contacto->estaciones_de_prueba = $input['estaciones_de_prueba'];
        $contacto->fecha_respuesta = $input['fecha_respuesta'];
        $contacto->estado_contrato_factibilidades_id = $input['estado_contrato_factibilidades_id'];
        $contacto->nro_servicio = $input['nro_servicio'];
        $contacto->plan_inicial = $input['plan_inicial'];
        $contacto->habilitacion = $input['habilitacion'];
        $contacto->nro_documentos = $input['nro_documentos'];
        $contacto->estados_id_2 = $input['estados_id_2'];
        $contacto->fecha_instalacion = $input['fecha_instalacion'];
        $contacto->fecha_activacion = $input['fecha_activacion'];



        $contacto->save();
        $actualizado = Contacto_Factibilidad::with(['estado','plan','torre','tipo_cliente','comentario_factibilidad','preventa_factibilidad','estado_contrato','estado_2'])->find($id);
        return response()->json([
            'data'=>$actualizado->toArray(),
            'mensaje'=>'Actualización Realizada'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factibilidad = Contacto_Factibilidad::find($id);
        $factibilidad->delete();
        return response()->json([
            'data'=>$factibilidad->toArray(),
            'mensaje'=>'Factibilidad Eliminada'
        ], 200);
    }

    /**
     * Muestra la vista con las factibilidades que vienen desde la página web
     */
    public function vista(){
        return view('factibilidad.factibilidad');
    }

    /**Muestra el detalle de una factibilidad */
    public function ver($id){
        return view('factibilidad.detalle',compact('id'));
    }

    /**devuelve los comentarios de un contacto factibilidad */
    public function comentarios($id){
        $comentarios = Comentario::with(['usuario','correo','correo.destinatarios'])->where('contacto_factibilidades_id',$id)->get();
        return response()->json([
            'data'=>$comentarios->toArray()
        ], 200);


    }

    /**genera exportacion a excel */
    public function excel($desde,$hasta){
        $url = FactibilidadExcel::Generar($desde,$hasta);
        return response()->json([
            'url'=>$url,
            'mensaje'=>'Archivo Generado Correctamente'
        ], 200);
    }

    public function dashboard(){
        $sql=DB::table('contacto_factibilidades')->select(DB::raw('MONTH(contacto_factibilidades.created_at) as mes' ),'estados_id' , 'estados.estado as estado',DB::raw('count(*) as total'))
           ->where('estados_id','!=',null)
           ->join('estados','estados.id','=','contacto_factibilidades.estados_id')
            ->groupBy(DB::raw('MONTH(contacto_factibilidades.created_at)'))->groupBy('estados_id')->get();
       $estados=Estado::query()->where('tipo_estado','!=',2)->orderBy('id')->get();

        $sql2=DB::table('servicios')->select(DB::raw('MONTH(servicios.create_at) as mes' ), DB::raw('count(*) as cantidad' ))
            ->where('EstatusServicio','=',1)
            ->where (DB::raw('YEAR(servicios.create_at)'),'=','2020')
            ->groupBy(DB::raw('MONTH(servicios.create_at)'))->get();

        $sabana=Servicio::query()->with('clientes')
            ->where (DB::raw('YEAR(servicios.create_at)'),'=','2020')->where('EstatusServicio','=','1')->get();

        $servicios_activados=DB::table('servicios')->select(DB::raw('MONTH(servicios.create_at) as mes' ), DB::raw('count(*) as cantidad' ))
            ->where('EstatusServicio','=',1)
            ->where (DB::raw('YEAR(servicios.create_at)'),'=','2020')
            ->where (DB::raw('MONTH(servicios.FechaInstalacion)'),'=','mes')
            ->where(DB::raw('servicios.UrlGraficos'),'!=',null)
            ->groupBy(DB::raw('MONTH(servicios.create_at)'))->get();
        $act_enero=DB::table('servicios')->select(DB::raw('MONTH(servicios.create_at) as mes' ), DB::raw('count(*) as cantidad' ))
            ->where('EstatusServicio','=',1)
            ->where (DB::raw('YEAR(servicios.create_at)'),'=','2020')
            ->where (DB::raw('MONTH(servicios.create_at)'),'=','7')
            ->where (DB::raw('MONTH(servicios.FechaInstalacion)'),'=','7')
            ->where(DB::raw('servicios.UrlGraficos'),'!=',null)
            ->groupBy(DB::raw('MONTH(servicios.create_at)'))->get();



        //dd($sql2);

        return view('factibilidad.dashboard',['estados'=>$estados,'sql'=>$sql, 'cont_servicios'=>$sql2, 'sabana'=>$sabana]);

        /*  $query=DB::table('contacto_factibilidades')->select('estados_id', DB::raw('count(*) as total'))
             ->groupBy('estados_id')->get();*/

     //   dd($query);


    }
}
