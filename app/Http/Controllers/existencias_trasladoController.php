<?php

namespace App\Http\Controllers;

use App\mantenedor_site;
use App\Models\Cliente;
use App\existencia;
use App\existencias_traslado_detalle;
use App\existencias_traslados;
use App\mantenedor_bodega;
use App\personaempresa;
use App\producto;
use App\servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection as RV_coleccion;
use Illuminate\Support\Collection as RV_coleccion2;
use Illuminate\Support\Facades\DB;

class existencias_trasladoController extends Controller
{
    public function show(){
        $registro=existencias_traslados::query()->with('detalle')->orderBy('id','desc')->get();
      //  dd($registro->count());
        if($registro->count() > 0 ) {
            for ($i = 0; $i < $registro->count(); $i++) {

                $detalle = collect($registro[$i]->detalle);
                if ($detalle->count() >0){


                    for($j=0; $j<$detalle->count();$j++){
                        $producto = collect($registro[$i]->detalle[$j]->producto);
                        $tipo = collect($registro[$i]->detalle[$j]->producto->tipo_producto);
                        $marca = collect($registro[$i]->detalle[$j]->producto->marca);
                        $modelo = collect($registro[$i]->detalle[$j]->producto->modelo);
                        $consulta = RV_coleccion2::make($registro,$detalle,$producto,$tipo,$marca,$modelo);
                    }

                }
                else{
                    $consulta=$registro;
                }
            }
           // dd($consulta);
        }
        else{
            $consulta="No hay registros";
        }



        return view('inventarios.traslados', ['consulta'=>$consulta]);
    }

    public function nuevo(){
        if ((Auth::user()->tipo_usuario == 0 )||(Auth::user()->tipo_usuario == 1)){

            $bodegas=mantenedor_bodega::query()->where('principal','!=','3')->get();
            $bodegas2=mantenedor_bodega::query()->where('principal','!=','0')->get();
        }
        else{
            $bodegas=mantenedor_bodega::query()->where('personal_id','=',Auth::user()->id)->where('principal','!=','3')->get();
            $bodegas2=mantenedor_bodega::query()->where('principal','!=','3')->get();
        }



        return view('inventarios.traslado_nuevo', ['bodegas'=>$bodegas, 'bodegas2'=>$bodegas2]);
    }


    public function combo($id_tipo){

        $bodegas2=mantenedor_bodega::query()->where('principal','!=','3')->where('principal','!=',$id_tipo)->get();
        return response()
            ->json(["Success"=>true, 'bodegas2'=>$bodegas2]);
    }


    public function continuar($traslado_id){
        $registro=existencias_traslados::query()->with('detalle')->where('id','=',$traslado_id)->first();
        //dd($registro);
        $consulta="";
        if($registro->count() > 0 ) {


                $detalle = collect($registro->detalle);
                if($detalle->count() > 0){
                    $contador_detalle=1;
                    for($j=0; $j<$detalle->count();$j++){
                        $producto = collect($registro->detalle[$j]->producto);
                        $tipo = collect($registro->detalle[$j]->producto->tipo_producto);
                        $marca = collect($registro->detalle[$j]->producto->marca);
                        $modelo = collect($registro->detalle[$j]->producto->modelo);
                        $consulta = RV_coleccion2::make($registro,$detalle,$producto,$tipo,$marca,$modelo);
                    }
                }
                else{
                    $contador_detalle=0;
                    $consulta=RV_coleccion2::make($registro);
                }
          //  dd($consulta);
        }
        else{
            return redirect()->route('/inventarios/traslados');
        }

        if ((Auth::user()->tipo_usuario == 0 )||(Auth::user()->tipo_usuario == 1)){

            $bodegas=mantenedor_bodega::query()->where('principal','!=','3')->get();
            $bodegas2=mantenedor_bodega::query()->where('principal','!=','0')->where('principal','!=','4')->get();
        }
        else{
            $bodegas=mantenedor_bodega::query()->where('personal_id','=',Auth::user()->id)->where('principal','!=','3')->get();
            $bodegas2=mantenedor_bodega::query()->where('principal','!=','3')->get();
        }



        return view('inventarios.traslado_editar', ['consulta'=>$consulta,'bodegas'=>$bodegas, 'bodegas2'=>$bodegas2, 'contador_detalle'=>$contador_detalle]);
    }

    public function guardar(Request $request){
        $hoy = date('Y-m-d H:i:s');
        $request->validate([
            'bodega_origen'=>'required',
            'bodega_destino'=>'required|different:bodega_origen',
            'motivo'=>'required',

        ]);

        $sql = new existencias_traslados();
        $sql->fecha = $hoy;
        $sql->bodega_origen_id = $request->bodega_origen;
        $sql->bodega_destino_id = $request->bodega_destino;
        $sql->motivo = $request->motivo;
        $sql->estado = 1;
        $sql->usuario_id = Auth::user()->id;

        if($sql->save()){
            return response()
                ->json(["Success"=>true, 'traslado_id'=>$sql->id]);
        }
        else{
            return response()
                ->json(["Success"=>false, 'traslado_id'=>0]);
        }

      //  return redirect()->route('inventarios/traslados');
    }
    public function guardar_detalle(Request $request){
       // dd($request);
        $hoy = date('Y-m-d H:i:s');
        $request->validate([
            'traslado_id'=>'required',
            'fdbodega_origen'=>'required',
            'codigo_producto'=>'required',
            'cantidad'=>'required',
            'mac_serie'=>'required',

        ]);

        $existe_producto=producto::query()->where('codigo_barra','=',$request->codigo_producto)->first();
        if ($existe_producto){
            $existe_stock=existencia::query()->where('bodega_id','=',$request->fdbodega_origen)->where('producto_id','=', $existe_producto->id)->where('mac_serie','=',$request->mac_serie)->first();

            if($existe_stock){
                $error=false;
                $msg= " ";
                $cantidad_origen=$existe_stock->cantidad;

                if ($cantidad_origen >= $request->cantidad)
                {

                    $nueva_cantidad_origen= $cantidad_origen -$request->cantidad;


                    $sql=new existencias_traslado_detalle;
                    $sql->existencia_traslado_id=$request->traslado_id;
                    $sql->producto_id=$existe_producto->id;
                    $sql->cantidad=$request->cantidad;
                    $sql->mac_serie=$request->mac_serie;

                    if( $sql->save())     {
                        $error=false;
                        $consulta=existencias_traslado_detalle::query()->where('id','=',$sql->id)->with('producto')->first();
                        $producto = collect($consulta->producto);
                        $tipo = collect($consulta->producto->tipo_producto);
                        $marca = collect($consulta->producto->marca);
                        $modelo = collect($consulta->producto->modelo);
                        //  $mac=collect($consulta[$i]->producto->detalle_mac);
                        $registro = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo);
                    }

                    else{
                        $error=true;
                        $msg="Hubo un Error de Conexion, Intente Iniciar Sesion Nuevamente ";
                        $registro="";
                    }

                }
                else{
                    $error=true;
                    $msg="El La Cantidad Ingresada Excede al Stock Disponible (".$cantidad_origen." Unidades) ";
                    $registro="";
                }
            }
            else{
                $error=true;
                $msg="El Codigo Ingresado o la Mac no Existe en la Bodega de Origen";
                $registro="";
            }

        }
        else{
            $error=true;
            $msg="El Codigo Ingresado no Existe en la Base de Datos";
            $registro="";
        }

        return response()
            ->json(["error"=>$error, 'msg'=>$msg, 'registro'=>$registro]);
    }

    public function finalizar($traslado_id){
        $hoy = date('Y-m-d H:i:s');
        $traslado=existencias_traslados::query()->where('id','=',$traslado_id)->first();
        $traslado_detalle=existencias_traslado_detalle::query()->where('existencia_traslado_id','=',$traslado_id)->get();

        for($i=0; $i<$traslado_detalle->count();$i++){

            if($traslado_detalle[$i]->mac_serie =='0'){

                        DB::beginTransaction();

                        try {
                            //********primero la salida de bodega_origen **************

                            $id_mov = DB::table('existencias_movimientos')->insertGetId(
                                ['fecha' => $hoy, 'e_s' => 's','producto_id' => $traslado_detalle[$i]->producto_id, 'mac_serie' => '0','cantidad' =>  $traslado_detalle[$i]->cantidad, 'bodega_origen_id' => 0,'bodega_destino_id' => $traslado->bodega_origen_id, 'num_doc' => $traslado->id, 'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => Auth::user()->id,'obs' => $traslado->motivo]
                            );

                            $stock=existencia::query()->where('producto_id','=',$traslado_detalle[$i]->producto_id)->where('bodega_id','=', $traslado->bodega_origen_id)->first();
                            $nueva_cantidad=$stock->cantidad- $traslado_detalle[$i]->cantidad;

                            DB::table('existencias')
                                ->where('id','=',$stock->id)
                                ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov]);


                            //******* ahora la entrada ala bodega destino *********************

                            $id_mov = DB::table('existencias_movimientos')->insertGetId(
                                ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $traslado_detalle[$i]->producto_id, 'mac_serie' => '0','cantidad' =>  $traslado_detalle[$i]->cantidad, 'bodega_origen_id' => 0,'bodega_destino_id' => $traslado->bodega_destino_id, 'num_doc' => $traslado->id, 'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => Auth::user()->id,'obs' => $traslado->motivo]
                            );

                            $stock=existencia::query()->where('producto_id','=',$traslado_detalle[$i]->producto_id)->where('bodega_id','=', $traslado->bodega_destino_id)->first();
                                if ($stock)
                                {
                                    $nueva_cantidad=$stock->cantidad + $traslado_detalle[$i]->cantidad;

                                    DB::table('existencias')
                                        ->where('id','=',$stock->id)
                                        ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov]);
                                }
                                else{
                                    DB::table('existencias')->insertGetId(
                                        ['bodega_id' => $traslado->bodega_destino_id, 'producto_id' => $traslado_detalle[$i]->producto_id, 'mac_serie' => 0,'cantidad' =>  $traslado_detalle[$i]->cantidad,  'cantidad_old' => 0,'movimiento_id' => $id_mov]
                                    );
                                }


                            DB::commit();


                        }

                        catch (\Throwable $e) {
                            DB::rollback();
                            //throw $e;
                            return response()
                                ->json(["Success"=>false, 'msg'=>$e]);
                        }

            }
            else{

                    DB::beginTransaction();

                    try {
                    //********primero la salida de bodega_origen **************


                        $id_mov1 = DB::table('existencias_movimientos')->insertGetId(
                            ['fecha' => $hoy, 'e_s' => 's','producto_id' => $traslado_detalle[$i]->producto_id, 'mac_serie' =>  $traslado_detalle[$i]->mac_serie,'cantidad' =>  $traslado_detalle[$i]->cantidad, 'bodega_origen_id' => 0,'bodega_destino_id' => $traslado->bodega_origen_id, 'num_doc' => $traslado->id, 'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => Auth::user()->id,'obs' => $traslado->motivo]
                        );

                        $id_mov = DB::table('existencias_movimientos')->insertGetId(
                            ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $traslado_detalle[$i]->producto_id, 'mac_serie' =>  $traslado_detalle[$i]->mac_serie,'cantidad' =>  $traslado_detalle[$i]->cantidad, 'bodega_origen_id' => 0,'bodega_destino_id' => $traslado->bodega_destino_id, 'num_doc' => $traslado->id, 'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => Auth::user()->id,'obs' => $traslado->motivo]
                        );

                            DB::commit();

                        }

                    catch (\Throwable $e) {
                        DB::rollback();
                        //throw $e;
                        return response()
                            ->json(["Success"=>false, 'msg'=>$e]);
                    }

                    $stock=existencia::query()->where('producto_id','=',$traslado_detalle[$i]->producto_id)->where('mac_serie','=', $traslado_detalle[$i]->mac_serie)->first();
                    $rvsql=existencia::find($stock->id);
                    $rvsql->bodega_id=$traslado->bodega_destino_id;
                    $rvsql->cantidad_old=$traslado->bodega_origen_id;
                    $rvsql->movimiento_id=$id_mov;
                    $rvsql->save();

            }

        }
        $sql2=existencias_traslados::find($traslado_id);
        $sql2->estado=0;
        $sql2->save();
        return response()
            ->json(["Success"=>true, 'msg'=>"Traslado Ejecutado con éxito"]);

    }

    public function entrega_equipo(){



        $coleccion_detalle="";
        if ((Auth::user()->tipo_usuario == 0 )||(Auth::user()->tipo_usuario == 1)||(Auth::user()->tipo_usuario == 2)){

            $bodegas=mantenedor_bodega::query()->where('principal','=','0')->get(); // 2 = tecnico, 1=cliene 0 =pricipal 2 = rma y transicion
        }
        else{
            //2 = tecnico, 1=cliene 0 =pricipal 3 = rma y transicion
            $bodegas=mantenedor_bodega::query()->where('personal_id','=',Auth::user()->id)->where('principal','=','0')->get();

        }
        $cliente=Cliente::query()->orderBy('nombre','asc')->get();
        //dd($cliente);
            //*** CARGA INVENTARIOS ***** //
                $consulta=existencia::with('producto')->where("bodega_id","=",$bodegas[0]->id)->orderBy('id','desc')->get();
                //dd($consulta->count());
                if($consulta->count() > 0 ) {

                    for($i=0; $i < $consulta->count(); $i++)
                    {

                        $producto = collect($consulta[$i]->producto);
                        $tipo = collect($consulta[$i]->producto->tipo_producto);
                        $marca = collect($consulta[$i]->producto->marca);
                        $modelo = collect($consulta[$i]->producto->modelo);
                        //  $mac=collect($consulta[$i]->producto->detalle_mac);

                        $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo);

                    }

                }
                else{

                    $coleccion_detalle='No hay Productos en esta bodega';
                }
        // 0 = central, 1=cliente 3 = rma y transicion, 2 = tecnico
        $bodegas2=mantenedor_bodega::query()->where('principal','=','1')->get();
                $estaciones=mantenedor_site::query()->get();

        return view('inventarios.entrega_equipos',['bodega_origen'=>$bodegas,'bodega_destino'=>$bodegas2, 'existencias'=>$coleccion_detalle, 'clientes'=>$cliente, 'estaciones'=>$estaciones]);

    }

    public function devolucion_equipo(){



        $coleccion_detalle="";
        if ((Auth::user()->tipo_usuario == 0 )||(Auth::user()->tipo_usuario == 1)||(Auth::user()->tipo_usuario == 2)){

            $bodegas=mantenedor_bodega::query()->where('principal','=','1')->get(); // 2 = tecnico, 1=cliene 0 =pricipal 3 = rma y transicion
        }
        else{
            // bodega principal
            $bodegas=mantenedor_bodega::query()->where('personal_id','=',Auth::user()->id)->get();

        }

       // $cliente=Cliente::query()->orderBy('nombre','asc')->get();
        //dd($cliente);
        //*** CARGA INVENTARIOS ***** //
        $consulta=existencia::with('producto')->with('servicio')->where("bodega_id","=",$bodegas[0]->id)->orderBy('id','desc')->get();
        //dd($consulta->count());
        if($consulta->count() > 0 ) {

            for($i=0; $i < $consulta->count(); $i++)
            {

                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                //  $mac=collect($consulta[$i]->producto->detalle_mac);

                $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo);

            }

        }
        else{

            $coleccion_detalle='No hay Productos en esta bodega';
        }

        // 0 = central, 1 =cliente  2 = tecnico 3 = rma y transicion,
        $bodegas2=mantenedor_bodega::query()->where('principal','=','0')->get();

        return view('inventarios.devolucion_equipos',['bodega_origen'=>$bodegas,'bodega_destino'=>$bodegas2, 'existencias'=>$coleccion_detalle]);

    }


    public function carga_inventarios($bodega){

        $consulta=existencia::with('producto')->where("bodega_id","=",$bodega)->orderBy('id','desc')->get();
        //dd($stock);
        if($consulta->count() > 0 ) {

            for($i=0; $i < $consulta->count(); $i++)
            {

                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                $estadus= collect($consulta[$i]->estadoo); //metodo del modelo

                //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo, $estadus);

            }
             return response()
                 ->json(["Success"=>true, 'coleccion'=>$coleccion_detalle]);
        }
        else{
            $coleccion_detalle='No hay Productos en esta bodega';
            return response()
                ->json(["Success"=>false, 'coleccion'=>$coleccion_detalle]);
        }


    }
    public function cargar_servicios($id_cliente){
        $servicios=servicio::query()->with('mantenedor_servicios')->where('Rut',$id_cliente)->get();
       // dd($servicios);
        return response()
            ->json(["servicios"=>$servicios]);
    }

    public function entrega_equipos_guardar( Request $request){
        $hoy = date('Y-m-d H:i:s');
        $msg="";
        $request->validate([
            'bodega_destino'=>'required',
            'observaciones'=>'required',
            'equipos_para_entrega'=>'required|integer|gt:0',


        ]);

      //  dd($request);
        $mensaje=" Entrega de equipo -> ";
        if(($request->servicio != null)||($request->estacion != null))
            $mensaje=" Entrega Equipo ";
        else
            $mensaje=" Devolución  Equipo ";

            DB::beginTransaction();

                $id_traslado= DB::table('existencias_traslados')->insertGetId(
                    ['fecha'=>$hoy,'bodega_origen_id' => $request->bodega_origen,'bodega_destino_id' => $request->bodega_destino, 'motivo' => $request->observaciones, 'usuario_id' =>Auth::user()->id,'estado' =>  0,  'created_at' => $hoy]
                );


                foreach($request->check as $selected) {
                    $nueva_cantidad_origen=0;
                    $nueva_cantidad_destino=0;

                    $rv_check = explode("-", $selected);
                    $msg = $msg . " *  " . $rv_check[2];
                    if ($rv_check[2]!='0')
                        $cantidad=1;
                    else
                        {
                        $puntero=$rv_check[0].'-'.$rv_check[1].'-0';
                        $campo="cantidad".$puntero;
                        $cantidad=$request->$campo;
                         //dd($cantidad);
                        }

                            $existe_stock=existencia::query()->where('bodega_id','=', $request->bodega_origen)->where('producto_id','=',$rv_check[1])->where('mac_serie','=','0')->first();
                            if ($existe_stock)
                                {
                                 $bandera_existe='si';
                                 if($request->servicio != null){

                                    $mensaje=" Entrega Equipo ";
                                 $existe_stock_destino=existencia::query()->where('bodega_id','=', $request->bodega_destino)->where('producto_id','=',$rv_check[1])->where('mac_serie','=','0')->where('servicio_id','=',$request->servicio)->first();
                                 }
                                 else{

                                     $mensaje=" Devolución Equipo ";
                                 $existe_stock_destino=existencia::query()->where('bodega_id','=', $request->bodega_destino)->where('producto_id','=',$rv_check[1])->where('mac_serie','=','0')->first();
                                 }
                                 if($existe_stock_destino)
                                        $bandera_existe_destino='si';
                                    else
                                        $bandera_existe_destino='no';
                                 // dd($existe_stock->cantidad);
                                }
                            else{
                                $bandera_existe='no';
                               // dd($existe_stock);
                            }


                    try {
                        //********primero la salida de bodega_origen **************

                        $id_mov1 = DB::table('existencias_movimientos')->insertGetId(
                            ['fecha' => $hoy, 'e_s' => 's', 'producto_id' => $rv_check[1], 'mac_serie' => $rv_check[2], 'cantidad' => $cantidad, 'bodega_origen_id' => 0, 'bodega_destino_id' => $request->bodega_origen, 'num_doc' => $rv_check[0], 'tipo_doc' => 0, 'tipo_movimiento' => $mensaje, 'responsable' => Auth::user()->id, 'obs' => $request->observaciones]
                        );

                        //********segundo la entrada de bodega_destino **************

                        $id_mov = DB::table('existencias_movimientos')->insertGetId(
                            ['fecha' => $hoy, 'e_s' => 'e', 'producto_id' => $rv_check[1], 'mac_serie' => $rv_check[2], 'cantidad' => $cantidad, 'bodega_origen_id' => 0, 'bodega_destino_id' => $request->bodega_destino, 'num_doc' => $rv_check[0], 'tipo_doc' => 0, 'tipo_movimiento' =>$mensaje, 'responsable' => Auth::user()->id, 'obs' => $request->observaciones]
                        );

                        //*************luego, si no tiene stock,es porque  es un producto con mac, por lo tanto su stock siempre sera 1 y con ese caso (la mayoria) solo hago un update en el registro del producto
                            if ($bandera_existe=='no'){

                                DB::table('existencias')
                                    ->where('id', '=', $rv_check[0])
                                    ->update(['bodega_id' => $request->bodega_destino, 'servicio_id' => $request->servicio,'estacion_id'=>$request->estacion, 'movimiento_id' => $id_mov]);
                            }
                            else{ //si se trata de un producto sin mac
                                $nueva_cantidad_origen=$existe_stock->cantidad - $cantidad;
                                DB::table('existencias')
                                    ->where('id', '=', $rv_check[0])
                                    ->update(['cantidad' => $nueva_cantidad_origen, 'cantidad_old'=>$existe_stock->cantidad, 'movimiento_id' => $id_mov1]);

                                    if($bandera_existe_destino=='si'){
                                        $nueva_cantidad_destino=$existe_stock_destino->cantidad+$cantidad;

                                        DB::table('existencias')
                                            ->where('id', '=', $existe_stock_destino->id)
                                            ->update(['cantidad' => $nueva_cantidad_destino,'cantidad_old'=>$existe_stock_destino->cantidad,  'movimiento_id' => $id_mov]);

                                    }
                                    else{
                                        DB::table('existencias')->insertGetId(
                                            ['bodega_id' => $request->bodega_destino, 'producto_id' => $rv_check[1], 'mac_serie' => 0,'cantidad' =>  $cantidad,  'cantidad_old' => 0,'movimiento_id' => $id_mov, 'servicio_id'=>$request->servicio , 'estacion_id'=>$request->estacion]
                                        );
                                    }


                            }



                        DB::table('existencias_traslado_detalle')->insertGetId(
                            ['existencia_traslado_id'=>$id_traslado,'producto_id' => $rv_check[1],'cantidad' => $cantidad, 'mac_serie' => $rv_check[2], 'created_at' =>$hoy ]
                        );


                    } catch (\Throwable $e) {
                        DB::rollback();
                        //throw $e;
                        return response()
                            ->json(["Success" => false, 'msg' => $e]);
                    }
                }

            DB::commit();

            return response()
                ->json(["Success" => true, 'msg' => 'Registro almacenado Exitosamente.']);


    }



}
