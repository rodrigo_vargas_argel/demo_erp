<?php

namespace App\Http\Controllers;

use DataTables;
use Barryvdh\DomPDF\Facade as PDF;

use App\User;
use App\producto;
use App\Models\Cliente;
use App\Models\Ticket;
use App\Models\TicketOt;
use App\Models\MotivoCorreo;
use App\Models\TicketCierre;
use App\Models\TicketEstado;
use App\Models\TicketOtEquipo;
use App\Models\TicketOtDetalle;
use App\Models\TicketAuditoria;
use App\Models\TicketComentario;

use App\existencia;
use App\mantenedor_site;
use App\mantenedor_bodega;
use Illuminate\Support\Collection as RV_coleccion;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Mail\EmailAsignaTicket;
use App\Mail\EmailCierreOt;
use Illuminate\Support\Facades\Mail;

class TicketOtController extends Controller
{
    private $enviar_email = false ;  //  DEJAR TRUE PARA ENVIAR CORREOS
     /**
     * Display a listing of tickets.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets()
    {
        $data = Ticket::with(['ticket_estado'])->latest()->paginate(20);

        return view('ticket_ot.tickets',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coleccion_detalle="";
        if ((Auth::user()->tipo_usuario <= 2)){
            $bodegas = mantenedor_bodega::query()->where('principal', '2')->get(); // 2 = tecnico, 1=cliene 0 =pricipal 3 = rma y transicion
        }  else {
            $bodegas = mantenedor_bodega::query()->where('personal_id', Auth::user()->id)->where('principal','!=','3')->get();
        }
        $cliente = Cliente::query()->orderBy('nombre','asc')->get();
        //dd($cliente);
            //*** CARGA INVENTARIOS ***** //
        $consulta = existencia::with('producto')->where("bodega_id","=",$bodegas[0]->id)->orderBy('id','desc')->get();
        //dd($consulta->count());
        if($consulta->count() > 0 ) {
            for($i=0; $i < $consulta->count(); $i++)
            {
                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo);
            }
        } else {
            $coleccion_detalle='No hay Productos en esta bodega';
        }
        // 0 = central, 1=cliente 3 = rma y transicion, 2 = tecnico
        $bodegas2 = mantenedor_bodega::query()->where('principal','=','1')->get();
        $estaciones = mantenedor_site::query()->get();

        $data = TicketOt::with(['ticket_ot_detalle', 'ticket_ot_equipo'])->latest()->paginate(20);

        return view('ticket_ot.index',['data' => $data,'bodega_origen' => $bodegas,'bodega_destino' => $bodegas2, 'existencias' => $coleccion_detalle, 'clientes' => $cliente, 'estaciones' => $estaciones])
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_from_ticket($id)
    {
        $ticket = Ticket::find($id);
        $estados = TicketEstado::select('id', 'nombre')->where('es_ot', 1)->where('nombre', 'Creado')->orderBy('nombre')->get();
        $tecnicos = User::select('id', 'nombre')->where('tipo_usuario', 3)->orderBy('nombre')->get();
        $trabajos_ot = TicketOtDetalle::select('nombre')->distinct()->get();
        $equipos = producto::with(['modelo', 'marca'])->select('id', 'mantenedor_marca_id', 'mantenedor_modelo_id')->get();
        return view('ticket_ot.create', compact('ticket', 'estados', 'equipos', 'tecnicos', 'trabajos_ot'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'ticket_id' => 'required',
            'fecha_visita' => 'required',
            'tecnico' => 'required',
            'nombre' => 'required',
            'equipo' => 'required',
            'cantidad' => 'required',
        ]);

        //valido que haya al menos 1 trabajo y 1 equipo
        $token1 = false;
        foreach ($request->nombre as $value) {    ///****  VALIDO QUE HAYAN TRABAJOS
            if (($value != null) && (trim($value) != "")) {
                 $token1 = true;
            }
        }
        $token2 = false;
        for ($i = 0; $i < 15 ; $i++) {     ///****  VALIDO QUE HAYAN EQUIPOS
            if (($request->equipo[$i] != null) && (trim($request->equipo[$i] ) != "") && 
               ($request->cantidad[$i] != null) && (trim($request->cantidad[$i] ) != "")) {
                 $token2 = true;
            }
        }                           ///****  SI NO HAY ALGUNO DEVUELVO ERRORES
        if (!$token1) request()->validate(['trabajos' => 'required']);
        //if (!$token2) request()->validate(['equipos_a_utilizar' => 'required']);

            ///****  GRABO EL NOMBRE
        $new_ot = TicketOt::create([   'ticket_id' => $request->ticket_id,
                                       'usuario_id' => Auth::user()->id,
                                       'estado_id' => 1,
                                       'descripcion' => $request->descripcion ]);

        $trabajos = $this->grabarTrabajos($request, $new_ot);

        $equipos = $this->grabarEquipos($request, $new_ot);

        TicketAuditoria::grabaAuditoria( 1, $request->ticket_id, "Creación OT ".$new_ot->id." con ".$trabajos." trabajos y ".$equipos." equipos." );
        //reviso si hay q actualizar el ticket
        $tokencito = false;
        $ticket = Ticket::find($request->ticket_id);
        if ($ticket->tecnico_id != $request->tecnico ) {
            $ticket->tecnico_id = $request->tecnico;
            $tokencito = true;
        }
        if ($ticket->fecha_visita != $request->fecha_visita ) {
            $ticket->fecha_visita = $request->fecha_visita;
            $tokencito = true;
        }
        if ($tokencito) $ticket->save();

        $msge_email = 'OT registrada correctamente.';
        
        if (isset($ticket->tecnico_id) && $this->enviar_email){
            $cc_user = User::select('email')->where('id', Auth::user()->id)->first();
            $cc = 'rvargas@teledata.cl';  // dejar email de envío si no hay email del que está logueado
            if (isset($cc_user->email)) $cc = $cc_user->email;

            $usuarios = User::select('email')->where('id', $ticket->tecnico_id)->get();
            Mail::to($usuarios)
            ->cc($cc)
            ->send(new EmailAsignaTicket($ticket, 2));
            if (Mail::failures()) {
                $msge_email = " OT registrada correctamente, pero hubo error al enviar emails a persona asignada en OT... ";
            } else {
                $emilio = $usuarios->first();
                $msge_email = 'OT registrada correctamente. E-mail enviado a '.$emilio->email;
            }
        }

        return redirect()->route('ots.index')->with('success', $msge_email);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TicketOt::find($id);
        $ot_detalle = TicketOtDetalle::where('ticket_ot_id', $id)->get();
        $ot_equipos = TicketOtEquipo::where('ticket_ot_id', $id)->get();

        $pdf = PDF::loadView('ticket_ot.pdf_ot', compact('data', 'ot_detalle', 'ot_equipos' ));

        return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = TicketOt::find($id);
        $ticket = Ticket::find($data->ticket_id);
        $estados = TicketEstado::select('id', 'nombre')->where('es_ot', 1)->orderBy('id')->get();
        $equipos = producto::with(['modelo', 'marca'])->select('id', 'mantenedor_marca_id', 'mantenedor_modelo_id')->get();
        $tecnicos = User::select('id', 'nombre')->where('tipo_usuario', 3)->orderBy('nombre')->get();
        $trabajos_ot = TicketOtDetalle::select('nombre')->distinct()->get();
        $ot_detalle = TicketOtDetalle::where('ticket_ot_id', $id)->get();
        $ot_equipos = TicketOtEquipo::where('ticket_ot_id', $id)->get();

        return view('ticket_ot.edit', compact('data', 'ticket', 'estados', 'equipos', 'tecnicos', 'trabajos_ot', 'ot_detalle', 'ot_equipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TicketOt $ot)
    {
        request()->validate([
            'ticket_id' => 'required',
            'fecha_visita' => 'required',
            'tecnico' => 'required',
            'nombre' => 'required',
            'equipo' => 'required',
            'cantidad' => 'required',
        ]);

        //valido que haya al menos 1 trabajo y 1 equipo
        $token1 = false;
        foreach ($request->nombre as $value) {    ///****  VALIDO QUE HAYAN TRABAJOS
            if (($value != null) && (trim($value) != "")) {
                 $token1 = true;
            }
        }
        $token2 = false;
        for ($i = 0; $i < 15 ; $i++) {     ///****  VALIDO QUE HAYAN EQUIPOS
            if (($request->equipo[$i] != null) && (trim($request->equipo[$i] ) != "") && 
               ($request->cantidad[$i] != null) && (trim($request->cantidad[$i] ) != "")) {
                 $token2 = true;
            }
        }                           ///****  SI NO HAY ALGUNO DEVUELVO ERRORES
        if (!$token1) request()->validate(['trabajos' => 'required']);
        //if (!$token2) request()->validate(['equipos_a_utilizar' => 'required']);

            ///****  GRABO EL NOMBRE
        $upd_ot = TicketOt::find($ot->id)->update([   'usuario_id' => Auth::user()->id,
                                                       'estado_id' => $request->estado_id,
                                                       'descripcion' => $request->descripcion ]);

        //reviso si hay q actualizar el ticket
        $tokencito = false;
        $ticket = Ticket::find($ot->ticket_id);
        if ($ticket->tecnico_id != $request->tecnico ) {
            $ticket->tecnico_id = $request->tecnico;
            $tokencito = true;
        }
        if ($ticket->fecha_visita != $request->fecha_visita ) {
            $ticket->fecha_visita = $request->fecha_visita;
            $tokencito = true;
        }
        if ($tokencito) $ticket->save();

        $trabajos = $this->grabarTrabajos($request, $ot);

        $equipos = $this->grabarEquipos($request, $ot);

        if (isset($request->comentario) &&  ($request->comentario != "") ) {
            TicketComentario::create([  'usuarios_id' => Auth::user()->id,
                                        'comentario' => $request->equipo[$i],
                                        'fecha'     => date('Y-m-d H:i:s'),
                                        'tickets_id' => $ot->ticket_id
                                    ]);
        }

        //auditoría
        $upd_ot = TicketOt::find($ot->id);
        TicketAuditoria::grabaAuditoria( 2, $request->ticket_id, "Modificada OT ".$ot->id." estado:".$upd_ot->ticket_estado->nombre." con ".$trabajos." trabajos creados o modificados y ".$equipos." equipos creados o modificados. Técnico:".$upd_ot->tickets->tecnico->nombre.". Fecha visita:".$upd_ot->tickets->fecha_visita );

        return redirect()->route('ots.index')->with('success','OT actualizada correctamente.');
    }


    public function cierre_ot(Request $request, TicketOt $ot)
    {
        request()->validate([
            'fecha_salida' => 'required',
            'hora_salida' => 'required',
            'fecha_llegada' => 'required',
            'hora_llegada' => 'required',
            'observaciones' => 'required',
            'ticket_id' => 'required',
        ]);
        //logica de cerrar ot y ticket
        
        $user_cierre = User::find(Auth::user()->id);

        $cierre = TicketCierre::where('ticket_id', $request->ticket_id)->first();
        if ( !isset($cierre->id) )
            $cierre = new TicketCierre;
        $cierre->ticket_id = $request->ticket_id;
        $cierre->fecha_salida = $request->ticket_id;
        $cierre->hora_salida = $request->hora_salida;
        $cierre->fecha_llegada = $request->fecha_llegada;
        $cierre->hora_llegada = $request->hora_llegada;
        $cierre->observaciones = $request->observaciones;
        $cierre->usuario_id = $user_cierre->id;
        $cierre->save();

        // CIERRE DEL TICKET
        $ticket = Ticket::find( $request->ticket_id )->update([ 'estado_id' => 9]);
        $ticket = Ticket::find( $request->ticket_id );
        // CIERRE DE LA OT
        $la_ot = TicketOt::where('ticket_id',$request->ticket_id)->first()->update([ 'estado_id' => 9]);
        //envio de email por cierre
        $emilios = MotivoCorreo::with('correo_usuarios.usuarios')->where('nombre', 'cierre-ticket')->first();

        $msge['estado']  = "OK";
        $msge['msge']  = " Se cerró OT y Ticket. No se envió correo de aviso de cierre. ";
        if ( isset($emilios->correo_usuarios) && $this->enviar_email ){  //&& $enviar_email 
            $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
            $cc = 'teledatadte@teledata.cl';
            if (strlen($user_cierre->email) > 5 ) {
                $cc = $user_cierre->email;
            }
            $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
            Mail::to($usuarios)->cc($cc)->send(new EmailCierreOt($ticket, 2));
            if (Mail::failures()) {
                $msge['msge'] = " se cerró OT y Ticket, pero hubo error al enviar emails ... ";
            } else {
                $msge['msge'] = " se cerró OT y Ticket. Se envió correo a: ".$usuarios->implode('email', ', ');
            }
        }
        TicketAuditoria::grabaAuditoria( 2, $request->ticket_id, "Cierre OT y Ticket nro. ".$ticket->id );
        return json_encode($msge);
    }

    public function grabarTrabajos($request, $ot){
        $trabajos = 0;
        for ($i = 0; $i < 10 ; $i++) {      ///****  GRABO LOS TRABAJOS
            if ( !is_null($request->nombre[$i]) && (trim($request->nombre[$i] ) != "") ) {
                if ( isset($request->ot_detalle[$i]) ) {
                    $det = TicketOtDetalle::find($request->ot_detalle[$i])
                            ->update([      'nombre' => $request->nombre[$i],
                                        'usuario_id' => Auth::user()->id ]);
                } else {
                    $det = TicketOtDetalle::create([  'ticket_ot_id' => $ot->id,
                                                            'nombre' => $request->nombre[$i],
                                                        'usuario_id' => Auth::user()->id
                                                       ]);
                }
                $trabajos++;
            } else { // si no hay datos, pero antes habia, los elimino
                if ( isset($request->ot_detalle[$i]) ) { 
                    $eq = TicketOtDetalle::find($request->ot_detalle[$i])->delete();
                }
            }
        }
        return $trabajos;
    }

    public function grabarEquipos($request, $ot){
        $equipos = 0;
        for ($i = 0; $i < 15 ; $i++) {    ///****  GRABO LOS EQUIPOS
            if ( !is_null($request->equipo[$i]) && (trim($request->equipo[$i] ) != "") && 
               !is_null($request->cantidad[$i]) && (trim($request->cantidad[$i] ) != "")) {
                if ( isset($request->ot_equipos[$i]) ) {
                    $eq = TicketOtEquipo::find($request->ot_equipos[$i])
                                ->update([   'equipo_id' => $request->equipo[$i],
                                              'cantidad' => $request->cantidad[$i],
                                            'usuario_id' => Auth::user()->id ]);
                } else {
                    $eq = TicketOtEquipo::create([ 'ticket_ot_id' => $ot->id,
                                                   'equipo_id'    => $request->equipo[$i],
                                                   'cantidad'     => $request->cantidad[$i],
                                                   'usuario_id'   => Auth::user()->id
                                                ]);
                }
                $equipos++;
            } else { // si no hay datos, pero antes habia, los elimino
                if ( isset($request->ot_equipos[$i]) ) { 
                    $eq = TicketOtEquipo::find($request->ot_equipos[$i])->delete();
                }
            }
        }
        return $equipos;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function trabajos($id)
    {
        $trabajos = TicketOtDetalle::select('nombre', 'estado')->where('ticket_ot_id', $id)->orderBy('nombre')->get();
        return DataTables::of($trabajos)->editColumn('estado', function ($t) {
                        if ($t->estado == 1) {
                            return "Completado";
                        } else {
                            return "Pendiente";
                        }
            })->make(true);
    }

    public function equipos($id)
    {
        $equipos = TicketOtEquipo::select('id', 'equipo_id', 'cantidad', 'entregado')->where('ticket_ot_id', $id)->get();
        return DataTables::of($equipos)
                    ->editColumn('equipo_id', function ($t) {
                        return @$t->equipos->marca->nombre." " .@$t->equipos->modelo->nombre;
                })->editColumn('entregado', function ($t) {
                    if ($t->entregado == 0) {
                        return '<input class="toggle-check" type="checkbox" checked data-toggle="toggle" data-on="Entregado" data-off="Pendiente" data-onstyle="success" data-offstyle="danger" attr="'.$t->id.'">';
                    } else {
                        return '<input class="toggle-check" type="checkbox" data-toggle="toggle" data-on="Entregado" data-off="Pendiente" data-onstyle="success" data-offstyle="danger" attr="'.$t->id.'">';
                    }
                })->rawColumns(['entregado'])->make(true);
    }

    public function comentarios($id)
    {
        $comentarios = TicketComentario::with('usuario')->select('usuarios_id', 'comentario', 'fecha')->where('tickets_id', $id);
        $comentarios = TicketAuditoria::selectRaw("'Sistema' AS usuarios_id, comentario, created_at AS fecha")->where('tickets_id', $id)->union($comentarios)->orderBy('fecha')->get();

        return DataTables::of($comentarios)->editColumn('fecha', function ($t) {
                        return @date("d-m-Y H:i", @strtotime($t->fecha));
                })->editColumn('usuarios_id', function ($t) {
                    if ($t->usuarios_id != 'Sistema') {
                        return @$t->usuario->nombre;
                    } else {
                        return 'Sistema';
                    }
                        
                })->make(true);
    }


}
