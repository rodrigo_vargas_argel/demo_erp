<?php

namespace App\Http\Controllers;

use App\factura_pago;
use DataTables;

use App\mantenedor_bodega;
use App\producto;
use App\Helpers\uf;
use App\Models\Plan;
use App\Models\Cliente;
use App\Models\Servicio;
use App\Models\LogEvento;
use App\mantenedor_site;   // Estaciones
use App\Models\TipoFactura;
use App\Models\TipoServicio;
use App\Models\GrupoServicio;
use App\Models\EstadoServicio;
use App\Models\ServicioEquipo;
use App\Models\ServicioInternet;
use App\Models\ArriendoEquiposDato;
use App\Models\PuertoPublico;
use App\Models\MantencionRed;
use App\Models\TraficoGenerado;
use App\Models\DireccionIPFija;

use App\User;
use App\Models\MotivoCorreo;
use  App\Mail\EmailServicio;
use App\Mail\EmailEstadoServicio;
use App\Rules\TipoServicioRule;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ServicioController extends Controller
{

	public function index(){
        $clientes = Cliente::orderBy('nombre')->get();
        $servicios = Servicio::with('clientes')->OrderByDesc('Id')->limit(25)->get();
        $tecnicos = User::select('id', 'nombre')->where('nivel', 3)->get();
        $estaciones = mantenedor_site::select('id', 'nombre')->get();
        $estados_activar = EstadoServicio::where('usar_en_pendiente', 1)->orderBy('nombre')->get();
        $estados = EstadoServicio::orderBy('nombre')->get();
        $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'cambio-estado')->first();
        $usuarios = collect([]);
        $productos=producto::query()->with('marca')->with('modelo')->get();
        if (isset($emilios->correo_usuarios))
            $usuarios = User::select('email')->whereIn('id', ($emilios->correo_usuarios->pluck('usuarios_id')->toArray()))->get();

        $pendientes = Servicio::where('EstatusServicio', 8)->orderBy('FechaComprometidaInstalacion')->get();
        return view('servicios.index', ['clientes'=>$clientes, 'servicios'=>$servicios, 'tecnicos'=>$tecnicos, 'estaciones'=>$estaciones, 'estados'=>$estados, 'estados_activar'=>$estados_activar, 'usuarios'=>$usuarios,'pendientes'=>$pendientes, 'productos'=>$productos]);
    }

    public function lista_filtro(Request $request){
        $clientes = Cliente::orderBy('nombre')->get();
        $servicios = Servicio::where('Rut', $request->cliente)->with('clientes')->OrderByDesc('Id')->get();
        $tecnicos = User::select('id', 'nombre')->where('nivel', 3)->get();
        $estaciones = mantenedor_site::select('id', 'nombre')->get();
        $estados_activar = EstadoServicio::where('usar_en_pendiente', 1)->orderBy('nombre')->get();
        $estados = EstadoServicio::orderBy('nombre')->get();
        $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'cambio-estado')->first();
        $usuarios = collect([]);
        $productos=producto::query()->with('marca')->with('modelo')->get();
        if (isset($emilios->correo_usuarios))
            $usuarios = User::select('email')->whereIn('id', ($emilios->correo_usuarios->pluck('usuarios_id')->toArray()))->get();
        $pendientes = Servicio::where('EstatusServicio', 8)->orderBy('FechaComprometidaInstalacion')->get();
        return view('servicios.index', ['clientes'=>$clientes,'servicios'=>$servicios, 'tecnicos'=>$tecnicos, 'estaciones'=>$estaciones, 'estados'=>$estados, 'estados_activar'=>$estados_activar, 'usuarios'=>$usuarios, 'pendientes'=>$pendientes,'productos'=>$productos]);
    }

    public function show(Servicio $servicio){

        $prods = "";  // mostrar los equipamientos guardados


        $equi = ServicioEquipo::with('productos')->where('servicios_id', $servicio->Id)->get();
        if ( (null != $equi) && ($equi->count() > 0) ) {
            foreach ($equi as $key => $value) {
                if ($prods == "") {
                    $prods .= @$value->productos->marca->nombre."/".@$value->productos->modelo->nombre;
                } else {
                    $prods .= ", ".@$value->productos->marca->nombre."/".@$value->productos->modelo->nombre;
                }
            }
        }

        if ($prods != "") $servicio->Equipamiento = $prods;
        $serv = Servicio::find($servicio->Id);
        $servicio->plancitoh = $serv->plan_nombre;
        $servicio->Direccion= @$servicio->clientes->direccion;
        $servicio->Ultima = @$servicio->clientes->ultima_factura;
        $servicio->Ultima2 = factura_pago::query()->where('FacturaId','=',@$servicio->clientes->ultima_factura->Id)->first();
        $servicio->Valor = @$servicio->Valor." UF";
        $servicio->EstatusServicio = @$servicio->estado_servicios->nombre;
        $servicio->TipoFactura = @$servicio->tipo_facturas->descripcion;
        $servicio->InstaladoPor = @$servicio->tecnico_instalador->nombre;
        $servicio->Latitud = @$servicio->Latitud. " & " .@$servicio->Longitud;
        $servicio->TecnicoAsignado=@$servicio->tecnico_asignado->nombre;
        if ($servicio->IdServicio == 3) {
            $servicio->tipo_moneda = @$servicio->puerto_publico->PuertoTCPUDP;    
        } elseif ($servicio->IdServicio == 4) {
            $servicio->tipo_moneda = @$servicio->ip_fija->DireccionIPFija;
        } elseif ($servicio->IdServicio == 5) {
            $servicio->tipo_moneda = @$servicio->mantencion_red->Descripcion;
        } elseif ($servicio->IdServicio == 6) {
            $servicio->tipo_moneda = @$servicio->trafico_generado->LineaTelefonica;
        }
        $servicio->IdServicio = @$servicio->tipo_servicios->servicio;
        
        return \Response::json($servicio);
    }

    public function create(){
        $clientes = Cliente::with('tipoClientes')->orderBy('nombre')->get();
        $select_clientes = $clientes->where('tipoClientes.id','!=', 3)->map(function($cliente){
                                return $cliente->only(['id', 'rut', 'nombre', 'telefono', 'contacto', 'tipo_cliente', 'tipoClientes']);
                            });

        $grupos = GrupoServicio::orderBy('Nombre')->get();
        $tipo_cobro = TipoFactura::orderBy('descripcion')->get();
        $estaciones = mantenedor_site::select('id', 'nombre')->get();
        $tipo_servicio = TipoServicio::orderBy('servicio')->get()->pluck('servicio','IdServicio')->all();
        $pendientes = Servicio::where('EstatusServicio', 8)->orderBy('FechaComprometidaInstalacion')->get();
        $tecnicos = User::select('id', 'nombre')->where('nivel', 3)->get();

        $estados = EstadoServicio::orderBy('nombre')->get();
        $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'cambio-estado')->first();

        $usuarios = collect([]);
        if (isset($emilios->correo_usuarios))
            $usuarios = User::select('email')->whereIn('id', ($emilios->correo_usuarios->pluck('usuarios_id')->toArray()))->get();


        return view('servicios.create', ['select_clientes'=>$select_clientes, 'grupos'=>$grupos, 'estados'=>$estados, 'tipo_cobro'=>$tipo_cobro, 'tipo_servicio'=>$tipo_servicio, 'estaciones'=>$estaciones, 'pendientes'=>$pendientes, 'tecnicos'=>$tecnicos, 'usuarios'=>$usuarios]);
    }

    public function store(Request $request){
        $request->validate([
            'Rut'=>'required',
            'Grupo'=>'required',
            'TipoFactura'=>'required',
            'IdServicio'=>'required',
            'Valor'=>'required|numeric',
            'IdServicio' => ['required', new TipoServicioRule($request->all())]
        ]);

        //'Rut', 'Grupo', 'TipoFactura', 'Valor', 'Descuento', 'IdServicio', 'Codigo', 'Descripcion', 'EstatusInstalacion', 'FechaInstalacion', 'InstaladoPor', 'Comentario', 'UsuarioPppoe', 'Conexion', 'IdUsuarioSession', 'Direccion', 'Latitud', 'Longitud', 'Referencia', 'Contacto', 'Fono', 'FechaComprometidaInstalacion', 'PosibleEstacion', 'Equipamiento', 'SenalTeorica','IdUsuarioAsignado','SenalFinal', 'EstacionFinal', 'EstatusFacturacion', 'CostoInstalacion', 'FacturarSinInstalacion', 'CostoInstalacionDescuento', 'UsuarioPppoeTeorico', 'FechaFacturacion', 'FechaInicioDesactivacion', 'FechaFinalDesactivacion', 'FechaUltimoCobro', 'NombreServicioExtra', 'EstatusServicio', 'tipo_moneda', 'create_at', 'update_at', 'CostoInstalacionBackup', 'descuentoTemporal'
        //dd($request->all());
        $dv = Cliente::where('Rut', $request->Rut)->first();
        $ultimo = Servicio::where('Rut', $request->Rut)->orderByDesc('Id')->first();
        $Correlativo = "01";
        $fechita = date('Y-m-d');
        if (isset($ultimo->Id)) {
            $Correlativo = intval(substr($ultimo->Codigo, -2));
            $Correlativo++;
            if ($Correlativo < 9){
                $Correlativo = "0".$Correlativo; 
            }
        }

        $codigo = TipoFactura::where('id', $request->TipoFactura)->first();
        $codigo = $codigo->codigo;
        $request->request->add(['Codigo' => $dv->rut."-".$dv->dv.$codigo.$Correlativo]);
        $request->request->add(['Equipamiento' => ""]);
        $request->request->add(['EstatusInstalacion' => 0]);
        $request->request->add(['EstatusFacturacion' => 0]);
        $request->request->add(['IdUsuarioSession' => Auth::user()->id]);
        $request->request->add(['CostoInstalacionDescuento' => 0]);
        $request->request->add(['descuentoTemporal' => 0]);
        $request->request->add(['FacturarSinInstalacion' => 0]);
        $request->request->add(['FechaFacturacion' => $fechita]);
        $request->request->add(['FechaUltimoCobro' => $fechita]);
        !isset($request->Descuento) ? $request->request->add(['Descuento' => 0]) : $codigo = $codigo;

        if ($request->TipoFactura == 25 || $request->TipoFactura == 26){
            //Servicio temporal mensual
            $request->request->add(['EstatusServicio' => 5]);
            if (isset($request->FechaInicioDesactivacion)){
                $request->merge(['FechaInicioDesactivacion' => $request->FechaInicioDesactivacionTMP]);
                $request->merge(['FechaFinalDesactivacion' => $request->FechaFinalDesactivacionTMP]);
            } else {
                $request->request->add(['FechaInicioDesactivacion' => $request->FechaInicioDesactivacionTMP]);
                $request->request->add(['FechaFinalDesactivacion' => $request->FechaFinalDesactivacionTMP]);
            }
        } else {
            //Servicio activo normal
            $request->request->add(['EstatusServicio' => 8]);
            $request->merge(['FechaInicioDesactivacion' => NULL]);
            $request->merge(['FechaFinalDesactivacion' => NULL]);
        }

        $model = Servicio::create($request->all());
        $servicios_id = $model->Id;

        if (($request->IdServicio == 1) || ($request->IdServicio == 2)) {
            if (isset($request->productos)){
                foreach ($request->productos as $key => $value) {
                    $serv_equipo = ServicioEquipo::create(['servicios_id'=>$servicios_id, 'producto_id'=>$value]);
                }
            }
        }

        $logg = LogEvento::create(['tabla'=>'servicios',                'id_tabla'=>$servicios_id,
                                    'usuarios_id'=>Auth::user()->id,    'tipo_evento'=>'crea servicio',
                                    'detalle'=>$model->Codigo]);

        switch ($request->IdServicio) {
            case 1:
                $plan = Plan::find($request->planes_id);
                ArriendoEquiposDato::create([
                    'IdServicio' => $servicios_id,
                    'planes_id' => $request->planes_id,
                    'Velocidad' => $plan->velocidad_subida."x".$plan->velocidad_bajada,
                    'Plan' => $plan->nombre,
                    'IdOrigen' => 0,
                    'IdProducto' => 0,
                    'TipoDestino' => 0
                ]);
                break;
            case 2:
                $plan = Plan::find($request->planes_id);
                ServicioInternet::create([
                    'IdServicio' => $servicios_id,
                    'planes_id' => $request->planes_id,
                    'Velocidad' => $plan->velocidad_subida."x".$plan->velocidad_bajada,
                    'Plan' => $plan->nombre,
                    'IdOrigen' => 0,
                    'IdProducto' => 0,
                    'TipoDestino' => 0
                ]);
                break;
            case 3:
                $model2 = new PuertoPublico;
                isset($request->descripcion) ? $model2->Descripcion = $request->descripcion : $model2->Descripcion = null;
                isset($request->puerto) ? $model2->PuertoTCPUDP = $request->puerto : $model2->PuertoTCPUDP = null;
                $model2->IdServicio = $servicios_id;
                $model2->save();
                break;
            case 4:
                $model2 = new DireccionIPFija;
                isset($request->descripcion) ? $model2->Descripcion = $request->descripcion : $model2->Descripcion = null;
                isset($request->ip) ? $model2->DireccionIPFija = $request->ip : $model2->DireccionIPFija = null;
                $model2->IdServicio = $servicios_id;
                $model2->save();
                break;
            case 5:
                $model2 = new MantencionRed;
                isset($request->descripcion) ? $model2->Descripcion = $request->descripcion : $model2->Descripcion = null;
                $model2->IdServicio = $servicios_id;
                $model2->save();
                break;
            case 6:
                $model2 = new TraficoGenerado;
                isset($request->descripcion) ? $model2->Descripcion = $request->descripcion : $model2->Descripcion = null;
                isset($request->linea) ? $model2->LineaTelefonica = $request->linea : $model2->LineaTelefonica = null;
                $model2->IdServicio = $servicios_id;
                $model2->save();
                break;
        }

        // si viene costo de instalación lo  grabo en notas de venta 
        $msje_nv = ""; $msge_email = "";
        if (( $request->BooleanCostoInstalacion == 1 ) && ( $request->CostoInstalacion > 0 )) {
                $ufi = new Uf;
                $uf = $ufi->getValue();
                $hoy = date('Y-m-d H:i:s');

                $valorsh = $request->CostoInstalacion;
                if ($request->tipo_moneda == 2) {
                    $valorsh = $request->CostoInstalacion * $uf;
                }
                $iva = $valorsh * 0.19;
                $bsale = Cliente::where('rut', $request->Rut)->first();

                DB::beginTransaction();
                try {
                    $id_nota = DB::table('nota_venta')->insertGetId(
                        ['rut' => $request->Rut, 'fecha' => $hoy, 'numero_oc' => null, 'fecha_oc' => null, 'solicitado_por' => Auth::user()->id, 'estatus_facturacion' => 0, 'factura_id' => null, 'numero_hes' => '0', 'fecha_hes' => null, 'tipo_pago_bsale'=>$bsale->tipo_pago_bsale_id,'neto'=>null,'iva'=>null,'total'=>null,'created_at'=>$hoy]
                    );
                    if ($id_nota) {
                            DB::table('nota_venta_detalle')->insertGetId(
                                ['nota_venta_id' => $id_nota, 'concepto' => $request->DescripcionNV, 'valor' => $valorsh, 'cantidad' => 1, 'precio' => $valorsh , 'fdescuento' => 0, 'descuento' => '0', 'subtotal' => $valorsh, 'ivva' => $iva, 'total' => ( $valorsh + $iva )]
                            );
                        DB::commit();
                    }
                } catch (\Throwable $e) {
                    DB::rollback();
                    $msje_nv = " No se pudo crear Nota de Ventas .";
                }
        }

        $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'crea-servicio')->first();

        if (isset($emilios->correo_usuarios)){
            $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();

            $cc = 'teledatadte@teledata.cl';
            if (isset($model->IdUsuarioAsignado) && ($model->IdUsuarioAsignado != "")) {
                $asignado = User::where('id', $model->IdUsuarioAsignado)->first();
                $cc = $asignado->email;
            }
            $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
            Mail::to($usuarios)
            ->cc($cc)
            ->send(new EmailServicio($model, 1));
            if (Mail::failures()) {
                $msge_email = " se creó el servicio, pero hubo error al enviar emails ... ";
            }

        }

        if (($msje_nv = "") && ($msge_email = "")) {
            \Session::flash('success', 'Servicio creado correctamente...');
            return redirect()->route('servicios.index');
        } else {
            \Session::flash('warning', $msge_email.$msje_nv);
            return redirect()->route('servicios.index');
        }
    }

     public function edit(Servicio $servicio){
        $cliente = Cliente::where('rut', $servicio->Rut)->first();

        $grupos = GrupoServicio::orderBy('Nombre')->get();
        $tipo_cobro = TipoFactura::orderBy('descripcion')->get();
        $estaciones = mantenedor_site::select('id', 'nombre')->get();
        $busca_estacion = $estaciones->where('nombre', $servicio->PosibleEstacion)->first();
        if (!isset($busca_estacion->nombre)) { // a veces las pinxes estaciones antiguas están escritas de forma diferente
            $modelin = new mantenedor_site;
            $modelin->id = $servicio->PosibleEstacion;
            $modelin->nombre = $servicio->PosibleEstacion;
            $estaciones->push($modelin);
        }
        $tipo_servicio = TipoServicio::orderBy('servicio')->get()->pluck('servicio','IdServicio')->all();
        $tecnicos = User::select('id', 'nombre')->where('nivel', 3)->get();
        $busca_tecnico = $tecnicos->where('id', $servicio->IdUsuarioAsignado)->first();
        if (!isset($busca_tecnico->nombre)) {// a veces asignaron instaladores a otros roles
            $luser = User::find($servicio->IdUsuarioAsignado);
            $modelin = new User;
            $modelin->id = $servicio->IdUsuarioAsignado;
            $modelin->nombre = @$luser->nombre;
            $tecnicos->push($modelin);
        }
        
        $planes = Plan::orderBy('nombre')->get();
        if ($servicio->plan_id == 0) {
            $modelin = new Plan;
            $modelin->id = 0;
            $modelin->nombre = @$servicio->plan_nombre;
            $planes->push($modelin);
        }

        $productos = producto::with('tipo_producto', 'marca', 'modelo')->get();
        $productos = $productos->sortBy('modelo.nombre')->sortBy('marca.nombre')->all();
        $prods = collect([]);        
        $equipitos = [];
        $equi = ServicioEquipo::where('servicios_id', $servicio->Id)->get();
        if ( (null != $equi) && ($equi->count() > 0) ) {
            foreach ($equi as $key => $value) {
                $equipitos[] =  $value->producto_id;
            }
        }
        $token = false;
        foreach ($productos  as $key => $value) {
            if (in_array($value->id, $equipitos)) {
                $prods->push(['id'=>$value->id, 'nombre'=>@$value->marca->nombre." ".@$value->modelo->nombre , 'seelect' => "selected" ]);
                $token = true;
            } else {
                $prods->push(['id'=>$value->id, 'nombre'=>@$value->marca->nombre." ".@$value->modelo->nombre , 'seelect' => "" ]);
            }
        }
        if (!$token && (($servicio->Equipamiento != NULL ) || ($servicio->Equipamiento != "" ))) {
            $prods->push(['id'=>$servicio->Equipamiento, 'nombre'=>$servicio->Equipamiento , 'seelect' => "selected" ]);
        }
        $servicio = Servicio::with('tipo_servicios')->where('Id', $servicio->Id)->first();

        return view('servicios.edit', ['cliente'=>$cliente, 'servicio'=>$servicio, 'grupos'=>$grupos, 'tipo_cobro'=>$tipo_cobro, 'tipo_servicio'=>$tipo_servicio, 'estaciones'=>$estaciones, 'tecnicos'=>$tecnicos, 'planes'=>$planes, 'productos'=>$prods]);

    }


    public function update(Request $request, Servicio $servicio)
    {
        $msge = array();
        $msge['estado'] = "NOP";
        $old_estado = $servicio->EstatusServicio;
        $estado_previo = @$servicio->estado_servicios->nombre;

        if ( ($request->has('IdUsuarioAsignado')) && ($request->missing('ModificarServ'))) {
           // \DB::connection()->enableQueryLog();
            $servicio->IdUsuarioAsignado = $request->IdUsuarioAsignado;
            $servicio->save();
            //$queries = \DB::getQueryLog();
           // dd($queries);
            $msge['estado'] = "OK";
            $msge['cliente'] = @$servicio->tecnico_asignado->nombre;
            if (isset($servicio->tecnico_asignado->email ) && ($servicio->tecnico_asignado->email != "") ) {
                if ($servicio->IdServicio == 3) {
                    $servicio = Servicio::with('puerto_publico')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 4) {
                    $servicio = Servicio::with('ip_fija')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 5) {
                    $servicio = Servicio::with('mantencion_red')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 6) {
                    $servicio = Servicio::with('trafico_generado')->find($servicio->Id);
                }
                Mail::to($servicio->tecnico_asignado->email)
                //->cc('teledatadte@teledata.cl')
                ->send(new EmailServicio($servicio, 2 ));

                if (Mail::failures()) {
                    $msge['email']  = " No se pudo enviar e-mail al técnico asignado";
                } else {
                    $msge['email']  = " ( ".$servicio->tecnico_asignado->email. " )";
                }
            }
        } elseif ($request->has('cambioEstado')) {

            $servicio->EstatusServicio = $request->cambioEstado;
            if ($request->fechaIniDes != "") $request->merge(['fechaIniDes' => $request->fechaIniDes]);
            if ($request->fechaFinDes != "") $request->merge(['fechaFinDes' => $request->fechaFinDes]);
            $servicio->FechaInicioDesactivacion = $request->fechaIniDes;
            $servicio->FechaFinalDesactivacion = $request->fechaFinDes;
            $servicio->save();

            if ($old_estado != $servicio->EstatusServicio) {
                $nuevo_estado = EstadoServicio::find($servicio->EstatusServicio);
                $logg = LogEvento::create(['tabla'=>'servicios',                'id_tabla'=>$servicio->Id,
                                            'usuarios_id'=>Auth::user()->id,    'tipo_evento'=>'modifica servicio',
                                            'detalle'=>" Código Servicio: ".$servicio->Codigo." estado previo = ".$estado_previo." nuevo estado = ". @$nuevo_estado->nombre]);


                $msge['estado'] = "OK";
                $msge['email'] = "ninguno";
                if (isset($request->correo ) && ($request->correo == "1")) {

                    $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'cambio-estado')->first();
                    if (isset($emilios->correo_usuarios)){
                        $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
                        $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
                        $servicio = Servicio::find($servicio->Id);

                        Mail::to($usuarios)
                           // ->cc('teledatadte@teledata.cl')
                        ->send(new EmailEstadoServicio($servicio, 1 ));

                        if (Mail::failures()) {
                            $msge['email']  = " No se pudo enviar e-mail ";
                        } else {
                            $msge['email']  = " ( ".$usuarios->implode('email', ', '). " )";
                        }
                    }
                }
            }
        } elseif (($request->has('actualizaFecha')) || ($request->has('ModificarServ'))) {

            if ( $request->has('ModificarServ') ){
                $request->validate([
                    'Grupo'=>'required',
                    'TipoFactura'=>'required',
                    'IdServicio'=>'required',
                    'Valor'=>'required|numeric',
                    'IdServicio' => ['required', new TipoServicioRule($request->all())]
                ]);
            } else {
                $request->validate([
                    'FechaInstalacion'=>'date',
                    'InstaladoPor'=>'required',
                    'EstatusServicio'=>'required'
                ]);
            }
            $servicio->fill($request->all());
            if ($request->actualizaFecha == 1)
                $servicio->FechaUltimoCobro = $request->FechaInstalacion;
            $servicio->save();

            if ($old_estado != $servicio->EstatusServicio) {  //LOG DE CAMBIO DE EVENTOS
                $nuevo_estado = EstadoServicio::find($servicio->EstatusServicio);
                $logg = LogEvento::create(['tabla'=>'servicios',                'id_tabla'=>$servicio->Id,
                                            'usuarios_id'=>Auth::user()->id,    'tipo_evento'=>'modifica servicio',
                                            'detalle'=>" Código Servicio: ".$servicio->Codigo." estado previo = ".$estado_previo." nuevo estado = ". @$nuevo_estado->nombre]);
            }

            if ((($servicio->IdServicio == 1) || ($servicio->IdServicio == 2)) && ($request->has('ModificarServ'))) {
                if (isset($request->productos)){
                    $serv_equipo = ServicioEquipo::where('servicios_id', $servicio->Id)->delete();
                    $token = false; $equip0 = "";
                    foreach ($request->productos as $key => $value) {
                        if (is_numeric($value)) {
                            $serv_equipo = ServicioEquipo::create(['servicios_id'=>$servicio->Id, 'producto_id'=>$value]);
                            $token = true;
                        } else {
                            $equip0 .= $value." ";
                        }
                    }
                    if (!$token) {
                        $servicio->Equipamiento = $equip0;
                        $servicio->save();
                    }
                }
                if ($request->planes_id != 0) {
                    $el_plan = Plan::find($request->planes_id);
                    $model = new Plan;
                    if ($servicio->IdServicio == 1){
                        $model = ArriendoEquiposDato::where('IdServicio', $servicio->Id)->first();
                        if (!isset($model->IdServicio))
                            $model = new ArriendoEquiposDato;
                    } elseif ($servicio->IdServicio == 2){
                        $model = ServicioInternet::where('IdServicio', $servicio->Id)->first();
                        if (!isset($model->IdServicio))
                            $model = new ServicioInternet;
                    }
                    $model->IdServicio = $servicio->Id;
                    $model->planes_id  = $request->planes_id;
                    $model->Velocidad  = $el_plan->velocidad_subida."x".$el_plan->velocidad_bajada;
                    $model->Plan       = $el_plan->nombre;
                    $model->IdOrigen = 0; $model->IdProducto = 0; $model->TipoDestino = 0;
                    $model->save();
                }

            } elseif ($servicio->IdServicio == 3) {
                $model = PuertoPublico::where('IdServicio', $servicio->Id)->first();
                if (!isset($model->IdServicio)) {
                    $model = new PuertoPublico;
                    $model->IdServicio = $servicio->Id;
                }
                $model->PuertoTCPUDP = $request->puerto;
                $model->Descripcion = $request->descripcion;
                $model->save();
            } elseif ($servicio->IdServicio == 4) {
                $model = DireccionIPFija::where('IdServicio', $servicio->Id)->first();
                if (!isset($model->IdServicio)) {
                    $model = new DireccionIPFija;
                    $model->IdServicio = $servicio->Id;
                }
                $model->DireccionIPFija = $request->ip;
                $model->Descripcion = $request->descripcion;
                $model->save();
            } elseif ($servicio->IdServicio == 5) {
                $model = MantencionRed::where('IdServicio', $servicio->Id)->first();
                if (!isset($model->IdServicio)) {
                    $model = new MantencionRed;
                    $model->IdServicio = $servicio->Id;
                }
                $model->Descripcion = $request->descripcion;
                $model->save();
            } elseif ($servicio->IdServicio == 6) {
                $model = TraficoGenerado::where('IdServicio', $servicio->Id)->first();
                if (!isset($model->IdServicio)) {
                    $model = new TraficoGenerado;
                    $model->IdServicio = $servicio->Id;
                }
                $model->LineaTelefonica = $request->linea;
                $model->Descripcion = $request->descripcion;
                $model->save();
            }

            $msge['estado'] = "OK";
            $msge['codigo'] = $servicio->Codigo;
            $msge['email']  = " NO SE ENVIÓ CORREO, YA QUE SIGUE PENDIENTE DE INSTALACIÓN";

            if ($servicio->EstatusServicio != 8) {
                if ($servicio->IdServicio == 3) {
                    $servicio = Servicio::with('puerto_publico')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 4) {
                    $servicio = Servicio::with('ip_fija')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 5) {
                    $servicio = Servicio::with('mantencion_red')->find($servicio->Id);
                } elseif ($servicio->IdServicio == 6) {
                    $servicio = Servicio::with('trafico_generado')->find($servicio->Id);
                }
                $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'activa-servicio')->first();
                $msge['email']  = 'ninguno';
                if (isset($emilios->correo_usuarios)){
                    $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
                    $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
                    
                    $tipoo = 3;
                    if ($request->has('ModificarServ'))
                        $tipoo = 4;

                    Mail::to($usuarios)
                    //->cc('teledatadte@teledata.cl')
                    ->send(new EmailServicio($servicio, $tipoo));

                    if (Mail::failures()) {
                        $msge['email']  = " No se pudo enviar e-mail a usuarios ";
                    } else {
                        $msge['email']  = " ( ".$usuarios->implode('email', ', '). " )";
                    }
                }
            }
        }
        if ($request->has('ModificarServ')) {
            \Session::flash('success', 'Servicio modificado correctamente...');
            return redirect()->route('servicios.index');
        } else {
            return json_encode($msge);
        }
    }


    public function selectTipoCobro($tipo)
    {
        $tipos = TipoFactura::with('tipos_facturacion')->where('tipo_documento', $tipo)->get();
        $list = '<option value=""> No se encontró tipo doc cliente ... </option>';
        if ($tipos->count() > 0) {
            $list = '<option value=""> Seleccione ... </option>';
            foreach ($tipos as $row) {
                $list .= '<option value="'.$row->id.'">'.$row->codigo.' - '.$row->descripcion.' - '.$row->tipos_facturacion->nombre.'</option>';
            }
        }
        return ($list);
    }


    public function tipoServicio(Request $request)
    {
        $equipitos = [];
        if ( isset($request->prods) && ($request->prods != null))
            $equipitos = explode(',', $request->prods);

        $productos = producto::with('tipo_producto', 'marca', 'modelo')->get();
        $bodegas = mantenedor_bodega::where('principal', 0)->get();
        $planes = Plan::orderBy('nombre')->get();
        $productos = $productos->sortBy('modelo.nombre')->sortBy('marca.nombre')->all();
        $prods = collect([]);
        
        foreach ($productos  as $key => $value) {
            if (in_array($value->id, $equipitos)) {
                $prods->push(['id'=>$value->id, 'nombre'=>@$value->marca->nombre." ".@$value->modelo->nombre , 'seelect' => "selected" ]);
            } else {
                $prods->push(['id'=>$value->id, 'nombre'=>@$value->marca->nombre." ".@$value->modelo->nombre , 'seelect' => "" ]);
            }
        }
        if (\Request::ajax()) {
            return \Response::json(\View::make('clientesServicios.'.$request->tipo,['planes' => $planes,'bodegas' => $bodegas,'productos' => $prods])->render());
        }
    }

    public function usuarioPppoe(Request $request)
    {
        $user = $request->ppoe;
        $ppoe = Servicio::where('UsuarioPppoeTeorico', $user)->first();
        if (isset($ppoe->Id)) {
            return "true";
        } else {
            return "false";
        }
    }


}