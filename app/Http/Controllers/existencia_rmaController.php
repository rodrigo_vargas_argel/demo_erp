<?php

namespace App\Http\Controllers;

use App\existencia;
use App\existencia_rma;
use App\mantenedor_proveedor;
use App\producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection as RV_coleccion;
use Illuminate\Support\Facades\DB;

class existencia_rmaController extends Controller
{
    public function show(){

        $consulta=existencia_rma::with('producto')->with('proveedor')->with('usuario')->with('usuario2')->orderBy('id','desc')->get();
        $proveedores=mantenedor_proveedor::all();
        if($consulta->count() > 0 ) {
            for ($i = 0; $i < $consulta->count(); $i++) {
                $usuario=collect($consulta[$i]->usuario);
                $usuario2=collect($consulta[$i]->usuario2);
                $proveedor=collect($consulta[$i]->proveedor);
                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $registro = RV_coleccion::make($consulta,$usuario,$usuario2,$proveedor, $producto, $tipo, $marca, $modelo);
            }
        }
        else{
            $registro=RV_coleccion::make(); //una coleccion vacia para cuando no hay registros
        }

        return view('inventarios.rma',['consulta'=>$registro,'proveedores'=>$proveedores]);

    }

    public function guardar(Request $request){
        $hoy = date('Y-m-d H:i:s');
        $request->validate([
            'codigo_producto'=>'required',
            'mac_serie'=>'required',
            'numero_rma'=>'required|numeric',
            'observaciones'=>'required',

        ]);

        $existe_producto=producto::query()->where('codigo_barra','=',$request->codigo_producto)->first();
        if ($existe_producto){
            $existe_stock=existencia::query()->where('bodega_id','=',10)->where('producto_id','=', $existe_producto->id)->where('mac_serie','=',$request->mac_serie)->first();

            if($existe_stock){
                $error=false;
                $msg= " ";
                $cantidad_origen=$existe_stock->cantidad;

                    $sql=new existencia_rma();
                    $sql->proveedor_id=$request->proveedor;
                    $sql->num_rma=$request->numero_rma;
                    $sql->producto_id= $existe_producto->id;
                    $sql->mac_serie=$request->mac_serie;
                    $sql->fecha_salida=$hoy;
                    $sql->obs_salida=$request->observaciones;
                    $sql->usuario_salida=Auth::user()->id;
                    $sql->estado=0;

                    if( $sql->save())     {

                        DB::beginTransaction();

                        try {
                            //********primero la salida de bodega_origen **************
                            $id_mov1 = DB::table('existencias_movimientos')->insertGetId(
                                ['fecha' => $hoy, 'e_s' => 's','producto_id' => $existe_producto->id, 'mac_serie' =>  $request->mac_serie,'cantidad' =>  '1', 'bodega_origen_id' => 0,'bodega_destino_id' => '10', 'num_doc' => $sql->id, 'tipo_doc' =>'RMA' , 'tipo_movimiento' => 'RMA','responsable' => Auth::user()->id,'obs' => $request->observaciones]
                            );
                            //***** mov. entrada bodega destino  id 14 = bodega RMA
                            $id_mov = DB::table('existencias_movimientos')->insertGetId(
                                ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $existe_producto->id, 'mac_serie' =>  $request->mac_serie,'cantidad' =>  '1', 'bodega_origen_id' => 0,'bodega_destino_id' => '14', 'num_doc' => $sql->id, 'tipo_doc' =>'RMA' , 'tipo_movimiento' => 'RMA','responsable' => Auth::user()->id,'obs' => $request->observaciones]
                            );


                                if ($request->mac_serie != '0') {
                                    $stock = existencia::query()->where('producto_id', '=', $existe_producto->id)->where('mac_serie', '=', $request->mac_serie)->first();
                                    $rvsql = existencia::find($stock->id);
                                    $rvsql->bodega_id = '14';
                                    $rvsql->cantidad_old ='10';
                                    $rvsql->movimiento_id = $id_mov;
                                    $rvsql->save();
                                }
                                else{
                                    //*************** primero hago la salida de stock
                                    $stock=existencia::query()->where('producto_id','=',$existe_producto->id)->where('bodega_id','=', '10')->first();
                                    if ($stock)
                                    {
                                        $nueva_cantidad=$stock->cantidad - 1;

                                        DB::table('existencias')
                                            ->where('id','=',$stock->id)
                                            ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov1]);
                                    }
                                    else{
                                        DB::table('existencias')->insertGetId(
                                            ['bodega_id' => '10', 'producto_id' => $existe_producto->id, 'mac_serie' => 0,'cantidad' =>  '0',  'cantidad_old' => 0,'movimiento_id' => $id_mov1]
                                        );
                                    }

                                    //**********ahora inserto el la entrada stock
                                    $stock=existencia::query()->where('producto_id','=',$existe_producto->id)->where('bodega_id','=', '14')->first();
                                    if ($stock)
                                    {
                                        $nueva_cantidad=$stock->cantidad + 1;

                                        DB::table('existencias')
                                            ->where('id','=',$stock->id)
                                            ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov]);
                                    }
                                    else{
                                        DB::table('existencias')->insertGetId(
                                            ['bodega_id' => '14', 'producto_id' => $existe_producto->id, 'mac_serie' => 0,'cantidad' =>  '1',  'cantidad_old' => 0,'movimiento_id' => $id_mov]
                                        );
                                    }
                                }

                            DB::commit();

                        }

                        catch (\Throwable $e) {
                            DB::rollback();
                            //throw $e;
                            return response()
                                ->json(["error"=>true, 'msg'=>$e, 'registro'=>""]);
                        }




                        $error=false;
                        $msg='el RMA ha sido ingresado con exito';
                        $registro="";


                        //  $mac=collect($consulta[$i]->producto->detalle_mac);
                        //$registro = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo);
                    }

                    else{
                        $error=true;
                        $msg="Hubo un Error de Conexion, Intente Iniciar Sesion Nuevamente ";
                        $registro="";
                    }


            }
            else{
                $error=true;
                $msg="El Codigo Ingresado o la Mac no Existe en la Bodega de Origen";
                $registro="";
            }

        }
        else{
            $error=true;
            $msg="El Codigo Ingresado no Existe en la Base de Datos";
            $registro="";
        }

        return response()
            ->json(["error"=>$error, 'msg'=>$msg, 'registro'=>$registro]);

    }

    public function actualizar(Request $request){
        $hoy = date('Y-m-d H:i:s');
        $request->validate([
            'rma_id'=>'required',
            'observaciones2'=>'required',

        ]);

        $rma_id=$request->rma_id;

        $sql=existencia_rma::find($rma_id);
        $sql->fecha_llegada=$hoy;
        $sql->obs_llegada=$request->observaciones2;
        $sql->usuario_llegada=Auth::user()->id;
        $sql->estado=$request->estado;

       if($sql->save()){

           DB::beginTransaction();



           try {

               if ($request->estado == 2){

                   //********primero la salida de bodega_origen **************
                   $id_mov1 = DB::table('existencias_movimientos')->insertGetId(
                       ['fecha' => $hoy, 'e_s' => 's','producto_id' => $sql->producto_id, 'mac_serie' =>  $sql->mac_serie,'cantidad' =>  '1', 'bodega_origen_id' => 0,'bodega_destino_id' => '14', 'num_doc' => $sql->id, 'tipo_doc' =>'RMA' , 'tipo_movimiento' => 'RMA NO RETORNADO','responsable' => Auth::user()->id,'obs' => $request->observaciones2]
                   );

               }
               else{

                   //********primero la salida de bodega_origen **************
                   $id_mov1 = DB::table('existencias_movimientos')->insertGetId(
                       ['fecha' => $hoy, 'e_s' => 's','producto_id' => $sql->producto_id, 'mac_serie' =>  $sql->mac_serie,'cantidad' =>  '1', 'bodega_origen_id' => 0,'bodega_destino_id' => '14', 'num_doc' => $sql->id, 'tipo_doc' =>'RMA' , 'tipo_movimiento' => 'RMA','responsable' => Auth::user()->id,'obs' => $request->observaciones2]
                   );
                   //***** mov. entrada bodega destino  id 14 = bodega RMA
                   $id_mov = DB::table('existencias_movimientos')->insertGetId(
                       ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $sql->producto_id, 'mac_serie' =>  $sql->mac_serie,'cantidad' =>  '1', 'bodega_origen_id' => 0,'bodega_destino_id' => '10', 'num_doc' => $sql->id, 'tipo_doc' =>'RMA' , 'tipo_movimiento' => 'RMA','responsable' => Auth::user()->id,'obs' => $request->observaciones2]
                   );

               }


                //*** ahora si es ún porducto con mac ire directo a la tabla stock a modificar el registro
               if ($request->mac_serie != '0') {
                   $stock = existencia::query()->where('producto_id', '=', $sql->producto_id)->where('mac_serie', '=', $sql->mac_serie)->first();
                   //*****si el producto es no retronado x el proveedor (dado de baja), eliminanos el regisdtro de stock
                   if ($request->estado == 2){
                       $rvsql = existencia::find($stock->id);
                       $rvsql->deleted_at=$hoy;
                       $rvsql->save();
                   }else{
                       //***** sino solo lo modificamos y  lo dejo disponible en bodega central
                       $rvsql = existencia::find($stock->id);
                       $rvsql->bodega_id = '10';
                       $rvsql->cantidad_old ='14';
                       $rvsql->movimiento_id = $id_mov;
                       $rvsql->save();
                   }

               }
               else{
                   //*************** primero hago la salida de stock
                   $stock=existencia::query()->where('producto_id','=',$sql->producto_id)->where('bodega_id','=', '14')->first();
                   if ($stock)
                   {
                       $nueva_cantidad=$stock->cantidad - 1;

                       DB::table('existencias')
                           ->where('id','=',$stock->id)
                           ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov1]);
                   }
                   else{
                       DB::table('existencias')->insertGetId(
                           ['bodega_id' => '10', 'producto_id' => $sql->producto_id, 'mac_serie' => 0,'cantidad' =>  '0',  'cantidad_old' => 0,'movimiento_id' => $id_mov1]
                       );
                   }

                   if ($request->estado == 1) {
                       //**********ahora inserto el la entrada stock
                       $stock = existencia::query()->where('producto_id', '=', $sql->producto_id)->where('bodega_id', '=', '10')->first();
                       if ($stock) {
                           $nueva_cantidad = $stock->cantidad + 1;

                           DB::table('existencias')
                               ->where('id', '=', $stock->id)
                               ->update(['cantidad' => $nueva_cantidad, 'cantidad_old' => $stock->cantidad, 'movimiento_id' => $id_mov1]);
                       } else {
                           DB::table('existencias')->insertGetId(
                               ['bodega_id' => '10', 'producto_id' => $sql->producto_id, 'mac_serie' => 0, 'cantidad' => '1', 'cantidad_old' => 0, 'movimiento_id' => $id_mov1]
                           );
                       }
                   }
               }

               DB::commit();

           }

           catch (\Throwable $e) {
               DB::rollback();

               throw $e;
               dd($e);
               return response()
                   ->json(["error"=>true, 'msg'=>$e, 'registro'=>""]);
           }

           $error=false;
           $msg="El RMA ha sido actualizado con exito";
           $registro="";
       }
       else{
           $error=true;
           $msg="No se ha podido actualizar el RMA por problemas en la Base de datos, intente nuevamente";
           $registro="";
       }
        return response()
            ->json(["error"=>$error, 'msg'=>$msg, 'registro'=>$registro]);



    }
}
