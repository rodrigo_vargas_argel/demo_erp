<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Region;
use App\Models\Ciudad;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\clase_cliente;
use App\mantenedor_tipo_cliente;
use App\Models\MantenedorTipoPagoBsale;
use App\Models\MantenedorTipoContacto;
use App\Models\EstadoServicio;
use App\Models\MotivoCorreo;
use App\User;
use App\Models\Contacto;
use App\Models\Giro;


use App\methods;                    //eliminar esto

//header('Content-type: application/json');

class ClienteController extends Controller
{
    /*** Display a listing of the resource.
     *
     *  @return \Illuminate\Http\Response
     */
    public function index(){
        $clientes = Cliente::orderBy('Id','desc')->get();
        $tipo_contactos = MantenedorTipoContacto::orderBy('nombre')->get();

        $estados = EstadoServicio::orderBy('nombre')->get();
        $emilios = MotivoCorreo::with('correo_usuarios')->with('correo_usuarios.usuarios')->where('nombre', 'cambio-estado')->first();
        $usuarios = collect([]);
        if (isset($emilios->correo_usuarios))
            $usuarios = User::select('email')->whereIn('id', ($emilios->correo_usuarios->pluck('usuarios_id')->toArray()))->get();

        return view('clientes.index',['clientes'=>$clientes, 'estados'=>$estados, 'usuarios'=>$usuarios, 'tipo_contactos'=>$tipo_contactos]);
    }

    public function getClientes()
    {
        $clientes = DB::table('personaempresa')->select('id', DB::raw("CONCAT(rut, '-', dv) AS rut"), 'nombre', 'correo', 'telefono',DB::raw('(SELECT COUNT(s.id) FROM servicios s WHERE s.Rut = personaempresa.Rut) AS servicios'))->get();
        return DataTables::of($clientes)->editColumn('servicios', function ($clientes) {
                return $clientes->servicios.' <i class="fa fa-eye " attr="'.$clientes->id.'" data-nombre="'.$clientes->nombre.'" aria-hidden="true" title="Ver Obras"></i> ';
                        
            })
            ->addColumn('action', function ($clientes) { //<i class="glyphicon glyphicon-zoom-in"><i class="glyphicon glyphicon-edit">

                return ' <a class="fa fa-pencil-square-o" attr="'.$clientes->id.'" href="'.route('clientes.edit',$clientes->id).'" aria-hidden="true" title="Editar"></a>
                        <a class="fa fa-eye view-cliente" id-cli="'.$clientes->id.'" cliente="'.$clientes->nombre.'" rut="'.$clientes->rut.'" aria-hidden="true" title="Visualizar"></a>
                        <a class="fa fa-phone abrir-modal-contactos" id="'.$clientes->id.'" rut="'.$clientes->rut.'" nombre="'.$clientes->nombre.'" aria-hidden="true" title="Contactos"></a>
                        ';

                        
            })
            ->rawColumns(['servicios', 'action'])
            ->make(true);
    }


    public function create(){
        $tipos_cliente = mantenedor_tipo_cliente::orderBy('nombre')->get();
        $tipo_contactos = MantenedorTipoContacto::orderBy('nombre')->get();
        $tipos_pago = MantenedorTipoPagoBsale::where('activo', 1)->orderBy('nombre')->get();
        $clase_cliente = clase_cliente::orderBy('nombre')->get();
        $giros = Giro::orderBy('nombre')->get();
        $regiones = Region::orderBy('nombre')->get();        
        //region y ciudades los haré por peticiones ajax a la accion clientes.region y cliente.ciudades
        return view('clientes.create', ['tipos_cliente'=>$tipos_cliente, 'tipos_pago'=>$tipos_pago, 'clase_cliente'=>$clase_cliente, 'giros'=>$giros, 'regiones'=>$regiones, 'tipo_contactos'=>$tipo_contactos]);
    }

    public function store(Request $request){
        $request->validate([
            'tipo_cliente'=>'required',
            'tipo_pago_bsale_id'=>'required',
            'rut'=>'required',
            'dv'=>'required',
            'clase_cliente'=>'required',
            'nombre'=>'required',
            'direccion'=>'required',
            'giro'=>'required',
            'region'=>'required',
            'ciudad'=>'required',
            'contacto'=>'required',
            'telefono'=>'required',
            'correo'=>'required|email',
        ]);

        $cliente = Cliente::create($request->all());

        return redirect()->route('clientes.index')->with('success', 'Cliente creado correctamente.');
    }

    public function edit(Cliente $cliente){
        $tipos_cliente = mantenedor_tipo_cliente::orderBy('nombre')->get();
        $tipo_contactos = MantenedorTipoContacto::orderBy('nombre')->get();
        $tipos_pago = MantenedorTipoPagoBsale::where('activo', 1)->orderBy('nombre')->get();
        $clase_cliente = clase_cliente::orderBy('nombre')->get();
        $giros = Giro::orderBy('nombre')->get();
        $regiones = Region::orderBy('nombre')->get();
        $ciudades = Ciudad::orderBy('nombre')->get();
        $contactos = Contacto::where('rut', $cliente->rut)->orderBy('contacto')->get();

        return view('clientes.edit', ['cliente'=>$cliente, 'tipos_cliente'=>$tipos_cliente, 'tipos_pago'=>$tipos_pago, 'tipo_contactos'=>$tipo_contactos, 'clase_cliente'=>$clase_cliente, 'giros'=>$giros, 'regiones'=>$regiones, 'ciudades'=>$ciudades, 'contactos'=>$contactos]);
    }

    public function update(Request $request, Cliente $cliente)
    {
        $request->validate([
            'tipo_cliente'=>'required',
            'tipo_pago_bsale_id'=>'required',
            'rut'=>'required',
            'dv'=>'required',
            'clase_cliente'=>'required',
            'nombre'=>'required',
            'direccion'=>'required',
            'giro'=>'required',
            'region'=>'required',
            'ciudad'=>'required',
            'telefono'=>'required',
            'correo'=>'required|email',
        ]);

        $cliente->fill($request->all())->save();

        return redirect()->route('clientes.index')->with('success', 'Cliente modificado correctamente.');

    }

    public function getCiudades(Region $region)
    {
        $provincias = $region->provincias->pluck('id')->toArray();
        $ciudades = Ciudad::select('id', 'nombre')->whereIn('provincia_id', $provincias)->orderBy('nombre')->get();
        //si quiere solo las ciudades (sin option) puede devolver  array con las 2 opciones
        $list ='<option value="">Seleccione....</option>';

        if ($ciudades->count() > 0) {
            foreach ($ciudades as $row) {
                $list.= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
            }
        }

        return ($list);
    }

    public function getGiros()
    {
        $giros = Giro::orderBy('nombre')->get();
        //si quiere solo los giros (sin option) puede devolver  array con las 2 opciones
        $list ='<option value="">--- Seleccione ---</option>';

        if ($giros->count() > 0) {
            foreach ($giros as $row) {
                $list.= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
            }
        }

        return ($list);
    }

    public function buscaCliente($rut)  // devuelve el id del cliente o el dv (si no existe)
    {
        $retorno = array();
        $rut = str_replace(".", "", str_replace(",", "", str_replace("-", "", $rut)));
        $cliente = Cliente::where('rut', $rut)->first();

        if (isset($cliente->dv)){
            array_push($retorno, ["msge" => "si", "id" => $cliente->id, "rut" => $rut]);
        } else {
            while($rut[0] == "0") {
                $rut = substr($rut, 1);
            }
            $factor = 2;
            $suma = 0;
            for($i = strlen($rut) - 1; $i >= 0; $i--) {
                $suma += $factor * $rut[$i];
                $factor = $factor % 7 == 0 ? 2 : $factor + 1;
            }
            $dv = 11 - $suma % 11;
            /* Por alguna razón me daba que 11 % 11 = 11. Esto lo resuelve. */
            $dv = $dv == 11 ? 0 : ($dv == 10 ? "K" : $dv);

            array_push($retorno, ["msge" => "no", "dv" => $dv, "rut" => $rut]);
        }

        return response()->json($retorno);
    }

    public function show(Cliente $cliente)
    {

        $contactos = Contacto::where('rut', $cliente->rut)->orderBy('contacto')->get();
        $cliente->push((['tipoClientes'=>$cliente->tipoClientes->nombre,'tipoPagos'=>$cliente->tipoPagos->nombre,'claseClientes'=>$cliente->claseClientes->nombre,'regiones'=>$cliente->regiones->nombre,'ciudades'=>$cliente->ciudades->nombre ]));
        return response()->json($cliente);
        //return view('clientes.show', ['cliente'=>$cliente, 'contactos'=>$contactos]);
    }

    public function resumen(Cliente $cliente){
        $vencidos =  $this->getDocsVencidos($cliente->rut);
        $activos = $this->getServiciosActivos($cliente->rut);
        $suspendidos = $this->getServiciosActivos($cliente->rut, 2);
        $cortados = $this->getServiciosActivos($cliente->rut, 3);
        $cambiados = $this->getServiciosActivos($cliente->rut, 4);
        $temporales = $this->getServiciosActivos($cliente->rut, 5);
        $finados = $this->getServiciosActivos($cliente->rut, 0);
        $saldo = $this->getDocsVencidos($cliente->rut, "2035-12-30");
        return view('clientes.resumen', ['cliente'=>$cliente, 'vencidos'=>$vencidos, 'activos'=>$activos, 'suspendidos'=>$suspendidos, 'cortados'=>$cortados, 'cambiados'=>$cambiados, 'temporales'=>$temporales, 'finados'=>$finados, 'saldo'=>$saldo, 'id_cliente'=>$cliente->id]);
    }


    //obtiene servicio activos - suspendidos - cortados
    public function getServiciosActivos($Rut, $estado=1) {
        $data = array();
        $total_data = array();
        $servicio['Tipo'] = 'Sin data';
        $data['error'] = '';
        if(isset($Rut) && $Rut != '') {
            $query_servicios = "SELECT
            Id,
            Grupo,
            Valor,
            Codigo,
            FechaInstalacion,
            EstatusServicio,
            Conexion,
            mantenedor_servicios.servicio as Tipo
            FROM servicios
            INNER JOIN mantenedor_servicios ON servicios.IdServicio = mantenedor_servicios.IdServicio 
            WHERE Rut = $Rut AND EstatusServicio = $estado";
            $servicios = DB::select($query_servicios);
            $total_servicios =  count($servicios);
            if($total_servicios > 0) {
                foreach($servicios as $servicio) {
                    $data['Codigo'] = $servicio->Codigo;
                    $data['FechaInstalacion'] = \DateTime::createFromFormat('Y-m-d',$servicio->FechaInstalacion)->format('d-m-Y');
                    $data['Conexion'] = $servicio->Conexion;
                    $data['Valor'] = number_format($servicio->Valor, 0, '.', '');
                    $data['Grupo'] = $servicio->Grupo;
                    $data['Id'] = $servicio->Id;
                    $data['Estatus'] = $servicio->EstatusServicio;
                    $data['Tipo'] = $servicio->Tipo;
                    array_push($total_data, $data);
                }
            } else {
                $data['error'] = 'No hay datos';
                array_push($total_data, $data);
            }
        } 
        else 
        {
            $data['error'] = 'No Existe el Rut para hacer la busqueda';
            array_push($total_data, $data);
        }
        return ($total_data);
    }

    private function getDocsVencidos($Rut, $fecha_actual=1){
        $ToReturn = array();
        $data = array();
        if ($fecha_actual==1) 
            $fecha_actual = date("Y-m-d");
        $query = "  SELECT
            personaempresa.nombre as Cliente,
            facturas.Id,
            facturas.NumeroDocumento,
            facturas.FechaFacturacion,
            facturas.FechaVencimiento,
            facturas.UrlPdfBsale,
            facturas.Grupo,
            facturas.TipoFactura,
            mantenedor_tipo_cliente.nombre AS TipoDocumento,
            facturas.IVA,
            facturas.EstatusFacturacion,
            IFNULL( ( SELECT SUM( Monto ) FROM facturas_pagos WHERE FacturaId = facturas.Id ), 0 ) AS Pagado 
        FROM
            facturas
            INNER JOIN mantenedor_tipo_cliente ON facturas.TipoDocumento = mantenedor_tipo_cliente.Id 
            INNER JOIN personaempresa ON facturas.Rut = personaempresa.rut 
            WHERE facturas.Rut = $Rut AND facturas.EstatusFacturacion = '1'  AND FechaVencimiento < '".$fecha_actual."' ";
        $documentos = DB::select($query);
        $saldo_doc = 0;
        $totalDeuda = 0;
        $totalsaldoFavor = 0;
        $saldo_favor = 0;
        // echo '<pre>'; print_r($documentos); echo '</pre>'; return;
        if (count($documentos) > 0) {
            foreach($documentos as $documento){   
                $Id = $documento->Id;    
                $FechaFacturacion = \DateTime::createFromFormat('Y-m-d',$documento->FechaFacturacion)->format('d-m-Y');
                $fechaVencimiento = \DateTime::createFromFormat('Y-m-d',$documento->FechaVencimiento)->format('d-m-Y');
                $TotalFactura = 0;
                $query = "SELECT *, (Descuento + IFNULL((SELECT SUM(Porcentaje) FROM descuentos_aplicados WHERE IdDetalle = facturas_detalle.Id),0)) as Descuento FROM facturas_detalle WHERE FacturaId = '".$Id."'";
                $detalles = DB::select($query);
                foreach($detalles as $detalle){
                    $iva=($detalle->Valor*$detalle->Cantidad*0.19);
                    $Total=($detalle->Valor*$detalle->Cantidad)+$iva;
                    //$Total = $detalle->Total;
                    $Descuento = floatval($detalle->Descuento) / 100;
                    $Descuento = 0;
                    $Descuento = $Total * $Descuento;
                    $Total -= $Descuento;
                    // $TotalFactura += round($Total,0);
                    $TotalFactura += $Total;
                }
                $TotalFactura = round($TotalFactura,0);
                $saldo_doc = $TotalFactura - $documento->Pagado;
                $saldo_favor = $documento->Pagado - $TotalFactura;

                if(($saldo_doc < 0)||($saldo_doc == 1)){
                    $saldo_doc = 0;
                }
                if($saldo_favor < 0){
                    $saldo_favor = 0;
                }
                //si es mayor es porque aún no pago todo
                if($saldo_doc > 0) {
                    $totalDeuda += $saldo_doc;
                    $totalsaldoFavor += $saldo_favor;
                    $data['NumeroDocumento'] = $documento->NumeroDocumento;
                    $data['TipoDocumento'] = $documento->TipoDocumento;
                    $data['FechaFacturacion'] = $FechaFacturacion;
                    $data['FechaVencimiento'] = $fechaVencimiento;
                    $data['totalDoc'] = number_format($TotalFactura, 0, '.', '');
                    $data['saldo_doc'] = number_format($saldo_doc, 0, '.', '');
                    $data['pagos'] = number_format($documento->Pagado, 0, '.', '');
                    $data['id_factura'] = $Id;
                    array_push($ToReturn, $data ); 
                }
            }   
        }
        $datos = array(
            'datos'                 => $ToReturn,
            'totalDocumentos'       => count($ToReturn),
            'totalDeuda'            => $totalDeuda,
            'totalSaldoFavor'       => $totalsaldoFavor
        );
        return($datos);
    }


    //Código antiguo 

	public function storeClase($Nombre, $LimiteFacturas){

        $response_array = array();

        $Nombre = isset($Nombre) ? trim($Nombre) : "";
        $LimiteFacturas = $LimiteFacturas ? trim($LimiteFacturas) : "0";

        if(!empty($Nombre)){

            $this->Nombre=$Nombre;
            $this->LimiteFacturas=$LimiteFacturas;

            $query = "INSERT INTO clase_clientes(nombre, limite_facturas) VALUES ('$this->Nombre','$this->LimiteFacturas')";
            $run = new Method;
            $id = $run->insert($query);

            if($id){

                $array = array('id'=> $id, 'nombre' => $this->Nombre, 'limite_facturas' => $this->LimiteFacturas);

                $response_array['array'] = $array;
                $response_array['status'] = 1; 
            }else{
                $response_array['status'] = 0; 
            }
        }else{
            $response_array['status'] = 2; 
        }

        echo json_encode($response_array);

	} 


    function updateClase($Nombre, $LimiteFacturas, $Id){

        $response_array = array();

        $Nombre = isset($Nombre) ? trim($Nombre) : "";
        $LimiteFacturas = $LimiteFacturas ? trim($LimiteFacturas) : "0";
        $Id = isset($Id) ? trim($Id) : "";

        if(!empty($Nombre) && !empty($Id)){

            $this->Nombre=$Nombre;
            $this->LimiteFacturas=$LimiteFacturas;
            $this->Id=$Id;

            $query = "UPDATE `clase_clientes` set `nombre` = '$this->Nombre', `limite_facturas` = '$this->LimiteFacturas' where `id` = '$this->Id'";
            $run = new Method;
            $data = $run->update($query);

            if($data){

                $array = array('nombre' => $this->Nombre, 'limite_facturas' => $this->LimiteFacturas, 'id' => $this->Id);

                $response_array['array'] = $array;
                $response_array['status'] = 1; 

            }else{
                $response_array['status'] = 0; 
            }
        }else{
            $response_array['status'] = 2; 
        }

        echo json_encode($response_array);
    }

    function deleteClase($Id){

        $response_array = array();

        $Id = isset($Id) ? trim($Id) : "";

        if(!empty($Id)){

            $this->Id=$Id;

            $query = "DELETE from `clase_clientes` where `id` = '$this->Id'";
            $run = new Method;
            $data = $run->delete($query);
            $response_array['status'] = 1; 
          
        }else{
            $response_array['status'] = 2; 
        }

        echo json_encode($response_array);
    }

    function showClase($id=null){

        $clase_cliente= clase_cliente::query()->get();
        $tipo_cliente= mantenedor_tipo_cliente::query()->get();

        //return view('clientes.index',['clase_cliente'=>$clase_cliente, 'tipo_cliente'=>$tipo_cliente]);
        return view('clientes.listaCliente',['Rut'=>$id]);
       // echo json_encode($response_array);

    }


    
    function serviciosClase(){

        //return view('clientes.index',['clase_cliente'=>$clase_cliente, 'tipo_cliente'=>$tipo_cliente]);
        return view('clientes.servicios');
       // echo json_encode($response_array);

    }

    public function resumenCliente($id){  // después configurarlo para que reciba la instancia del cliente
        return view('clientes.resumenCliente', ['id_cliente' => $id]);
    }

    /*SELECT id AS 'Id', CONCAT(rut, '-', dv) AS 'Rut', nombre AS 'Nombre', correo AS 'Correo', telefono AS 'Teléfono', ( SELECT COUNT(id) FROM servicios WHERE Rut = personaempresa.Rut ) AS Servicios FROM personaempresa ORDER BY nombre*/

    public function arriendoEquipos(){
        $velocidad = isset($_POST['Velocidad']) ? trim($_POST['Velocidad']) : "";
        $plan = isset($_POST['Plan']) ? trim($_POST['Plan']) : "";
        return view('clientesServicios.arriendoEquipos', ['Velocidad' => $velocidad, 'Plan' => $plan]);
    }
    public function servicioInternet(){ 
        $velocidad = isset($_POST['Velocidad']) ? trim($_POST['Velocidad']) : "";
        $plan = isset($_POST['Plan']) ? trim($_POST['Plan']) : "";
        return view('clientesServicios.servicioInternet', ['Velocidad' => $velocidad, 'Plan' => $plan]);
    }
    public function mensualidadPuertoPublicos(){ 
        return view('clientesServicios.mensualidadPuertoPublicos');
    }
    public function mensualidadIPFija(){ 
        return view('clientesServicios.mensualidadIPFija');
    }
    public function mantencionRed(){ 
        return view('clientesServicios.mantencionRed');
    }
    public function traficoGenerado(){ 
        return view('clientesServicios.traficoGenerado');
    }
    public function otroServicio(){
        return view('clientesServicios.otroServicio');
    }
    

}

