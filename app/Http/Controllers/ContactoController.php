<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Contacto;
use App\Models\Cliente;
use Illuminate\Http\Request;

class ContactoController extends Controller
{

	public function index()
    {
        $contactos = Contacto::OrderBy('contacto')->get();
        return view('contactos.index',['contactos'=>$contactos]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'contacto'=>'required',
            'rut'=>'required'
        ]); 
        $contacto = Contacto::create($request->all());

        return json_encode("OK");
    }

    public function update(Request $request, Contacto $contacto)
    {
        $request->validate([
            'contacto'=>'required',
            'rut'=>'required'
        ]);
        $contacto->fill($request->all())->save();

        return json_encode("OK");
    }

    public function getContactos(Request $request, Cliente $cliente)
    {
        $contactos = Contacto::where('rut',$cliente->rut)->get();
        return DataTables::of($contactos)->editColumn('tipo_contacto', function ($contactos) {
                return @$contactos->tipos_contacto->nombre;})
            ->addColumn('action', function ($contactos) use($cliente, $request) {
                return '<div class="col-md-5"> <a class="glyphicon glyphicon-edit edit-contacto" cid="'.$contactos->id.'" contacto="'.$contactos->contacto.'" tc="'.$contactos->tipo_contacto.'" correo="'.$contactos->correo.'" fono="'.$contactos->telefono.'" aria-hidden="true" title="Editar"></a> </div>
                    <div class="col-md-7"><form method="POST" action="'.route('contactos.destroy',$contactos->id).'" accept-charset="UTF-8" class="delete-contacto">
                        <input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.$request->session()->token().'">
                        <button type="submit" class="btn btn-danger btn-xs" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>
                    </form></div> ';
            })->rawColumns(['tipo_contacto', 'action'])
                ->make(true);
    }

    public function destroy(Request $request, Contacto $contacto)
    {
        Contacto::destroy($contacto->id);
        return redirect()->route('clientes.index')->with('success', 'Contacto eliminado correctamente.');
    }

    public function selectContactos($cliente)
    {
        $contactos = Contacto::where('rut',$cliente)->get();
        $list = "";
        if ($contactos->count() > 0) {
            foreach ($contactos as $row) {
                $list .= '<option value="'.$row->contacto.'">'.$row->contacto.'</option>';
            }
        }
        return ($list);
    }

}