<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\mantenedor_site;

class mantenedor_siteController extends Controller
{
    public function index(){
        $mantenedor_sites = mantenedor_site::orderBy('nombre')->get();
        return response()->json([
            'data'=> $mantenedor_sites->toArray()
        ], 200);
    }

    public function show(){
        $consulta=mantenedor_site::query()->get();
        return view('mantenedores.sites.sites',['consulta'=>$consulta]);
    }

    public function nuevo(){
//        $consulta=mantenedor_bodegaController::query()->get();
        $responsables=User::all();
        return view('mantenedores.sites.sites_nuevo',['responsables'=>$responsables]);
    }

    public function guardar(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric|min:9',
            'contacto'=>'required',
            'correo'=>'required|email',
            'personal_id'=>'required|numeric|min:1',
            'dueno_cerro'=>'required',
            'latitud_coordenada'=>'required|numeric',
            'longitud_coordenada'=>'required|numeric',
            'latitud_coordenada_site'=>'required|numeric',
            'longitud_coordenada_site'=>'required|numeric',
            'datos_proveedor_electrico'=>'required'


        ]);

        $sql = new mantenedor_site;
        $sql->nombre = $request->nombre;
        $sql->direccion = $request->direccion;
        $sql->telefono = $request->telefono;
        $sql->contacto = $request->contacto;
        $sql->correo = $request->correo;
        $sql->personal_id = $request->personal_id;
        $sql->dueno_cerro = $request->dueno_cerro;
        $sql->latitud_coordenada = $request->latitud_coordenada;
        $sql->longitud_coordenada = $request->longitud_coordenada;
        $sql->latitud_coordenada_site = $request->latitud_coordenada_site;
        $sql->longitud_coordenada_site = $request->longitud_coordenada_site;
        $sql->datos_proveedor_electrico = $request->datos_proveedor_electrico;
        $sql->save();

        return redirect()->route('mantenedores/sites');
    }

    public function editar($id_site){
        $consulta=mantenedor_site::query()->where('id','=',$id_site)->get();
        $responsables=User::all();
//        dd($consulta);
        return view('mantenedores.sites.sites_editar', ['consulta'=>$consulta,'responsables'=>$responsables]);

    }

    public function modificar(Request $request){
        $id_site=$request->id;


        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric|min:9',
            'contacto'=>'required',
            'correo'=>'required|email',
            'personal_id'=>'required|numeric|min:1',
            'dueno_cerro'=>'required',
            'latitud_coordenada'=>'required|numeric',
            'longitud_coordenada'=>'required|numeric',
            'latitud_coordenada_site'=>'required|numeric',
            'longitud_coordenada_site'=>'required|numeric',
            'datos_proveedor_electrico'=>'required'


        ]);

        $sql=mantenedor_site::find($id_site);
        $sql->nombre = $request->nombre;
        $sql->direccion = $request->direccion;
        $sql->telefono = $request->telefono;
        $sql->contacto = $request->contacto;
        $sql->correo = $request->correo;
        $sql->personal_id = $request->personal_id;
        $sql->dueno_cerro = $request->dueno_cerro;
        $sql->latitud_coordenada = $request->latitud_coordenada;
        $sql->longitud_coordenada = $request->longitud_coordenada;
        $sql->latitud_coordenada_site = $request->latitud_coordenada_site;
        $sql->longitud_coordenada_site = $request->longitud_coordenada_site;
        $sql->datos_proveedor_electrico = $request->datos_proveedor_electrico;
        $sql->save();

        return redirect()->route('mantenedores/sites');

    }

    public function eliminar($id_site){
//        dd($id_proveedor);
        $sql=mantenedor_site::find($id_site);
        $sql->delete();
        return redirect()->route('mantenedores/sites');
    }
}
