<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\mantenedor_marca;

class mantenedor_marcaController extends Controller
{
    public function show(){
        $consulta=mantenedor_marca::query()->get();
        return view('mantenedores.marcas.marcas',['consulta'=>$consulta]);
    }

    public function nuevo(){
        return view('mantenedores.marcas.marcas_nuevo');
    }

    public function guardar(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required'
        ]);

        $sql = new mantenedor_marca;
        $sql->nombre = $request->nombre;
        $sql->descripcion = $request->descripcion;
        $sql->save();

        return redirect()->route('mantenedores/marcas');
    }

    public function editar($id_marca){
        $consulta=mantenedor_marca::query()->where('id','=',$id_marca)->get();
        return view('mantenedores.marcas.marcas_editar', ['consulta'=>$consulta]);
    }

    public function modificar(Request $request){
        $id_marca=$request->id;
        $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required'

        ]);

        $sql=mantenedor_marca::find($id_marca);
        $sql->nombre = $request->nombre;
        $sql->descripcion = $request->descripcion;
        $sql->save();

        return redirect()->route('mantenedores/marcas');
    }

    public function eliminar($id_marca){
        $sql=mantenedor_marca::find($id_marca);

        if (($sql->modelo->count() == 0) && ($sql->producto == null)){
            $sql->delete();
        }
        else
            return response()->json('error');
         $sql=mantenedor_marca::find($id_marca);

         //dd($sql->modelo->count());

         if (($sql->modelo->count() ==0 && $sql->producto->count() ==0))
             $sql->delete();
         else
             return error;
    }
}
