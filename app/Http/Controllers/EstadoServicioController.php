<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EstadoServicio;

class EstadoServicioController extends Controller
{

    //los estados de servicios no deberían cambiarse xq están programados por código
    /*public function index(){
        $estado = EstadoServicio::orderBy('nombre')->get();
        return view('mantenedores.estadoServicios.index',['estado'=>$estado]);
    }

    public function create(){
        return view('mantenedores.estadoServicios.create');
    }

    public function store(Request $request){
//        dd($request->nombre);
        $request->validate([
            'id'=>'required',
            'nombre'=>'required'
        ]);

        $plan = EstadoServicio::create($request->all());

        return redirect()->route('estadoServicios.index')->with('success', 'Estado del servicio creado correctamente.');
    }

    public function edit(EstadoServicio $estado){
        return view('mantenedores.estadoServicios.edit', ['estado'=>$estado]);
    }

    public function update(Request $request, EstadoServicio $estado){
        $request->validate([
            'id'=>'required',
            'nombre'=>'required'
        ]);

        $estado->fill($request->all())->save();

        return redirect()->route('estadoServicios.index')->with('success', 'Estado del servicio modificado correctamente.');

    }

    public function destroy(EstadoServicio $estado)
    {
        EstadoServicio::destroy($estado->id);
        return redirect()->route('estadoServicios.index')->with('success', 'Estado del servicio eliminado correctamente.');
    }*/
   
}
