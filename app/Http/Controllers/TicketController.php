<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TicketOrigen;
use App\Models\TicketTipo;
use App\Models\TicketArea;
use App\Models\TicketEstado;
use App\Models\TicketComentario;
use App\Models\TicketNivel;
use App\Models\TicketPrioridad;
use App\Models\Cliente;
//use App\Models\Servicio;
use App\mantenedor_servicio;
use App\mantenedor_site;
use App\User;
use App\Models\Ticket;
use App\Models\TicketAuditoria;
use Auth;

use App\Models\MotivoCorreo;
use App\Mail\EmailAsignaTicket;
use Illuminate\Support\Facades\Mail;

use Validator;
class TicketController extends Controller
{
    private $enviar_email = false ;  //  DEJAR TRUE PARA ENVIAR CORREOS
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::with(['ticket_origen'])->orderby('id','desc')->get();
        return view('tickets.lista',['tickets'=>$tickets]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ticket_origen = TicketOrigen::all();
        $ticket_tipo = TicketTipo::all();
        $ticket_area = TicketArea::all();
        $ticket_prioridad = TicketPrioridad::all();
        $clientes = Cliente::all();
        $estaciones = mantenedor_site::all();
        $tecnicos = User::where('tipo_usuario',3)->orderBy('nombre')->get();
        $tickets_estados = TicketEstado::all();
        $ticket_niveles = TicketNivel::all();

        return view('tickets.crear',[
            'origenes'=> $ticket_origen,
            'tipos'=>$ticket_tipo,
            'areas'=>$ticket_area,
            'prioridades'=>$ticket_prioridad,
            'clientes'=>$clientes,
            'estaciones'=>$estaciones,
            'tecnicos'=>$tecnicos,
            'estados'=>$tickets_estados,
            'niveles'=>$ticket_niveles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request->flash();
        $inputs = $request->all();
        $inputs['estado_id'] = 1; //por defecto estado creado
        $inputs['usuarios_id'] = Auth::user()->id;
        //dd($inputs);
        $mensajes = [
            'origen_id.required'=>'El Origen es obligatorio',
            'tipo_id.required'=>'El Tipo es obligatorio',
            'area_id.required'=>'El Area es obligatoria',
            'estado_id.required'=>'El Estado es obligatorio',
            'descripcion.required'=>'La Descripción es obligatoria'

        ];
        $validator = Validator::make($inputs, [
            'origen_id' => 'required',
            'tipo_id' => 'required',
            'area_id' => 'required',
            'estado_id' => 'required',
            'descripcion'=>'required'

        ],$mensajes);

        if ($validator->fails()) {
            //return redirect('/tickets/create')->withInput()->withErrors($validator);
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try {
            $ticket = Ticket::create($inputs);
            $ticket_auditoria = TicketAuditoria::create([
                'comentario'=>'El usuario '.Auth::user()->nombre.' ha Creado el Ticket con id '.$ticket->id,
                'evento_id'=>1,
                'tickets_id'=> $ticket->id,
                'usuarios_id'=>Auth::user()->id
            ]);

            // ENVIO DE EMAIL
            //PARA DESACTIVAR ENVIO DE EMAIL PONER variable $envio EN FALSO
            $destino_email = 'tkt-area-tecnic';
            if ($ticket->area_id == 2) $destino_email = 'tkt-area-comerc';
            if ($ticket->area_id == 3) $destino_email = 'tkt-area-logist';

            $emilios = MotivoCorreo::with('correo_usuarios.usuarios')->where('nombre', $destino_email)->first();
            $user_crea = User::find(Auth::user()->id);
            $msge_email = 'Ticket Creado Correctamente';
            if ( isset($emilios->correo_usuarios) && $this->enviar_email ) { // siempre se envia email && isset($ticket->nivel_id) && (($ticket->nivel_id) > 1 ) && isset($ticket->tecnico_id)){
                $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
                $cc = 'teledatadte@teledata.cl';
                if (strlen($user_crea->email) > 5 ) {
                    $cc = $user_crea->email;
                }
                $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
                Mail::to($usuarios)->cc($cc)->send(new EmailAsignaTicket($ticket, 1));  // el 1 es para tickets ... el 2 es cuando tiene OT
                if (Mail::failures()) {
                    $msge_email = " Ticket registrado correctamente, pero hubo error al enviar emails a persona asignada ... ";
                } else {
                    $msge_email = 'Ticket registrado correctamente. E-mail enviado a '.$usuarios->implode('email', ', ');
                }
            }

            return redirect()->back()->withInput(array('msg' => $msge_email));
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket_origen = TicketOrigen::all();
        $ticket_tipo = TicketTipo::all();
        $ticket_area = TicketArea::all();
        $ticket_prioridad = TicketPrioridad::all();
        $clientes = Cliente::all();
        $estaciones = mantenedor_site::all();
        $tecnicos = [];
        $tickets_estados = TicketEstado::all();
        $ticket_niveles = TicketNivel::all();
        $ticket = Ticket::find($id);
        
        if($ticket->nivel_id == 2 || $ticket->nivel_id == 3){
            $tecnicos = User::where('tipo_usuario',$ticket->nivel_id)->orderBy('nombre')->get();
        }

        return view('tickets.editar',[
            'origenes'=> $ticket_origen,
            'tipos'=>$ticket_tipo,
            'areas'=>$ticket_area,
            'prioridades'=>$ticket_prioridad,
            'clientes'=>$clientes,
            'estaciones'=>$estaciones,
            'tecnicos'=>$tecnicos,
            'estados'=>$tickets_estados,
            'niveles'=>$ticket_niveles,
            'ticket'=>$ticket
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        //$inputs['usuarios_id'] = Auth::user()->id;
        //dd($inputs);
        $mensajes = [
            'origen_id.required'=>'El Origen es obligatorio',
            'tipo_id.required'=>'El Tipo es obligatorio',
            'area_id.required'=>'El Area es obligatoria',
            'estado_id.required'=>'El Estado es obligatorio',
            //'descripcion.required'=>'La Descripción es obligatoria'
            'comentario.required' => 'Debe proporcionar un comentario de modificación'

        ];
        $validator = Validator::make($request->all(), [
            'origen_id' => 'required',
            'tipo_id' => 'required',
            'area_id' => 'required',
            'estado_id' => 'required',
            'comentario' => 'required'
            //'descripcion'=>'required'

        ],$mensajes);

        if ($validator->fails()) {
            //return redirect('/tickets/create')->withInput()->withErrors($validator);
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try {
            $ticket = Ticket::find($id);
            $modificaciones = " ";
            if($ticket->origen_id != $inputs['origen_id']){
                $ticket->origen_id = $inputs['origen_id'];
                $modificaciones.= ", origen";
            }
            if($ticket->tipo_id != $inputs['tipo_id']){
                $ticket->tipo_id = $inputs['tipo_id'];
                $modificaciones.= ", tipo ticket";
            }
            if($ticket->nivel_id != $inputs['nivel_id']){
                $ticket->nivel_id = $inputs['nivel_id'];
                $modificaciones.= ", nivel";
            }
            if($ticket->area_id != $inputs['area_id']){
                $ticket->area_id = $inputs['area_id'];
                $modificaciones.= ", area";
            }
            if($ticket->cliente_id != $inputs['cliente_id']){
                $ticket->cliente_id = $inputs['cliente_id'];
                $modificaciones.= ", cliente";

            }
            if(isset($inputs['servicios_id']) && ($ticket->servicios_id != $inputs['servicios_id'])){
                $ticket->servicios_id = $inputs['servicios_id'];
                $modificaciones.= ", servicios";

            }
            /* if($ticket->prioridad_id != $inputs['prioridad_id']){
                $ticket->prioridad_id = $inputs['prioridad_id'];
                $modificaciones.= ", prioridad";
            } */
            if($ticket->tecnico_id != $inputs['tecnico_id']){
                $ticket->tecnico_id = $inputs['tecnico_id'];
                $modificaciones.= ", tecnico asignado";
            }
            if($ticket->estacion_id != $inputs['estacion_id']){
                $ticket->estacion_id = $inputs['estacion_id'];
                $modificaciones.= ", estacion";
            }
            if($ticket->fecha_visita != $inputs['fecha_visita']){
                $ticket->fecha_visita = $inputs['fecha_visita'];
                $modificaciones.= ", fecha visita";
            }
            if($ticket->hora_visita != $inputs['hora_visita']){
                $ticket->hora_visita = $inputs['hora_visita'];
                $modificaciones.= ", hora visita";
            }
            if($ticket->estado_id != $inputs['estado_id']){
                $ticket->estado_id = $inputs['estado_id'];
                $modificaciones.= ", estado ticket";
            }
            if($ticket->descripcion != $inputs['descripcion']){
                $ticket->descripcion = $inputs['descripcion'];
                $modificaciones.= ", descripcion";
            }
            $ticket_auditoria = TicketAuditoria::create([
                'comentario'=>'El usuario '.Auth::user()->nombre.' ha editado el Ticket con id '.$ticket->id.' en los campos'.$modificaciones,
                'evento_id'=>2,
                'tickets_id'=> $ticket->id,
                'usuarios_id'=>Auth::user()->id
            ]);
            $ticket->save();

            /**agregando comentario de motivo de modificación */
            if($inputs['comentario']!=null){
                $comentario = TicketComentario::create([
                    'usuarios_id' => Auth::user()->id,
                    'comentario' => $inputs['comentario'],
                    'tickets_id' => $ticket->id
                ]);
            }

            $msge_email = 'No se encontraron modificaciones en ticket'; // ENVIO CORREO SI HAY CAMBIOS
            if ($modificaciones != " ") {
                $msge_email = 'Ticket modificado Correctamente';
                $destino_email = 'tkt-area-tecnic';
                if ($ticket->area_id == 2) $destino_email = 'tkt-area-comerc';
                if ($ticket->area_id == 3) $destino_email = 'tkt-area-logist';

                $emilios = MotivoCorreo::with('correo_usuarios.usuarios')->where('nombre', $destino_email)->first();
                $user_crea = User::find(Auth::user()->id);
                if ( isset($emilios->correo_usuarios) && $this->enviar_email ) {
                    $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
                    $cc = 'teledatadte@teledata.cl';
                    if (strlen($user_crea->email) > 5 ) $cc = $user_crea->email;

                    $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
                    Mail::to($usuarios)->cc($cc)->send(new EmailAsignaTicket($ticket, 3, $modificaciones));  // el 1 es para tickets ... el 2 es cuando tiene OT ... el 3 para modificaiones
                    if (Mail::failures()) {
                        $msge_email = " Ticket modificado correctamente (".$modificaciones."), pero hubo error al enviar emails a usuarios ... ";
                    } else {
                        $msge_email = 'Ticket modificado correctamente. E-mail enviado a '.$usuarios->implode('email', ', ');
                    }
                }
            }

            return redirect()->route('tickets.index')->withInput(array('msg' => $msge_email));
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**retorna la vista de tickets en un calendario */
    public function calendario(){
        return view('tickets.calendario');
    }

    /**retorna todos los tickets de nivel 3 */
    public function tickets_fecha_evento(){
        //dd('algo');
        $tickets = Ticket::with(['clientes','servicios','ticket_estado'])
                        ->where('nivel_id',3)
                        ->get();
        //dd($tickets);
        return response()->json([
            'data'=>$tickets->toArray()
        ], 200);
    }

    public function tickets_calendario(){
        dd('algo');
        return view('otro');
    }

    /**actualiza los datos de un ticket desde el calendario */
    public function actualizar(Request $request,$id){
        $inputs = $request->all();
        //$inputs['usuarios_id'] = Auth::user()->id;
        //dd($inputs);
        $mensajes = [
            //'origen_id.required'=>'El Origen es obligatorio',
            //'tipo_id.required'=>'El Tipo es obligatorio',
            //'area_id.required'=>'El Area es obligatoria',
            'estado_id.required'=>'El Estado es obligatorio',
            'tecnico_id.required'=>'El Tecnico es obligatorio',
            'fecha_visita.required'=>'La Fecha de Visita es obligatoria',
            'hora_visita.required'=>'La Hora de Visita es obligatoria'
            //'descripcion.required'=>'La Descripción es obligatoria'

        ];
        $validator = Validator::make($request->all(), [
            //'origen_id' => 'required',
            //'tipo_id' => 'required',
            //'area_id' => 'required',
            'estado_id' => 'required',
            'tecnico_id' => 'required',
            'fecha_visita' => 'required',
            'hora_visita' => 'required'
            //'descripcion'=>'required'

        ],$mensajes);

        if ($validator->fails()) {
            //return redirect('/tickets/create')->withInput()->withErrors($validator);
            $errores = $validator->getMessageBag()->toArray();
            $err = collect();
            foreach ($errores as $key => $value) {
                //dd($value);
                foreach ($value as $key => $e) {
                    $err->push($e);
                }
            }
            //dd($validator->getMessageBag()->toArray());
            return response()->json([
                'success' => false,
                'errors' => $err->toArray()
            ], 200);
        }

        try {
            $ticket = Ticket::find($id);
            $modificaciones = " ";
            /* if($ticket->origen_id != $inputs['origen_id']){
                $ticket->origen_id = $inputs['origen_id'];
                $modificaciones.= ", origen_id";
            }
            if($ticket->tipo_id != $inputs['tipo_id']){
                $ticket->tipo_id = $inputs['tipo_id'];
                $modificaciones.= ", tipo_id";
            }
            if($ticket->nivel_id != $inputs['nivel_id']){
                $ticket->nivel_id = $inputs['nivel_id'];
                $modificaciones.= ", nivel_id";
            }
            if($ticket->area_id != $inputs['area_id']){
                $ticket->area_id = $inputs['area_id'];
                $modificaciones.= ", area_id";
            }
            if($ticket->cliente_id != $inputs['cliente_id']){
                $ticket->cliente_id = $inputs['cliente_id'];
                $modificaciones.= ", cliente_id";

            }
            if($ticket->servicios_id != $inputs['servicios_id']){
                $ticket->servicios_id = $inputs['servicios_id'];
                $modificaciones.= ", servicios_id";

            }
            if($ticket->prioridad_id != $inputs['prioridad_id']){
                $ticket->prioridad_id = $inputs['prioridad_id'];
                $modificaciones.= ", prioridad_id";
            } */
            if($ticket->tecnico_id != $inputs['tecnico_id']){
                $ticket->tecnico_id = $inputs['tecnico_id'];
                $modificaciones.= ", tecnico asignado";
            }
            /* if($ticket->estacion_id != $inputs['estacion_id']){
                $ticket->estacion_id = $inputs['estacion_id'];
                $modificaciones.= ", estacion_id";
            } */
            if($ticket->fecha_visita != $inputs['fecha_visita']){
                $ticket->fecha_visita = $inputs['fecha_visita'];
                $modificaciones.= ", fecha visita";
            }
            if($ticket->hora_visita != $inputs['hora_visita']){
                $ticket->hora_visita = $inputs['hora_visita'];
                $modificaciones.= ", hora visita";
            }
            if($ticket->estado_id != $inputs['estado_id']){
                $ticket->estado_id = $inputs['estado_id'];
                $modificaciones.= ", estado ticket";
            }
            /* if($ticket->descripcion != $inputs['descripcion']){
                $ticket->descripcion = $inputs['descripcion'];
                $modificaciones.= ", descripcion";
            } */
            $ticket_auditoria = TicketAuditoria::create([
                'comentario'=>'El usuario '.Auth::user()->nombre.' ha editado el Ticket con id '.$ticket->id.' en los campos'.$modificaciones,
                'evento_id'=>2,
                'tickets_id'=> $ticket->id,
                'usuarios_id'=>Auth::user()->id
            ]);
            $ticket->save();

            $msge_email = 'No se encontraron modificaciones en ticket'; // ENVIO CORREO SI HAY CAMBIOS
            if ($modificaciones != " ") {
                $msge_email = 'Ticket modificado Correctamente';
                $destino_email = 'tkt-area-tecnic';
                if ($ticket->area_id == 2) $destino_email = 'tkt-area-comerc';
                if ($ticket->area_id == 3) $destino_email = 'tkt-area-logist';

                $emilios = MotivoCorreo::with('correo_usuarios.usuarios')->where('nombre', $destino_email)->first();
                $user_crea = User::find(Auth::user()->id);
                if ( isset($emilios->correo_usuarios) && $this->enviar_email ) {
                    $users_ids = $emilios->correo_usuarios->pluck('usuarios_id')->toArray();
                    $cc = 'teledatadte@teledata.cl';
                    if (strlen($user_crea->email) > 5 ) $cc = $user_crea->email;

                    $usuarios = User::select('email')->whereIn('id', $users_ids)->get();
                    Mail::to($usuarios)->cc($cc)->send(new EmailAsignaTicket($ticket, 3, $modificaciones));  // el 1 es para tickets ... el 2 es cuando tiene OT ... el 3 para modificaiones
                    if (Mail::failures()) {
                        $msge_email = " Ticket modificado correctamente (".$modificaciones."), pero hubo error al enviar emails a usuarios ... ";
                    } else {
                        $msge_email = 'Ticket modificado correctamente. E-mail enviado a '.$usuarios->implode('email', ', ');
                    }
                }
            }


            return response()->json([
                'success'=>true,
                'message'=>'Ticket Actualizado',  // aqui puedes poner $msge_email 
                'data'=>$ticket->toArray()
            ], 200);
            return redirect()->route('tickets.index');
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'errors' => $th->getMessage()
            ], 200);
        }
    }

    public function resumen_historico($id){
        $ticket = Ticket::find($id);
        $ids_comentarios = $ticket->comentarios_tickets->pluck('id');
        $comentarios = TicketComentario::with(['usuario'])->whereIn('id',$ids_comentarios)->get();
        //dd($comentarios);
        $ids_audi = $ticket->ticket_auditorias->pluck('id');
        $auditorias  = TicketAuditoria::with(['usuario','ticket_auditoria_evento'])->whereIn('id',$ids_audi)->get();
        return response()->json([
            'comentarios' => $comentarios->count()>0?$comentarios->toArray():[],
            'auditorias' => $auditorias->count()>0?$auditorias->toArray():[]
        ], 200);

    }
}
