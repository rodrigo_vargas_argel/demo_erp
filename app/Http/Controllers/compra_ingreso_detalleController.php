<?php

namespace App\Http\Controllers;

use App\compra_ingreso;
use App\compra_ingreso_detalle;
use App\producto;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as RV_coleccion;

class compra_ingreso_detalleController extends Controller
{
    public function nuevo($compra_id){ //desde el listado

        $compra=compra_ingreso::query()->where('id','=',$compra_id)->first();
        $productos=producto::all();
        $detalle=compra_ingreso_detalle::with('detalle_mac')->with('producto')->where('compras_ingresos_id','=',$compra_id)->get();
       // dd($detalle->count());
            if($detalle->count() > 0 ) {
                for($i=0; $i < $detalle->count(); $i++)
                {
                    $producto = collect($detalle[$i]->producto);
                   // dd('hola' . $detalle);
                    $tipo = collect($detalle[$i]->producto->tipo_producto);
                    $marca = collect($detalle[$i]->producto->marca);
                    $modelo = collect($detalle[$i]->producto->modelo);
                   // $mac=collect($detalle[$i]->producto->detalle_mac);
                    $coleccion_detalle = RV_coleccion::make($detalle, $producto, $tipo, $marca, $modelo);
                   //dd($coleccion_detalle);
                }

            }
            else{
                $coleccion_detalle='No hay Registros';
            }
        return view('inventarios.compras_detalle_nuevo',['compra'=>$compra,'productos'=>$productos,'detalles'=>$coleccion_detalle]);

    }
    public function nuevo2(){ //desde el form
        session_start();
        $compra_id=$_SESSION['id_compra']; //hubo que pasar la id por session en este caso porque un llamado a ruta con parametro no me funciono desde el compra_ingresoController
        $compra=compra_ingreso::query()->where('id','=',$compra_id)->first();
        $_SESSION['id_compra']="";
        $productos=producto::all();
        $detalle=compra_ingreso_detalle::query()->where('compras_ingresos_id','=',$compra_id)->get();
        if($detalle->count() > 0 ) {
            for ($i = 0; $i < $detalle->count(); $i++) {
                $producto = collect($detalle[$i]->producto);
                $tipo = collect($detalle[$i]->producto->tipo_producto);
                $marca = collect($detalle[$i]->producto->marca);
                $modelo = collect($detalle[$i]->producto->modelo);
                $coleccion_detalle = RV_coleccion::make($detalle, $producto, $tipo, $marca, $modelo);
            }
        }
        else{
            $coleccion_detalle='No hay Registros';
        }

       // dd($coleccion_detalle);
        return view('inventarios.compras_detalle_nuevo',['compra'=>$compra,'productos'=>$productos,'detalles'=>$coleccion_detalle]);
    }
    public function busca_prducto($codigo)
    {
        // dd($codigo);
        $consulta = producto::query()->where('codigo_barra', '=', $codigo)->first();
        if ($consulta != null) {

        $tipo = collect($consulta->tipo_producto);
        $marca = collect($consulta->marca);
        $modelo = collect($consulta->modelo);
       // $nomatch="";
        $coleccion = RV_coleccion::make($consulta, $tipo, $marca, $modelo);
           // dd($coleccion);
            return response()
                ->json(["coleccion"=>$coleccion]);
        }
        else{
            $nomatch="No Existen Productos con este código";
            return response()
                ->json(["nomatch"=>$nomatch]);
        }

    }
    public function guardar(Request $request){
        $request->validate([
            'codigo'=>'required',
            'cantidad'=>'required|integer|between:1,999',
            'precio'=>'required|integer|between:1,9999999',

        ]);

        $total=$request->precio*$request->cantidad;
        $sql=new compra_ingreso_detalle;
        $sql->compras_ingresos_id=$request->compra_id;
        $sql->producto_id=$request->producto_id;
        $sql->precio_neto=$request->precio;
        $sql->cantidad=$request->cantidad;
        $sql->total_neto=$total;
        if ($sql->save())
        {
            $id_detalle=$sql->id;

            $consulta = compra_ingreso_detalle::query()->where('id', '=', $id_detalle)->first();
            $producto=collect($consulta->producto);
            $tipo = collect($consulta->producto->tipo_producto);
            $marca = collect($consulta->producto->marca);
            $modelo = collect($consulta->producto->modelo);
            $coleccion = RV_coleccion::make($consulta, $producto,$tipo, $marca, $modelo);
            //dd($coleccion);
            return response()
                ->json(["coleccion"=>$coleccion]);

        }
        else{
            return response()
                ->json(['error'=>true]);
        }

    }
    public function eliminar($id){
        $sql=compra_ingreso_detalle::find($id);
       // dd($id);
        $sql->delete();
        return response()
            ->json(['Success'=>true]);
    }
}
