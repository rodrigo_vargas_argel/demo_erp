<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plan;

class PlanController extends Controller
{

     public function index(){
        $planes = Plan::orderBy('nombre')->get();
        return view('mantenedores.planes.index',['planes'=>$planes]);
    }

    public function create(){
        return view('mantenedores.planes.create');
    }

    public function store(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'velocidad_subida'=>'required',
            'velocidad_bajada'=>'required',
            'valor_referencia'=>'required|numeric',
            'tipo_moneda'=>'required'
        ]);

        $plan = Plan::create($request->all());

        return redirect()->route('planes.index')->with('success', 'Plan creado correctamente.');
    }

    public function edit(Plan $plane){
        return view('mantenedores.planes.edit', ['planes'=>$plane]);
    }

    public function update(Request $request, Plan $plane){
        $request->validate([
            'nombre'=>'required',
            'velocidad_subida'=>'required',
            'velocidad_bajada'=>'required',
            'valor_referencia'=>'required|numeric',
            'tipo_moneda'=>'required'
        ]);

        $plane->fill($request->all())->save();

        return redirect()->route('planes.index')->with('success', 'Plan modificado correctamente.');

    }

    public function destroy(Plan $plane)
    {
        Plan::destroy($plane->id);
        return redirect()->route('planes.index')->with('success', 'Plan eliminado correctamente.');
    }
   

}
