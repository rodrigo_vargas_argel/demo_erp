<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Cliente;
use App\Models\FacturasDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

    //header('Content-type: application/json');

class ClienteServicioController extends Controller
{

	public function servicios(Cliente $cliente)
	{
		if (!isset($cliente->rut)) 
            $cliente->rut = (int) $id;
		$services = DB::select(	"SELECT servicios.Id AS Id,
                        servicios.Codigo AS 'Codigo',
                        servicios.Conexion AS 'Conexion',
                        servicios.Valor,
                        servicios.IdServicio,
                        COALESCE ( grupo_servicio.Nombre, servicios.Grupo ) AS Grupo,
                        DATE_FORMAT(servicios.FechaInstalacion, '%d-%m-%Y')  AS 'Fecha',
                        DATE_FORMAT(servicios.FechaInicioDesactivacion, '%Y-%m-%d')  AS 'FIniD',
                        DATE_FORMAT(servicios.FechaFinalDesactivacion, '%Y-%m-%d')  AS 'FFinD',
                        servicios.EstatusServicio,
                        estado_servicios.nombre AS Estado,
                        ( CASE servicios.IdServicio WHEN 7 THEN servicios.NombreServicioExtra ELSE mantenedor_servicios.servicio END ) AS 'Tipo',
                        ( CASE servicios.IdServicio WHEN 1 THEN arriendo_equipos_datos.Velocidad ELSE '' END ) AS 'Velocidad',
                        ( CASE servicios.IdServicio WHEN 1 THEN arriendo_equipos_datos.Plan ELSE '' END ) AS 'Plan'
                    FROM servicios
                        LEFT JOIN mantenedor_tipo_factura ON mantenedor_tipo_factura.id = servicios.TipoFactura
                        LEFT JOIN mantenedor_tipo_facturacion ON mantenedor_tipo_facturacion.id = mantenedor_tipo_factura.tipo_facturacion
                        LEFT JOIN mantenedor_servicios ON servicios.IdServicio = mantenedor_servicios.IdServicio
                        LEFT JOIN grupo_servicio ON grupo_servicio.IdGrupo = servicios.Grupo
                        LEFT JOIN estado_servicios ON servicios.EstatusServicio = estado_servicios.id
                        LEFT JOIN arriendo_equipos_datos ON arriendo_equipos_datos.IdServicio = servicios.Id
                    WHERE
                        servicios.Rut = ?", [$cliente->rut]);

		// return response()->json(['data' => $services]);
		// return($services);
///////AQUIIIII VOOOYYYYYY
		$plucked = [];
		foreach($services as $srv) {
		  $plucked[] = $srv->Id;
		}
        $facturas = new FacturasDetalle;
        if (count($plucked) > 0) {
            $facturas = FacturasDetalle::whereIn('IdServicio', $plucked)->get();
        }
		

		return DataTables::of($services)
            ->addColumn('action', function ($services) use ($facturas) { //<i class="glyphicon glyphicon-zoom-in"><i class="glyphicon glyphicon-edit">
            	$eliminar = " ";
            	$filtro = $facturas->where('IdServicio', $services->Id);
            	if ($filtro->count() > 0) {
            		$eliminar = '<i class="fa fa-times eliminarServicio" attr="'.$services->Id.'" aria-hidden="true" title="eliminar"></i>';
            	}
                return ' <i class="fa fa-power-off estado-servicio" attr="'.$services->Id.'" cod="'.$services->Codigo.'" data-toggle="modal" finid="'.$services->FIniD.'" ffind="'.$services->FFinD.'" estado="'.$services->Estado.'" status="'.$services->EstatusServicio.'" data-target="#modalEstatus" aria-hidden="true" title="Activar/Desactivar"></i> |
							<i class="fa fa-eye ver-servicio" attr="'.$services->Id.'" id-serv="'.$services->IdServicio.'" cod="'.$services->Codigo.'" data-toggle="modal" data-target="#verServicios" aria-hidden="true" title="Ver servicio"></i>';

            })
            ->rawColumns(['servicios', 'action'])
            ->make(true);
	}

    public function filtrarDocsExtras(Cliente $cliente){
        $startDate = $cliente->startDate;
        $endDate = $cliente->endDate;
        $Rut = $cliente->rut;
        $documentType = $cliente->documentType;
        $NumeroDocumento = $cliente->NumeroDocumento;

        $ToReturn = array();
        $query = "  SELECT
                        personaempresa.nombre as Cliente,
                        facturas.Id,
                        facturas.NumeroDocumento,
                        facturas.FechaFacturacion,
                        facturas.FechaVencimiento,
                        facturas.UrlPdfBsale,
                        facturas.Grupo,
                        facturas.TipoFactura,
                        mantenedor_tipo_cliente.nombre AS TipoDocumento,
                        facturas.IVA,
                        facturas.EstatusFacturacion,
                        IFNULL( ( SELECT SUM( Monto ) FROM facturas_pagos WHERE FacturaId = facturas.Id ), 0 ) AS TotalSaldo 
                    FROM
                        facturas
                        INNER JOIN mantenedor_tipo_cliente ON facturas.TipoDocumento = mantenedor_tipo_cliente.Id 
                        INNER JOIN personaempresa ON facturas.Rut = personaempresa.rut
                    WHERE
                        facturas.EstatusFacturacion != '0' AND facturas.EstatusFacturacion != '3' AND facturas.EstatusFacturacion != '4' ";
        if($startDate){
            $dt = \DateTime::createFromFormat('Y/m/d',$startDate);
            $startDate = $dt->format('Y-m-d');
            $dt = \DateTime::createFromFormat('Y/m/d',$endDate);
            $endDate = $dt->format('Y-m-d');
            $query .= " AND facturas.FechaFacturacion BETWEEN '".$startDate."' AND '".$endDate."'";
        }
        if($Rut || $Rut == 0 && !$startDate && !$NumeroDocumento){
            $query .= " AND facturas.Rut = '".$Rut."'";
        }
        if($documentType){
            $query .= " AND facturas.TipoDocumento = '".$documentType."'";
        }
        if($NumeroDocumento){
            $query .= " AND facturas.NumeroDocumento = '".$NumeroDocumento."' ";
        }
        $query .= " ORDER BY facturas.FechaFacturacion DESC, MONTH(facturas.FechaFacturacion) DESC  ";
        $facturas = DB::select($query);
        if($facturas){
            //include("FacturasDetalleClass.php");
            $FacturasDetalle = new FacturasDetalle(); 
            foreach($facturas as $factura){
                $SaldoConNotaCredito = 0;
                $Id = $factura->Id;
                $EstatusFacturacion = $factura->EstatusFacturacion;
                $TotalFactura = 0;
                $query = "SELECT IdServicio, Total, (Descuento + IFNULL((SELECT SUM(Porcentaje) FROM descuentos_aplicados WHERE IdDetalle = facturas_detalle.Id),0)) as Descuento FROM facturas_detalle WHERE facturas_detalle.IdServicio = '0' AND FacturaId = '".$Id."' ";
                // echo $query;exit;
                $detalles = DB::select($query);
                if($detalles){
                    // print_r($detalles);
                    foreach($detalles as $detalle){
                        $Total = $detalle->Total;
                        $Descuento = floatval($detalle->Descuento) / 100;
                        $Descuento = 0;
                        $Descuento = $Total * $Descuento;
                        $Total -= $Descuento;
                        // $TotalFactura += round($Total,0);
                        $TotalFactura += $Total;
                    }
                    $TotalFactura = round($TotalFactura,0);
                    $SaldoFavor = 0;
                    $TotalSaldo = $factura->TotalSaldo;
                    $TotalSaldo = $TotalFactura - $TotalSaldo;
                    $SaldoFavor = $factura->TotalSaldo - $TotalFactura;
                    if($SaldoFavor < 0)
                        $SaldoFavor = 0;
                    if($TotalSaldo < 0){
                        $TotalSaldo = 0;
                    }
                    $TotalSaldoFactura = $TotalSaldo;
                    if($EstatusFacturacion != 2){
                        $Acciones = 1;
                    }
                    else{
                        $TotalSaldo = 0;
                        $Acciones = 0;
                        $query = "SELECT  DevolucionAmount, priceAdjustment FROM devoluciones WHERE FacturaId = '".$Id."'";
                        $devoluciones = DB::select($query);
                        if($devoluciones){
                            $devolucion = $devoluciones[0];
                            $SaldoConNotaCredito = $TotalFactura - $devolucion->DevolucionAmount;
                            $TotalSaldo = $TotalFactura - (double)$devolucion->DevolucionAmount;
                            $query = "SELECT  Monto FROM facturas_pagos WHERE FacturaId = '".$Id."'";
                            $pagos = DB::select($query);
                            $TotalPagado = 0;
                            if($pagos){
                                foreach ($pagos as $pago) {
                                    $TotalPagado += $pago->Monto;
                                }
                            }
                            $TotalSaldo = $TotalSaldo - $TotalPagado;
                            if($TotalSaldo < 0){
                                $TotalSaldo = 0;
                            }
                            if($devolucion->priceAdjustment == 1) {
                                $Acciones = 1;
                            }
                        }
                    }
                    
                    $Id = $factura->Id;
                    $data = array();
                    $Detalle = '*';
                    if($Rut || $Rut == 0 ){
                        $query = "SELECT * FROM facturas_detalle WHERE FacturaId = '".$Id."' ";
                        $detalles = DB::select($query);
                        if($detalles){
                            $Detalle = $detalles[0]->Concepto;
                        }else{
                            $Detalle = false;
                        }
                    }
                
                    $data['Id'] = $Id;
                    $data['DocumentoId'] = $Id;
                    $data['Cliente'] = $factura->Cliente;
                    $data['NumeroDocumento'] = $factura->NumeroDocumento;
                    $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$factura->FechaFacturacion)->format('d-m-Y');        
                    $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$factura->FechaVencimiento)->format('d-m-Y');
                    $data['Detalle'] = $Detalle;  
                    $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                    $data['TotalSaldo'] = number_format($TotalSaldo, 0, ',', '.');
                    $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                    $data['UrlPdfBsale'] = $factura->UrlPdfBsale;
                    $data['Grupo'] = $factura->Grupo;
                    $data['TipoDocumento'] = $factura->TipoDocumento;
                    $data['Acciones'] = $Acciones;
                    $data['EstatusFacturacion'] = 1;
                    $data['SaldoConNotaCredito'] = $SaldoConNotaCredito;
                    array_push($ToReturn,$data);
                    if( $EstatusFacturacion == 2 ){
                        $query = "SELECT Id, FechaDevolucion, NumeroDocumento, UrlPdfBsale, DevolucionAnulada, DevolucionAmount, priceAdjustment FROM devoluciones WHERE FacturaId = '".$Id."'  ";
                        if($startDate){
                            $query .= " AND FechaDevolucion BETWEEN '".$startDate."' AND '".$endDate."'";
                        }
                        $devoluciones = DB::select($query);
                        if($devoluciones){
                            $devolucion = $devoluciones[0];
                            $DevolucionAnulada = $devolucion->DevolucionAnulada;
                            if($DevolucionAnulada == 0 || $devolucion->priceAdjustment == 1){
                                $Acciones = 1;
                            }else{
                                $Acciones = 0;
                            }
                            $data = array();
                            $data['Id'] = $devolucion->Id;
                            $data['DocumentoId'] = $Id;
                            $data['Cliente'] = $factura->Cliente;
                            $data['NumeroDocumento'] = $devolucion->NumeroDocumento.' Doc. Ref '.$factura->NumeroDocumento;
                            $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$devolucion->FechaDevolucion)->format('d-m-Y');        
                            $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$devolucion->FechaDevolucion)->format('d-m-Y');
                            $data['Detalle'] = '*';
                            $data['DevolucionAmount'] = (double)$devolucion->DevolucionAmount; 
                            $data['TotalFactura'] = $devolucion->DevolucionAmount;
                            // $data['TotalSaldo'] = $devolucion['DevolucionAmount'];
                            $data['TotalSaldo'] = 0;
                            $data['SaldoFavor'] = $SaldoFavor;
                            $data['UrlPdfBsale'] = $devolucion->UrlPdfBsale;
                            $data['Grupo'] = $factura->Grupo;
                            $data['TipoDocumento'] = 'Nota de crédito';
                            $data['Acciones'] = $Acciones;
                            $data['EstatusFacturacion'] = 2;
                            $data['SaldoConNotaCredito'] = $TotalFactura - $devolucion->DevolucionAmount;
                            array_push($ToReturn,$data);
                            if($DevolucionAnulada == 1){
                                $DevolucionId = $devolucion->Id;
                                $query = "SELECT Id, FechaAnulacion, NumeroDocumento, UrlPdfBsale FROM anulaciones WHERE DevolucionId = '".$DevolucionId."'";
                                $anulaciones = DB::select($query);
                                if($anulaciones){
                                    $anulacion = $anulaciones[0];
                                    $data = array();
                                    $data['Id'] = $anulacion->Id;
                                    $data['DocumentoId'] = $Id;
                                    $data['Cliente'] = $factura->Cliente;
                                    $data['NumeroDocumento'] = $anulacion->NumeroDocumento;
                                    $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');
                                    $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');
                                    $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                                    $data['TotalSaldo'] = number_format($TotalSaldoFactura, 0, ',', '.');
                                    $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                                    $data['UrlPdfBsale'] = $anulacion->UrlPdfBsale;
                                    $data['Grupo'] = $factura->Grupo;
                                    $data['TipoDocumento'] = 'Nota de debito';
                                    $data['EstatusFacturacion'] = 3;
                                    $data['SaldoConNotaCredito'] = 0;
                                    array_push($ToReturn,$data);
                                }
                            }
                        }
                    }
                }
                
            }
            // fin foreach facturas
        }
        // por num doc para ver n de credito
        if($NumeroDocumento){
            $query = "SELECT Id, FacturaId, FechaDevolucion, NumeroDocumento, UrlPdfBsale, DevolucionAnulada, DevolucionAmount, priceAdjustment FROM devoluciones WHERE NumeroDocumento = '".$NumeroDocumento."'  ";
            $devoluciones = DB::select($query);
            if($devoluciones){
                $devolucion = $devoluciones[0];
                $DevolucionAnulada = $devolucion->DevolucionAnulada;
                $DevolucionFacturaId = $devolucion->FacturaId;
                $query = "  SELECT
                    personaempresa.nombre as Cliente,
                    facturas.Id,
                    facturas.NumeroDocumento,
                    facturas.FechaFacturacion,
                    facturas.FechaVencimiento,
                    facturas.UrlPdfBsale,
                    facturas.Grupo,
                    facturas.TipoFactura,
                    mantenedor_tipo_cliente.nombre AS TipoDocumento,
                    facturas.IVA,
                    facturas.EstatusFacturacion,
                    IFNULL( ( SELECT SUM( Monto ) FROM facturas_pagos WHERE FacturaId = facturas.Id ), 0 ) AS TotalSaldo 
                FROM
                    facturas
                    INNER JOIN mantenedor_tipo_cliente ON facturas.TipoDocumento = mantenedor_tipo_cliente.Id 
                    INNER JOIN personaempresa ON facturas.Rut = personaempresa.rut 
                WHERE
                    facturas.Id = '".$DevolucionFacturaId."' ";
                $query .= " ORDER BY facturas.FechaFacturacion DESC, MONTH(facturas.FechaFacturacion) DESC";
                $facturaDevolucion = DB::select($query);
                if($facturaDevolucion){
                    $factura['NumeroDocumento'] = $facturaDevolucion[0]->NumeroDocumento;
                    $factura['Cliente'] = $facturaDevolucion[0]->Cliente;
                }
                
                if($DevolucionAnulada == 0 || $devolucion->priceAdjustment == 1){
                    $Acciones = 1;
                }else{
                    $Acciones = 0;
                }
                $data = array();
                $data['Id'] = $devolucion->Id;
                $data['DocumentoId'] = $Id;
                $data['Cliente'] = $factura->Cliente;
                $data['NumeroDocumento'] = $devolucion->NumeroDocumento.' Doc. Ref '.$factura->NumeroDocumento;
                $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$devolucion->FechaDevolucion)->format('d-m-Y');        
                $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$devolucion->FechaDevolucion)->format('d-m-Y');
                $data['Detalle'] = '*';
                $devolucion['DevolucionAmount'] = (double)$devolucion->DevolucionAmount; 
                $data['TotalFactura'] = $devolucion->DevolucionAmount;
                // $data['TotalSaldo'] = $devolucion->DevolucionAmount;
                $data['TotalSaldo'] = 0;
                $data['SaldoFavor'] = $SaldoFavor;
                $data['UrlPdfBsale'] = $devolucion->UrlPdfBsale;
                $data['Grupo'] = $factura->Grupo;
                $data['TipoDocumento'] = 'Nota de crédito';
                $data['Acciones'] = $Acciones;
                $data['EstatusFacturacion'] = 2;
                $data['SaldoConNotaCredito'] = $TotalFactura - $devolucion->DevolucionAmount;
                array_push($ToReturn,$data);
                if($DevolucionAnulada == 1){
                    $DevolucionId = $devolucion->Id;
                    $query = "SELECT Id, FechaAnulacion, NumeroDocumento, UrlPdfBsale FROM anulaciones WHERE DevolucionId = '".$DevolucionId."'";
                    $anulaciones = DB::select($query);
                    if($anulaciones){
                        $anulacion = $anulaciones[0];
                        $data = array();
                        $data['Id'] = $anulacion->Id;
                        $data['DocumentoId'] = $Id;
                        $data['Cliente'] = $factura->Cliente;
                        $data['NumeroDocumento'] = $anulacion->NumeroDocumento;
                        $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');        
                        $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');        
                        $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                        $data['TotalSaldo'] = number_format($TotalSaldoFactura, 0, ',', '.');
                        $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                        $data['UrlPdfBsale'] = $anulacion->UrlPdfBsale;
                        $data['Grupo'] = $factura->Grupo;
                        $data['TipoDocumento'] = 'Nota de debito';
                        $data['EstatusFacturacion'] = 3;
                        $data['SaldoConNotaCredito'] = 0;
                        array_push($ToReturn,$data);
                    }
                }
            }
        }
        // echo "</pre>"; print_r($ToReturn); "<pre>";
        return DataTables::of($ToReturn)->make(true);
    }

    public function filtrarDocPagados(Cliente $cliente){
        $Rut = $cliente->rut;
        $ToReturn = array();
        $data = array();
        $query = "  SELECT
        (SELECT ROUND(SUM( Total )) FROM facturas_detalle WHERE FacturaId = facturas.Id ) AS totalDoc,
        facturas.NumeroDocumento,
        facturas.Id, 
        facturas.FechaFacturacion,
        facturas.FechaVencimiento,
        facturas_pagos.Detalle as Detalle,
        personaempresa.nombre AS Cliente,
        facturas_pagos.FechaPago AS FechaPago,
        ROUND(facturas_pagos.Monto) AS Pagado,
        mt.nombre AS tipo_Factura
        FROM
            facturas
            LEFT JOIN facturas_pagos ON facturas_pagos.FacturaId = facturas.Id
            LEFT JOIN personaempresa ON personaempresa.rut = facturas.Rut
            INNER JOIN mantenedor_tipo_cliente mt ON facturas.TipoDocumento = mt.id
        WHERE
            facturas.Rut = $Rut AND facturas.deleted_at IS NULL ";
    
        $documentos = DB::select($query);
        $saldo_doc = 0;
        $saldo_favor = 0;
        // echo '<pre>'; print_r($documentos); echo '</pre>'; return;
        if (count($documentos) > 0) {
    
            foreach($documentos as $documento){
                
                $FechaFacturacion = \DateTime::createFromFormat('Y-m-d',$documento->FechaFacturacion)->format('d-m-Y');
                $fechaVencimiento = \DateTime::createFromFormat('Y-m-d',$documento->FechaVencimiento)->format('d-m-Y');
                $saldo_doc = $documento->totalDoc - $documento->Pagado;
                $saldo_favor = $documento->Pagado - $documento->totalDoc;
                if($saldo_doc < 0){
                    $saldo_doc = 0;
                }
                if($saldo_favor < 0){
                    $saldo_favor = 0;
                }
                //si es mayor es porque pago
                if($documento->totalDoc > $saldo_doc) {
                    $data['NumeroDocumento'] = $documento->NumeroDocumento;
                    $data['TipoDocumento'] = $documento->tipo_Factura;
                    $data['FechaFacturacion'] = $FechaFacturacion;
                    $data['FechaVencimiento'] = $fechaVencimiento;
                    $data['totalDoc'] = number_format($documento->totalDoc, 0, ',','.');
                    $data['saldo_doc'] = number_format($saldo_doc, 0, ',','.');
                    $data['pagos'] = number_format($documento->Pagado, 0, ',','.');
                    array_push($ToReturn, $data ); 
                }
            }   
        }
        return DataTables::of($ToReturn)->make(true);
    }

    public function filtrarFacturas(Cliente $cliente){
        $startDate = $cliente->startDate;
        $endDate = $cliente->endDate;
        $Rut = $cliente->rut;
        $documentType = $cliente->documentType;
        $NumeroDocumento = $cliente->NumeroDocumento;

        $ToReturn = array();
        $query = "SELECT
                        personaempresa.nombre as Cliente,
                        facturas.Id,
                        facturas.NumeroDocumento,
                        facturas.FechaFacturacion,
                        facturas.FechaVencimiento,
                        facturas.UrlPdfBsale,
                        facturas.Grupo,
                        facturas.TipoFactura,
                        mantenedor_tipo_cliente.nombre AS TipoDocumento,
                        facturas.IVA,
                        facturas.EstatusFacturacion,
                        IFNULL( ( SELECT SUM( Monto ) FROM facturas_pagos WHERE FacturaId = facturas.Id ), 0 ) AS TotalSaldo 
                    FROM
                        facturas
                        INNER JOIN mantenedor_tipo_cliente ON facturas.TipoDocumento = mantenedor_tipo_cliente.Id 
                        INNER JOIN personaempresa ON facturas.Rut = personaempresa.rut 
                    WHERE
                        facturas.EstatusFacturacion != '0' AND facturas.EstatusFacturacion != '3' AND facturas.EstatusFacturacion != '4' ";
        if($startDate){
            $query .= " AND facturas.FechaFacturacion BETWEEN '".$startDate."' AND '".$endDate."'";
        }
        if($Rut || $Rut == 0 && !$startDate && !$NumeroDocumento){
            $query .= " AND facturas.Rut = '".$Rut."'";
        }
        if($documentType){
            $query .= " AND facturas.TipoDocumento = '".$documentType."'";
        }
        if($NumeroDocumento){
            $query .= " AND facturas.NumeroDocumento = '".$NumeroDocumento."' ";
        }
        $query .= " ORDER BY facturas.FechaFacturacion DESC, MONTH(facturas.FechaFacturacion) DESC, facturas.NumeroDocumento DESC ";
        $facturas = DB::select($query);
        if($facturas){
            //include("FacturasDetalleClass.php");
            $FacturasDetalle = new FacturasDetalle(); 
            foreach($facturas as $factura){
                $SaldoConNotaCredito = 0;
                $Id = $factura->Id;
                $IVA = $factura->IVA;  
                $EstatusFacturacion = $factura->EstatusFacturacion;
                $TotalFactura = 0;
                $query = "SELECT Total, (Descuento + IFNULL((SELECT SUM(Porcentaje) FROM descuentos_aplicados WHERE IdDetalle = facturas_detalle.Id),0)) as Descuento FROM facturas_detalle WHERE FacturaId = '".$Id."'";
                $detalles = DB::select($query);
                foreach($detalles as $detalle){
                    $Total = $detalle->Total;
                    $Descuento = floatval($detalle->Descuento) / 100;
                    $Descuento = 0;
                    $Descuento = $Total * $Descuento;
                    $Total -= $Descuento;
                    // $TotalFactura += round($Total,0);
                    $TotalFactura += $Total;
                }
                $TotalFactura = round($TotalFactura,0);
                $SaldoFavor = 0;
                $TotalSaldo = $factura->TotalSaldo;
                $TotalSaldo = $TotalFactura - $TotalSaldo;
                $SaldoFavor = $factura->TotalSaldo - $TotalFactura;
                if($SaldoFavor < 0)
                    $SaldoFavor = 0;
                if($TotalSaldo < 0){
                    $TotalSaldo = 0;
                }
                $TotalSaldoFactura = $TotalSaldo;
                if($EstatusFacturacion != 2){
                    $Acciones = 1;
                }
                else{
                    $TotalSaldo = 0;
                    $Acciones = 0;
                    $query = "SELECT  DevolucionAmount, priceAdjustment FROM devoluciones WHERE FacturaId = '".$Id."'";
                    $devoluciones = DB::select($query);
                    if($devoluciones){
                        $DevolucionAmount = 0;
                        // $devolucion = $devoluciones[0];
                        foreach($devoluciones as $devolucion){
                            $DevolucionAmount += $devolucion->DevolucionAmount;
                        }
                        $SaldoConNotaCredito = $TotalFactura - $DevolucionAmount;
                        $TotalSaldo = $TotalFactura - (double)$DevolucionAmount;
                        $query = "SELECT  Monto FROM facturas_pagos WHERE FacturaId = '".$Id."'";
                        $pagos = DB::select($query);
                        $TotalPagado = 0;
                        if($pagos){
                            foreach ($pagos as $pago) {
                                $TotalPagado += $pago->Monto;
                            }
                        }
                        $TotalSaldo = $TotalSaldo - $TotalPagado;
                        if($TotalSaldo < 0){
                            $TotalSaldo = 0;
                        }
                        if($devolucion->priceAdjustment == 1 && $TotalSaldo ) {
                            $Acciones = 1;
                        }
                    }
                }
                
                $Id = $factura->Id;
                $Detalles = ' ';
                $data = array();
                if($Rut || $Rut == 0 ){
                    $query = "SELECT * FROM facturas_detalle WHERE FacturaId = '".$Id."' ";
                    $detalles = DB::select($query);
                    if(count($detalles)){
                        foreach($detalles as $det){
                            $Detalles .= ' *'.$det->Concepto;
                        }
                    }else{
                        $Detalles = 'Sin detalle';
                    }
                }
               
                $data['Id'] = $Id;
                $data['DocumentoId'] = $Id;
                $data['Cliente'] = $factura->Cliente;
                $data['NumeroDocumento'] = $factura->NumeroDocumento;
                // $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$factura['FechaFacturacion'])->format('d-m-Y');        
                // $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$factura['FechaVencimiento'])->format('d-m-Y');
                $data['FechaFacturacion'] = $factura->FechaFacturacion;      
                $data['FechaVencimiento'] = $factura->FechaVencimiento;
                $data['Detalle'] = $Detalles;
                $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                $data['TotalSaldo'] = number_format($TotalSaldo, 0, ',', '.');
                $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                $data['UrlPdfBsale'] = $factura->UrlPdfBsale;
                $data['Grupo'] = $factura->Grupo;
                $data['TipoDocumento'] = $factura->TipoDocumento;
                $data['Acciones'] = $Acciones;
                $data['EstatusFacturacion'] = 1;
                $data['SaldoConNotaCredito'] = number_format($SaldoConNotaCredito, 0, ',', '.');
                array_push($ToReturn,$data);
                if( $EstatusFacturacion == 2 ){
                    $query = "SELECT Id, Motivo, FechaDevolucion, NumeroDocumento, UrlPdfBsale, DevolucionAnulada, DevolucionAmount, priceAdjustment FROM devoluciones WHERE FacturaId = '".$Id."'  ";
                    if($startDate){
                        $query .= " AND FechaDevolucion BETWEEN '".$startDate."' AND '".$endDate."'";
                    }
                    $devoluciones = DB::select($query);
                    $DevolucionAmount = 0;
                    if($devoluciones){
                        // $devolucion = $devoluciones[0];
                        foreach($devoluciones as $devolucion){
                            $DevolucionAmount += $devolucion->DevolucionAmount;
                            $DevolucionAnulada = $devolucion->DevolucionAnulada;
                            if($DevolucionAnulada == 0 || $devolucion->priceAdjustment == 1){
                                $Acciones = 1;
                            }else{
                                $Acciones = 0;
                            }
                            $data = array();
                            $data['Id'] = $devolucion->Id;
                            $data['DocumentoId'] = $Id;
                            $data['Cliente'] = $factura->Cliente;
                            $data['NumeroDocumento'] = $devolucion->NumeroDocumento.' Doc. Ref '.$factura->NumeroDocumento;
                            // $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$devolucion['FechaDevolucion'])->format('d-m-Y');        
                            // $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$devolucion['FechaDevolucion'])->format('d-m-Y');
                            $data['FechaFacturacion'] = $devolucion->FechaDevolucion;      
                            $data['FechaVencimiento'] = $devolucion->FechaDevolucion;
                            $data['Detalle'] = $devolucion->Motivo;
                            $devolucion_amount = (double)$devolucion->DevolucionAmount;
                            $devolucion->DevolucionAmount = $devolucion_amount;
                            $data['TotalFactura'] = $devolucion_amount;
                            // $data['TotalSaldo'] = $devolucion->DevolucionAmount;
                            $data['TotalSaldo'] = 0;
                            $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                            $data['UrlPdfBsale'] = $devolucion->UrlPdfBsale;
                            $data['Grupo'] = $factura->Grupo;
                            $data['TipoDocumento'] = 'Nota de crédito';
                            $data['Acciones'] = $Acciones;
                            $data['EstatusFacturacion'] = 2;
                            $data['SaldoConNotaCredito'] = number_format(($TotalFactura - $devolucion->DevolucionAmount), 0, ',', '.');
                            array_push($ToReturn,$data);
                            if($DevolucionAnulada == 1){
                                $DevolucionId = $devolucion->Id;
                                $query = "SELECT Id, FechaAnulacion, NumeroDocumento, UrlPdfBsale FROM anulaciones WHERE DevolucionId = '".$DevolucionId."'";
                                $anulaciones = DB::select($query);
                                if($anulaciones){
                                    $anulacion = $anulaciones[0];
                                    $data = array();
                                    $data['Id'] = $anulacion->Id;
                                    $data['DocumentoId'] = $Id;
                                    $data['Cliente'] = $factura->Cliente;
                                    $data['NumeroDocumento'] = $anulacion->NumeroDocumento;
                                    // $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');        
                                    // $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$anulacion->FechaAnulacion)->format('d-m-Y');
                                    $data['FechaFacturacion'] = $anulacion->FechaAnulacion;      
                                    $data['FechaVencimiento'] = $anulacion->FechaAnulacion;        
                                    $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                                    $data['TotalSaldo'] = number_format($TotalSaldoFactura, 0, ',', '.');
                                    $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                                    $data['UrlPdfBsale'] = $anulacion->UrlPdfBsale;
                                    $data['Grupo'] = $factura->Grupo;
                                    $data['TipoDocumento'] = 'Nota de debito';
                                    $data['EstatusFacturacion'] = 3;
                                    $data['SaldoConNotaCredito'] = 0;
                                    array_push($ToReturn,$data);
                                }
                            }
                        }
                    }
                }
            }
        }
        // por num doc para ver n de credito
        if($NumeroDocumento){
            $query = "SELECT Id, FacturaId, FechaDevolucion, NumeroDocumento, UrlPdfBsale, DevolucionAnulada, DevolucionAmount, priceAdjustment FROM devoluciones WHERE NumeroDocumento = '".$NumeroDocumento."'  ORDER BY FechaDevolucion DESC, MONTH(FechaDevolucion) DESC, NumeroDocumento DESC ";
            $devoluciones = DB::select($query);
            if($devoluciones){
                $devolucion = $devoluciones[0];
                $DevolucionAnulada = $devolucion->DevolucionAnulada;
                $DevolucionFacturaId = $devolucion->FacturaId;
                $query = "  SELECT
                    personaempresa.nombre as Cliente,
                    facturas.Id,
                    facturas.NumeroDocumento,
                    facturas.FechaFacturacion,
                    facturas.FechaVencimiento,
                    facturas.UrlPdfBsale,
                    facturas.Grupo,
                    facturas.TipoFactura,
                    mantenedor_tipo_cliente.nombre AS TipoDocumento,
                    facturas.IVA,
                    facturas.EstatusFacturacion,
                    IFNULL( ( SELECT SUM( Monto ) FROM facturas_pagos WHERE FacturaId = facturas.Id ), 0 ) AS TotalSaldo 
                FROM
                    facturas
                    INNER JOIN mantenedor_tipo_cliente ON facturas.TipoDocumento = mantenedor_tipo_cliente.Id 
                    INNER JOIN personaempresa ON facturas.Rut = personaempresa.rut 
                WHERE
                    facturas.Id = '".$DevolucionFacturaId."' ";
                $query .= " ORDER BY facturas.FechaFacturacion DESC, MONTH(facturas.FechaFacturacion) DESC";
                $facturaDevolucion = DB::select($query);
                if($facturaDevolucion){
                    $factura['NumeroDocumento'] = $facturaDevolucion[0]->NumeroDocumento;
                    $factura['Cliente'] = $facturaDevolucion[0]->Cliente;
                }
                
                if($DevolucionAnulada == 0 || $devolucion->priceAdjustment == 1){
                    $Acciones = 1;
                }else{
                    $Acciones = 0;
                }
                $data = array();
                $data['Id'] = $devolucion->Id;
                $data['DocumentoId'] = $Id;
                $data['Cliente'] = $factura->Cliente;
                $data['NumeroDocumento'] = $devolucion->NumeroDocumento.' Doc. Ref '.$factura->NumeroDocumento;
                // $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$devolucion['FechaDevolucion'])->format('d-m-Y');        
                // $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$devolucion['FechaDevolucion'])->format('d-m-Y');
                $data['FechaFacturacion'] = $devolucion->FechaDevolucion;      
                $data['FechaVencimiento'] = $devolucion->FechaDevolucion;  
                $data['Detalle'] = '*';
                $devolucion['DevolucionAmount'] = (double)$devolucion->DevolucionAmount; 
                $data['TotalFactura'] = $devolucion->DevolucionAmount;
                // $data['TotalSaldo'] = $devolucion->DevolucionAmount;
                $data['TotalSaldo'] = 0;
                $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                $data['UrlPdfBsale'] = $devolucion->UrlPdfBsale;
                $data['Grupo'] = $factura->Grupo;
                $data['TipoDocumento'] = 'Nota de crédito';
                $data['Acciones'] = $Acciones;
                $data['EstatusFacturacion'] = 2;
                $data['SaldoConNotaCredito'] = number_format(($TotalFactura - $devolucion->DevolucionAmount), 0, ',', '.');
                array_push($ToReturn,$data);
                if($DevolucionAnulada == 1){
                    $DevolucionId = $devolucion->Id;
                    $query = "SELECT Id, FechaAnulacion, NumeroDocumento, UrlPdfBsale FROM anulaciones WHERE DevolucionId = '".$DevolucionId."'";
                    $anulaciones = DB::select($query);
                    if($anulaciones){
                        $anulacion = $anulaciones[0];
                        $data = array();
                        $data['Id'] = $anulacion->Id;
                        $data['DocumentoId'] = $Id;
                        $data['Cliente'] = $factura->Cliente;
                        $data['NumeroDocumento'] = $anulacion->NumeroDocumento;
                        // $data['FechaFacturacion'] = \DateTime::createFromFormat('Y-m-d',$anulacion['FechaAnulacion'])->format('d-m-Y');        
                        // $data['FechaVencimiento'] = \DateTime::createFromFormat('Y-m-d',$anulacion['FechaAnulacion'])->format('d-m-Y');    
                        $data['FechaFacturacion'] = $anulacion->FechaAnulacion;        
                        $data['FechaVencimiento'] = $anulacion->FechaAnulacion;     
                        $data['TotalFactura'] = number_format($TotalFactura, 0, ',', '.');
                        $data['TotalSaldo'] = number_format($TotalSaldoFactura, 0, ',', '.');
                        $data['SaldoFavor'] = number_format($SaldoFavor, 0, ',', '.');
                        $data['UrlPdfBsale'] = $anulacion->UrlPdfBsale;
                        $data['Grupo'] = $factura->Grupo;
                        $data['TipoDocumento'] = 'Nota de debito';
                        $data['EstatusFacturacion'] = 3;
                        $data['SaldoConNotaCredito'] = 0;
                        array_push($ToReturn,$data);
                    }
                }
            }
        }
        // echo "</pre>"; print_r($ToReturn); "<pre>";
        return DataTables::of($ToReturn)->make(true);
    }
}

