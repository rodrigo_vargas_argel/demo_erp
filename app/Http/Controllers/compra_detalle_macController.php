<?php

namespace App\Http\Controllers;

use App\compra_detalle_mac;
use App\compra_ingreso;
use App\compra_ingreso_detalle;
use App\producto;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Collection as RV_coleccion;

class compra_detalle_macController extends Controller
{
    public function guardar(Request $request){
      //  dd($request);
        $cantidad=$request->cantidad2;
        $total=$request->precio*$cantidad;

        $detalle=new compra_ingreso_detalle;
        $detalle->compras_ingresos_id=$request->compra_id;
        $detalle->producto_id=$request->producto_id;
        $detalle->precio_neto=$request->precio;
        $detalle->cantidad=$cantidad;
        $detalle->total_neto=$total;
        $detalle->save();
        $detalle_id=$detalle->id;


        for ($i=0; $i< $cantidad; $i++){
           // dd($request->input('MAC'.$i));
            $sql=new compra_detalle_mac;
            $sql->producto_id=$request->producto_id;
            $sql->compra_id=$request->compra_id;
            $sql->compra_detalle_id=$detalle_id;
            $sql->mac=$request->input('MAC'.$i);
            $sql->save();
        }

        //$url = route('/inventarios/compras_detalle_nuevo/'.$request->compra_id); //asi no funciona xq la ruta la busca por el name y no por la URI
       // return redirect()->url('/inventarios/compras_detalle_nuevo/'.$request->compra_id);
        return redirect()->route('/inventarios/compras_detalle_nuevo/', ['id_compra' => $request->compra_id]); // asi funciona un redirect hacia una ruta con parametros
        /*
        $compra=compra_ingreso::query()->where('id','=',$request->compra_id)->first();
        $productos=producto::all();
        $detalle=compra_ingreso_detalle::with('detalle_mac')->with('producto')->where('compras_ingresos_id','=',$request->compra_id)->get();
        // dd($detalle->count());
        if($detalle->count() > 0 ) {
            for($i=0; $i < $detalle->count(); $i++)
            {
                $producto = collect($detalle[$i]->producto);
                // dd('hola' . $detalle);
                $tipo = collect($detalle[$i]->producto->tipo_producto);
                $marca = collect($detalle[$i]->producto->marca);
                $modelo = collect($detalle[$i]->producto->modelo);
                //$mac=collect($detalle[$i]->detalle_mac);
                $coleccion_detalle = RV_coleccion::make($detalle, $producto, $tipo, $marca, $modelo);
                // dd($coleccion_detalle);
            }

        }
        else{
            $coleccion_detalle='No hay Registros';
        }
        return view('inventarios.compras_detalle_nuevo',['compra'=>$compra,'productos'=>$productos,'detalles'=>$coleccion_detalle]);
        */
    }
}
