<?php

namespace App\Http\Controllers;

use App\servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
session_start();

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $_SESSION['idUsuario']=Auth::user()->id;
        $tservicios=\App\Models\Servicio::query()->count();
        $servicios=\App\Models\Servicio::query()->with('clientes')
            ->where('EstatusServicio','=',1)->get();
        $servicios_nuevos=\App\Models\Servicio::query()->with('clientes')
            ->where('EstatusServicio','=',1)
            ->where('FechaInstalacion','>=' ,'2020-01-01')->orderBy('FechaInstalacion','desc')->get();

        $servicios_corte=\App\Models\Servicio::query()->with('clientes')
            ->where('EstatusServicio','=',3)->get();

        $servicios_fin=\App\Models\Servicio::query()->with('clientes')
            ->where('EstatusServicio','=',0)
            ->where('FechaInicioDesactivacion','>=' ,'2020-01-01')->orderBy('FechaInicioDesactivacion', 'desc')->get();
        $pendientes = Servicio::with('cliente')->where('EstatusServicio', 8)->orderBy('FechaComprometidaInstalacion')->get();

        //dd($servicios->count());
        return view('home', ['servicios'=>$servicios, 'servicios_nuevos'=>$servicios_nuevos, 'servicios_corte'=>$servicios_corte,'servicios_fin'=>$servicios_fin,'tservicios'=>$tservicios,'pendientes'=>$pendientes]);
    }
}
