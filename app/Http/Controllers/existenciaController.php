<?php

namespace App\Http\Controllers;

use App\compra_detalle_mac;
use App\existencia_movimiento;
use App\Http\Requests\carga_stockRequest;
use App\mantenedor_bodega;
use App\producto;
use App\producto_archivo;
use Illuminate\Http\Request;
use App\existencia;
use Illuminate\Support\Collection as RV_coleccion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class existenciaController extends Controller
{
    public function show(){

        $consulta=existencia::with('bodega')->with('producto')->selectRaw("bodega_id, producto_id, sum(cantidad) as total")->groupBy('bodega_id')->groupBy('producto_id')->get();
        //dd($consulta->count());
        if ((Auth::user()->tipo_usuario == 0 )||(Auth::user()->tipo_usuario == 1)){

            $bodegas=mantenedor_bodega::query()->where('principal','=','0')->get();
        }
        else{
            $bodegas=mantenedor_bodega::query()->where('personal_id','=',Auth::user()->id)->where('principal','=','0')->get();
        }
        if($consulta->count() > 0 ) {

            for($i=0; $i < $consulta->count(); $i++)
            {
                $bodega = collect($consulta[$i]->bodega);
                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
               //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $coleccion_detalle = RV_coleccion::make($consulta, $bodega,$producto,  $tipo, $marca, $modelo);

            }
           // dd($coleccion_detalle);

        }
        else{
            $coleccion_detalle='No hay Registros';
        }

        return view('inventarios.existencias', ['consulta'=>$coleccion_detalle,'bodegas'=>$bodegas]);
    }
    public function get_mac($producto_id, $bodega_id){
        $mac=existencia::query()->with('producto')->with('servicio')->with('bodega')->where('producto_id','=',$producto_id)->where('bodega_id','=',$bodega_id)->get();

        if ($mac)
        {
           // dd($mac);
            return response()
                ->json(["mac"=>$mac]);

        }
        else{
            return response()
                ->json(['error'=>true]);
        }
    }
    public function get_movimientos($producto_id, $bodega_id){
        $mac=existencia_movimiento::query()->where('producto_id','=',$producto_id)->where('bodega_destino_id','=',$bodega_id)->orderBy('id','desc')->get();

        if ($mac)
        {
            // dd($mac);
            return response()
                ->json(["mac"=>$mac]);

        }
        else{
            return response()
                ->json(['error'=>true]);
        }
    }

    public function get_movimientos_mac($mac_serie,$producto_id){
        $mac=existencia_movimiento::query()->with('bodega_destino')->where('producto_id','=',$producto_id)->where('mac_serie','=',$mac_serie)->orderBy('id','desc')->get();  //->where('bodega_destino_id','=',$bodega_id)  x si queres que aparesca solo historico de su bodega actual
       // dd($mac);
        if ($mac)
        {
            $id_producto=$mac[0]->producto_id;
            $patch_foto=producto_archivo::query()->where('producto_id','=',$id_producto)->first();
            if ($patch_foto){
                $patch_foto=$patch_foto->path;
            }else{
                $patch_foto="NO";
            }
            //dd($patch_foto);
            return response()
                ->json(["mac"=>$mac, "patch_foto"=>$patch_foto]);

        }
        else{
            return response()
                ->json(['error'=>true]);
        }
    }
    public function ajuste_stock(Request $request){

        $request->validate([
            'stock'=>'required|numeric',
            'obs'=>'required',
            'fproducto_id'=>'required',
            'fbodega_id'=>'required',

        ]);
        $hoy = date('Y-m-d H:i:s');
        $existencia=existencia::query()->where('bodega_id','=', $request->fbodega_id)->where('producto_id','=',$request->fproducto_id)->first();
        $cantidad_old=$existencia->cantidad;


        //here we go!!!
        DB::beginTransaction();

        try {

                    $id_mov = DB::table('existencias_movimientos')->insertGetId(
                        ['fecha' => $hoy, 'e_s' => 'a','producto_id' => $request->fproducto_id, 'mac_serie' => '0','cantidad' =>  $request->stock, 'bodega_origen_id' => 0,'bodega_destino_id' => $request->fbodega_id, 'num_doc' => 0,'tipo_doc' =>0 , 'tipo_movimiento' => 'Ajuste Inv','responsable' => @Auth::user()->id,'obs' => $request->obs]
                    );


                    DB::table('existencias')
                        ->where('id','=',$existencia->id)
                        ->update(['cantidad' => $request->stock,  'cantidad_old' => $cantidad_old,'movimiento_id' => $id_mov]);



            DB::commit();
            return response()
                ->json(["Success"=>true, 'fila_afectadas'=>$existencia->id]);

        }

        catch (\Throwable $e) {
            DB::rollback();
            //throw $e;
            return response()
                ->json(["Success"=>false, 'fila_afectadas'=>$e]);
        }



    }

    public function traslado_stock(Request $request){

        $request->validate([
            'bodega_destino'=>'required|numeric|different:ftbodega_origen',
            'stock'=>'required|numeric|lte:ftexistencia',
            'motivo'=>'required',
            'ftproducto_id'=>'required',
            'ftbodega_origen'=>'required',
            'ftexistencia'=>'required',


        ]);
        $hoy = date('Y-m-d H:i:s');
        $existencia=existencia::query()->where('bodega_id','=', $request->ftbodega_origen)->where('producto_id','=',$request->ftproducto_id)->first();
        $cantidad_old=$existencia->cantidad;


        //here we go!!!

        DB::beginTransaction();

        try {

            // RV DICE: primero la salida BODEGA ORIGEN
            $id_mov = DB::table('existencias_movimientos')->insertGetId(
                ['fecha' => $hoy, 'e_s' => 's','producto_id' => $request->ftproducto_id, 'mac_serie' => '0','cantidad' =>  $request->stock, 'bodega_origen_id' => $request->bodega_destino,'bodega_destino_id' => $request->ftbodega_origen , 'num_doc' => 0,'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => @Auth::user()->id,'obs' => $request->obs]
            );


            $stock=existencia::query()->where('producto_id','=',$request->ftproducto_id)->where('bodega_id','=',$request->ftbodega_origen)->first();
            if ($stock){
                $nueva_cantidad=$stock->cantidad- $request->stock;
                DB::table('existencias')
                    ->where('id','=',$stock->id)
                    ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov]);
            }

            //***************************************************


            // RV DICE: AHORA la entrada BODEGA DESTINO
            $id_mov = DB::table('existencias_movimientos')->insertGetId(
                ['fecha' => $hoy, 'e_s' => 'e','producto_id' => $request->ftproducto_id, 'mac_serie' => '0','cantidad' =>  $request->stock, 'bodega_origen_id' => $request->ftbodega_origen,'bodega_destino_id' => $request->bodega_destino, 'num_doc' => 0,'tipo_doc' =>0 , 'tipo_movimiento' => 'Traslado','responsable' => @Auth::user()->id,'obs' => $request->obs]
            );


            $stock=existencia::query()->where('producto_id','=',$request->ftproducto_id)->where('bodega_id','=',$request->bodega_destino)->first();
            if ($stock){
                $nueva_cantidad=$stock->cantidad+ $request->stock;
                DB::table('existencias')
                    ->where('id','=',$stock->id)
                    ->update(['cantidad' => $nueva_cantidad,  'cantidad_old' => $stock->cantidad,'movimiento_id' => $id_mov]);
            }
            else{
                //dd( $detalle->cantidad);
                DB::table('existencias')->insertGetId(
                    ['bodega_id' => $request->bodega_destino, 'producto_id' => $request->ftproducto_id, 'mac_serie' => 0,'cantidad' =>  $request->stock,  'cantidad_old' => 0,'movimiento_id' => $id_mov]
                );
            }





            DB::commit();
            return response()
                ->json(["Success"=>true, 'fila_afectadas'=>$existencia->id]);

        }

        catch (\Throwable $e) {
            DB::rollback();
            //throw $e;
            return response()
                ->json(["Success"=>false, 'fila_afectadas'=>$e]);
        }


    }

    public function stock_inicial(){
        $bodegas=mantenedor_bodega::query()->where('principal','=','0')->get();
        $productos=producto::query()->with('marca')->with('modelo')->get();
        $consulta=existencia::with('producto')->where("bodega_id","=",$bodegas[0]->id)->orderBy('id','desc')->get();

        if($consulta->count() > 0 ) {
            for($i=0; $i < $consulta->count(); $i++)
            {

                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $estado= collect($consulta[$i]->estadoo); //metodo del modelo

                $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo, $estado);

            }
            // dd($coleccion_detalle);

        }
        else{
            $coleccion_detalle='No hay Registros';
        }
        $detalles="No hay Registros"; //para cuando carga por primera vez
        return view('inventarios.stock_inicial',['bodegas'=>$bodegas, 'detalles'=>$coleccion_detalle,'productos'=>$productos]);
    }

    public function stok_inicial_guardar(Request $request){
        $hoy = date('Y-m-d H:i:s');
        $request->validate([
            'codigo_producto'=>'required',
            'cantidad'=>'required|integer|between:1,999',


        ]);

        //$total=$request->precio*$request->cantidad;
        $sql=new existencia();
        $sql->bodega_id=$request->bodega;
        $sql->producto_id=$request->id_producto;
        $sql->mac_serie="0";
        $sql->cantidad=$request->cantidad;
        $sql->estado=$request->estado;


        if ($sql->save())
        {

            // RV DICE: AHORA la entrada en tabla moviiento
            DB::beginTransaction();

            try {
                $id_mov = DB::table('existencias_movimientos')->insertGetId(
                    ['fecha' => $hoy, 'e_s' => 'e', 'producto_id' => $request->id_producto, 'mac_serie' => '0', 'cantidad' => $request->cantidad, 'bodega_origen_id' => $request->bodega, 'bodega_destino_id' => $request->bodega, 'num_doc' => 0, 'tipo_doc' => 0, 'tipo_movimiento' => 'Ajuste', 'responsable' => @Auth::user()->id, 'obs' => 'Ajuste Stock Inicial']
                );
                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                //throw $e;
                return response()
                    ->json(["error"=>true, 'coleccion'=>$e]);
            }

            $id_detalle=$sql->id;

            $consulta = existencia::query()->where('id', '=', $id_detalle)->first();
            $producto=collect($consulta->producto);
            $tipo = collect($consulta->producto->tipo_producto);
            $marca = collect($consulta->producto->marca);
            $modelo = collect($consulta->producto->modelo);
            $estadus= collect($consulta[$i]->estadoo); //metodo del modelo
            $coleccion = RV_coleccion::make($consulta, $producto,$tipo, $marca, $modelo,$estadus);
            //dd($coleccion);
            return response()
                ->json(["coleccion"=>$coleccion]);

        }
        else{
            return response()
                ->json(['error'=>true]);
        }

    }

    public function stock_inicial_mac_guardar(Request  $request){

        $hoy = date('Y-m-d H:i:s');
        // dd($request);
        $cantidad=$request->cantidad2;


        for ($i=0; $i< $cantidad; $i++){
            // dd($request->input('MAC'.$i));
            $sql=new existencia();
            $sql->producto_id=$request->producto_id;
            $sql->bodega_id=$request->bodega_id;
            $sql->cantidad=1;
            $sql->estado=$request->estado2;
            $sql->mac_serie=$request->input('mac_serie'.$i);
            $sql->save();

            // RV DICE: AHORA la entrada en tabla moviiento
            DB::beginTransaction();

            try {
                $id_mov = DB::table('existencias_movimientos')->insertGetId(
                    ['fecha' => $hoy, 'e_s' => 'e', 'producto_id' => $request->producto_id, 'mac_serie' => $request->input('mac_serie'.$i), 'cantidad' => 1, 'bodega_origen_id' => $request->bodega_id, 'bodega_destino_id' => $request->bodega_id, 'num_doc' => 0, 'tipo_doc' => 0, 'tipo_movimiento' => 'Ajuste', 'responsable' => @Auth::user()->id, 'obs' => 'Ajuste Stock Inicial']
                );
                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                //throw $e;
                return response()
                    ->json(["error"=>true, 'coleccion'=>$e]);
            }

        }

        $consulta=existencia::with('producto')->with('estadoo')->where("bodega_id","=",$request->bodega_id)->orderBy('id','desc')->get();

        if($consulta->count() > 0 ) {
            for($i=0; $i < $consulta->count(); $i++)
            {

                $producto = collect($consulta[$i]->producto);
                $tipo = collect($consulta[$i]->producto->tipo_producto);
                $marca = collect($consulta[$i]->producto->marca);
                $modelo = collect($consulta[$i]->producto->modelo);
                $estadoo= collect($consulta[$i]->estadoo); //metodo del modelo
                //  $mac=collect($consulta[$i]->producto->detalle_mac);
                $coleccion_detalle = RV_coleccion::make($consulta, $producto,  $tipo, $marca, $modelo,$estadoo);

            }
          //  dd($coleccion_detalle);

        }
        else{
            $coleccion_detalle='No hay Registros';
        }
        return response()
            ->json(["coleccion"=>$coleccion_detalle]);

    }
    public function busca_prducto($codigo, $bodega)
    {
        // dd($codigo);
        $consulta = producto::query()->where('codigo_barra', '=', $codigo)->first();
        if ($consulta != null) {

            $tipo = collect($consulta->tipo_producto);
            $marca = collect($consulta->marca);
            $modelo = collect($consulta->modelo);
            // $nomatch="";
            $coleccion = RV_coleccion::make($consulta, $tipo, $marca, $modelo);
            // dd($coleccion);
            $existe=existencia::query()->where('producto_id','=',$consulta->id)->where('bodega_id','=',$bodega)->first();
            if ($existe){
                $mensaje=$existe->cantidad;
                return response()
                    ->json(["coleccion"=>$coleccion, "mensaje"=>$mensaje]);
            }
            else{

                $mensaje=0;
                return response()
                    ->json(["coleccion"=>$coleccion, "mensaje"=>$mensaje]);

            }

        }

        else{
            $nomatch="No Existen Productos con este código";
            return response()
                ->json(["nomatch"=>$nomatch]);
        }

    }

}
