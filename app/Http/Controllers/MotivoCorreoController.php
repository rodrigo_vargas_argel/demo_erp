<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MotivoCorreo;
use App\Models\MotivoCorreoUsuario;
use App\User;

class MotivoCorreoController extends Controller
{

    public function create(){
        $usuarios = User::orderBy('nombre')->get();
        $motivos = MotivoCorreo::orderBy('nombre')->get();
        $correos = MotivoCorreoUsuario::orderBy('motivo_correos_id')->get();
        return view('mantenedores.motivo_correos.create', ['motivos'=>$motivos, 'usuarios'=>$usuarios, 'correos'=>$correos]);
    }


    public function store(Request $request){
//        dd($request->nombre);
        $request->validate([
            'motivo_correos_id'=>'required',
            'usuarios_id'=>'required'
        ]);
        $correo = MotivoCorreoUsuario::create($request->all());

        return redirect()->route('correos.create')->with('success', 'Envío de correo  creado correctamente.');
    }


    public function edit(MotivoCorreoUsuario $correo){
        $usuarios = User::orderBy('nombre')->get();
        $motivos = MotivoCorreo::orderBy('nombre')->get();
        return view('mantenedores.motivo_correos.edit', ['motivos'=>$motivos, 'usuarios'=>$usuarios, 'correo'=>$correo]);
    }


    public function update(Request $request, MotivoCorreoUsuario $correo){
        $request->validate([
            'motivo_correos_id'=>'required',
            'usuarios_id'=>'required'
        ]);
        $correo->fill($request->all())->save();

        return redirect()->route('correos.create')->with('success', 'Envío de correo modificado correctamente.');
    }


    public function destroy(MotivoCorreoUsuario $correo)
    {
        MotivoCorreoUsuario::destroy($correo->id);
        return redirect()->route('correos.create')->with('success', 'Envío de correo eliminado correctamente.');
    }
   
}
