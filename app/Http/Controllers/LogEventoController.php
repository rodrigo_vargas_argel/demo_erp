<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\LogEvento;
use Illuminate\Http\Request;

class LogEventoController extends Controller
{
    public function show(){
       $logs = LogEvento::with('usuarios')->with('servicio')->get();
       //$consulta=mantenedor_bodegaController::first();
       // dd($consulta[1]->responsable->nombre);
        return view('mantenedores.logs.eventos_index', ['logs'=>$logs]);
    }
}
