<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Comentario;
use App\Models\Correo_Factibilidad;

class ComentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $datos = $request->all();
        $datos['usuarios_id'] = Auth::user()->id;
        $comentario = Comentario::create($datos);
        $comentario->save();

        if($request->has('destinatarios')){
            /**registrar los destinatarios y enviar el correo */
            $destinatarios = $request->destinatarios;
            $correo = new Correo_Factibilidad();
            $correo->asunto = 'Nuevo Comentario Factibilidad';
            $correo->comentarios_id = $comentario->id;
            $correo->save();

            /**registrar los destinatarios */
            foreach ($destinatarios as $key => $value) {
                DB::table('correo_factibilidades_has_destinatarios')->insert(
                    [
                        'correo_factibilidades_id' => $correo->id, 
                        'usuarios_id' => $value['id']
                    ]
                );
            }

            /**envio dse correo */
        }


        $comentario = Comentario::with(['usuario','correo','correo.destinatarios'])->find($comentario->id);
        return response()->json([
                'data'=>$comentario->toArray()
        ], 200);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
