<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\mantenedor_bodega;



class mantenedor_bodegaController extends Controller
{
    public function show(){
       $consulta=mantenedor_bodega::query()->get();
       //$consulta=mantenedor_bodegaController::first();
       // dd($consulta[1]->responsable->nombre);
        return view('mantenedores.bodegas.bodegas',['consulta'=>$consulta]);
    }

    public function nuevo(){
//        $consulta=mantenedor_bodegaController::query()->get();
        $responsables=User::all();
      //  return view('mantenedores.bodegas.bodegas_nuevo',['responsables'=>$responsables]);
        return view('facturacion.ejemplo_plantilla');
    }

    public function guardar(Request $request){
//        dd($request->nombre);
        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric|min:9',
            'personal_id'=>'required|numeric|min:1',
            'correo'=>'required|email',
            'principal'=>'required',

        ]);

        $sql = new mantenedor_bodega;
        $sql->nombre = $request->nombre;
        $sql->direccion = $request->direccion;
        $sql->telefono = $request->telefono;
        $sql->personal_id = $request->personal_id;
        $sql->correo = $request->correo;
        $sql->principal = $request->principal;

        $sql->save();

        return redirect()->route('mantenedores/bodegas');
    }




    public function editar($id_bodega){
        $consulta=mantenedor_bodega::query()->where('id','=',$id_bodega)->get();
        $responsables=User::all();
//        dd($consulta);
        return view('mantenedores.bodegas.bodegas_editar', ['consulta'=>$consulta,'responsables'=>$responsables]);

    }

    public function modificar(Request $request){
        $id_bodega=$request->id;


        $request->validate([
            'nombre'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric',
            'personal_id'=>'numeric',
            'correo'=>'required|email',
            'principal'=>'required|numeric',
        ]);

        $sql=mantenedor_bodega::find($id_bodega);
        $sql->nombre=$request->nombre;
        $sql->direccion=$request->direccion;
        $sql->telefono=$request->telefono;
        $sql->personal_id=$request->personal_id;
        $sql->correo=$request->correo;
        $sql->principal=$request->principal;
        $sql->save();

        return redirect()->route('mantenedores/bodegas');

    }



    public function eliminar($id_bodega){
        $hoy = date('Y-m-d H:i:s');
        $sql=mantenedor_bodega::find($id_bodega);
        $sql->deleted_at=$hoy;
        $sql->save();

        return response()
            ->json(["Success"=>true]);
        //return redirect()->route('mantenedores/bodegas');

    }

    public function bodegasPrincipales(){   //REVISAR SI LAS BODEGAS PRINCIPALES SON LAS 0 O LAS 1
        $pricipales = mantenedor_bodega::where('principal',0)->all();
        return $this->sendResponse($pricipales->toArray(), 'success');
    }

}
