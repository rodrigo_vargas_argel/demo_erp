<?php

namespace App\Http\Controllers;

use App\Models\Giro;
use App\Models\Cliente;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;


class GiroController extends Controller
{
	public function store(Request $request){
        $request->validate([
            'nombre'=>'required',
        ]);

        $list = "error";
        $sql = new Giro;
        $sql->nombre = $request->nombre;
        if ($sql->save()){
            $list = '<option value="'.$sql->nombre.'">'.$sql->nombre.'</option>';
        }
        return ($list);
    }

}