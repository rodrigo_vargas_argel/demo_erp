<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class carga_stockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(count($this->request));
        for($i=0; $i<count($this->request)-4;$i++)
        {
            //dd($this->request->get('MAC'.$i));
            $rules['mac_serie'.$i]='required|unique:existencias,mac_serie';
        }
        return$rules;
    }
}
