<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_tipo_producto extends Model
{
    protected $table = 'mantenedor_tipo_producto';
    protected $fillable = ['nombre'];
   // public $timestamps = false;
    use SoftDeletes;
}
