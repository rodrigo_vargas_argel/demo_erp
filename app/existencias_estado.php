<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existencias_estado extends Model
{
    protected $table = 'existencias_estado';
    public $timestamps = false;

    public function exitencias()    {

        return $this->hasMany(existencia::class,'estado', 'id');
    }
}
