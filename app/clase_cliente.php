<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clase_cliente extends Model
{
    protected $table = 'clase_clientes';
    public $timestamps = false;


    public function clientes(){
        return $this->hasMany(\App\Models\Cliente::class, 'clase_cliente' );
    }
}
