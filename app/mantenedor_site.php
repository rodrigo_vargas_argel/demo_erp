<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_site extends Model
{
    protected $table = 'mantenedor_site';
    protected $fillable = ['nombre','direccion','telefono','personal_id','correo','kmz','contacto','dueno_cerro',
'latitud_coordenada','longitud_coordenada','latitud_coordenada_site','longitud_coordenada_site','datos_proveedor_electrico'];
    protected $dates = ['deleted_at'];
    public $timestamps = false;
    use SoftDeletes;

    // metodo usado para las relaciones
    // public function responsable()
    // {

        //return $this->belongsTo('App\User', 'foreign_key', 'other_key');
        // return $this->belongsTo(User::class,'personal_id', 'id');

    // }

    public function contacto_factibilidades()
    {
        return $this->hasMany(App\Models\Contacto_Factibilidad::class, 'mantenedor_site_id', 'id');
    }
    public function responsable()
    {
        return $this->belongsTo(User::class, 'personal_id', 'id');
    }
    public function servicios()
    {
        return $this->hasMany(App\Models\Servicio::class, 'EstacionFinal', 'id');
    }

}

