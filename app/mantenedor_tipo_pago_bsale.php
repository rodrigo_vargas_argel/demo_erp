<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mantenedor_tipo_pago_bsale extends Model
{
    protected $table = 'mantenedor_tipo_pago_bsale';
    public $timestamps = false;
}
