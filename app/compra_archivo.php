<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class compra_archivo extends Model
{
    protected $table = 'compras_archivos';

    public function compras(){
        return $this->belongsTo(compra_ingreso::class, 'compra_id');
    }

}
