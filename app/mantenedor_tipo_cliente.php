<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_tipo_cliente extends Model
{
    protected $table = 'mantenedor_tipo_cliente';
    public $timestamps = false;
    protected $fillable = ['nombre','nombre2'];

    public function clientes(){
        return $this->hasMany(\App\Models\Cliente::class, 'tipo_cliente' );
    }
}
