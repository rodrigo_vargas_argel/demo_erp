<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class producto extends Model
{
    protected $table = 'productos';
   // protected $fillable='nombre';
   // public $timestamps = false;
    use SoftDeletes;

    public function marca(){
        return $this->belongsTo(mantenedor_marca::class, 'mantenedor_marca_id');
    }
    public function modelo(){
        return $this->belongsTo(mantenedor_modelo_marca::class, 'mantenedor_modelo_id');
    }
    public function  tipo_producto(){
        return $this->belongsTo(mantenedor_tipo_producto::class,'mantenedor_tipo_producto_id');
    }
    public function proveedor(){
        return $this->belongsTo(mantenedor_proveedor::class,'mantenedor_proveedores_id');
    }
    public function detalle_mac(){
        return $this->hasMany(compra_detalle_mac::class,'producto_id');
    }
    public function archivos(){
        return $this->hasMany(producto_archivo::class,'producto_id');
    }
    public function traslados(){
        return $this->hasMany(existencias_traslados::class,'producto_id');
    }
    public function equipos(){
        return $this->hasMany(Models\ServicioEquipo::class,'producto_id');
    }
}
