<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class factura_detalle extends Model
{
    protected $table = 'facturas_detalle';
    public $timestamps = false;

    public function detalle(){
        return  $this->belongsTo(factura::class, 'FacturaId');
    }
}
