<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existencias_traslado_detalle extends Model
{
    protected $table = 'existencias_traslado_detalle';

    public function traslado(){
        return $this->belongsTo(existencias_traslados::class, 'existencia_traslado_id');
    }

    public function producto(){
        return $this->belongsTo(producto::class, 'producto_id');
    }
}
