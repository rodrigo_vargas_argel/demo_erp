<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mantenedor_tipo_proveedor extends Model
{
    protected $table = 'mantenedor_tipo_proveedores';
    // public $timestamps = false;
    use SoftDeletes;
}
