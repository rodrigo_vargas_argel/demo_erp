<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class anulaciones extends Model
{
    protected $table = 'anulaciones';
    public $timestamps = false;

    public function devolucion()    {

        return $this->belongsTo(devoluciones::class,'DevolucionId', 'Id');
    }
}
