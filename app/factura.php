<?php

namespace App;

use App\Models\Cliente;
use App\Models\Contacto;
use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    protected $table = 'facturas';

   public $timestamps = false;

    protected $fillable = [
        'Rut', 'Grupo', 'TipoFactura', 'EstatusFacturacion','DocumentoIdBsale', 'NumeroDocumento', 'UrlPdfBsale', 'informedSiiBsale','responseMsgSiiBsale', 'FechaFacturacion', 'HoraFacturacion', 'TipoDocumento','FechaVencimiento', 'IVA', 'NumeroOC', 'FechaOC','Referencia',
    ];

    const CREATED_AT = 'create_at';

    public function nota_venta(){
        return  $this->hasOne('App\nota_de_venta', 'factura_id','Id');
    }

    public function detalle(){
        return  $this->hasMany(factura_detalle::class, 'FacturaId', 'Id');
    }

    public function cliente(){
        return  $this->belongsTo(Cliente::class, 'Rut','rut');
    }
    public function cliente_contacto(){
        return  $this->belongsTo(Contacto::class, 'Rut','rut');
    }

    public function pagos(){
        return  $this->hasMany(factura_pago::class, 'FacturaId', 'Id');
    }

    public function devoluciones(){
        return  $this->hasOne(devoluciones::class, 'FacturaId', 'Id');
    }

    public function tipo(){
        return  $this->belongsTo(mantenedor_tipo_cliente::class, 'TipoFactura', 'id');
    }




}
