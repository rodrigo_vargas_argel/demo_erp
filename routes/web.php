<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {


        Route::get('/', function () {
            return view('home');
        });

      /*  Route::post('/register', function () {
            return view('auth.register');
        });*/

    Route::get('/','HomeController@index' )->name('home');

        // RUTAS PARA CLIENTES, SERVICIOS y OTS

        Route::resources([
            'clientes'      => 'ClienteController',
            'giros'         => 'GiroController',
            'contactos'     => 'ContactoController',
            'servicios'     => 'ServicioController',
            'planes'        => 'PlanController',
            'correos'       => 'MotivoCorreoController',
            'ots'           => 'TicketOtController'
        ]);
        
        Route::get('/ots_tickets', 'TicketOtController@tickets')->name('ots.tickets');
        Route::get('/ots/crear/{ticket}', 'TicketOtController@create_from_ticket')->name('ots.crear');
        Route::get('/ots/trabajos/{id}', 'TicketOtController@trabajos')->name('ots.trabajos');
        Route::get('/ots/equipos/{id}', 'TicketOtController@equipos')->name('ots.equipos');
        Route::get('/ots/comentarios/{id}', 'TicketOtController@comentarios')->name('ots.comentarios');
        Route::post('/ots/cierre/{id}', 'TicketOtController@cierre_ot')->name('ots.cierre');
        
        /**============================ RUTAS FERMIN ============================== */
        Route::resource('contacto_factibilidad', 'Contacto_FactibilidadController');
        Route::resource('estados','EstadoController');
        Route::resource('planes','PlanController');
        Route::resource('tipo_clientes','Mantenedor_Tipo_ClienteController');
        Route::resource('comentarios','ComentarioController');
        Route::resource('usuarios','Usuario_Controller');
        Route::resource('comentarios_factibilidades','Comentario_FactibilidadController');
        Route::resource('preventa_factibilidades','PreventaFactibilidadController');
        Route::resource('estados_contrato','EstadoContratoController');


        Route::get('estados/tipo/{tipo}','EstadoController@estado_por_tipo');
        
        Route::get('contacto_factibilidad/{desde}/{hasta}','Contacto_FactibilidadController@cargar_factibilidades');

        /**============================ TICKETS =================================== */
        Route::resource('tickets','TicketController');
        Route::resource('ticket_estado','TicketEstadoController');
        Route::get('/calendario','TicketController@calendario')->name('tickets.calendario');
        Route::get('/eventos/calendario','TicketController@tickets_fecha_evento')->name('tickets.eventos.calendario');
        Route::get('/usuarios/nivel_usuario/{nivel_id}','Usuario_Controller@usuarios_nivel')->name('usuarios.nivel');
        Route::put('/ticket/{id}/actualizar','TicketController@actualizar')->name('tickets.actualizar');
        Route::get('/ticket/{id}/resumen_historico','TicketController@resumen_historico');
        /**============================ TICKETS =================================== */

        /**============================ RUTAS FERMIN ============================== */

         Route::get('/UpdateMasiva', 'UpdateMasivaController@update_detalles')->name('UpdateMasiva');

        Route::get('/get_clientes', 'ClienteController@getClientes')->name('get_clientes');
        Route::get('/clientes/servicios/{cliente}', 'ClienteServicioController@servicios');
        Route::get('/clientes/ciudad/{region}', 'ClienteController@getCiudades');
        Route::get('/clientes/existe/{rut}', 'ClienteController@buscaCliente');
        Route::get('/clientes/resumen/{cliente}', 'ClienteController@resumen')->name('clientes_resumen');

        Route::get('/clientes/docs_extras/{cliente}', 'ClienteServicioController@filtrarDocsExtras');
        Route::get('/clientes/docs_pagados/{cliente}', 'ClienteServicioController@filtrarDocPagados');
        Route::get('/clientes/docget_movimientos_macs_emitidos/{cliente}', 'ClienteServicioController@filtrarFacturas');

        Route::get('/contactos/lista/{cliente}', 'ContactoController@getContactos');
        Route::get('/contactos/lista2/{cliente}', 'ContactoController@getContactos2');
        Route::get('/contactos/select/{cliente}', 'ContactoController@selectContactos');
        Route::get('/tipo_factura/select/{tipo}', 'ServicioController@selectTipoCobro');

        Route::get('/servicios/tipo_servicio/{tipo}/{prods?}', 'ServicioController@tipoServicio');
        Route::post('/servicios/filtro', 'ServicioController@lista_filtro');
        Route::post('/servicios/ppoe', 'ServicioController@usuarioPppoe');
        Route::get('/logs/eventos', 'LogEventoController@show');

        Route::get('/main', 'HomeController@index')->name("main");
        Route::get('/minor', 'HomeController@index')->name("minor");

        Route::get('/home', 'HomeController@index')->name('home');

        
        Route::get('/cliente/{id?}', 'ClienteController@showClase')->name('cliente');
        
        Route::get('/mantenedores/bodegas', 'mantenedor_bodegaController@show')->name('mantenedores/bodegas');
        Route::get('/mantenedores/bodegas_nuevo', 'mantenedor_bodegaController@nuevo')->name('mantenedores/bodegas_nuevo');
        Route::post('/mantenedores/bodegas_guardar', 'mantenedor_bodegaController@guardar')->name('mantenedores/bodegas_guardar');
        Route::get('/mantenedores/bodegas_editar/{id}', 'mantenedor_bodegaController@editar')->name('mantenedores/bodegas_editar');
        Route::post('/mantenedores/bodegas_modificar', 'mantenedor_bodegaController@modificar')->name('mantenedores/bodegas_modificar');
        Route::get('/mantenedores/bodegas_eliminar/{id}', 'mantenedor_bodegaController@eliminar')->name('mantenedores/bodegas_eliminar');
        Route::get('/mantenedores/bodegas_principales', 'mantenedor_bodegaController@bodegasPrincipales');

        Route::get('/mantenedores/costos', 'mantenedor_costoController@show')->name('mantenedores/costos');
        Route::get('/mantenedores/costos_nuevo', 'mantenedor_costoController@nuevo')->name('mantenedores/costos_nuevo');
        Route::post('/mantenedores/costos_guardar', 'mantenedor_costoController@guardar')->name('mantenedores/costos_guardar');
        Route::get('/mantenedores/costos_editar/{id}', 'mantenedor_costoController@editar')->name('mantenedores/costos_editar');
        Route::post('/mantenedores/costos_modificar', 'mantenedor_costoController@modificar')->name('mantenedores/costos_modificar');
        Route::get('/mantenedores/costos_eliminar/{id}', 'mantenedor_costoController@eliminar')->name('mantenedores/costos_eliminar');

        Route::get('/mantenedores/proveedores', 'mantenedor_proveedorController@show')->name('mantenedores/proveedores');
        Route::get('/mantenedores/proveedores_nuevo', 'mantenedor_proveedorController@nuevo')->name('mantenedores/proveedores_nuevo');
        Route::post('/mantenedores/proveedores_guardar', 'mantenedor_proveedorController@guardar')->name('mantenedores/proveedores_guardar');
        Route::get('/mantenedores/proveedores_editar/{id}', 'mantenedor_proveedorController@editar')->name('mantenedores/proveedores_editar/');
        Route::post('/mantenedores/proveedores_modificar', 'mantenedor_proveedorController@modificar')->name('mantenedores/proveedores_modificar');
        Route::get('/mantenedores/proveedores_eliminar/{id}', 'mantenedor_proveedorController@eliminar')->name('mantenedores/proveedores_eliminar/');

        Route::get('/mantenedores/sites', 'mantenedor_siteController@show')->name('mantenedores/sites');
        Route::get('/mantenedores/sites_nuevo', 'mantenedor_siteController@nuevo')->name('mantenedores/sites_nuevo');
        Route::post('/mantenedores/sites_guardar', 'mantenedor_siteController@guardar')->name('mantenedores/sites_guardar');
        Route::get('/mantenedores/sites_editar/{id}', 'mantenedor_siteController@editar')->name('mantenedores/sites_editar');
        Route::post('/mantenedores/sites_modificar', 'mantenedor_siteController@modificar')->name('mantenedores/sites_modificar');
        Route::get('/mantenedores/sites_eliminar/{id}', 'mantenedor_siteController@eliminar')->name('mantenedores/sites_eliminar');

        Route::get('/mantenedores/productos', 'productoController@show')->name('mantenedores/productos');
        Route::get('/mantenedores/productos_nuevo', 'productoController@nuevo')->name('mantenedores/productos_nuevo');
        Route::post('/mantenedores/productos_guardar', 'productoController@guardar')->name('mantenedores/productos_guardar');
        Route::get('/mantenedores/productos_editar/{id}', 'productoController@editar')->name('mantenedores/productos_editar');
        Route::post('/mantenedores/productos_modificar', 'productoController@modificar')->name('mantenedores/productos_modificar');
        Route::post('/mantenedores/productos_agregar_archivos', 'productoController@subir_archivos')->name('mantenedores/productos_agregar_archivos');
        Route::get('/cargar_marcas_modelos/{id}', 'productoController@cargar_marcas_modelos')->name('cargar_marcas_modelos');
        Route::get('/nuevo_tipo_producto/{id}', 'productoController@nuevo_tipo_producto')->name('nuevo_tipo_producto');
        Route::get('/nueva_marca_producto/{id}', 'productoController@nueva_marca_producto')->name('nueva_marca_producto');
        Route::get('/nuevo_modelo_producto/{id}/{nombre}', 'productoController@nuevo_modelo_producto')->name('nuevo_modelo_producto');

        Route::get('/mantenedores/marcas', 'mantenedor_marcaController@show')->name('mantenedores/marcas');
        Route::get('/mantenedores/marcas_nuevo', 'mantenedor_marcaController@nuevo')->name('mantenedores/marcas_nuevo');
        Route::post('/mantenedores/marcas_guardar', 'mantenedor_marcaController@guardar')->name('mantenedores/marcas_guardar');
        Route::get('/mantenedores/marcas_editar/{id}', 'mantenedor_marcaController@editar')->name('mantenedores/marcas_editar');
        Route::post('/mantenedores/marcas_modificar', 'mantenedor_marcaController@modificar')->name('mantenedores/marcas_modificar');
        Route::get('/mantenedores/marcas_eliminar/{id}', 'mantenedor_marcaController@eliminar')->name('mantenedores/marcas_eliminar');

        Route::get('/mantenedores/modelos', 'mantenedor_modelo_marcaController@show')->name('mantenedores/modelos');
        Route::get('/mantenedores/modelos_nuevo', 'mantenedor_modelo_marcaController@nuevo')->name('mantenedores/modelos_nuevo');
        Route::post('/mantenedores/modelos_guardar', 'mantenedor_modelo_marcaController@guardar')->name('mantenedores/modelos_guardar');
        Route::get('/mantenedores/modelos_editar/{id}', 'mantenedor_modelo_marcaController@editar')->name('mantenedores/modelos_editar');
        Route::post('/mantenedores/modelos_modificar', 'mantenedor_modelo_marcaController@modificar')->name('mantenedores/modelos_modificar');
        Route::get('/mantenedores/modelos_eliminar/{id}', 'mantenedor_modelo_marcaController@eliminar')->name('mantenedores/modelos_eliminar');

       Route::get('/mantenedores/planes', 'PlanController@index')->name('/mantenedores/planes');



    Route::get('/inventarios/compras', 'compra_ingresoController@show')->name('/inventarios/compras');
        Route::get('/inventarios/compras_nuevo', 'compra_ingresoController@nuevo')->name('/inventarios/compras_nuevo');
        Route::post('/inventarios/compras_guardar', 'compra_ingresoController@guardar')->name('/inventarios/compras_guardar');
        Route::get('/inventarios/compras_guardar/home', 'compra_ingresoController@show')->name('/inventarios/compras_guardar/home');
        Route::get('/inventarios/compras_detalle_nuevo/{id_compra}', 'compra_ingreso_detalleController@nuevo')->name('/inventarios/compras_detalle_nuevo/'); //desde el listado para seguir completando >>
        Route::get('/detalle_nuevo', 'compra_ingreso_detalleController@nuevo2')->name('detalle_nuevo'); //desde el form nuevo
        Route::get('/inventarios/compras_detalle_busca_producto/{codigo}', 'compra_ingreso_detalleController@busca_prducto')->name('/inventarios/compras_detalle_busca_producto');
        Route::post('/inventarios/compras_detalle_nuevo/guardar', 'compra_ingreso_detalleController@guardar')->name('/inventarios/compras_detalle_nuevo/guardar');
        Route::get('/compras_detalle_eliminar/{codigo}', 'compra_ingreso_detalleController@eliminar')->name('/compras_detalle_eliminar');
        Route::get('/inventarios/compras_editar/{id}', 'compra_ingresoController@editar')->name('/inventarios/compras_editar');
        Route::post('/inventarios/compras_modificar', 'compra_ingresoController@modificar')->name('/inventarios/compras_modificar');
        Route::post('/compra_detalle_mac_guardar', 'compra_detalle_macController@guardar')->name('/compra_detalle_mac_guardar');
        Route::get('/inventarios/compras_finalizar/{id}', 'compra_ingresoController@finalizar')->name('/inventarios/compras_finalizar');
        Route::post('/inventarios/compras_agregar_archivos', 'compra_ingresoController@subir_archivos')->name('/inventarios/compras_agregar_archivos');

        Route::get('/inventarios/existencias', 'existenciaController@show')->name('/inventarios/existencias');
        Route::get('/inventarios/get_mac/{producto_id}/{bodega_id}', 'existenciaController@get_mac')->name('/inventarios/get_mac');
        Route::get('/inventarios/get_movimientos/{producto_id}/{bodega_id}', 'existenciaController@get_movimientos')->name('/inventarios/get_movimientos');
        Route::get('/inventarios/get_movimientos_mac/{mac_serie}/{bodega_id}', 'existenciaController@get_movimientos_mac')->name('/inventarios/get_movimientos_mac');
        Route::post('/ajuste_stock', 'existenciaController@ajuste_stock')->name('/ajuste_stock');
        Route::post('/traslado_stock', 'existenciaController@traslado_stock')->name('/traslado_stock');

        Route::get('/inventarios/traslados', 'existencias_trasladoController@show')->name('/inventarios/traslados');
        Route::get('/inventarios/traslado_nuevo', 'existencias_trasladoController@nuevo')->name('/inventarios/traslado_nuevo');
        Route::get('/inventarios/traslado_combo/{tipo_bodega}', 'existencias_trasladoController@combo')->name('/inventarios/traslado_combo'); //xq no se pueden hacer traslados entre un mismo tipo de bodega
        Route::post('/inventarios/traslado_guardar', 'existencias_trasladoController@guardar')->name('/inventarios/traslado_guardar');
        Route::post('/inventarios/traslado_detalle_guardar', 'existencias_trasladoController@guardar_detalle')->name('/inventarios/traslado_detalle_guardar');
        Route::get('inventarios/traslado_finalizar/{traslado_id}', 'existencias_trasladoController@finalizar')->name('/inventarios/traslado_finalizar');
        Route::get('inventarios/traslado_continuar/{traslado_id}', 'existencias_trasladoController@continuar')->name('/inventarios/traslado_continuar');

       Route::get('/inventarios/rma', 'existencia_rmaController@show')->name('/inventarios/rma');
       Route::post('/inventarios/rma_guardar', 'existencia_rmaController@guardar')->name('/inventarios/rma_guardar');
       Route::post('/inventarios/rma_actualizar', 'existencia_rmaController@actualizar')->name('/inventarios/rma_actualizar');



       Route::get('/inventarios/entrega_equipo', 'existencias_trasladoController@entrega_equipo')->name('/inventarios/entrega_equipo');
       Route::get('/inventarios/devolucion_equipo', 'existencias_trasladoController@devolucion_equipo')->name('/inventarios/devolucion_equipo');
       Route::get('/inventarios/carga_stock/{id_bodega}', 'existencias_trasladoController@carga_inventarios')->name('/inventarios/carga_stock');
       Route::get('/inventarios/cargar_servicios/{id_cliente}', 'existencias_trasladoController@cargar_servicios')->name('/inventarios/cargar_servicios');
       Route::post('/inventarios/entrega_equipos_guardar', 'existencias_trasladoController@entrega_equipos_guardar')->name('/inventarios/entrega_equipos_guardar');

       Route::get('/inventarios/stock_inicial', 'existenciaController@stock_inicial')->name('/inventarios/stock_inicial');
       Route::post('/inventarios/stock_inicial/guardar', 'existenciaController@stok_inicial_guardar')->name('/inventarios/stock_inicial/guardar');
       Route::post('/inventarios/stock_inicial_mac_guardar', 'existenciaController@stock_inicial_mac_guardar')->name('/inventarios/stock_inicial_mac_guardar');
       Route::get('/inventarios/busca_producto/{codigo}/{bodega}', 'existenciaController@busca_prducto')->name('/inventarios/busca_producto');

       Route::get('/facturacion/nota_de_venta', 'nota_de_ventaController@show')->name('/facturacion/nota_de_venta');
       Route::get('/facturacion/nota_de_venta_nuevo', 'nota_de_ventaController@nuevo')->name('/facturacion/nota_de_venta_nuevo');
       Route::post('/facturacion/nota_de_venta_guardar', 'nota_de_ventaController@guardar')->name('/facturacion/nota_de_venta_guardar');
       Route::get('/facturacion/facturar_nota_venta/{id_nota_venta}/{modo}', 'facturaController@facturar_nota_venta')->name('/facturacion/facturar_nota_venta');
       Route::get('/facturacion/enviar_documento/{id_factura}', 'facturaController@enviar_documento')->name('/facturacion/enviar_documento');
       Route::get('/facturacion/eliminar_nv/{id_factura}', 'nota_de_ventaController@eliminar_nv')->name('/facturacion/eliminar_nv');
       Route::get('/facturacion/morosidades', 'facturaController@morosidades')->name('/facturacion/morosidades');
       Route::get('/facturacion/facturas', 'facturaController@show')->name('/facturacion/facturas');
       Route::post('/facturacion/lista_filtro', 'facturaController@lista_filtro')->name("/facturacion/lista_filtro");
    Route::post('/facturacion/lista_filtro_nv', 'nota_de_ventaController@lista_filtro')->name("/facturacion/lista_filtro_nv");
      Route::post('/facturacion/lista_filtro', 'facturaController@lista_filtro')->name("/facturacion/lista_filtro");
      Route::get('/facturacion/lista_filtro2/{rut_cliente}', 'facturaController@lista_filtro2')->name("/facturacion/lista_filtro2");
       Route::get('/facturacion/guardar_pago/{id_factura}/{monto}/{fecha_pago}/{tipo_pago}', 'facturaController@guardar_pago')->name('/facturacion/guardar_pago');
    Route::get('/facturacion/eliminar_pago/{id_pago}', 'facturaController@eliminar_pago')->name('/facturacion/eliminar_pago');
    Route::get('/facturacion/rpt_pagos', 'facturaController@rpt_pagos')->name('/facturacion/rpt_pagos');
    Route::post('/facturacion/rpt_pagos_filtro/', 'facturaController@rpt_pagos_filtro')->name('/facturacion/rpt_pagos_filtro');


    Route::get('/facturacion/deudas', 'deudas2Controller@show')->name('/facturacion/deudas');
    Route::post('/facturacion/guardar_nc', 'facturaController@guardar_nc')->name('/facturacion/guardar_nc');
    Route::get('/facturacion/guardar_nd/{id_devolucion}', 'facturaController@guardar_nd')->name('/facturacion/guardar_nd');

    //*******masiva ******
    Route::get('/facturacion/masiva', 'facturaController@masiva_index')->name('/facturacion/masiva');
    Route::post('/facturacion/masiva_get/', 'facturaController@masiva_get')->name('/facturacion/masiva_get');
    Route::post('/facturacion/facturar_masiva', 'facturaController@facturar_masiva')->name('/facturacion/facturar_masiva');


    /**=========== DASGHBOARD ============ */

    Route::get('/dashboard/factibilidades', 'Contacto_FactibilidadController@dashboard')->name('/dashboard/factibilidades');
    /**=========== DASGHBOARD ============ */

    /**=========== Rutas Fermin ============ */
       Route::get('/factibilidades', 'Contacto_FactibilidadController@vista')->name('factibilidades');
       Route::get('/factibilidades/{id}', 'Contacto_FactibilidadController@ver')->name('factibilidades.ver');
       Route::get('/torres','mantenedor_siteController@index')->name('/torres');
       Route::get('/contacto_factibilidades/{id}/comentarios','Contacto_FactibilidadController@comentarios');
       Route::get('/contacto_factibilidades/excel/{desde}/{hasta}','Contacto_FactibilidadController@excel');
       /**=========== Rutas Fermin ============ */



});

Route::get('/logout', function () {

    Auth::logout();

    return redirect('login');
})->name('auth_logout');


