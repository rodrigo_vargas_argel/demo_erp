<?php

$run = new Method;

if (!isset($_SESSION['idUsuario'])) {
	echo "<script> window.location = '../index.php' </script>";
}
$_SESSION['idUsuario'] = @Auth::user()->id;
$_SESSION['idNivel'] = @Auth::user()->nivel;
$query = 'SELECT nombre, email FROM usuarios WHERE id ='.$_SESSION['idUsuario'];
$data = $run->select($query);

if (file_exists('../public/ajax/perfil/img-profile/'.$_SESSION['idUsuario'].'.jpg')) {
	$img = '<img style="width:30px; height:30px; class="img-circle img-user media-object"  src="../public/ajax/perfil/img-profile/'.$_SESSION['idUsuario'].'.jpg" class="img-lg img-circle" alt="Profile Picture">';
} else {
	$img =  '<img style="width:20px; height:20px; class="img-circle img-user media-object" src="../../img/av1.png" class="img-lg img-circle" alt="Profile Pictures">';
}
?>
<header id="navbar">
	<div id="navbar-container" class="boxed">
		<!--Logo-->
		<div class="navbar-header col-md-3">
			<a href="../bienvenida/bienvenida.php" class="navbar-brand">
				<div class="brand-title">
					<i style="margin-left: 10px;" class="fa fa-home"></i> ERP | Teledata
				</div>
			</a>
		</div>
		<div class="navbar-content clearfix col-md-9">
			<ul class="nav navbar-top-links pull-left col-md-2">
				<li class="tgl-menu-btn" id="togl-menu-btn">
					<a class="mainnav-toggle" href="#">
						<i class="pli-view-list"></i>
					</a>
				</li>
			</ul>
			<ul class="nav navbar-top-links pull-right col-md-10">
				<li style="padding-top:8px;" class="col-md-5">
					<label class="col-md-5" style="color:green"><small>Verificar correo:</small></label>
					<input type="email" placeholder="Ingrese correo a verificar" id="verificarCorreo" name="verificarCorreo" class="verificarCorreo col-md-7">
				</li>
				<li class="username col-md-2" style="color:green; padding-top:12px;">
					<small> UF:</small><span class="ValorUF"><small>0</small></span>
				</li>
				<li id="dropdown-user col-md-5" class="dropdown pull-right">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right row" style="padding: 0px 0px; padding-top:10px;">
						
						<div id="username" class="username hidden-xs"><?php echo $data[0][0]; ?> <span class="pull-right">
							<?php echo $img; ?>
						</span></div>
						<input type="hidden" id="IdUsuarioSession" value="<?php echo $_SESSION['idUsuario'] ?>">
						<input type="hidden" id="IdNivelUsuarioSession" value="<?php echo $_SESSION['idNivel'] ?>">
					</a>
					<div class="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">
						<!-- Dropdown heading  -->
						<!-- User dropdown menu -->
						<ul class="head-list">
							<li>
								<a href="../perfil">
									<i class="pli-male icon-lg icon-fw"></i> Perfil
								</a>
							</li>
							<!-- <li>
									<a href="#">
											<i class="pli-gear icon-lg icon-fw"></i> Configuración
									</a>
							</li> -->
						</ul>
						<!-- Dropdown footer -->
						<div class="pad-all text-right">
							<!-- <a href="../index.php?doLogout=true" class="btn btn-primary"> -->
							<a href="../destruir_sesion.php" class="btn btn-primary destruir_sesion">
								<i class="pli-unlock"></i> Salir
							</a>
						</div>
					</div>
				</li>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End user dropdown-->
			</ul>
		</div>
		<!--================================-->
		<!--End Navbar Dropdown-->
	</div>
</header>
<style media="screen">
.bootbox.modal {
background-color: transparent !important;
z-index: 9999 !important;
background-image: none !important;
}
#navbar .brand-title {
padding: 0 1.5em 0 5px;
}
</style>
<!--===================================================-->
<!--END NAVBAR-->