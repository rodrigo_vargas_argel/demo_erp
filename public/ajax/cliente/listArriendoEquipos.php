<?php
	//require_once('../../class/methods_global/methods.php');
    require_once('../../methods/Method.php');
	$query = "	SELECT
					arriendo_equipos_datos.IdArriendoEquiposDatos,
					(
						CASE
						WHEN arriendo_equipos_datos.IdProducto IS NULL THEN
							CONCAT(
								mantenedor_modelos_marca.nombre,
								' ',
								mantenedor_marcas.nombre,
								' ',
								mantenedor_tipo_producto.nombre
							)
						ELSE
							'No hay producto asignado'
						END
					) AS Producto,
					arriendo_equipos_datos.Velocidad,
					arriendo_equipos_datos.Plan
				FROM
					arriendo_equipos_datos
				LEFT JOIN inventario_ingresos ON arriendo_equipos_datos.IdProducto = inventario_ingresos.id
				LEFT JOIN mantenedor_modelos_marca ON inventario_ingresos.modelo_producto_id = mantenedor_modelos_marca.id
				LEFT JOIN mantenedor_marcas ON mantenedor_modelos_marca.mantenedor_marca_id = mantenedor_marcas.id
				LEFT JOIN mantenedor_tipo_producto ON mantenedor_modelos_marca.mantenedor_marca_id = mantenedor_tipo_producto.id
				WHERE
					arriendo_equipos_datos.IdServicio = ".$_POST['id'];
	$run = new Method;
	$lista = $run->listViewDelete($query,$_POST['id'],1);
	echo $lista;
 ?>