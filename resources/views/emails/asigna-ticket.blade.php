<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Teledata ERP</title>
  <style type="text/css">
      body{ font-family: "Times New Roman", Times, serif; }
      h4{ margin-top: 5px; margin-bottom: 5px; }
      .titulo{ max-height:50px; width:100%; background-color:green; padding:1em; text-align:center; color:#fff; }
      .sub-titulo{ color: #263238; }
      .mensaje{ background-color:#F5F5F5; width:100%; padding:1em; }
      .parrafo{ color:#424242; font-weight: normal;}
      .footer{ background-color:#9E9E9E; max-height:50px; min-height: 50px; padding:1em; color:#ffffff; }
      .titulo2{ max-height:50px; width:50%; background-color:green; padding:1em; text-align:center; color:#fff; }
  </style>
</head>
<body>
  <div class="titulo">
      {{ $titulo }}
  </div>
  <div class="mensaje">
    @if ( $tipo == 3 )
        <h4 class="sub-titulo">Modificaciones realizadas<span class="parrafo">{{ $modificacion }}</span></h4>
    @endif

    <h4 class="sub-titulo">Ticket N°: 
      <span class="parrafo">{{ $ticket->id }}</span> 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |  &nbsp; &nbsp; &nbsp; 
      Estado: 
      <span class="parrafo">{{ @$ticket->ticket_estado->nombre }}</span>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|  &nbsp; &nbsp; &nbsp;
      Prioridad: 
      <span class="parrafo">{{ @$ticket->prioridad_id}}</span>
    </h4>
    <h4 class="sub-titulo">Fecha Evento: 
      <span class="parrafo">{{ @date("d-m-Y", @strtotime($ticket->fecha_evento)) }} {{ @date("H:i", @strtotime($ticket->hora_evento)) }}</span>
      &nbsp;&nbsp;&nbsp;&nbsp; |  &nbsp; &nbsp; &nbsp;
      Fecha Visita:
      <span class="parrafo">{{ @date("d-m-Y", @strtotime($ticket->fecha_visita)) }} {{ @date("H:i", @strtotime($ticket->hora_visita)) }}</span>
    </h4>
    <h4 class="sub-titulo">Origen:
      <span class="parrafo">{{ @$ticket->ticket_origen->nombre }}</span>
      &nbsp;&nbsp;&nbsp;&nbsp; |  &nbsp; &nbsp; &nbsp;
      Tipo:
      <span class="parrafo">{{ @$ticket->ticket_tipo->nombre }}</span>

    </h4>
    <h4 class="sub-titulo">Area: 
      <span class="parrafo">{{ @$ticket->ticket_area->nombre }}</span>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |  &nbsp; &nbsp; &nbsp;
      Nivel: <span class="parrafo">{{ @$ticket->ticket_nivel->nombre }}</span>

    </h4>
    <h4 class="sub-titulo">Cliente: <span class="parrafo">{{ @$ticket->clientes->nombre }}</span></h4>
    <h4 class="sub-titulo">Servicio: <span class="parrafo">{{ @$ticket->servicios->Codigo }}</span></h4>
    <h4 class="sub-titulo">Estación: <span class="parrafo">{{ @$ticket->servicios->estaciones->nombre }}</span></h4>
    <h4 class="sub-titulo">Técnico: <span class="parrafo">{{ @$ticket->tecnico->nombre }}</span></h4>
    <h4 class="sub-titulo">Descripción: <span class="parrafo">{{ @$ticket->descripcion }}</span></h4>

    @if ( $tipo == 2 )
      <hr>
      <div class="titulo2"> Detalle Orden de Trabajo </div>
      @php($ot = $ticket->ticket_ot)
      @php($ot = $ot->first())
      <h4 class="sub-titulo">Trabajos a realizar: </h4>

      @foreach ($ot->ticket_ot_detalle as $fila)
        <h4 class="sub-titulo"> - {{ $fila->nombre }} </h4>
      @endforeach

      <h4 class="sub-titulo">Equipos a utilizar: </h4>

      @foreach ($ot->ticket_ot_equipo as $fila)
        <h4 class="sub-titulo"> - {{ @$fila->cantidad }} {{ @$fila->equipos->marca->nombre }} {{ @$fila->equipos->modelo->nombre }} </h4>
      @endforeach

    @endif


  </div>
  <div class="footer">
    E-mail generado de forma automática por el sistema Teledata ERP.
  </div>
</body>
</html>