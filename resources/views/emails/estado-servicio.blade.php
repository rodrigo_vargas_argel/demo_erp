<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Teledata ERP</title>
  <style type="text/css">
      body{
        font-family: "Times New Roman", Times, serif;
      }
      h4{
        margin-top: 5px;
        margin-bottom: 5px;
      }
      .titulo{
        max-height:50px;
        width:100%;
        background-color:green;
        padding:1em;
        text-align:center;
        color:#fff;
      }

      .sub-titulo{
        color: #263238;
      }

      .mensaje{
        background-color:#F5F5F5;
        width:100%;
        padding:1em;
      }

      .parrafo{
        color:#424242;
      }

      .footer{
        background-color:#9E9E9E;
        max-height:50px;
        min-height: 50px;
        padding:1em;
        color:#ffffff;
      }
  </style>
</head>
<body>
  <div class="titulo">
    {{$titulo }}
  </div>
  <div class="mensaje">
    <h4 class="sub-titulo">Cliente: <span class="parrafo">{{$servicio->clientes->nombre }}</span></h4>
    <h4 class="sub-titulo">Dirección: <span class="parrafo">{{$servicio->Direccion }}</span></h4>
    <h4 class="sub-titulo">Contacto cliente: <span class="parrafo">{{$servicio->Contacto}}</span> || Fono contacto: <span class="parrafo">{{$servicio->Fono}}</span></h4>
    <h4 class="sub-titulo">Tipo Servicio: <span class="parrafo">{{$servicio->tipo_servicios->servicio }}</span></h4>
    <h4 class="sub-titulo">Descripción: <span class="parrafo">{{$servicio->Descripcion}}</span></h4>
    <h4 class="sub-titulo">Estado: <span class="parrafo">{{ $servicio->estado_servicios->nombre }}</span></h4>
    
    <hr>

    <h4 class="sub-titulo">Fecha de Instalación: <span class="parrafo">{{$servicio->FechaInstalacion}}</span></h4>
    @if (($servicio->IdServicio == 1) || ($servicio->IdServicio == 2))
      <h4 class="sub-titulo">Usuario Pppoe: <span class="parrafo">{{$servicio->UsuarioPppoe}}</span> || Señal Final: <span class="parrafo">{{$servicio->SenalFinal}}</span></h4>
      <h4 class="sub-titulo">Estacion Final: <span class="parrafo">{{$servicio->EstacionFinal}}</span></h4>
    @elseif ($servicio->IdServicio == 3)
      <h4 class="sub-titulo">Puerto TCP/UDP: <span class="parrafo">{{@$servicio->puerto_publico->PuertoTCPUDP}}</span></h4>
      <h4 class="sub-titulo">Descripción: <span class="parrafo">{{@$servicio->puerto_publico->Descripcion}}</span></h4>
    @elseif ($servicio->IdServicio == 4)
      <h4 class="sub-titulo">Direccion IP Fija: <span class="parrafo">{{@$servicio->ip_fija->DireccionIPFija}}</span></h4>
      <h4 class="sub-titulo">Descripción: <span class="parrafo">{{@$servicio->ip_fija->Descripcion}}</span></h4>
    @elseif ($servicio->IdServicio == 5)
      <h4 class="sub-titulo">Descripción: <span class="parrafo">{{@$servicio->mantencion_red->Descripcion}}</span></h4>
    @elseif ($servicio->IdServicio == 6)
      <h4 class="sub-titulo">Linea Telefonica (tráfico): <span class="parrafo">{{@$servicio->trafico_generado->LineaTelefonica}}</span></h4>
      <h4 class="sub-titulo">Descripción: <span class="parrafo">{{@$servicio->trafico_generado->Descripcion}}</span></h4>
    @elseif ($servicio->IdServicio == 7)
      <h4 class="sub-titulo">Nombre Servicio Extra: <span class="parrafo">{{$servicio->NombreServicioExtra}}</span></h4>
    @endif

    <h4 class="sub-titulo">Comentario: <span class="parrafo">{{$servicio->Comentario}}</span></h4>

</div>
  <div class="footer">
    E-mail generado de forma automática por el sistema Teledata ERP.
  </div>
</body>
</html>