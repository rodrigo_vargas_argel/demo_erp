@extends('layouts.app')
@section('title')
    Servicios
@endsection
@section('content')
    <style type="text/css"> 
        .img-thumbnail{background-color: #eafaf1 }
    </style>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-3">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Servicios</a>
                </li>
            </ol>
        </div>

        <div class="col-md-7 img-thumbnail">
            <ul> <b> @php echo count($pendientes); @endphp Servicios pendientes de Activación: </b>
            @php $hoy = date('Y-m-d'); @endphp
            @foreach ($pendientes as $row)
                @if ( $row->FechaComprometidaInstalacion < $hoy )
                    <li class="text-danger"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                        <a class="text-success   activar" title="Activar" id-serv="{{$row->IdServicio}}" tipo="{{$row->tipo_servicios->servicio}}"
                           cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" serv="{{$row->Id}}" ip="{{$row->InstaladoPor}}"
                           fi="{{is_null($row->FechaInstalacion) ? '' : @\DateTime::createFromFormat('Y-m-d', $row->FechaInstalacion)->format('d-m-Y')}}"
                           ppt="{{$row->UsuarioPppoeTeorico}}" upp="{{$row->UsuarioPppoe}}" st="{{$row->SenalTeorica}}" sf="{{$row->SenalFinal}}"
                           pe="{{$row->PosibleEstacion}}" ef="{{$row->EstacionFinal}}" est="{{$row->EstatusServicio}}" com="{{$row->Comentario}}" asig="{{$row->IdUsuarioAsignado}}" lin="{{@$row->trafico_generado->LineaTelefonica}}" pto="{{@$row->puerto_publico->PuertoTCPUDP}}" ipf="{{@$row->ip_fija->DireccionIPFija}}"
                        > Activar <i  class="fa fa-check"></i></a>
                @elseif ( $row->FechaComprometidaInstalacion == $hoy )
                    <li class="text-warning"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                        {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                        <a class="text-success   activar" title="Activar" id-serv="{{$row->IdServicio}}" tipo="{{$row->tipo_servicios->servicio}}"
                           cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" serv="{{$row->Id}}" ip="{{$row->InstaladoPor}}"
                           fi="{{is_null($row->FechaInstalacion) ? '' : @\DateTime::createFromFormat('Y-m-d', $row->FechaInstalacion)->format('d-m-Y')}}"
                           ppt="{{$row->UsuarioPppoeTeorico}}" upp="{{$row->UsuarioPppoe}}" st="{{$row->SenalTeorica}}" sf="{{$row->SenalFinal}}"
                           pe="{{$row->PosibleEstacion}}" ef="{{$row->EstacionFinal}}" est="{{$row->EstatusServicio}}" com="{{$row->Comentario}}" asig="{{$row->IdUsuarioAsignado}}" lin="{{@$row->trafico_generado->LineaTelefonica}}" pto="{{@$row->puerto_publico->PuertoTCPUDP}}" ipf="{{@$row->ip_fija->DireccionIPFija}}"
                        > Activar <i  class="fa fa-check"></i></a>
                @else
                    <li class="text-success"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                        {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                        <a class="text-success   activar" title="Activar" id-serv="{{$row->IdServicio}}" tipo="{{$row->tipo_servicios->servicio}}"
                           cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" serv="{{$row->Id}}" ip="{{$row->InstaladoPor}}"
                           fi="{{is_null($row->FechaInstalacion) ? '' : @\DateTime::createFromFormat('Y-m-d', $row->FechaInstalacion)->format('d-m-Y')}}"
                           ppt="{{$row->UsuarioPppoeTeorico}}" upp="{{$row->UsuarioPppoe}}" st="{{$row->SenalTeorica}}" sf="{{$row->SenalFinal}}"
                           pe="{{$row->PosibleEstacion}}" ef="{{$row->EstacionFinal}}" est="{{$row->EstatusServicio}}" com="{{$row->Comentario}}" asig="{{$row->IdUsuarioAsignado}}" lin="{{@$row->trafico_generado->LineaTelefonica}}" pto="{{@$row->puerto_publico->PuertoTCPUDP}}" ipf="{{@$row->ip_fija->DireccionIPFija}}"
                        > Activar <i  class="fa fa-check"></i></a>
                @endif                    
            @endforeach 
            </ul>
        </div>

        <div class="col-md-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ route('servicios.create') }}" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    @include('servicios.modales.asignarTecnico')
    @include('servicios.modales.modal_EditarTarea')
    @include('servicios.modales.modalStatusServicio')
    @include('servicios.show')
   
   
    
    @if (Session::has('success'))
        <div class="col-md-12 img-thumbnail">
            {!! session('success') !!}
        </div>
    @endif
    @if (Session::has('warning'))
        <div class="col-md-12 alert alert-success">
            {!! session('warning') !!}
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">

            </div>

            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class=" fa fa-list"></i>  Lista de Servicios</h5>
                        <form name="filtro" method="post" action="{{ url('/servicios/filtro') }}">
                            <div class="col-md-6 pull-right ">
                                {{csrf_field()}}
                                <select class="selectpicker form-control " style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="this.form.submit()">
                                    <option value="">- Desde aquí puede digitar y seleccionar un cliente -</option>
                                    @foreach($clientes as $cliente)
                                        <option value="{{$cliente->rut}}" rut="{{$cliente->rut}}">{{$cliente->nombre}}  {{$cliente->rut}}-{{$cliente->dv}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </form>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th style="width:4% !important">Id</th>
                                    <th style="width:15% !important">Tipo Servicio</th>
                                    <th style="width:20% !important">Cliente</th>
                                    <th style="width:6% !important">Código</th>
                                    <th style="width:7% !important">Direccion</th>
                                    <th style="width:15% !important">Conexión Descripción Referencia</th>
                                    <th style="width:8% !important">Fecha Instalación</th>
                                    <th style="width:5% !important">Valor UF</th>
                                    <th style="width:8% !important">Plan</th>
                                    <th style="width:5% !important">Estado</th>
                                    <th style="width:5% !important">Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($servicios as $row)
                                    <tr>
                                        <td>
                                            {{ $row->Id }}
                                        </td>
                                        <td>
                                            {{ @$row->tipo_servicios->servicio }}
                                        </td>
                                        <td>
                                            {{ @$row->clientes->nombre }}
                                        </td>
                                        <td>
                                            {{ $row->Codigo }}
                                        </td>
                                        <td>
                                            {{ $row->Direccion }}
                                        </td>
                                        <td>
                                            {{ $row->Conexion }}  {{ $row->Descripcion }} {{ $row->Comentario }} {{ $row->Referencia }}
                                        </td>
                                        <td>
                                            {{ $row->FechaInstalacion }}
                                        </td>
                                        <td>
                                            {{ $row->Valor }}
                                        </td>
                                        <td>{{$row->plan_nombre}}</td>
                                        <td class="estado-servicio" cod="{{ $row->Codigo }}" status="{{ $row->EstatusServicio }}" finid="{{ $row->FechaInicioDesactivacion }}" ffind="{{ $row->FechaFinalDesactivacion }}" attr="{{ $row->Id }}">
                                            {{ @$row->estado_servicios->nombre }}
                                        </td>
                                        <td>
                                          @if ( $row->EstatusServicio == 8 )    
                                            <a class="btn-primary fa fa-user-plus fa-lg asignar-tecnico" title="Asignar a un Técnico"
                                                cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" tec="{{$row->IdUsuarioAsignado}}" serv="{{$row->Id}}"
                                                ></a>
                                            <span><bold> | </bold></span>
                                          @endif
                                            
                                          @if (( $row->EstatusServicio == 8 ) && ( $row->IdUsuarioAsignado != NULL ))
                                            <a class="btn-success fa fa-check fa-lg activar" title="Activar" id-serv="{{$row->IdServicio}}" tipo="{{$row->tipo_servicios->servicio}}"
                                                cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" serv="{{$row->Id}}" ip="{{$row->InstaladoPor}}"
                                                fi="{{is_null($row->FechaInstalacion) ? '' : @\DateTime::createFromFormat('Y-m-d', $row->FechaInstalacion)->format('d-m-Y')}}" 
                                                ppt="{{$row->UsuarioPppoeTeorico}}" upp="{{$row->UsuarioPppoe}}" st="{{$row->SenalTeorica}}" sf="{{$row->SenalFinal}}"
                                                pe="{{$row->PosibleEstacion}}" ef="{{$row->EstacionFinal}}" est="{{$row->EstatusServicio}}" com="{{$row->Comentario}}" asig="{{$row->IdUsuarioAsignado}}" lin="{{@$row->trafico_generado->LineaTelefonica}}" pto="{{@$row->puerto_publico->PuertoTCPUDP}}" ipf="{{@$row->ip_fija->DireccionIPFija}}"
                                                ></a>
                                            <span><bold> | </bold></span>                                            

                                          @endif
                                            <a class="fa fa-pencil-square-o fa-lg" href="{{route('servicios.edit',$row->Id)}}" aria-hidden="true" title="Editar Servicio"></a>
                                            <span><bold> | </bold></span>

                                            <a class="fa fa-eye fa-lg ver-servicio" title="Ver detalle" cli="{{$row->clientes->nombre}}" cod="{{$row->Codigo}}" serv="{{$row->Id}}" id-serv="{{$row->IdServicio}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>
    <script>
        //imprimir modal info servicio
        document.getElementById("btnPrint").onclick = function () {
            printElement(document.getElementById("printThis"));
        }

        function printElement(elem) {
            var domClone = elem.cloneNode(true);

            var $printSection = document.getElementById("printSection");

            if (!$printSection) {
                var $printSection = document.createElement("div");
                $printSection.id = "printSection";
                document.body.appendChild($printSection);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }


        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                order: [[ 0, "desc" ]],
                dom: '<"html5buttons"B>lTfgitp',
                autoWidth: false,
                buttons: [

                    {extend: 'excel', title: 'Servicios'},
                    {extend: 'pdf', title: 'Servicios'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });
        });
        
        //ver los contactos del cliente
        $(document).on('click', '.asignar-tecnico', function() {
            $('#agregarTecnico').modal('show');
            let tech = $(this).attr('tec');
            $("#IdUsuarioAsignado").val(tech).change();
            $('.clientito').html("Cliente: " + $(this).attr('cli'));
            $('.codiguito').html("Código servicio: " + $(this).attr('cod'));
            $('.id-serv').val($(this).attr('serv'));
        });

        $('body').on('click', '#grabarTecnico', function() {
            document.getElementById("grabarTecnico").disabled=true;
            let id = $('.id-serv').val();
            let url = "{{ url('/servicios') }}/"+id
            let csrf_token = "{{ csrf_token() }}";
            var form = {
                _method: "PATCH",
                _token: csrf_token,
                IdUsuarioAsignado: $('#IdUsuarioAsignado').val()
            };
            
            $.ajax({
                url: url,
                type: 'PATCH',
                data: form,
                success: function(response) {
                    var data = JSON.parse(response)
                    if (( data.estado != null) && (data.estado == "OK")) {
                        $('#agregarTecnico').modal('hide');
                        bootbox.alert('Tecnico ( '+ data.cliente +' ) asignado correctamente . E-mail enviado a ' + data.email);
                        setTimeout('location.reload()', 5000);
                    } else {
                        bootbox.alert('Ha ocurrido un error :-(  No se asignó el Técnico');
                    }
                },
                error:function (jqXHR, exception) {
                    let errores = "";
                    obj = jqXHR.responseJSON.errors;
                    Object.keys(obj).forEach(function(key) {
                      errores += obj[key];
                    })
                    bootbox.alert('Ha ocurrido un error :-( '+ errores );
                }
            });
        });

        //Activar servicio
        $(document).on('click', '.activar', function() {
            var arr_ids = ['Id','FechaInstalacion','InstaladoPor','UsuarioPppoeTeorico','UsuarioPppoe','SenalTeorica','SenalFinal','PosibleEstacion','EstacionFinal','Estatus','Comentario','IdUsuarioAsignado','linea','puerto','ip'];
            var arr_values = ['serv','fi','ip','ppt','upp','st','sf','pe','ef','est','com','asig','lin','pto','ipf']

            $('#modalTarea').modal('show');
            $('.Codigo').html("Código servicio: " + $(this).attr('cod') + "<br> Cliente: " + $(this).attr('cli')+ "<br> Servicio: " + $(this).attr('tipo') );
            for (var i = 0; i < 15; i++) {
                if (( i == 2 ) && ($(this).attr(arr_values[i]) == "" )) {
                    $('#'+arr_ids[i]).val($(this).attr(arr_values[11]));  
                } else if (( i == 1 ) && (($(this).attr(arr_values[i]) == null ) || $(this).attr(arr_values[i]) == "" )) {
                    document.getElementById("FechaInstalacion").valueAsDate = new Date();      
                } else if (( i == 9 ) && ($(this).attr(arr_values[i]) == "8" )) {
                    $('#'+arr_ids[i]).val("1");    
                } else {
                    $('#'+arr_ids[i]).val($(this).attr(arr_values[i]));    
                }
                
                if ((i == 2) || (i == 8) || (i == 9)) {
                    $('#'+arr_ids[i]).change();
                }
            }

            let tipoServ = $(this).attr('id-serv');
            $('.tipos').hide();
            $('.serv-'+tipoServ).show();
        });

        $('body').on('click', '#guardarTarea', function() {
            document.getElementById("guardarTarea").disabled=true;
            let id = $('#Id').val();
            let url = "{{ url('/servicios') }}/"+id
            let csrf_token = "{{ csrf_token() }}";
            var form = {
                _method: "PATCH",
                _token: csrf_token,
                FechaInstalacion: $('#FechaInstalacion').val(),
                actualizaFecha: $('#actualizaFecha').val(),
                InstaladoPor: $('#InstaladoPor').val(),
                UsuarioPppoe: $('#UsuarioPppoe').val(),
                SenalFinal: $('#SenalFinal').val(),
                EstacionFinal: $('#EstacionFinal').val(),
                EstatusServicio: $('#Estatus').val(),
                Comentario:  $('#Comentario').val(),
                puerto: $('#puerto').val(),     //3
                ip: $('#ip').val(),         //4
                linea: $('#linea').val(),       //6
                descripcion: $('#descripcion').val()
            };

            $.ajax({
                url: url,
                type: 'PATCH',
                data: form,
                success: function(response) {
                    var data = JSON.parse(response)
                    if (( data.estado != null) && (data.estado == "OK")) {
                        $('#modalTarea').modal('hide');
                        bootbox.alert('Servicio ( '+ data.codigo +' ) actualizado correctamente . E-mail enviado a ' + data.email);
                        setTimeout('location.reload()', 5000);
                    } else {
                        bootbox.alert('Ha ocurrido un error :-( '+ data.errors );
                    }
                },
                error:function (jqXHR, exception) {
                    let errores = "";
                    obj = jqXHR.responseJSON.errors;
                    if (null != obj) {
                        Object.keys(obj).forEach(function(key) {
                          errores += obj[key];
                        })
                    }
                    document.getElementById("guardarTarea").disabled=false;
                    $('#modalTarea').modal('hide');
                    bootbox.alert('Ha ocurrido un error, tal vez faltaron datos :-( '+ errores );
                }
            });
        });

        
        // Ver servicio
        $(document).on('click', '.ver-servicio', function() {
            $('#modalServicio').modal('show');
            let id = $(this).attr('serv');
            let tipoServ = $(this).attr('id-serv');
            $('.titulito').html("Código servicio: " + $(this).attr('cod') + "<br> Cliente: " + $(this).attr('cli'));
            var nombrecliene=$(this).attr('cli');
            let url = "{{ url('/servicios') }}/"+id;

            $.ajax({
                url: url,
                type: 'GET',
                success: function(data) {
                    console.log(data)
                    console.log('pagos: '+data.Ultima2);
                    Object.keys(data).forEach(key => $('.'+key).html(data[key]))

                    if((data.Ultima2 === null)) //si tiene pagada su factura
                    {
                        var pago="<span class='text-danger'>No</span>"

                        $('.dtepagado').html(pago);
                    }
                    else
                        {
                        var pago="<span class='text-info'>Pagado el "+data.Ultima2["FechaPago"]+"</span>";


                        $('.dtepagado').html(pago);
                        }
                     if (data.Ultima === null){  // si tiene factura
                         $('.ultimodte').html('No Tiene');
                         $('.fechaultimodte').html("No Tiene");
                     }
                     else{

                         $('.ultimodte').html('<a href="'+data.Ultima["UrlPdfBsale"]+'" target="_blank"><i  class="fa fa-file-pdf-o"></i>  '+data.Ultima["NumeroDocumento"]+'</a>');
                         $('.fechaultimodte').html(data.Ultima["FechaFacturacion"]);

                     }
                         $('.rvNombreCliente').html(nombrecliene);
                        $('.tipos').hide();
                        $('.serv-'+tipoServ).show()

                },
                error:function (jqXHR, exception) {
                    bootbox.alert('Error al traer los datos' );
                }
            });
        });


        $('body').on('focus', ".date", function() {
            $('.date').datepicker({
                format: "dd-mm-yyyy",
                weekStart: 1,
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            });
        });

        $('body').on('change', "#IdUsuarioAsignado", function() {
            document.getElementById("grabarTecnico").disabled=false;
        });

        
        $('#Activo').on('change', function() {
            n =  new Date(); y = n.getFullYear(); m = ("0" + (n.getMonth() + 1)).slice(-2); d = n.getDate();
            if (($(this).val() == "5" ) || ($(this).val() == "2")) {
                $('#divFechaActivacion').show()
                $('input[name="FechaInicioDesactivacion"]').attr('validate', 'not_null');
                $('#FechaInicioDesactivacion').val(y + "-" + m + "-" + d);
                $('#FechaFinalDesactivacion').attr('validate', 'not_null');
                document.getElementById('FechaFinalDesactivacion').disabled = false; 
            } else if (($(this).val() == "0" ) || ($(this).val() == "3")) {
                $('#divFechaActivacion').show()
                $('#FechaInicioDesactivacion').val(y + "-" + m + "-" + d);
                $('#FechaFinalDesactivacion').val(''); 
                document.getElementById('FechaFinalDesactivacion').disabled = true; 
            } else {
                $('#divFechaActivacion').hide()
                $('input[name="FechaInicioDesactivacion"]').val('')
                $('input[name="FechaFinalDesactivacion"]').val('')
            }
        });

        $(document).on('click', '.estado-servicio', function() {
            $('#modalEstatus').modal('show');
            $('.Codigo').html($(this).attr('cod'));
            $('#Id').val($(this).attr('attr'));
            $("#Activo").val($(this).attr('status')).change();
            $('#FechaInicioDesactivacion').val($(this).attr('finid'));
            $('#FechaFinalDesactivacion').val($(this).attr('ffind'));
        });

        $('body').on('click', '#updateEstatus', function() {
            document.getElementById("updateEstatus").disabled = true;
            let id = $('#Id').val();
            let url = "{{ url('/servicios') }}/"+id
            let csrf_token = "{{ csrf_token() }}";
            var form = {
                _method: "PATCH",
                _token: csrf_token,
                cambioEstado: $('#Activo').val(),
                fechaIniDes: $('#FechaInicioDesactivacion').val(),
                fechaFinDes: $('#FechaFinalDesactivacion').val(),
                correo: "1"
            };

            $.ajax({
                url: url,
                type: 'PATCH',
                data: form,
                success: function(response) {
                    var data = JSON.parse(response)
                    if (( data.estado != null) && (data.estado == "OK")) {
                        $('#modalEstatus').modal('hide');
                        bootbox.alert('Estado cambiado correctamente . E-mail enviado a ' + data.email);
                        setTimeout('location.reload()', 3000);
                    } else {
                        bootbox.alert('Ha ocurrido un error :-(  No se cambió estado');
                    }
                    document.getElementById("updateEstatus").disabled = false;
                },
                error:function (jqXHR, exception) {
                    let errores = "";
                    obj = jqXHR.responseJSON.errors;
                    Object.keys(obj).forEach(function(key) {
                      errores += obj[key];
                    })
                    bootbox.alert('Ha ocurrido un error :-( '+ errores );
                    document.getElementById("updateEstatus").disabled = false;
                }
            });
        });




    </script>

@endsection

