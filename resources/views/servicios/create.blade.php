@extends('layouts.app')
@section('title', ' Clientes - Servicios')
@section('content')

<!--Estilos para que el select no pise al encabezado del datatables-->
<style type="text/css"> 
	.filter-option{ font-size: 12px !important;}
	#tabla_ver_servicios_length, #tabla_ver_servicios_info, .margencito {margin-left: 200px !important;}
	.bordecito{border-color:green; border-style:solid; border-width:1px}
	.img-thumbnail{background-color: #eafaf1 }
</style>
<link href="{!! asset('css/plugins/select2/select2.min.css') !!}" rel="stylesheet" />
<!-- ASSETS SELECT2 HERE ;-)   
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
-->

<?php
	$id_usuario = @Auth::user()->id;
	//require_once('../public/class/methods_global/Method.php'); 
?>
<div class="boxed">
	<div id="content-container">
		<div class="col-md-4">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Servicios</a>
                </li>
                <li class="active">
                    <strong>Crear</strong>
                </li>
            </ol>
        </div>
        <div class="col-md-7 img-thumbnail">
            <ul> <b> @php echo count($pendientes); @endphp Servicios pendientes de Activación: </b>
            @php $hoy = date('Y-m-d'); @endphp
            @foreach ($pendientes as $row)
                @if ( $row->FechaComprometidaInstalacion < $hoy )
                    <li class="text-danger"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                @elseif ( $row->FechaComprometidaInstalacion == $hoy )
                    <li class="text-warning"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                @else
                    <li class="text-success"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} -- {{ $row->Codigo }}
                @endif                    
            @endforeach 
            </ul>
        </div>
		<div id="page-content">
			<div class="row">
				<div class="col-md-5 bordecito">
					<div class="panel ">
						<!--Panel heading-->
						<div class="panel-heading">
							<h3 class="panel-title">Módulo crear servicios</h3>
						</div>
						<!--Panel body-->
						<div class="panel-body container-form">
							 <div class="row">
                                @if (count($errors)>0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
							<div class="row">
								<form id="formServicio" name="frmServicio" method="POST" action="{{ route('servicios.store') }}">
									@csrf
									<div class="col-md-12 form-group">
										<label class="campo-cliente">*Cliente</label>
										<div class="input-group campo-cliente">
											<select id="Rut" name="Rut" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Cliente">
												<option value="">-- Seleccione --</option>
													@foreach ($select_clientes as $row )
	                                                    <option class="text" nombre="{{$row['nombre']}}" iid="{{$row['id']}}" tipo="{{$row['tipo_cliente']}}" value="{{$row['rut']}}" fono="{{$row['telefono']}}" contacto="{{$row['contacto']}}" {{ old('Rut') == $row['rut'] ? 'selected' : '' }}>{{$row['rut']." - ".$row['nombre']." - ".$row['tipoClientes']['nombre']}}</option>
														}
	                                                @endforeach
											</select>
											<span class="input-group-btn">
												<a href="{{ route('clientes.create') }}" type="button" class="btn btn-primary" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i></a>
											</span>
											<input type="hidden" name="usuario" value="<?php echo $id_usuario?>"/>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label class="compo-grupo">*Grupo</label>
										<div class="input-group compo-grupo">
											<select name="Grupo" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Grupo"> 
												@foreach ($grupos as $row )
                                                    <option value="{{$row->IdGrupo}}" {{ old('Grupo') == $row->IdGrupo ? 'selected' : '' }}>{{$row->Nombre}}</option>
                                                @endforeach
											</select>
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#grupo"><i class="fa fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<!-- aqui -->
										<label class="campo-cobreServicio">*Tipo de Cobro de servicio</label>
										<div class="input-group campo-cobreServicio">
											<select id="TipoFactura" name="TipoFactura" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Tipo de Cobro">
												<option value="">Seleccione...</option>
												@foreach ($tipo_cobro as $row )
                                                    <option value="{{$row->id}}" {{ old('TipoFactura') == $row->id ? 'selected' : '' }}>{{$row->descripcion}}</option>
                                                @endforeach
											</select>
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalTipoFacturacion"><i class="fa fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
									</div>

									<div id="divFechaActivacionTMP" style="display:none;">
										<div class="col-md-12 form-group callout callout-warning">
											<label>Duración del servicio</label>
											<div class="form-group">
												<div id="date-range">
													<div class="input-daterange input-group">
														<input type="date" class="form-control" id="FechaInicioDesactivacionTMP" name="FechaInicioDesactivacionTMP" data-nombre="Fecha de Activación"/>
														<span class="input-group-addon">a</span>
														<input type="date" class="form-control" id="FechaFinalDesactivacionTMP" name="FechaFinalDesactivacionTMP" data-nombre="Fecha de Activación"/>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-7 form-group">
										<div class="campo-servicio">
											<label>*Tipo de Servicio</label>
											<select name="IdServicio" id="TipoServicio" class="form-control selectpicker" id-serv="{{old('IdServicio')}}" data-live-search="true" validate="not_null" data-nombre="Servicio">
												<option value="">Seleccione...</option>
												@foreach ($tipo_servicio as $key => $row )
                                                    <option value="{{$key}}" {{ old('IdServicio') == $key ? 'selected' : '' }}>{{$row}}</option>
                                                @endforeach
											</select>
										</div>
									</div>

									<div class="col-md-4 form-group">
										<label class="campo-Valor">*Valor</label>
										<div class="form-group">
											<input type="number" id="Valor" name="Valor" class="form-control" validate="not_null" data-nombre="Valor" value="{{old('Valor')}}" min="0.0" max="1000.0" step="0.1">
										</div>
									</div>
									<div class="col-md-1 form-group">
										<label class="campo-Valor">....</label>
										<p><b>UF</b></p>
									</div>

									<div class="col-md-12 containerTipoServicioFormulario"></div>

									

									<div id="otrosServicios" class="col-md-12 callout callout-success" {{ ((old('IdServicio') != null) && ((old('IdServicio') == "1") || (old('IdServicio') == "2"))) ? '' : 'style=display:none' }} >
										<div class="col-md-12 form-group">
											<label class="campo-Conexiones">Alias del Servicio o Conexión </label>
											<div class="form-group">
												<input type="text" name="Conexion" class="form-control campo-Conexiones" value="{{old('Conexion')}}">
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-direccion">Dirección (de la antena)</label>
											<textarea name="Direccion" class="form-control campo-direccion" rows="3"> {{old('Direccion')}}</textarea>
											<br class="campo-direccion">
										</div>

										<div class="col-md-6 campo-cordenadas">
											<div class="form-group">
												<label class="control-label" for="Latitud">*Coordenadas</label>
												<input id="Latitud" name="Latitud" type="text" placeholder="Ingrese la latitud" class="form-control input-sm coordenadas" value="{{ (null !== old('Latitud')) ? old('Latitud') : '-41.3214705' }}">
											</div>
										</div>
										<div class="col-md-6 campo-cordenadas">
											<div class="form-group">
												<label class="control-label" for="name">&nbsp;</label>
												<input id="Longitud" name="Longitud" type="text" placeholder="Ingrese la longitud" class="form-control input-sm coordenadas" value="{{ (null !== old('Latitud')) ? old('Latitud') : '-73.0138898' }}">
											</div>
										</div>

										<div class="col-md-12 form-group">
											<div id="Map" style="height:350px; width:100%;" class="campo-cordenadas"></div>
										</div>

										<div class="col-md-12 form-group">
											<label class="campo-contacto">*Contacto</label>
											<div class="form-group campo-contacto">
												<input type="text" name="Contacto" id="cli-cont" class="form-control" value="{{old('Contacto')}}">
											</div>
											<!--<br class="campo-contacto"> -->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-telefonoContacto">*Fono Contacto</label>
											<div class="form-group campo-telefonoContacto">
												<input type="text" name="Fono" id="cli-fono" class="form-control" value="{{old('Fono')}}">
											</div>
											<!--<br class="campo-telefonoContacto"> -->
										</div>
										<div class="col-md-12 form-group">
											<label>*Fecha Comprometida de Instalación</label>
											<div class="form-group campo-FechaComprometidaInstalacion">
												<input type="date" name="FechaComprometidaInstalacion" class="form-control" value="{{old('FechaComprometidaInstalacion')}}">
											</div>
											<!--<br class="campo-FechaComprometidaInstalacion">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-estacionReferencia">*Estaciones de Referencia</label>
											<div class="form-group campo-estacionReferencia">
												<select name="PosibleEstacion" id="PosibleEstacion" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Posible Estacion">
				                                    <option class="seleccione" value="">Seleccione...</option>
				                                        @foreach ($estaciones as $row )
				                                            <option value="{{$row->nombre}}" {{ old('PosibleEstacion') == $row->nombre ? 'selected' : '' }}>{{$row->nombre}}</option>
				                                        @endforeach
				                                </select>
											</div>
											<!--<br class="campo-estacionReferencia">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-usuarioPPPoE">*Posible Usuario PPPoE</label>
											<div class="form-group campo-usuarioPPPoE">
												<input type="text" name="UsuarioPppoeTeorico" id="cli-ppoe" class="form-control text-right" value="{{old('UsuarioPppoeTeorico')}}">
											</div>
											<!--<br class="campo-usuarioPPPoE">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-señalTeorica">*Señal Teorica</label>
											<div class="form-group campo-señalTeorica">
												<input type="text" name="SenalTeorica" class="form-control" value="{{old('SenalTeorica')}}">
											</div>
										</div>
									</div>

									<div class="col-md-12 form-group">
										<label class="campo-señalTeorica">*Facturación Costo de instalación / Habilitación</label>
										<div class="form-group campo-señalTeorica">
											<select id="BooleanCostoInstalacion" name="BooleanCostoInstalacion" class="form-control selectpicker">
												<option value="1" {{ old('BooleanCostoInstalacion') === 1 ? 'selected' : '' }}>Si</option>
												<option value="0" {{ old('BooleanCostoInstalacion') === 0 ? 'selected' : '' }}>No</option>
											</select>
										</div>
									</div>

									<div id="divCostoInstalacion">
										<div class="col-md-7 form-group">
											<label class="campo-CostoInstalacion">Costo de instalación / Habilitación</label>
											<div class="input-group">
												<input type="text" id="CostoInstalacion" name="CostoInstalacion" class="form-control number" validate="not_null" data-nombre="Costo de Instalacion" value="{{old('CostoInstalacion')}}">
												<input type="hidden" id="CostoInstalacionIva" name="CostoInstalacionIva" class="form-control number" value="{{old('CostoInstalacionIva')}}">
												<span class="input-group-addon" id="CostoInstalacionPesos">0</span>
											</div>
										</div>
										
										<div class="form-group col-md-5">
											<label class="control-label" for="moneda"><stong>Moneda</stong></label>
											<select class="selectpicker form-control" name="tipo_moneda" id="moneda" data-live-search="true" data-container="body" validate="not_null" data-nombre="Moneda">
												<option value="1" {{ old('tipo_moneda') == 1 ? 'selected' : '' }}>Pesos</option>
												<option value="2" {{ old('tipo_moneda') == 2 ? 'selected' : '' }}>UF</option>
											</select>
										</div>
										<div class="col-md-12 form-group">
											<label> Descripción del Servicio | Adjunto en el Doc enviado al SII</label>
											<textarea id="text-descr" name="DescripcionNV" class="form-control" rows="3" placeholder="Descripción del Servicio | Adjunto en el Doc enviado al sii">{{old('Descripcion')}}</textarea>
										</div>
									</div>

									<div class="col-md-12 form-group">
										<label>Técnico Asignado</label>
										<select class="form-control" name="IdUsuarioAsignado" id="IdUsuarioAsignado"  data-live-search="true" data-container="body">
											<option class="seleccione" value="">Seleccione...</option>
								            @foreach ($tecnicos as $row )
								                <option value="{{$row->id}}" {{ old('IdUsuarioAsignado') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
								            @endforeach
								        </select>
								    </div>
									<br>
									<button type="submit" class="btn btn-primary guardarServ">Guardar</button>
									
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7 table-responsive bordecito">
					<h3 class="panel-title margencito">Servicios registrados</h3>
					
					<div class="col-md-12">
						<h3 class="margencito">
							<i title="Descargar servicios" style="cursor: pointer;" class="fa fa-file-excel-o" id="fa-file-excel-o"></i>
						</h3>
						<div class="col-md-12 dataServicios" id="tab-Servicios" style="min-width: 1000px">
							@include('clientesServicios.tablaServicios')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('servicios.modales.modalStatusServicio')
<!--<div class="modal fade" tabindex="-1" role="dialog" id="verServicios" aria-labelledby="verServicios">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lista de Datos técnicos</h4>
			</div>
			<div class="modal-body containerListDatosTecnicos">
			</div>
		</div>
	</div>
</div>-->
<div class="modal fade" id="agregarDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Agregar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary guardarDatosTecnicos">Guardar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="ModalDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Actualizar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary actualizarDatosTecnicos">Actualizar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="ModalTipoFacturacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content containerTipoFactura">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Tipo de Facturación</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Código</label>
						<input name="TipoFacCodigo" class="form-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Descripción</label>
						<input name="TipoFacDescripcion" class="form-control">
						<input type="hidden" name="TipoCliente" id="getTipoDoc" class="form-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Tiempo de Facturación</label>
						<select id="TipoFacturacion" name="TipoFacturacion" class="form-control selectpicker" data-live-search="true">
							<option value="1">Mensual</option>
							<option value="2">Semestral</option>
							<option value="3">Anual</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary agregarTipoFacturacion">Guardar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="grupo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Grupo</h4>
			</div>
			<div class="modal-body containerGrupo">
				<div class="row">
					<div class="col-md-12 form-group">
						<label>Nombre del grupo</label>
						<input name="NomGrupo" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary agregarGrupo">Guardar</button>
			</div>
		</div>
	</div>
</div>
@include('servicios.show')




@endsection

@section('mijava')

	<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>

	<script src="{!! asset('js/methods_global/mapa.js?v=1389884346') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1247385395') !!}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap"></script>
	<!-- <script src="{!! asset('js/servicios/controller.js?v=19080736') !!}"></script>


	<script src="{!! asset('js/methods_global/mapa.js?v=1580805263') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1601188581') !!}"></script>
	<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap">
	</script>
	<script src="{!! asset('js/servicios/controller.js?v=2070837879') !!}"></script>
-->
	<script src="{!! asset('js/swalExtend.js') !!}"></script>
<script type="text/javascript">

	$(document).ready(function() {
        var id_serv = $('#TipoServicio').attr('id-serv');
        if ((id_serv !== null) && (id_serv > 0)) {
        	$('#Rut').trigger('change');
        	$('#TipoServicio').trigger('change');
        }
    });

	//ver los contactos del cliente
    $(document).on('change', '#Rut', function() {
        // parametros para armar la data table
        var id = $('option:selected', this).attr('iid');
        var tipo = $('option:selected', this).attr('tipo');
        var rut = $('option:selected', this).val();
        var nombre = $('option:selected', this).attr('nombre');
        var url = "{{ url('/clientes/servicios') }}/"+id;
        tablita = $('#tabla_ver_servicios').DataTable({
        	"autoWidth": false,
        	"destroy": true,
        	"paging": true,
        	"pageLength": 5,
        	"lengthMenu": [5, 10, 25, 50, 75, 100 ],
    		"searching": true,
	        "processing": true,
	        "serverSide": false,
	        "order": [[4, 'desc']],
	        
	        "dom": '<"html5buttons"B>lTfgitp',
	        "ajax": url,
	        "columns": [
	                    { data: 'Codigo', name: 'Codigo', width: '14% !important' },
	                    { data: 'Conexion', name: 'Conexion', width: '8% !important'},
	                    { data: 'Valor', name: 'Valor', width: '8% !important'},
	                    { data: 'Grupo', name: 'Grupo', width: '8% !important'},
	                    { data: 'Fecha', name: 'Fecha', width: '10% !important'},
	                    { data: 'Estado', name: 'Estado', width: '8% !important'},
	                    { data: 'Tipo', name: 'Tipo', width: '22% !important'},
	                    { data: 'Velocidad', name: 'Velocidad', width: '6% !important'},
	                    { data: 'Plan', name: 'Plan', width: '6% !important'},
	                    { data: 'action', name: 'action', orderable: false, searchable: false, width: '10% !important'}
	                 ],
	        "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });

	    $('#getTipoDoc').val(tipo);
        setTimeout(() => { $('#getTipoDoc').selectpicker('refresh'); }, 1000);
        $("#servicio_rut_dv").val(rut);
        $("#servicio_nombre_cliente").val(nombre);
        $('#TipoFactura').load( "{{ url('/tipo_factura/select') }}/"+tipo , function(data) { $('#TipoFactura').selectpicker('refresh'); });
    });


    $('#BooleanCostoInstalacion').change(function(event) {
    	$('#CostoInstalacion').val("0");
    	if ($(this).val() == "1") {
    		$('#divCostoInstalacion').show();
    	} else {
    		$('#CostoInstalacion').val("");
    		$('#text-descr').val("");
    		$('#divCostoInstalacion').hide('slow');
    	}
    	setTimeout(() => { $('#BooleanCostoInstalacion').selectpicker('refresh'); }, 1000);
    });


    $('#TipoServicio').change(function(event) {

        Latitud = $('#Latitud').val()
        Longitud = $('#Longitud').val()
        $('.tipo-serv').hide();
        var tipo_servicio = $(this).val();
        switch (tipo_servicio) {
            case '1': 	//Arriendo de Equipos de Datos
                url = "arriendoEquipos";
                //$('#arriendoEquipos').show();
                $('#otrosServicios').show();
                $('#cli-cont').val($('#Rut option:selected').attr('contacto'));
                $('#cli-fono').val($('#Rut option:selected').attr('fono'));
                $('#cli-ppoe').val('@teledata');
                $('#BooleanCostoInstalacion').val('1');
                $('#BooleanCostoInstalacion').trigger('change');
                break;
            case '2': 	//Servicio de Internet
                url = "servicioInternet";
                //$('#servicioInternet').show();
                $('#otrosServicios').show();
                $('#cli-cont').val($('#Rut option:selected').attr('contacto'));
                $('#cli-fono').val($('#Rut option:selected').attr('fono'));
                $('#cli-ppoe').val('@teledata');
                $('#BooleanCostoInstalacion').val('1');
                $('#BooleanCostoInstalacion').trigger('change');
                break;
            case '3':  	//Servicio de Puertos Públicos
                url = "mensualidadPuertoPublicos";
                //$('#mensualidadPuertoPublicos').show();
                $('#BooleanCostoInstalacion').val('0');
                $('#BooleanCostoInstalacion').trigger('change');
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
                break;
            case '4': 	//Servicio de IP Pública
                url = "mensualidadIPFija";
                //$('#mensualidadIPFija').show();
                $('#BooleanCostoInstalacion').val('0');
                $('#BooleanCostoInstalacion').trigger('change');
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
                break;
            case '5': 	//Servicio de Mantención de Red
                url = "mantencionRed";
                //$('#mantencionRed').show();
                $('#BooleanCostoInstalacion').val('0');
                $('#BooleanCostoInstalacion').trigger('change');
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
                break;
            case '6':  	//Arriendo de equipos de Telefonía IP
                url = "traficoGenerado";
                //$('#traficoGenerado').show();
                $('#BooleanCostoInstalacion').val('0');
                $('#BooleanCostoInstalacion').trigger('change');
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
                break;
            case '7': 	//Otros Servicios
                url = "otroServicio";
               // $('#otroServicio').show();
                $('#BooleanCostoInstalacion').val('1');
                $('#BooleanCostoInstalacion').trigger('change');
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
                break;
            default:
                url = "404.html";
                $('#otrosServicios').hide();
                $('#otrosServicios').find('input').val('');
        }

        $('#Latitud').val(Latitud)
        $('#Longitud').val(Longitud)
        
        /*if (Latitud && Longitud) {
            Resize(Latitud, Longitud)
        }*/

        @if (null != old('productos'))
        	@php $prods = "";
	        	foreach (old('productos') as  $value) :
	                if ($prods == ""):
	                    $prods .= $value;
	                else:
	                    $prods .= ",".$value;
	                endif;
	            endforeach;
            @endphp
        	var url2 = "{{ url('/servicios/tipo_servicio') }}/"+url+"/"+"{{$prods}}";
        @else
        	var url2 = "{{ url('/servicios/tipo_servicio') }}/"+url;
        @endif
        
    	$.get(url2, function(data){
            $('.containerTipoServicioFormulario').empty().append(data);
            if ((tipo_servicio == 1) || (tipo_servicio == 2)){
	            $("#producto_id").select2({
				    tags: true,
				    tokenSeparators: [',', ' ']
				});
			}
        });

        @if (null != old('planes_id'))
        	setTimeout(() => { $("#planes_id").val({{old('planes_id')}}).change();}, 2000);
        @endif

    });

	$('#cli-ppoe').on('blur', function(event) {
        var camo = this;
        var form = {
        	ppoe: $(this).val(),
            _token: "{{ csrf_token() }}"
        };

        $.ajax({
            type: "POST",
            data: form,
            url: "{{ url('/servicios/ppoe') }}",
            success: function(response) {
                if (response == "true") {
	                $(camo).parent('.form-group').addClass('has-error');
	                $(camo).val('@teledata');
	                bootbox.alert('<h3 class="text-center">El usuario Pppoe ya esta registrado.</h3>');
	            } else {
	            	$(camo).parent('.form-group').addClass('has-success');
	            }
            }
        });
    });

    //para cuando seleccione tipo de fac temporal
    $('#TipoFactura').change(function(event) {
        var tipodocumento = $(this).val();
        if (tipodocumento == 25 || tipodocumento == 26) {    
            $('#divFechaActivacionTMP').show();
            $('input[name="FechaInicioDesactivacionTMP"]').attr('validate', 'not_null');
            $('input[name="FechaFinalDesactivacionTMP"]').attr('validate', 'not_null');
        } else {
            $('#divFechaActivacionTMP').hide();
            $('input[name="FechaInicioDesactivacionTMP"]').val('');
            $('input[name="FechaFinalDesactivacionTMP"]').val('');
            $('input[name="FechaInicioDesactivacionTMP"]').removeAttr('validate');
            $('input[name="FechaFinalDesactivacionTMP"]').removeAttr('validate');
        }
    });

   /* $('body').on('focus', ".datepicker", function() {
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 1,
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true,
            language: 'es'
        });
    });*/

    $('#Activo').on('change', function() {
    	n =  new Date(); y = n.getFullYear(); m = ("0" + (n.getMonth() + 1)).slice(-2); d = n.getDate();
        if (($(this).val() == "5" ) || ($(this).val() == "2")) {
        	$('#divFechaActivacion').show()
        	$('#FechaInicioDesactivacion').attr('validate', 'not_null');
        	$('#FechaInicioDesactivacion').val(y + "-" + m + "-" + d);
            $('#FechaFinalDesactivacion').attr('validate', 'not_null');
            document.getElementById('FechaFinalDesactivacion').disabled = false; 
        } else if (($(this).val() == "0" ) || ($(this).val() == "3")) {
            $('#divFechaActivacion').show()
            $('#FechaInicioDesactivacion').val(y + "-" + m + "-" + d);
            $('#FechaFinalDesactivacion').val(''); 
            document.getElementById('FechaFinalDesactivacion').disabled = true; 
        } else {
            $('#divFechaActivacion').hide()
            $('input[name="FechaInicioDesactivacion"]').val('')
            $('input[name="FechaFinalDesactivacion"]').val('')
        }
    });

    $(document).on('click', '.estado-servicio', function() {
        $('#modalEstatus').modal('show');
        $('.Codigo').html($(this).attr('cod'));
        $('#Id').val($(this).attr('attr'));
        $("#Activo").val($(this).attr('status')).change();
        $('#FechaInicioDesactivacion').val($(this).attr('finid'));
        $('#FechaFinalDesactivacion').val($(this).attr('ffind'));
    });

    $('body').on('click', '#updateEstatus', function() {
        document.getElementById("updateEstatus").disabled = true;
        let id = $('#Id').val();
        let url = "{{ url('/servicios') }}/"+id
        let csrf_token = "{{ csrf_token() }}";
        var form = {
            _method: "PATCH",
            _token: csrf_token,
            cambioEstado: $('#Activo').val(),
            fechaIniDes: $('#FechaInicioDesactivacion').val(),
            fechaFinDes: $('#FechaFinalDesactivacion').val(),
            correo: "1"
        };

        $.ajax({
            url: url,
            type: 'PATCH',
            data: form,
            success: function(response) {
                var data = JSON.parse(response)
                if (( data.estado != null) && (data.estado == "OK")) {
                    $('#modalEstatus').modal('hide');
                    bootbox.alert('Estado cambiado correctamente . E-mail enviado a ' + data.email);
                    setTimeout('location.reload()', 3000);
                } else {
                    bootbox.alert('Ha ocurrido un error :-(  No se cambió estado');
                }
                document.getElementById("updateEstatus").disabled = false;
            },
            error:function (jqXHR, exception) {
                let errores = "";
                obj = jqXHR.responseJSON.errors;
                Object.keys(obj).forEach(function(key) {
                  errores += obj[key];
                })
                bootbox.alert('Ha ocurrido un error :-( '+ errores );
                document.getElementById("updateEstatus").disabled = false;
            }
        });
    });


    // Ver servicio
    $(document).on('click', '.ver-servicio', function() {
        $('#modalServicio').modal('show');
        let id = $(this).attr('attr');
        let tipoServ = $(this).attr('id-serv');
        $('.titulito').html("Código servicio: " + $(this).attr('cod'));
        let url = "{{ url('/servicios') }}/"+id;

        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                Object.keys(data).forEach(key => $('.'+key).html(data[key]))
                $('.tipos').hide();
                $('.serv-'+tipoServ).show();
            },
            error:function (jqXHR, exception) {
                bootbox.alert('Error al traer los datos' );
            }
        });
    });

</script>
@endsection


