@extends('layouts.app')
@section('title', ' Clientes - Servicios')
@section('content')

<!--Estilos para que el select no pise al encabezado del datatables-->
<style type="text/css"> 
	.filter-option{ font-size: 12px !important;}
	#tabla_ver_servicios_length, #tabla_ver_servicios_info, .margencito {margin-left: 200px !important;}
	.bordecito{border-color:green; border-style:solid; border-width:1px}
	.img-thumbnail{background-color: #eafaf1 }
</style>
<link href="{!! asset('css/plugins/select2/select2.min.css') !!}" rel="stylesheet" />
<!-- ASSETS SELECT2 HERE ;-)   
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
-->

<?php
	$id_usuario = @Auth::user()->id;
	//require_once('../public/class/methods_global/Method.php'); 
?>
<div class="boxed">
	<div id="content-container">
		<div class="col-md-4">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Servicios</a>
                </li>
                <li class="active">
                    <strong>Editar</strong>
                </li>
            </ol>
        </div>
		<div id="page-content">
			<div class="row">
				<div class="panel ">
					<!--Panel heading-->
					<div class="panel-heading">
						<h3 class="panel-title">Módulo Editar servicios</h3>
					</div>
					<!--Panel body-->
					<div class="panel-body container-form">
						<div class="row">
                            @if (count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
						<div class="row">
							<form id="formServicio" name="frmServicio" method="POST" action="{{ route('servicios.update', $servicio->Id) }}">
								@csrf
								@method('PUT')
								<div class="col-md-12 form-group">
									<h4>Código : {{$servicio->Codigo}}  &nbsp;&nbsp;&nbsp;&nbsp;Tipo de Servicio : {{ @$servicio->tipo_servicios->servicio }}</h4>

									<h4 class="campo-cliente">Cliente : {{$cliente->nombre}}</h4>
									<div class="input-group campo-cliente">
										<input type="hidden" name="Rut" value="<?php echo $servicio->Rut?>"/>
										<input type="hidden" name="IdServicio" value="<?php echo $servicio->IdServicio?>"/>
										<input type="hidden" name="usuario" value="<?php echo $id_usuario?>"/>
									</div>
								</div>
								<div class="col-md-3 form-group">
									<label class="compo-grupo">*Grupo</label>
									<div class="input-group campo-grupo">
										<select name="Grupo" class="form-control" data-live-search="true" validate="not_null" data-nombre="Grupo"> 
											@foreach ($grupos as $row )
                                                <option value="{{$row->IdGrupo}}" {{ $servicio->Grupo == $row->IdGrupo ? 'selected' : '' }}>{{$row->Nombre}}</option>
                                            @endforeach
										</select>
									</div>
								</div>
								<div class="col-md-5 form-group">
									<!-- aqui -->
									<label class="campo-cobreServicio">*Tipo de Cobro de servicio</label>
									<div class="input-group campo-cobreServicio">
										<select id="TipoFactura" name="TipoFactura" class="form-control" data-live-search="true" validate="not_null" data-nombre="Tipo de Cobro">
											<option value="">Seleccione...</option>
											@foreach ($tipo_cobro as $row )
                                                <option value="{{$row->id}}" {{ $servicio->TipoFactura == $row->id ? 'selected' : '' }}>{{$row->descripcion}}</option>
                                            @endforeach
										</select>
									</div>
								</div>

								@if (($servicio->IdServicio == 2) || ($servicio->IdServicio == 5))
									<div id="divFechaActivacionTMP">
										<div class="col-md-4 form-group callout callout-warning">
											<label>Duración del servicio</label>
											<div class="form-group">
												<div id="date-range">
													<div class="input-daterange input-group">
														<input type="date" class="form-control" value="{{$servicio->FechaInicioDesactivacion}}" name="FechaInicioDesactivacion" data-nombre="Fecha de Activación"/>
														<span class="input-group-addon">a</span>
														<input type="date" class="form-control" value="{{$servicio->FechaFinalDesactivacion}}" name="FechaFinalDesactivacion" data-nombre="Fecha de Activación"/>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif

								<div class="col-md-2 form-group">
									<label class="campo-Valor">*Valor</label>
									<div class="form-group">
										<input type="number" id="Valor" name="Valor" class="form-control" validate="not_null" data-nombre="Valor" value="{{ old('Valor') ?? $servicio->Valor ?? 'default' }}" min="0.0" max="1000.0" step="0.1">
									</div>
								</div>
								<div class="col-md-1 form-group">
									<label class="campo-Valor">....</label>
									<p><b>UF</b></p>
								</div>

								<div class="col-md-12">
									@if ($servicio->IdServicio == 1)
										@include('clientesServicios.arriendoEquipos')
									@elseif ($servicio->IdServicio == 2)
										@include('clientesServicios.servicioInternet')
									@elseif ($servicio->IdServicio == 3)
										@include('clientesServicios.mensualidadPuertoPublicos')
									@elseif ($servicio->IdServicio == 4)
										@include('clientesServicios.mensualidadIPFija')
									@elseif ($servicio->IdServicio == 5)
										@include('clientesServicios.mantencionRed')
									@elseif ($servicio->IdServicio == 6)
										@include('clientesServicios.traficoGenerado')
									@elseif ($servicio->IdServicio == 7)
										@include('clientesServicios.otroServicio')
									@endif
								</div>

								@if (($servicio->IdServicio == 1) || ($servicio->IdServicio == 2))

									<div class="col-md-12 callout callout-success">
										<div class="col-md-6 form-group">
											<div class="col-md-12 form-group">
												<label class="campo-Conexiones">Alias del Servicio o Conexión </label>
												<div class="form-group">
													<input type="text" name="Conexion" class="form-control campo-Conexiones" value="{{ old('Conexion') ?? $servicio->Conexion ?? 'default' }}">
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label class="campo-direccion">Dirección (de la antena)</label>
												<textarea name="Direccion" class="form-control campo-direccion" rows="3"> {{ old('Direccion') ?? $servicio->Direccion ?? 'default' }}</textarea>
												<br class="campo-direccion">
											</div>
											<div class="col-md-6 campo-cordenadas">
												<div class="form-group">
													<label class="control-label" for="Latitud">*Coordenadas</label>
													<input id="LatitudEdit" name="Latitud" type="text" placeholder="Ingrese la latitud" class="form-control input-sm coordenadas" value="{{ old('Latitud') ?? $servicio->Latitud ?? 'default' }}">
												</div>
											</div>
											<div class="col-md-6 campo-cordenadas">
												<div class="form-group">
													<label class="control-label" for="name">&nbsp;</label>
													<input id="LongitudEdit" name="Longitud" type="text" placeholder="Ingrese la longitud" class="form-control input-sm coordenadas" value="{{ old('Longitud') ?? $servicio->Longitud ?? 'default' }}">
												</div>
											</div>
										</div>

										<div class="col-md-6 form-group">
											<div id="MapEdit" style="height:350px; width:100%;" class="campo-cordenadas"></div>
										</div>

										<div class="col-md-8 form-group">
											<label class="campo-contacto">*Contacto</label>
											<div class="form-group campo-contacto">
												<input type="text" name="Contacto" id="cli-cont" class="form-control" value="{{ old('Contacto') ?? $servicio->Contacto ?? 'default' }}">
											</div>
											<!--<br class="campo-contacto"> -->
										</div>
										<div class="col-md-4 form-group">
											<label class="campo-telefonoContacto">*Fono Contacto</label>
											<div class="form-group campo-telefonoContacto">
												<input type="text" name="Fono" id="cli-fono" class="form-control" value="{{ old('Fono') ?? $servicio->Fono ?? 'default' }}">
											</div>
											<!--<br class="campo-telefonoContacto"> -->
										</div>
										<div class="col-md-4 form-group">
											<label>*Fecha Comprometida de Instalación</label>
											<div class="form-group campo-FechaComprometidaInstalacion">
												<input type="date" name="FechaComprometidaInstalacion" class="form-control" value="{{ old('FechaComprometidaInstalacion') ?? $servicio->FechaComprometidaInstalacion ?? 'default' }}">
											</div>
											<!--<br class="campo-FechaComprometidaInstalacion">-->
										</div>
										<div class="col-md-3 form-group">
											<label class="campo-estacionReferencia">*Estaciones de Referencia</label>
											<div class="form-group campo-estacionReferencia">
												<select name="PosibleEstacion" id="PosibleEstacion" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Posible Estacion">
				                                    <option class="seleccione" value="">Seleccione...</option>
				                                        @foreach ($estaciones as $row )
				                                            <option value="{{$row->nombre}}" {{ $servicio->PosibleEstacion == $row->nombre ? 'selected' : '' }}>{{$row->nombre}}</option>
				                                        @endforeach
				                                </select>
											</div>
											<!--<br class="campo-estacionReferencia">-->
										</div>
										<div class="col-md-3 form-group">
											<label class="campo-usuarioPPPoE">*Posible Usuario PPPoE</label>
											<div class="form-group campo-usuarioPPPoE">
												<input type="text" name="UsuarioPppoeTeorico" id="cli-ppoe" class="form-control text-right" value="{{ old('UsuarioPppoeTeorico') ?? $servicio->UsuarioPppoeTeorico ?? 'default' }}">
											</div>
											<!--<br class="campo-usuarioPPPoE">-->
										</div>
										<div class="col-md-2 form-group">
											<label class="campo-señalTeorica">*Señal Teorica</label>
											<div class="form-group campo-señalTeorica">
												<input type="text" name="SenalTeorica" class="form-control" value="{{ old('SenalTeorica') ?? $servicio->SenalTeorica ?? 'default' }}">
											</div>
										</div>
									</div>
								@endif

								<div class="col-md-6 form-group">
									<label class="campo-señalTeorica">*Facturación Costo de instalación / Habilitación</label>
									<div class="form-group campo-señalTeorica">
										<select id="BooleanCostoInstalacion" name="BooleanCostoInstalacion" class="form-control selectpicker">
											<option value="1" {{ $servicio->BooleanCostoInstalacion == 1 ? 'selected' : '' }}>Si</option>
											<option value="0" {{ $servicio->BooleanCostoInstalacion == 0 ? 'selected' : '' }}>No</option>
										</select>
									</div>
								</div>

								<div id="divCostoInstalacion">
									<div class="col-md-4 form-group">
										<label class="campo-CostoInstalacion">Costo de instalación / Habilitación</label>
										<div class="input-group">
											<input type="text" id="CostoInstalacion" name="CostoInstalacion" class="form-control number" validate="not_null" data-nombre="Costo de Instalacion" value="{{ $servicio->CostoInstalacion }}">
											<input type="hidden" id="CostoInstalacionIva" name="CostoInstalacionIva" class="form-control number" value="{{ $servicio->CostoInstalacionIva }}">
											<span class="input-group-addon" id="CostoInstalacionPesos">0</span>
										</div>
									</div>
									
									<div class="form-group col-md-2">
										<label class="control-label" for="moneda"><stong>Moneda</stong></label>
										<select class="selectpicker form-control" name="tipo_moneda" id="moneda" data-live-search="true" data-container="body" validate="not_null" data-nombre="Moneda">
											<option value="1" {{ $servicio->tipo_moneda == 1 ? 'selected' : '' }}>Pesos</option>
											<option value="2" {{ $servicio->tipo_moneda == 2 ? 'selected' : '' }}>UF</option>
										</select>
									</div>

									<div class="col-md-12 form-group">
										<label> Descripción del Servicio | Adjunto en el Doc enviado al SII</label>
										<textarea id="text-descr" name="DescripcionNV" class="form-control" rows="3" placeholder="Descripción del Servicio | Adjunto en el Doc enviado al sii">{{ $servicio->Descripcion }}</textarea>
									</div>
								</div>

								<div class="col-md-6 form-group">
									<label>Técnico Asignado</label>
									<select class="form-control" name="IdUsuarioAsignado" id="IdUsuarioAsignado"  data-live-search="true" data-container="body">
										<option class="seleccione" value="">Seleccione...</option>
							            @foreach ($tecnicos as $row )
							                <option value="{{$row->id}}" {{ $servicio->IdUsuarioAsignado == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
							            @endforeach
							        </select>
							    </div>
							    <div class="col-md-6 tipos">
	                                <div class="form-group">
	                                    <label class="control-label" for="UrlGraficos">Link Gráficos</label>
	                                    <input id="UrlGraficos" name="UrlGraficos" type="text" placeholder="Link para ver los gráficos" value="{{ old('UrlGraficos') ?? $servicio->UrlGraficos ?? 'default' }}" class="form-control input-sm" validate="not_null" data-nombre="Link Gráficos">
	                                </div>
	                            </div>
								<br>
								<div class="col-md-12 form-group">
									<button type="submit" name="ModificarServ" class="btn btn-primary ModificarServ">Actualizar Servicio</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="agregarDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Agregar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary guardarDatosTecnicos">Guardar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="ModalDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Actualizar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary actualizarDatosTecnicos">Actualizar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection

@section('mijava')

	<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>

	<script src="{!! asset('js/methods_global/mapa.js?v=1389884346') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1247385395') !!}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap"></script>
	<!-- <script src="{!! asset('js/servicios/controller.js?v=19080736') !!}"></script>


	<script src="{!! asset('js/methods_global/mapa.js?v=1580805263') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1601188581') !!}"></script>
	<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap">
	</script>
	<script src="{!! asset('js/servicios/controller.js?v=2070837879') !!}"></script>
-->
	<script src="{!! asset('js/swalExtend.js') !!}"></script>
<script type="text/javascript">

	$(document).ready(function() {
    	$('#BooleanCostoInstalacion').trigger('change');

    	$("#producto_id").select2({
		    tags: true,
		});

    	var Latitud = {{ $servicio->Latitud != "" ? $servicio->Latitud : 0 }} ;
    	var Longitud = {{ $servicio->Longitud != "" ? $servicio->Longitud : 0 }} ;
    	if (Longitud != 0 && Latitud != 0){
            ResizeEdit(Latitud, Longitud)
    	}

		setTimeout(() => { $('#BooleanCostoInstalacion').selectpicker('refresh'); }, 1000);
    });

    $('#BooleanCostoInstalacion').change(function(event) {
    	$('#CostoInstalacion').val("0");
    	if ($(this).val() == "1") {
    		$('#divCostoInstalacion').show();
    	} else {
    		$('#CostoInstalacion').val("");
    		$('#text-descr').val("");
    		$('#divCostoInstalacion').hide('slow');
    	}
    	setTimeout(() => { $('#BooleanCostoInstalacion').selectpicker('refresh'); }, 1000);
    });


	$('#cli-ppoe').on('blur', function(event) {
        var camo = this;
        var form = {
        	id_serv: {{ $servicio->Id }},
        	ppoe: $(this).val(),
            _token: "{{ csrf_token() }}"
        };

        $.ajax({
            type: "POST",
            data: form,
            url: "{{ url('/servicios/ppoe') }}",
            success: function(response) {
                if (response == "true") {
	                $(camo).parent('.form-group').addClass('has-error');
	                $(camo).val('@teledata');
	                bootbox.alert('<h3 class="text-center">El usuario Pppoe ya esta registrado.</h3>');
	            } else {
	            	$(camo).parent('.form-group').addClass('has-success');
	            }
            }
        });
    });

    //para cuando seleccione tipo de fac temporal
    $('#TipoFactura').change(function(event) {
        var tipodocumento = $(this).val();
        if (tipodocumento == 25 || tipodocumento == 26) {    
            $('#divFechaActivacion').show();
            $('input[name="FechaInicioDesactivacion"]').attr('validate', 'not_null');
            $('input[name="FechaFinalDesactivacion"]').attr('validate', 'not_null');
        } else {
            $('#divFechaActivacion').hide();
            $('input[name="FechaInicioDesactivacion"]').val('');
            $('input[name="FechaFinalDesactivacion"]').val('');
            $('input[name="FechaInicioDesactivacion"]').removeAttr('validate');
            $('input[name="FechaFinalDesactivacion"]').removeAttr('validate');
        }
    });

    $('body').on('focus', ".datepicker", function() {
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 1,
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true,
            language: 'es'
        });
    });

    $('#Activo').on('change', function() {
        if (($(this).val() == "5" ) || ($(this).val() == "2")) {
        	$('#divFechaActivacion').show()
        	$('input[name="FechaInicioDesactivacion"]').attr('validate', 'not_null')
            $('input[name="FechaFinalDesactivacion"]').attr('validate', 'not_null')
        } else {
            $('#divFechaActivacion').hide()
            $('input[name="FechaInicioDesactivacion"]').val('')
            $('input[name="FechaFinalDesactivacion"]').val('')
        }
    });

</script>
@endsection


