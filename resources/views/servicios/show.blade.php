<style>
	@media screen {
		#printSection {
			display: none;
		}
	}

	@media print {
		body * {
			visibility:hidden;
		}
		#printSection, #printSection * {
			visibility:visible;
		}
		#printSection {
			position:absolute;
			left:0;
			top:0;
		}
	}



</style>
<div id="printThis">
	<div class="modal fade" role="dialog" id="modalServicio" tabindex="-1"  aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="active">Servicio</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="panel">
						<!--Panel heading-->
						<div class="panel-heading">
							<h3 class="text-center"><span class="titulito">  </span></h3>
						</div>
						<hr>

						<div class="callout callout-primary col-md-5">
							<h4 class="text-center">Información Comercial</h4>
								<table class="view-show table" width="100%">
									<tbody>
										<tr>
											<td class="text-right odd"> RUT : &nbsp;</td>
											<td class="Rut"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Nombre : &nbsp;</td>
											<td class="rvNombreCliente"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Direccion : &nbsp;</td>
											<td class="Direccion"></td>
										</tr>

										<tr>
											<td class="text-right odd"> Contacto Cliente: &nbsp;</td>
											<td class="Contacto"></td>
										</tr>

										<tr>
											<td class="text-right odd"> Fono Contacto : &nbsp;</td>
											<td class="Fono"></td>
										</tr>
										<hr>
										<tr>
											<td class="text-right odd"> Tipo de Servicio : &nbsp;</td>
											<td class="IdServicio" width="58%"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Plan : &nbsp;</td>
											<td class="plancitoh" ></td>
										</tr>

										<tr>
											<td class="text-right odd"> Valor: &nbsp;</td>
											<td class="Valor"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Estado Servicio : &nbsp;</td>
											<td class="EstatusServicio" width="58%"></td>
										</tr>
										<hr>
										<tr>
											<td class="text-right odd"> Tipo de Cobro : &nbsp;</td>
											<td class="TipoFactura" width="58%"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Último DTE : &nbsp;</td>
											<td class="ultimodte" width="58%"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Fecha Último DTE : &nbsp;</td>
											<td class="fechaultimodte" width="58%"></td>
										</tr>
										<tr>
											<td class="text-right odd"> DTE Pagado : &nbsp;</td>
											<td class="dtepagado" width="58%"></td>
										</tr>
									</tbody>
								</table>
						</div>
						<div class="callout callout-success col-md-6">
							<h4 class="text-center">Información Técnica</h4>
								<table class="view-show table">
									<tbody>
									<tr class="tipos serv-1 serv-2">
										<td width="42%" class="text-right odd"> Tecnico Asignado : &nbsp;</td>
										<td class="TecnicoAsignado" width="58%"></td>
									</tr>
										<tr class="tipos serv-1 serv-2">
											<td width="42%" class="text-right odd"> Alias de Conexión : &nbsp;</td>
											<td class="Conexion" width="58%"></td>
										</tr>
										<tr class="tipos serv-6">
											<td width="42%" class="text-right odd"> Linea Telefónica : &nbsp;</td>
											<td class="tipo_moneda" width="58%"></td>
										</tr>
										<tr class="tipos serv-7">
											<td width="42%" class="text-right odd"> Nombre Servicio : &nbsp;</td>
											<td class="NombreServicioExtra" width="58%"></td>
										</tr>
										<tr class="tipos serv-4">
											<td width="42%" class="text-right odd"> Dirección IP Fija : &nbsp;</td>
											<td class="tipo_moneda" width="58%"></td>
										</tr>
										<tr class="tipos serv-3">
											<td width="42%" class="text-right odd"> Puerto TCP/UDP : &nbsp;</td>
											<td class="tipo_moneda" width="58%"></td>
										</tr>

										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Equipamiento : &nbsp;</td>
											<td class="Equipamiento"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Dirección (de la antena) : &nbsp;</td>
											<td class="Direccion"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Referencia : &nbsp;</td>
											<td class="Referencia"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Coordenadas : &nbsp;</td>
											<td class="Latitud"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd">Estacion Teórica : &nbsp;</td>
											<td class="PosibleEstacion"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Usuario PPPoE Teórico : &nbsp;</td>
											<td class="UsuarioPppoeTeorico"></td>
										</tr>
										<tr>
											<td class="text-right odd "> Fecha Comprometida : &nbsp;</td>
											<td class="FechaComprometidaInstalacion"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Fecha Instalacion : &nbsp;</td>
											<td class="FechaInstalacion"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Estacion Final : &nbsp;</td>
											<td class="EstacionFinal"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Usuario PPPoE Final : &nbsp;</td>
											<td class="UsuarioPppoe"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Señal Final : &nbsp;</td>
											<td class="SenalFinal"></td>
										</tr>
										<tr class="tipos serv-1 serv-2">
											<td class="text-right odd"> Link Gráficos : &nbsp;</td>
											<td class="UrlGraficos"></td>
										</tr>
										<tr>
											<td width="42%" class="text-right odd"> Descripción : &nbsp;</td>
											<td class="Descripcion" width="58%"></td>
										</tr>
										
									</tbody>
								</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="btnPrint" type="button" class="btn btn-success"><i class="fa fa-print"></i> Imprimir</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
</div>



