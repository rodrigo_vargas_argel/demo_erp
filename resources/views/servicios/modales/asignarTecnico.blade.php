<div class="modal fade" role="dialog" id="agregarTecnico">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Asignar Técnico</h4>
			</div>
			<div class="modal-body">
				<form id="insertTecnico">
					<div class="row">
						<p class="bg-success clientito"></p>
						<p class="bg-success codiguito"></p>
						{{csrf_field()}}
						<div class="col-md-12 form-group">
						<input type="hidden" name="Id" class="form-control id-serv" value="" >
						<label>Tecnicos</label>
						<select class="form-control" name="IdUsuarioAsignado" id="IdUsuarioAsignado"  data-live-search="true" data-container="body">
							<option class="seleccione" value="">Seleccione...</option>
				            @foreach ($tecnicos as $row )
				                <option value="{{$row->id}}" {{ old('IdUsuarioAsignado') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
				            @endforeach
				        </select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple" id="grabarTecnico">Guardar</button>
			</div>
		</div>
	</div>
</div>