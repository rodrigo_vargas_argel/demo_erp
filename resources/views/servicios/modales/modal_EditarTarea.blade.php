<div id="modalTarea" class="modal fade row" tabindex="-1" role="dialog" id="load">
    <div class="modal-dialog modal-lg" role="document" style="min-width: 700px">
        <div class="modal-content">
            <div class="modal-header bg-gris-oscuro p-t-10 p-b-10">
                <h4 class="modal-title c-negro">Código: <span class="Codigo"></span> <button type="button" data-dismiss="modal" class="close c-negro f-25" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding:20px">
                    <form class="form-horizontal" id = "storeTarea">
                        <input type="hidden" id="Id" name="Id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="FechaInstalacion">* Fecha de Instalación (Fecha usada para cálculo de los propocionales o mes completo)</label>
                                <input type="date" id="FechaInstalacion" name="FechaInstalacion" validate="not_null"  placeholder="Seleccione la Fecha de Instalacion" class="form-control" data-nombre="Fecha de Instalacion">
                            </div>
                        </div>
                        <div class="clearfix m-b-10"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="actualizaFecha">Quieres actualizar la fecha del último cobro a la de esta intalación para facturación automática?</label>
                                <div class="select">
                                    <select class="form-control" name="actualizaFecha" id="actualizaFecha" data-live-search="true" data-container="body" validate="not_null" data-nombre="Actualizar Fecha Último Cobro">
                                        <option value = "1">Si Actualizar</option>
                                        <option value = "0">No actualizar</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix m-b-10"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="InstaladoPor">* Instalado Por</label>
                                <div class="select">
                                    <select class="form-control" name="InstaladoPor" id="InstaladoPor" data-live-search="true" data-container="body" validate="not_null" data-nombre="Instalado Por">
                                        <option class="seleccione" value="">Seleccione...</option>
                                        @foreach ($tecnicos as $row )
                                            <option value="{{ $row->id }}" 
                                            @if (null != old('InstaladoPor'))
                                                {{ old('InstaladoPor') == $row->id ? 'selected' : '' }}
                                            @endif
                                            > {{ $row->nombre }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix m-b-10"></div>
                        <div class="row callout callout-success">
                            <div class="col-md-6 tipos serv-1 serv-2" >
                                <div class="form-group">
                                    <label class="control-label" for="UsuarioPppoeTeorico">Posible Usuario PPPoE</label>
                                    <input id="UsuarioPppoeTeorico" name="UsuarioPppoeTeorico" type="text" class="form-control input-sm" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="UsuarioPppoe">Usuario PPPoE Final</label>
                                    <input id="UsuarioPppoe" name="UsuarioPppoe" type="text" placeholder="Ingrese el Usuario PPPoE" class="form-control input-sm text-right" validate="not_null" data-nombre="Usuario PPPoE">
                                </div>
                            </div>
                            <div class="clearfix m-b-10"></div>                         
                            <div class="col-md-6 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="SenalTeorica">Señal Teorica</label>
                                    <input id="SenalTeorica" name="SenalTeorica" type="text" class="form-control input-sm" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="SenalFinal">Señal Final</label>
                                    <input id="SenalFinal" name="SenalFinal" type="text" placeholder="Ingrese la Señal Final" class="form-control input-sm" validate="not_null" data-nombre="Señal Final">
                                </div>
                            </div>
                            <div class="clearfix m-b-10"></div>
                            <div class="col-md-6 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="PosibleEstacion">Estaciones de Referencia</label>
                                    <input id="PosibleEstacion" name="PosibleEstacion" type="text" class="form-control input-sm" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="EstacionFinal">Estación Final</label>
                                    <select name="EstacionFinal" id="EstacionFinal" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Estación Final">
                                        <option class="seleccione" value="">Seleccione...</option>
                                            @foreach ($estaciones as $row )
                                                <option value="{{$row->nombre}}" {{ old('EstacionFinal') == $row->nombre ? 'selected' : '' }}>{{$row->nombre}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix m-b-10"></div>
                            <div class="col-md-12 tipos serv-1 serv-2">
                                <div class="form-group">
                                    <label class="control-label" for="UrlGraficos">Link Gráficos</label>
                                    <input id="UrlGraficos" name="UrlGraficos" type="text" placeholder="Link para ver los gráficos" class="form-control input-sm" validate="not_null" data-nombre="Link Gráficos">
                                </div>
                            </div>

                            <div class="col-md-6 tipos serv-3">
                                <div class="form-group">
                                    <label class="control-label" for="puerto">Puerto TCP/UDP</label>
                                    <input id="puerto" name="puerto" type="text" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-md-6 tipos serv-4">
                                <div class="form-group">
                                    <label class="control-label" for="ip">Dirección IP Fija</label>
                                    <input id="ip" name="ip" type="text" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-md-6 tipos serv-6">
                                <div class="form-group">
                                    <label class="control-label" for="linea">Linea Telefónica</label>
                                    <input id="linea" name="linea" type="text" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-md-12 tipos serv-3 serv-4 serv-5 serv-6">
                                <div class="form-group">
                                    <label class="control-label" for="descripcion">Descripción</label>
                                    <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese del servicio" class="form-control input-sm" validate="not_null" data-nombre="descripcion">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix m-b-10"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="Estatus">* Estatus</label>
                                <div class="select">
                                    <select id="Estatus" name="EstatusServicio" class="form-control selectpicker" data-live-search="true">
                                        @foreach ($estados_activar as $row )
                                            <option value="{{$row->id}}" {{ old('EstatusServicio') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                        @endforeach                                     
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix m-b-10"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="Comentario">* Comentario</label>
                                <textarea id="Comentario" name="Comentario" rows="4" class="form-control" placeholder="Ingrese el Comentario" data-nombre="Comentario"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="Comentario"><i class="fa fa-shopping-cart"></i>
                                    <a href="#" onclick="$('#myModal3').modal('toggle');"> Agregar Equipos</a></label>
                                <div id="label_hes"  name="label_hes"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-body -->
            <div class="modal-footer p-b-20 m-b-20">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-white" onclick="$('#modalTarea').modal('toggle');" >Cerrar</button>
                    <button type="button" class="btn btn-primary" id="guardarTarea" name="guardarTarea">Guardar</button>
                </div>
                <div class="col-sm-12">
                    <div class="cargando"></div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal inmodal " id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated  bounceInRight">
            <div class="modal-header">
                <h3>Agregar Equipos al Servicio</h3>
            </div>
            <div class="panel-bordered-primary">

                <div class="form-group"><label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped table-hover">
                                <thead>
                                <th width="50%">Equipo Descripción</th>
                                <th width="50%">MAC Equipo</th>


                                </thead>
                                <tbody id="tabla_hes">
                                <tr>
                                    <td width="50%">
                                        <select class="selectpicker form-control" name="codigo_producto0" id="codigo_producto0" data-live-search="true" >
                                            <option value="">-Digite y Seleccione-</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->codigo_barra}}" descripcion="{{$producto->descripcion}}">{{$producto->codigo_barra}} - {{@$producto->marca->nombre}}- {{@$producto->modelo->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td width="50%">
                                        <input type="text" name="MAC0" id="MAC0" class="form-control" >
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <select class="selectpicker form-control" name="codigo_producto1" id="codigo_producto1" data-live-search="true"  >
                                            <option value="">-Digite y Seleccione-</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->codigo_barra}}" descripcion="{{$producto->descripcion}}">{{$producto->codigo_barra}} - {{@$producto->marca->nombre}}- {{@$producto->modelo->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="MAC1" id="MAC1" class="form-control" >
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <select class="selectpicker form-control" name="codigo_producto2" id="codigo_producto2" data-live-search="true"  >
                                            <option value="">-Digite y Seleccione-</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->codigo_barra}}" descripcion="{{$producto->descripcion}}">{{$producto->codigo_barra}} - {{@$producto->marca->nombre}}- {{@$producto->modelo->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="MAC2" id="MAC2" class="form-control" >
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <select class="selectpicker form-control" name="codigo_producto3" id="codigo_producto3" data-live-search="true"  >
                                            <option value="">-Digite y Seleccione-</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->codigo_barra}}" descripcion="{{$producto->descripcion}}">{{$producto->codigo_barra}} - {{@$producto->marca->nombre}}- {{@$producto->modelo->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="MAC3" id="MAC3" class="form-control" >
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" onclick="$('#myModal3').modal('toggle');" >Cerrar</button>
                <button type="button" name="btn_tipo" id="btn_tipo" class="btn btn-primary" onclick="guarda_hes()"><i class="fa fa-save"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>

<script>
    function levanta_hes(){
        alert('pico');
        hes=[];
        var contador=$('select[id=cuantos]').val();
        $('#tabla_hes').empty();
        for(var i=0; i<contador; i++){
            // console.log('servicios2: '+data.servicios);
            $("#tabla_hes").append("<tr><td><input type='number' class='form-control' name='num_hes-" + i +  "' id='num_hes-" + i +  "'   ></td><td><input type='date'   name='fecha_hes-" + i +  "'  id='fecha_hes-" + i + "' > </td></tr>");
            //$("#tabla_servicios").append(" ");
        }

        $('#myModal3').modal('toggle');

    }

    function guarda_hes(){

        var cont_hes=3;
        var str_hes='<table class=""> <tr>';
        var bandera_hes=0;
        for (var i=0;i<=cont_hes;i++){

                if($('#codigo_producto'+i).children("option:selected").val() != "")
                str_hes=str_hes +"<td>"+ $('#codigo_producto'+i).children("option:selected").val()+ ", </td>";
               if($('#MAC'+i).val()!=""){
                    str_hes=str_hes +" <td> <small>mac:</small>" + $('#MAC'+i).val() + " ";
                    str_hes=str_hes+"</td><td class='text-success'> || </td>";
               }
            }
        str_hes=str_hes+'</tr></table>';
            $('#label_hes').html(str_hes);
            $('#myModal3').modal('toggle');

    }
</script>