<div id="modalEstatus" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gris-oscuro p-t-10 p-b-10">
				<h4 class="modal-title c-negro">Código: <span class="Codigo"></span> <button type="button" data-dismiss="modal" class="close c-negro f-25" aria-label="Close"><span aria-hidden="true">×</span></button></h4>
			</div>
			<div class="modal-body">
			<div id="loader_servicios"></div>
				<form id="formEstatus">
				    <input type="hidden" class="Id" name="Id" id="Id">
				    <input type="hidden" class="" name="servicio_rut_dv" id="servicio_rut_dv">
				    <input type="hidden" class="" name="servicio_nombre_cliente" id="servicio_nombre_cliente">
				    <input type="hidden" class="" name="servicio_codigo_cliente" id="servicio_codigo_cliente">
				    <div class="row" style="padding:20px">
				        <div class="col-md-12">
				            <div class="form-group">
				                <label class="compo-grupo">Estado del Servicio</label>
				                <div class="compo-grupo">
				                    <select id="Activo" name="EstatusServicio" class="form-control selectpicker" data-live-search="true">
				                    	@foreach ($estados as $row )
                                            <option value="{{$row->id}}" {{ old('EstatusServicio') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                        @endforeach
				                    </select>
				                </div>
				                <br>
				                <div id="divFechaActivacion" style="display:none">
				                    <label for="FechaInicioDesactivacion">Fecha de suspensión</label>
				                    <div class="form-group">
				                        <div id="date-range">
				                            <div class="input-daterange input-group">
				                                <input type="date" class="form-control" id="FechaInicioDesactivacion" name="FechaInicioDesactivacion" data-nombre="Fecha de Activación"/>
				                                <span class="input-group-addon">a</span>
				                                <input type="date" class="form-control" id="FechaFinalDesactivacion" name="FechaFinalDesactivacion" data-nombre="Fecha de Activación"/>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div id="divFechaSuspension" style="display:none">
				                    <label for="FechaInicioSuspension">Ingrese fecha de suspensión para cobrar proporcional</label>
				                    <div class="form-group">
			                            <div class="input-daterange" >
			                                <input type="text" class="form-control datepicker" id="FechaInicioSuspension" name="FechaInicioSuspension" data-nombre="Fecha de suspensión"/>
			                            </div>
				                    </div>
				                </div>

				            </div>
				            
				            <div class="form-group">
				                <label for="selectEnviaCorreo" class="compo-grupo">
									<input class="text-success" type="checkbox" name="enviar_mail" id="enviar_mail" checked>
									  Envio de correos a: </label><hr>
				                @foreach ($usuarios as $row )
                                    {{$row->email}}
                                @endforeach

				            </div>
				        </div>
				    </div>
				</form>	
			</div><!-- /.modal-body -->
			<div class="modal-footer p-b-20 m-b-20">
				<div class="col-sm-12">
					<button type="button" class="btn btn-purple" id="updateEstatus" name="updateEstatus"><i class="fa fa-plane"></i> Guardar</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->