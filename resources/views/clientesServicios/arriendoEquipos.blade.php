<div class="row container-form-datosTecnicos callout callout-success" attr="insertArriendoEquipos.php">
	<div class="col-md-12">
		<h3>Arriendo de equipos</h3>
	</div>

	<input name="id_servicio" type="hidden" value="{{old('id_servicio')}}">
	<input name="Tipo" type="hidden" value="1">

	<div class="col-md-12 form-group">
		<label>* Plan</label>
		<select name="planes_id" id="planes_id" class="form-control" data-live-search="true" validate="not_null" data-nombre="planes_id" data-container="body">
			<option value="">Seleccione...</option>
			@if (isset($servicio->Id))
				@foreach ($planes as $row )
	                <option value="{{$row->id}}" {{ $servicio->plan_id == $row->id ? 'selected' : '' }}>{{$row->nombre.'-'.$row->velocidad_subida.'x'.$row->velocidad_bajada.'- valor referencial: $'.$row->valor_referencia.' '.$row->tipo_moneda}}</option>
	            @endforeach
	        @else
	        	@foreach ($planes as $row )
	                <option value="{{$row->id}}" {{ old('planes_id') == $row->id ? 'selected' : '' }}>{{$row->nombre.'-'.$row->velocidad_subida.'x'.$row->velocidad_bajada.'- valor referencial: $'.$row->valor_referencia.' '.$row->tipo_moneda}}</option>
	            @endforeach
	        @endif
		</select>
	</div>
	<div class="col-md-12 form-group">
		<label class="control-label">Activo a Transferir</label>
		<select class="form-control" id="producto_id" name="productos[]" multiple="multiple">
			<option value="">Seleccione Opción</option>
			@if (isset($servicio->Id))
				@foreach ($productos as $row )
	                <option value="{{$row['id']}}" {{$row['seelect']}}>{{$row['nombre']}}</option>
	            @endforeach
	        @else
	        	@foreach ($productos as $row )
	                <option value="{{$row['id']}}" {{@$row['seelect']}}>{{$row['nombre']}}</option>
	            @endforeach
	        @endif
		</select>
	</div>


</div>