<div class="row">
	<div class="dataServicios">
		<table class="table table-responsive table-striped" id="tabla_ver_servicios" width="100%">
		    <thead>
		        <tr>
		            <th style="width:14%  !important">Código de Servicios</th>
		            <th style="width:8% !important">Conexión</th>
		            <th style="width:8% !important">Valor</th>
		            <th style="width:8% !important">Grupo</th>
		            <th style="width:10% !important">Fecha Instalación</th>
		            <th style="width:6% !important">Estado</th>
		            <th style="width:24% !important">Tipo de Servicio</th>
		            <th style="width:6% !important">Velocidad</th>
		            <th style="width:6% !important">Plan</th>
		            <th style="width:10% !important">Opciones</th>
		        </tr>
		    </thead>
		    <tbody></tbody>
		</table>
	</div>
</div>