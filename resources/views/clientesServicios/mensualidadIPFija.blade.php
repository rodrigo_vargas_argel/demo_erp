<div class="row container-form-datosTecnicos callout callout-success" attr="insertMensualidadIPFija.php">
	<div class="col-md-12">
		<h3>Mensualidad de Ip Fija</h3>
	</div>
	<div class="col-md-12 form-group" rows="5">
		<label>Dirección IP Fija</label>
		<input name="ip" class="form-control" value="{{old('ip')}}">
		<input name="id_servicio" type="hidden" value="{{old('id_servicio')}}">
	</div>
	<div class="col-md-12 form-group" rows="5">
		<label>Descripción</label>
		<textarea name="descripcion" class="form-control"> {{old('descripcion')}}</textarea>
	</div>
</div>