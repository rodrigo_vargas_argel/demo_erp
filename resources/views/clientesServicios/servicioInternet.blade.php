<div class="row container-form-datosTecnicos callout callout-success" attr="insertServicioInternet.php">
	<div class="col-md-12">
		<h3>Servicio de Internet</h3>
	</div>
	<input name="id_servicio" type="hidden" value="{{old('id_servicio')}}">
	<input name="Tipo" type="hidden" value="2">

	<div class="col-md-12 form-group">
		<label>* Plan</label>
		<select name="planes_id" id="planes_id" class="form-control" data-live-search="true" validate="not_null" data-nombre="planes_id" data-container="body">
			<option value="">Seleccione...</option>
			@foreach ($planes as $row )
                <option value="{{$row->id}}" {{ old('planes_id') == $row->id ? 'selected' : '' }}>{{$row->nombre.'-'.$row->velocidad_subida.'x'.$row->velocidad_bajada.'- valor referencial: $'.$row->valor_referencia.' '.$row->tipo_moneda}}</option>
            @endforeach
		</select>
	</div>
</div>