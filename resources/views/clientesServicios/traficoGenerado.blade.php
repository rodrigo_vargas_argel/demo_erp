<div class="row container-form-datosTecnicos callout callout-success" attr="insertTraficoGenerado.php">
	<div class="col-md-12">
		<h3>Trafico Generado</h3>
	</div>
	<div class="col-md-12 form-group" rows="5">
		<label>* Linea Telefónica</label>
		<input name="linea" class="form-control" value="{{old('linea')}}">
		<input name="id_servicio" type="hidden" value="{{old('id_servicio')}}">
	</div>
	<div class="col-md-12 form-group" rows="5">
		<label>Descripción</label>
		<textarea name="descripcion" class="form-control">{{old('descripcion')}}</textarea>
	</div>
</div>