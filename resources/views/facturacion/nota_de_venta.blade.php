@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php
    use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> DEMO ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Cliente</a>
                </li>
                <li class="active">
                    <strong>Facturar Clientes</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
          <!--  <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/facturacion/nota_de_venta_nuevo') }}" class="btn btn-default btn-active-default pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
           -->
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <div class="col-lg-5">
                                <h5><i class="fa fa-files-o"></i>  Facturar Cliente</h5>
                            </div>
                            <!--
                            <div class="col-lg-7">
                                <form name="filtro" method="post" action="{{ url('facturacion/lista_filtro_nv') }}">
                                {{csrf_field()}}
                                <select class="selectpicker form-control" style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="this.form.submit()" >
                                    <option value="">-Digite y Seleccione Cliente-</option>
                                    @foreach($clientes as $cliente)
                                        <option value="{{$cliente->rut}}" rut="{{$cliente->rut}}">{{$cliente->nombre}} - {{$cliente->rut}}</option>
                                    @endforeach
                                </select>
                                </form>
                            </div>
                            -->

                    <div class="ibox-content table-responsive">

                         <table class="table  table-bordered table-striped dataTables-example "  >
                            <thead>
                            <tr>

                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Cliente</th>
                                <th>Neto</th>
                                <th>Total</th>

                                <th>Responsable</th>
                                <th>Numero OC / OT</th>
                                <th>Vista Previa Factura</th>

                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>01</td>
                                    <td>01-10-2020</td>
                                    <td>Constructora Baquedano S.A.</td>
                                    <td>99.900</td>
                                    <td>119.800</td>
                                    <td>Usuario sistema</td>
                                    <td>10892130</td>
                                    <td>
                                        <a href="http://app2.bsale.cl/view/show_pdf?cpn_id=18790&document_token=3ea7227526e0&sfd=99" class="badge badge-default"  title="Vista Previa" target="_blank" ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>02</td>
                                    <td>02-10-2020</td>
                                    <td>Constructora Claro Vicuña S.A.</td>
                                    <td>100.000</td>
                                    <td>120.000</td>
                                    <td>Usuario sistema</td>
                                    <td>21129900</td>
                                    <td>
                                        <a href="http://app2.bsale.cl/view/show_pdf?cpn_id=18790&document_token=3ea7227526e0&sfd=99" class="badge badge-default"  title="Vista Previa" target="_blank" ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>03</td>
                                    <td>02-10-2020</td>
                                    <td>Constructora Nueummann.</td>
                                    <td>199.900</td>
                                    <td>219.800</td>
                                    <td>Usuario sistema</td>
                                    <td>4129900</td>
                                    <td>
                                        <a href="http://app2.bsale.cl/view/show_pdf?cpn_id=18790&document_token=3ea7227526e0&sfd=99" class="badge badge-default"  title="Vista Previa" target="_blank" ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                            <!--
                            @if($consulta != "No hay registros")
                                @foreach($consulta as $compra)
                                    <?php
                                    $fecha1=Carbon::parse($compra->fecha);
                                    $fecha1=$fecha1->format('d-m-Y');
                                    $neto=round( $compra->neto, 0, PHP_ROUND_HALF_UP);
                                    if ($neto==0)
                                        $neto=$compra->detalle->sum('precio');
                                    $total=$compra->total;
                                    if ($total==0)
                                        $total=$compra->detalle->sum('total');
                                    ?>
                                    <tr>
                                        <td>
                                            <small> {{$compra->id}}</small>
                                        </td>
                                        <td>
                                            <small>{{$fecha1}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->cliente->nombre}}</small>
                                        </td>
                                        <td>
                                            <small>{{number_format($neto,0,',','.')}}</small>
                                        </td>
                                        <td>
                                            <small>{{number_format($total,0,',','.')}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->numero_oc}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->responsable->nombre}}</small>
                                        </td>

                                        <td>

                                            @if ($compra->numero_hes > 0)
                                                @foreach($compra->hes as  $index => $hes)
                                                    @if ($index == 0)
                                                    {{$hes->num_hes}}
                                                    @else
                                                    - {{$hes->num_hes}}
                                                    @endif

                                                    @endforeach

                                                @endif
                                        </td>

                                        <td>
                                            <small>
                                            @if($compra->estatus_facturacion ==1)
                                                <!--  <a href="{--{ url('/inventarios/compras_editar/'.$compra->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                                 <a class="del" id="{--{$compra->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a> ||-->
                                                   @if($compra->factura != null)
                                                      <a href="{{$compra->factura->UrlPdfBsale}}" title="ver documento" target="_blank"><i class="fa fa-file-pdf-o"> {{$compra->factura->NumeroDocumento}}</i></a> ||
                                                    <a href="#"  title="Enviar Documento al Cliente"  onclick="enviar_documento({{$compra->factura->Id}})" class="badge badge-success" > <i class="fa fa-send-o"></i></a>
                                                    @else
                                                       no hay factura asociada
                                                    @endif
                                                @else
                                                    <a href="#" class="badge badge-default"  title="Vista Previa" onclick="genera_factura_previa({{$compra->id}})"><i class="fa fa-eye"></i></a> |
                                                    <a href="#" class="badge badge-info  "  title="Emitir Factura" onclick="genera_factura({{$compra->id}})"><i class="fa fa-dollar"></i></a> |
                                                    <a href="#" class="badge badge-mint text-danger  "  title="Eliminar Nota Venta" onclick="elimina_nv({{$compra->id}})"><i class="fa fa-trash"> </i></a>
                                                @endif


                                            </small>
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="5">{{$consulta}}</td></tr>
                            @endif
                          -->
                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>

        $(document).ready(function() {

           // $('.footable').footable();
           // $('.footable2').footable();

            $('.dataTables-example').DataTable({
                pageLength: 50,
                order: [[ 0, "desc" ]],
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Notas de Venta de Teledata'},
                    {extend: 'pdf', title: 'Notas de Venta de Teledata'},


                ]

            });

        });

        function genera_factura(id) {
          //  alert(id);
            var rvid=id;

            swal({
                    title: "Estas seguro de Facturar?",
                    text: "se generará un documento en Bsale y en SII!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, Generar Documento!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {

                        swal('OK, espere un momento','Se está generando una DTE','warning')

                        //console.log('holaa: '+ (dis.name));
                        $.get("/facturacion/facturar_nota_venta/"+rvid+"/1", function (response) {
                         //   var win = window.open(response.url, '_blank');
                            if (response.success===true){

                                swal("Exito!", "se ha generado el documento exitosamente. N° " +response.numdoc, "success");
                                window.location.href="{{ url('/facturacion/nota_de_venta') }}";
                               // console.log(response.arr);
                                //console.log('*******************');
                                //console.log(response.arr.Message);

                            }else {

                                swal("Error desde Bsale!", response.arr.Message, "success");
                                console.log(response.arr);
                                console.log('*******************');
                                console.log(response.arr.Message);
                            }
                        });


                    } else {
                        swal("Cancelado", "Se conserva estado de nota de venta", "error");
                    }
                });
        }
        function genera_factura_previa(id){
            var rvid=id;
            swal('Espere','Se va a generar una vista previa del documento','warning')
            $.get("/facturacion/facturar_nota_venta/"+rvid+"/2", function (response) {
               //  console.log(response.url);
               // console.log(response.arr);
                var win = window.open(response.url, '_blank');
                win.focus();
                // $(dis).closest('tr').remove();
            });
        }

        function enviar_documento(id_factura) {

            var rvid=id_factura;

            swal({
                    title: "Estas seguro de Enviar?",
                    text: "Se Enviará un e-mail al cliente",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, Enviar Documento!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/facturacion/enviar_documento/"+rvid+"", function (response) {
                            // console.log(response.success)
                            swal("Exito!", "se ha enviado el documento exitosamente.", "success");

                        });


                    } else {
                        swal("Cancelado", "Se conserva estado de nota de venta", "error");
                    }
                });
        }

        function elimina_nv(id_nv){

            var rvid=id_nv;

            swal({
                    title: "¿Eliminar?",
                    text: "Se Borrará el registro de Nota de Venta",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, Eliminar!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //alert(rvid);
                        //console.log('holaa: '+ (dis.name));
                       $.get("/facturacion/eliminar_nv/"+rvid+"", function (response) {
                            // console.log(response.success)
                           if (response.Success===true){
                               swal("Eliminado!", "se ha borrado el el registro.", "success");
                               window.location.href="{{ url('/facturacion/nota_de_venta') }}";

                           }
                           else{
                               swal("Error!", "No se ha podiro eliminar el registro.", "error");
                           }


                        });


                    } else {
                        swal("Cancelado", "Se conserva estado de nota de venta", "error");
                    }
                });

        }



    </script>
    @endsection

