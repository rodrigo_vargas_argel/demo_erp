@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php
    if (isset($cliente_old)){
        $clienteold=$cliente_old;
    }
    else{
        $clienteold="";
    }
    use Carbon\Carbon;
    setlocale(LC_ALL,"es_ES");
    Carbon::setLocale('es');
    $total_saldo=0;
    $total_afavor=0;
    $total_facturado=0;
    //$carbon = new \Carbon\Carbon();
    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Facturacion</a>
                </li>
                <li class="active">
                    <strong>Pagos</strong>
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <div class="col-lg-5">
                            <h5><i class="fa fa-money"></i>  Pagos Ingresados</h5>
                        </div>

                        <div class="col-lg-7">
                            <form name="filtro" method="post" action="{{ url('facturacion/rpt_pagos_filtro') }}">
                                {{csrf_field()}}
                                <select class="selectpicker form-control" style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="this.form.submit()" >
                                    <option value="">-Digite y Seleccione Cliente-</option>
                                    <option value="-1">-TODOS-</option>
                                    @foreach($clientes as $cliente)
                                        <option value="{{$cliente->rut}}" rut="{{$cliente->rut}}">{{$cliente->nombre}} - {{$cliente->rut}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>


                        <div class="ibox-content table-responsive">
                            @if($pagos != "na")
                                <div class="row">
                                  <div class="table-responsive">
                                    <table class="table  table-bordered table-striped dataTables-example "  id="dataTables-example" >
                                        <thead>
                                        <th>Cliente</th>
                                        <th>RUT</th>
                                        <th>N° DTE</th>
                                        <th>Tipo DTE</th>
                                        <th>Glosa DTE</th>
                                        <th>Fecha DTE</th>
                                        <th>Fecha Pago</th>
                                        <th>Mes Pago</th>
                                        <th>Monto DTE</th>
                                        <th>Monto Pago</th>
                                        <th>Saldo</th>
                                        </thead>

                                            @foreach($pagos as $pago)
                                                    <tr>
                                                        <?php
                                                        if ($pago->factura->detalle != null){

                                                            $glosa="";
                                                            $neto=$pago->factura->detalle->sum('Valor');
                                                            $total=$pago->factura->detalle->sum('Total');
                                                            $total=round( $total, 0, PHP_ROUND_HALF_UP);


                                                        }
                                                        $fecha2=Carbon::parse($pago->FechaPago);

                                                        $ano = $fecha2->year;
                                                        $mes = $fecha2->month;
                                                       // $diff_dias = $fecha1->diffInDays($fecha2);
                                                         ?>


                                                        <td>{{@$pago->factura->cliente->nombre}}</td>
                                                        <td>{{@$pago->factura->cliente->rut}}-{{@$pago->factura->cliente->dv}}</td>
                                                        <td>{{@$pago->factura->NumeroDocumento}}</td>
                                                        <td>{{@$pago->factura->cliente->tipoClientes->nombre}}</td>
                                                        <td>{{@$pago->factura->detalle[0]->Concepto}}</td>
                                                            <td>{{@$pago->factura->FechaFacturacion}}</td>
                                                        <td>{{$pago->FechaPago}}</td>
                                                            <td>{{$mes}}</td>
                                                            <td>{{$total}}</td>
                                                        <td>{{$pago->Monto}}</td>
                                                        <td>{{($total-$pago->Monto)}}</td>
                                                    </tr>
                                            @endforeach

                                    </table>

                                  </div>
                                </div>
                            @endif
                            <table class="table  table-bordered table-striped dataTables-example "  id="dataTables-example" >
                                <thead>
                                <tr>

                                    <th width="15%">Nombre Cliente</th>
                                    <th >RUT</th>
                                    <th>Numero DTE</th>
                                    <th>Tipo DTE</th>
                                    <th>Glosa</th>
                                    <th>Fecha_DTE</th>
                                    <th>Fecha_Pago</th>
                                    <th>Diff</th>
                                    <th>Año</th>
                                    <th>Mes</th>
                                    <th>Total Doc</th>
                                    <th>Pagos</th>
                                   <!-- <th>Saldo</th>
                                    <th>A favor</th>-->
                                    

                                </tr>
                                </thead>
                                <tbody>
                                 @if($facturas != "Seleccione Cliente en la lista de arriba a la derecha...")

                                     @foreach($facturas as $index=>$factura)
                                            @if($factura->pagos != null)
                                                @foreach($factura->pagos as $fpagos)
                                                    <?php
                                                   // setlocale(LC_ALL, 'es_ES');
                                                    $fecha1=Carbon::parse($factura->FechaFacturacion);
                                                   // $fecha1=$fecha1->format('d-m-Y');
                                                    $fecha2=Carbon::parse($fpagos->FechaPago);
                                                    //$fecha2=$fecha2->format('d-m-Y');
                                                    $date = $fecha2->year; //con esto revise que el lenguaje fuera es
                                                   //$mes=$fecha->monthName;
                                                    $mes = $fecha2->month;
                                                    //$mes = $mes->formatLocalized('%B');

                                                    $diff_dias = $fecha1->diffInDays($fecha2);
                                                    $pagos=0;
                                                    $neto=0;
                                                    $total=0;
                                                    $saldo=0;
                                                    $afavor=0;
                                                    $total_pagos=0;

                                                    if ($factura->detalle != null){
                                                        //var_dump($factura->detalle);

                                                        $glosa="";
                                                        $neto=$factura->detalle->sum('Valor');
                                                        $total=$factura->detalle->sum('Total');
                                                        $total=round( $total, 0, PHP_ROUND_HALF_UP);
                                                        //$pagos=$factura->pagos->sum('Monto');
                                                       // $concepto=$factura->detalle->values()->first();

                                                    }

                                                    else{
                                                        $neto=0;
                                                        $total=0;
                                                        $pagos=0;

                                                    }
                                                    if ($fpagos != null){
                                                        $pagos=$fpagos->Monto;
                                                        $saldo=$total-$pagos;
                                                        $saldo=round( $saldo, 0, PHP_ROUND_HALF_UP);
                                                        $total_pagos=$total_pagos+$pagos;
                                                        if ($saldo < 0){
                                                            $afavor=$saldo;
                                                            $saldo=0;
                                                            $afavor=round( $afavor, 0, PHP_ROUND_HALF_UP);
                                                            $afavor=$afavor*(-1);  // pff el valor es negativo, pero las titis se confunden con el signo (-) por eso se los dejo en (+) y en columna aparte.
                                                        }

                                                    }
                                                    else{
                                                        $pagos=0;
                                                        $saldo=0;
                                                    }

                                                    if($factura->EstatusFacturacion==2){
                                                        $estilo='text-danger';
                                                        $saldo="";
                                                        $pagos="";
                                                        if($factura->devoluciones->DevolucionAmount < $total){
                                                            $pagos=$factura->pagos->sum('Monto');
                                                            $saldo=$total-$factura->devoluciones->DevolucionAmount-$pagos;

                                                        }

                                                    }

                                                    else{
                                                        $estilo='text-dark';
                                                    }

                                                    if ($factura->pagos != null){
                                                    ?>

                                                    <tr class="  {{$estilo}}">

                                                        <td class="{{$estilo}}">
                                                            <small>{{@$factura->cliente->nombre}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{@$factura->cliente->rut}}-{{@$factura->cliente->dv}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{@$factura->NumeroDocumento}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{@$factura->cliente->tipoClientes->nombre}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{@$factura->detalle[0]->Concepto}}</small>
                                                        </td>

                                                        <td class="{{$estilo}}">
                                                            <small>{{$fecha1}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{$fecha2}}</small>
                                                        </td>
                                                        <td class="{{$estilo}}">
                                                            <small>{{$diff_dias}}</small>
                                                        </td>
                                                        <td>
                                                            <small>{{$date}}</small>
                                                        </td>
                                                        <td>
                                                            <small>{{@$mes}} </small>
                                                        </td>
                                                        <td class="{{$estilo}}" align="right">
                                                            <small>{{$total}} </small>
                                                        </td>

                                                        <td class="{{$estilo}}" align="right">

                                                                <small>{{$pagos}} </small>

                                                        </td>
                                                      <!--  <td class="{{$estilo}}" align="right">
                                                            @if($saldo != 0)
                                                                <small>{{$saldo}} </small>
                                                            @else
                                                                <small>{{$saldo}}</small>
                                                            @endif
                                                        </td>
                                                        <td class="{{$estilo}}" align="right">
                                                            @if($afavor != 0)
                                                                <small>{{$afavor}} </small>
                                                            @else
                                                                <small>{{$afavor}}</small>
                                                            @endif
                                                        </td>-->

                                                    </tr>

                                                    <?php
                                                            } //fin if ($factura->pagos != null)
                                                    if (($saldo != "")&&($saldo !=null))
                                                        $total_saldo=$total_saldo + $saldo;
                                                    if($factura->EstatusFacturacion!=2)
                                                        $total_facturado=$total_facturado+$total;

                                                    $total_afavor=$total_afavor+$afavor;
                                                    ?>
                                                @endforeach
                                            @endif
                                        @endforeach


                                     @else
                                     <tr><td colspan="5">{{$facturas}}</td></tr>
                                 @endif

                                </tbody>


                            </table>




                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
  @section('mijava')
     <script>
        $(document).ready(function() {

        // $('.footable').footable();
        // $('.footable2').footable();

            $('.dataTables-example').DataTable({

            pageLength: 25,
            order: [[0, "asc"]],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            sum: 5,
            buttons: [

            {extend: 'excel', title: 'Documentos Pagados'},
            {extend: 'pdf', title: 'Documentos Pagados'},


            ],


            });


        });


     </script>
  @endsection