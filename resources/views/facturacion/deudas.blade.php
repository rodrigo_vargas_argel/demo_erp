@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php
    if (isset($cliente_old)){
        $clienteold=$cliente_old;
    }
    else{
        $clienteold="";
    }
    use Carbon\Carbon;
    $total_saldo=0;
    $total_afavor=0;
    $total_facturado=0;
    //$carbon = new \Carbon\Carbon();
    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Facturacion</a>
                </li>
                <li class="active">
                    <strong>Pagos</strong>
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <div class="col-lg-5">
                            <h5><i class="fa fa-bell"></i>  Documentos Impagos Vencidos </h5>
                        </div>



                        <div class="ibox-content table-responsive">

                            <table class="table  table-bordered table-striped dataTables-example "  id="dataTables-example" >
                                <thead>
                                    <tr>

                                        <th>Cliente</th>
                                        <th>RUT</th>
                                        <th>Num DTE</th>
                                        <th>Tipo DTE</th>
                                        <th>Glosa</th>
                                        <th>Fecha_Emis DTE</th>
                                        <th>Fecha_Venc DTE</th>
                                        <th>Monto Total</th>
                                        <th>Monto Pagado</th>
                                        <th>Anulado</th>
                                        <th>Total Deuda</th>


                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($facturas as $index=>$factura)

                                                <?php
                                                $fecha1=Carbon::parse($factura->FechaFacturacion);
                                                $fecha1=$fecha1->format('d-m-Y');
                                                $fecha2=Carbon::parse($factura->FechaVencimiento);
                                                $fecha2=$fecha2->format('d-m-Y');
                                                $total=round( $factura->detalle->sum('Total'), 0, PHP_ROUND_HALF_UP);
                                                if($factura->pagos != null)
                                                $pagos=round( $factura->pagos->sum('Monto'), 0, PHP_ROUND_HALF_UP);
                                                else
                                                    $pagos=0;
                                                if ($factura->devoluciones != null)
                                                $devolucion=round( $factura->devoluciones->DevolucionAmount, 0, PHP_ROUND_HALF_UP);
                                                else
                                                    $devolucion=0;

                                                $saldo= $total -($pagos + $devolucion);
                                                $afavor=0;
                                                $total_pagos=0;

                                                ?>
                                                 @if ($saldo > 2)
                                                <tr>

                                                    <td>
                                                        <small>{{$factura->cliente->nombre}}</small>
                                                    </td>
                                                    <td>
                                                        <small>{{$factura->cliente->rut}}-{{$factura->cliente->dv}}</small>
                                                    </td>
                                                    <td >
                                                        <small>{{$factura->NumeroDocumento}} </small>
                                                    </td>
                                                    <td >
                                                        <small>{{$factura->cliente->tipoClientes->nombre}} </small>
                                                    </td>
                                                    <td >
                                                        <small>{{$factura->detalle[0]->Concepto}}</small>
                                                    </td>
                                                    <td >
                                                        <small>{{$factura->FechaFacturacion}}</small>
                                                    </td>
                                                    <td >
                                                        <small>{{$factura->FechaVencimiento}}</small>
                                                    </td>
                                                    <td align="right">
                                                        <small>{{$total}} </small>
                                                    </td>
                                                    <td  align="right">
                                                        <small>{{$pagos}}  </small>
                                                    </td>
                                                    <td  align="right">
                                                        <small> {{$devolucion}} </small>
                                                    </td>

                                                    <td  align="right">

                                                        <small> {{$saldo}} </small>

                                                    </td>


                                                </tr>
                                                @endif
                                        <?php
                                        $total_saldo=$total_saldo+$saldo;
                                        ?>


                                    @endforeach
                                    <div>
                                        <h4>
                                        <span class="pull-right text-bold"  name="total_saldo" id="total_saldo" >MONTO TOTAL VENCIDOS: $ {{number_format($total_saldo,0,',','.')}}</span>
                                        </h4>
                                    </div>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
        @section('mijava')
            <script>
                $(document).ready(function() {

                    // $('.footable').footable();
                    // $('.footable2').footable();

                    $('.dataTables-example').DataTable({

                        pageLength: 50,
                        order: [[0, "asc"]],
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        sum: 5,
                        buttons: [

                            {extend: 'excel', title: 'Documentos Adeudados'},
                            {extend: 'pdf', title: 'Documentos Adeudados'},


                        ],


                    });


                });


            </script>
        @endsection