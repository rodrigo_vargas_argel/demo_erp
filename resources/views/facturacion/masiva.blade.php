@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php
    if (isset($tipo_factura_old)){
        $old=$tipo_factura_old;
    }
    else{
        $old="";
    }
    use Carbon\Carbon;
    Carbon::setLocale('es');
    //$carbon = new \Carbon\Carbon();
            $ufr=round( $uf, 0, PHP_ROUND_HALF_UP);
    $fecha1=Carbon::parse($fecha_uf);
    $mes_actual= $fecha1->format('F');
    $fecha1=$fecha1->format('d-m-Y');

    //$date = \Carbon\Carbon::now();
    //$mes_actual= $fecha1->format('F'); // July
    //echo $date->subMonth()->format('F'); // June


    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Facturacion</a>
                </li>
                <li class="active">
                    <strong>Facturación Masiva</strong> {{$mes_actual}}
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <div class="col-lg-5">

                          <li>
                            <label> Fecha  Facturación:</label> {{$fecha1}}
                          </li>
                            <li>
                                <label> Valor  UF para Hoy: </label> ${{number_format($ufr,0,',','.')}}
                            </li>
                            


                        </div>

                        <div class="col-lg-7">
                            <form name="filtro" method="post" action="{{ url('facturacion/masiva_get') }}">
                                {{csrf_field()}}
                                <select class="form-control" style="border-color: green;" name="tipo_factura" id="tipo_factura" data-live-search="true" onchange="this.form.submit()" >
                                    <option value="">- Seleccione Tipo de Facturación-</option>
                                    <option value="todos">-Todos-</option>
                                    @foreach($mantenedor_tipo_factura as $tipo)

                                        <option value="{{$tipo->id}}" {{ $old ==$tipo->id ? 'selected' : '' }} >{{$tipo->codigo}} - {{$tipo->descripcion}}</option>
                                    @endforeach
                                        <option value="">-Todos-</option>

                                </select>
                            </form>
                        </div>
                        <div class="ibox-content table-responsive">


                            <table class="table  table-bordered table-striped dataTables-example "  >
                                <thead>
                                    <th width="10%">Cliente</th>
                                    <th>Último DTE</th>
                                    <th>URL último DTE</th>
                                    <th>Items</th>
                                    <th>Detalle de Servicios</th>
                                    <th>Grupo</th>
                                    <th>Valor Total en UF</th>
                                    <th>Valor Total en pesos</th>

                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                @if($consulta != "No hay registros")
                                    @foreach($clientes as $index=>$cliente)
                                        @if($cliente->servicios->count()!=0)
                                            <?php
                                            $valorpesos=$ufr * $cliente->servicios->sum('Valor');
                                            $totalpesos=$valorpesos+($valorpesos*0.19)
                                                    ?>
                                           <tr>
                                               <td>{{@$cliente->nombre}} - {{$index}}</td>
                                               <td>{{@$cliente->ultima_factura->create_at}}</td>
                                               <td><a target="_blank" href="{{@$cliente->ultima_factura->UrlPdfBsale}}"><i class="fa fa-file-pdf-o"></i>{{@$cliente->ultima_factura->Id}} | {{@$cliente->ultima_factura->NumeroDocumento}}</a> </td>
                                               <td>{{@$cliente->servicios->count()}}</td>
                                               <td> @foreach(@$cliente->servicios as $index2=>$servicio)
                                                       <?php echo $servicio->Codigo . " | ".$servicio->tipo_facturas->codigo." | ".$servicio->Valor."UF | ".$servicio->estado_servicios->nombre. "</br>";
                                                       ?>
                                                   @endforeach</td>
                                               <td>{{$servicio->tipo_facturas->codigo}}</td>
                                               <td>{{@$cliente->servicios->sum('Valor')}}</td>
                                               <td>{{round( $totalpesos, 0, PHP_ROUND_HALF_UP)}}</td>
                                               <?php
                                               $date = Carbon::parse($cliente->ultima_factura->create_at);
                                               $mes_facturacion= $date->format('F'); // July
                                               ?>
                                               @if ($mes_facturacion==$mes_actual)
                                                   <td><a target="_blank" href="{{@$cliente->ultima_factura->UrlPdfBsale}}"><i class="fa fa-file-pdf-o"></i>{{@$cliente->ultima_factura->Id}} | {{@$cliente->ultima_factura->NumeroDocumento}}</a></td>
                                               @else
                                                   @if($servicio->tipo_facturas->codigo != "FSMOC")
                                                   <td><div id="factura{{$index}}"><input type='checkbox' class='js-switch_3' name='check[]' id="{{$index}}"   cliente="{{@$cliente->Rut}}" onchange="check_factura(this)"></div></td>
                                                    @else
                                                       <td><div id="factura{{$index}}"><a href="#" data-toggle="modal" id="{{$index}}" data-target="#myModal2" onclick="set_id(this)"><i class="fa fa-file"></i> (+)OC/HES</a> </div></td>
                                                   @endif
                                                @endif
                                           </tr>
                                         @endif



                                    @endforeach
                                @endif
                                </tbody>

                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!----- modales ---->
        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Agregar OC/HES</h4>
                        <small class="font-bold" id="lbl_cliente"></small>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-responsive table-bordered table-hover">
                                    <thead>
                                            <th>OC</th>
                                            <th>Fecha OCo</th>


                                    </thead>
                                    <tbody id="tabla_oc">
                                    <td><input type="text" class="form-control" name="num_oc" id="num_oc" value=""> </td>
                                    <td><input type="date" class="form-control" name="fecha_oc" id="fecha_oc"  value=""> </td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-responsive table-bordered table-hover">
                                    <thead>
                                            <th>HES</th>
                                            <th>Fecha HES</th>


                                    </thead>
                                    <tbody id="tabla_hes">
                                    <input type="hidden" name="id_factura" id="id_factura" value="">
                                    <tr>
                                    <td><input type="text" class="form-control" name="num_hes1" id="num_hes1" value=""> </td>
                                    <td><input type="date" class="form-control" name="fecha_hes1" id="fecha_hes1"  value=""> </td>
                                    </tr>
                                    <tr>
                                    <td><input type="text" class="form-control" name="num_hes2" value="" id="num_hes2"> </td>
                                    <td><input type="date" class="form-control" name="fecha_hes2"  id="fecha_hes2"  value=""> </td>
                                    </tr>
                                    <tr>
                                    <td><input type="text" class="form-control" name="num_hes3" value="" id="num_hes3"> </td>
                                    <td><input type="date" class="form-control" name="fecha_hes3" value="" id="fecha_hes3"> </td>
                                    </tr>
                                    <tr>
                                    <td><input type="text" class="form-control" name="num_hes4" value="" id="num_hes4"> </td>
                                    <td><input type="date" class="form-control" name="fecha_hes4" value="" id="fecha_hes4"> </td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" name="num_hes5" value="" id="num_hes5"> </td>
                                        <td><input type="date" class="form-control" name="fecha_hes5" value="" id="fecha_hes5">  </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                     <button type="button" class="btn btn-primary" onclick="agrega_hes()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('mijava')
            <script>
                var jsn_oc=[];
                var jsn_hes=[];
                var arr_facturas=[];
                var id_fila_seleccionada=0;


                function set_id(e){
                    $('#id_factura').val(e.id);
                    id_fila_seleccionada=e.id;
                    console.log('******* Fila seleccionada: '+ id_fila_seleccionada + '***************************');
                }


                function agrega_hes() {
                    var tempArray = <?php echo json_encode($clientes); ?>;
                    console.log(tempArray[id_fila_seleccionada]);
                    var factura=tempArray[id_fila_seleccionada];
                    var i = 0;
                    var bandera_hes=0;
                    //alert('num oc' + $('#num_oc').val())

                    if (($('#num_oc').val() === null)||($('#num_oc').val() === "") ){
                        swal('ERROR', 'Debes ingresar el Número de OC', 'error')

                    } else {
                        if (($('#fecha_oc').val() === null)||($('#fecha_oc').val() === "")) {
                            swal('ERROR', 'Debes ingresar la fecha de OC', 'error')
                        }
                        else {

                            jsn_oc.push({id:factura.id, num_oc: $('#num_oc').val(), fecha_oc: $('#fecha_oc').val()});
                           // factura.push(jsn_oc);
                            factura.oc=jsn_oc;
                            console.log(factura);

                            for (i = 1; i <= 5; i++) {

                                //primero pregunto si el hes viene con algun valor
                                if (($('#num_hes' + i).val() === null) || ($('#num_hes' + i).val() === "")) {
                                    console.log('falta hes ' + i)

                                }
                                else {
                                    bandera_hes=1;
                                    console.log('num hes ' + i + ' ' + $('#num_hes' + i).val())
                                    console.log('fecha hes ' +i+ ' ' + $('#fecha_hes' + i).val())
                                    //entonces tambien pregunto si venir su respectiva fecha con valor
                                    if (($('#fecha_hes' + i).val() === null) || ($('#fecha_hes' + i).val() === "")) {
                                        swal('ERROR', 'Debes ingresar la fecha de HES ' + $('#num_hes' + i).val(), 'error')
                                    }
                                    else {
                                        //aki inserto el aray hes en el cliente y la factura
                                        jsn_hes.push({id:factura.id,num_hes:$('#num_hes' + i).val(), fecha_hes:$('#fecha_hes' + i).val()})
                                        factura.hes=jsn_hes;
                                        console.log(jsn_hes);
                                       // swal('OK','Muy bien :)','success')

                                    }
                                }
                            }

                            if (bandera_hes===0){ //estp quiere decir que solo va la OC
                                console.log('listo con OC solamente');
                                console.log(factura);
                                factura.hes=jsn_hes;
                                arr_facturas[id_fila_seleccionada]=factura;
                                var link='<input type="button" class="btn btn-danger" name="check_oc" id="'+id_fila_seleccionada+'"   onclick="check_factura(this)" value="Emitir DTE">';
                                $("#factura"+id_fila_seleccionada).html(link)
                                swal('OK','Muy bien :) sólo con OC','success')
                                $('#myModal2').modal('toggle');

                            }
                            else{
                                console.log('listo con HES y OC');
                                console.log(factura);
                                arr_facturas[id_fila_seleccionada]=factura;
                                var link='<input type="button" class="btn btn-danger" name="check_oc" id="'+id_fila_seleccionada+'"   onclick="check_factura(this)" value="Emitir DTE">';
                                $("#factura"+id_fila_seleccionada).html(link)
                                swal('OK','Muy bien, con Hes y OC :D ','success')
                                $('#myModal2').modal('toggle');
                            }


                        }

                    }
                }
                function check_factura(e){
                var html='<span class="text-muted loading circle fa-2x"></span>';
                    // MAGIA: atrapo collection desde blade que viene del bakend
                    console.log('cliente oc?: '+e.name);

                    var index=e.id;
                    $("#factura"+index).html(html);
                    var tempArray = <?php echo json_encode($clientes); ?>;
                    if (e.name==="check_oc"){
                        console.log(arr_facturas[e.id]);
                        console.log(tempArray[e.id]);
                        var factura=arr_facturas[e.id];
                    }
                    else{
                        console.log(tempArray[e.id]);
                        var factura=tempArray[e.id];
                    }

                    console.log('factura que se va al bakend: ');
                    console.log(factura);

                        $.ajax("/facturacion/facturar_masiva", {
                            type: 'POST',
                            dataType: "JSON",
                            data: {factura:factura},
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },

                            success: function (response) {

                                if (response.success===true)
                                {
                                    // swal('OK',response.msg + ' url: '+response.url,'success')
                                    var link='<a href="'+response.url+'" target="_blank"  ><i class="fa fa-file-pdf-o"></i></a>'
                                    $("#factura"+index).html(link)
                                }
                                else{
                                    // swal('ERROR',response.msg,'error')
                                    $("#factura"+index).html(response.msg)
                                }

                            },
                            error: function (response) {
                                //console.log(data)
                                swal("Ups!", "Hubo problema lógico con base de datos!", "error");


                            }
                        });

                }

                $(document).ready(function() {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

                    elems.forEach(function(html) {
                        var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
                    });
                    // $('.footable').footable();
                    // $('.footable2').footable();

                    $('.dataTables-example').DataTable({
                        pageLength: 50,
                       // order: [[ 0, "desc" ]],
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [

                            {extend: 'excel', title: 'Notas de Venta de Teledata'},
                            {extend: 'pdf', title: 'Notas de Venta de Teledata'},


                        ]

                    });

                });

                function genera_factura(id) {
                    //  alert(id);
                    var rvid=id;

                    swal({
                            title: "Estas seguro de Facturar?",
                            text: "se generará un documento en Bsale y en SII!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#5cb85c",
                            confirmButtonText: "Sí, Generar Documento!",
                            cancelButtonText: "No, me arrepentí!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function (isConfirm) {
                            if (isConfirm) {


                                //console.log('holaa: '+ (dis.name));
                                $.get("/url/"+rvid+"/1", function (response) {

                                    if (response.success===true){

                                        swal("Exito!", "se ha generado el documento exitosamente.", "success");
                                        window.location.href="{{ url('/facturacion/nota_de_venta') }}";
                                        // console.log(response.arr);
                                        //console.log('*******************');
                                        //console.log(response.arr.Message);

                                    }else {

                                        swal("Error desde Bsale!", response.arr.Message, "success");
                                        console.log(response.arr);
                                        console.log('*******************');
                                        console.log(response.arr.Message);
                                    }
                                });


                            } else {
                                swal("Cancelado", "Se conserva estado de nota de venta", "error");
                            }
                        });
                }
                function genera_factura_previa(id){
                    var rvid=id;
                    swal('Espere','Se va a generar una vista previa del documento','warning')
                    $.get("/facturacion/facturar_nota_venta/"+rvid+"/2", function (response) {
                        //  console.log(response.url);
                        // console.log(response.arr);
                        var win = window.open(response.url, '_blank');
                        win.focus();
                        // $(dis).closest('tr').remove();
                    });
                }

                function enviar_documento(id_factura) {

                    var rvid=id_factura;

                    swal({
                            title: "Estas seguro de Enviar?",
                            text: "Se Enviará un e-mail al cliente",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#5cb85c",
                            confirmButtonText: "Sí, Enviar Documento!",
                            cancelButtonText: "No, me arrepentí!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function (isConfirm) {
                            if (isConfirm) {
                                //console.log('holaa: '+ (dis.name));
                                $.get("/facturacion/enviar_documento/"+rvid+"", function (response) {
                                    // console.log(response.success)
                                    swal("Exito!", "se ha enviado el documento exitosamente.", "success");

                                });

                            } else {
                                swal("Cancelado", "Se conserva estado de nota de venta", "error");
                            }
                        });
                }

            </script>
@endsection