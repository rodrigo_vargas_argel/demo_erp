@extends('layouts.app')
@section('title')
   Titulo Pagina
@endsection
@section('content')



    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Nombre Modulo</a>
                </li>
                <li class="active">
                    <strong>Pagina</strong>
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">

        <!-- row formulario ejemplo -->
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Titulo Form <small> any descriptionss..</small></h5>

                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">

                                <form name="frmBodega" method="post" action="{{ url('mantenedores/bodegas_guardar') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Dirección</label>
                                        <input type="text" name="direccion" class="form-control" value="{{old('direccion')}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Télefono</label>
                                        <input type="text" name="telefono" class="form-control" value="{{old('telefono')}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Responsable</label>
                                        <select name="personal_id" class="form-control">
                                            <option value="">-Seleccione-</option>

                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label >Correo</label>
                                        <input type="text" name="correo" class="form-control" value="{{old('correo')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tipo</label>
                                        <select name="principal" class="form-control">
                                            <option value="0" {{ old('principal') == 0 ? 'selected' : '' }}>Principal</option>
                                            <option value="1" {{ old('principal') == 1 ? 'selected' : '' }}>Estacion</option>
                                            <option value="2" {{ old('principal') == 2 ? 'selected' : '' }}>Movil (tecnico)</option>
                                        </select>

                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick=""><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row tabla ejemplo -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <div class="col-lg-5">
                            <h5><i class="fa fa-money"></i>  Titulo de la Tabla</h5>
                        </div>

                        <div class="col-lg-7">
                            <form name="filtro" method="post" action="{{ url('/') }}">
                                {{csrf_field()}}
                                <select class="selectpicker form-control" style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="this.form.submit()" >
                                    <option value="">-Digite y Seleccione Cliente-</option>

                                </select>
                            </form>
                        </div>


                        <div class="ibox-content table-responsive">

                            <table class="table  table-bordered table-striped dataTables-example "  id="dataTables-example" >
                                <thead>
                                <tr>

                                    <th width="15%">Cliente</th>
                                    <th>Num Doc</th>
                                    <th>Fecha_Doc</th>
                                    <th>Fecha_Pago</th>
                                    <th>Glosa</th>

                                    <th>Total Doc</th>
                                    <th>Pagos</th>
                                    <th>Saldo</th>
                                    <th>A favor</th>

                                </tr>
                                </thead>
                                <tbody>

                                <!-- aqui pones tu foreach pa ccompletar la tabla -->

                                </tbody>


                            </table>




                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
        @endsection
<!-- tus funciones javascript locales -->
        @section('mijava')
            <script>
                $(document).ready(function() {

                    // $('.footable').footable();
                    // $('.footable2').footable();

                    $('.dataTables-example').DataTable({

                        pageLength: 25,
                        order: [[0, "desc"]],
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        sum: 5,
                        buttons: [

                            {extend: 'excel', title: 'Documentos Pagados'},
                            {extend: 'pdf', title: 'Documentos Pagados'},


                        ],


                    });


                });


            </script>
@endsection