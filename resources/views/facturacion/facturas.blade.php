@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php
            if (isset($cliente_old)){
                $clienteold=$cliente_old;
            }
            else{
                $clienteold="0";
            }
    use Carbon\Carbon;
    $total_saldo=0;
    $total_afavor=0;
    $total_facturado=0;
    //$carbon = new \Carbon\Carbon();
    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Facturacion</a>

                </li>
                <li class="active">
                    <strong>Documentos Emitidos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        <div id="totales_check" name="totales_check"></div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                       <div class="ibox-title">
                             <div class="col-lg-4">
                                <h5><i class="fa fa-files-o"></i>  Documentos Emitidos</h5>
                             </div>

                            <div class="col-lg-8 ">
                                <form name="filtro" id="filtro" method="post" action="{{ url('facturacion/lista_filtro') }}">
                                    {{csrf_field()}}
                                    <div class="col-lg-3 pull-left" >
                                    <input type="number" class="form-control" name="num_doc" id="num_doc" placeholder="Numero Documento" >
                                    </div>
                                    <div class="col-lg-9">
                                    <select class="selectpicker form-control " style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="$('#toggleSpinners').click(); this.form.submit()" >
                                        <option value="">-Digite y Seleccione Cliente-</option>
                                        @foreach($clientes as $cliente)
                                            <option value="{{$cliente->rut}}" {{ $clienteold ==$cliente->rut ? 'selected' : '' }} rut="{{$cliente->rut}}">{{$cliente->nombre}}  {{$cliente->rut}}-{{$cliente->dv}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </form>

                             </div>



                         </div>
                    <div class="ibox-content table-responsive">

                        <table class="table  table-bordered table-striped dataTables-example "  id="dataTables-example" >
                            <thead>
                            <tr>

                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Fecha v.</th>
                                <th>Tipo Doc.</th>
                                <th width="20%">Cliente</th>
                                <th width="15%">Descripción</th>
                                <th>Total</th>
                                <th>Pagos</th>
                                <th>Saldo</th>
                                <th>A favor</th>
                                <th>Num Docs</th>
                                <th>Acciones</th>
                                <!--<th>Responsable</th>
                               <th>Numero_hes</th>
                               <th>Acciones</th>-->

                            </tr>
                            </thead>
                            <tbody>

                                @foreach($facturas as $index=>$factura)
                                    <?php
                                    $fecha1=Carbon::parse($factura->FechaFacturacion);
                                    $fecha1=$fecha1->format('d-m-Y');
                                    $fecha2=Carbon::parse($factura->FechaVencimiento);
                                    $fecha2=$fecha2->format('d-m-Y');
                                    $pagos=0;
                                    $neto=0;
                                    $total=0;
                                    $saldo=0;
                                    $afavor=0;

                                    if ($factura->detalle != null){
                                        //var_dump($factura->detalle);

                                        $glosa="";
                                        $neto=$factura->detalle->sum('Valor');
                                        $total=$factura->detalle->sum('Total');
                                        $pagos=$factura->pagos->sum('Monto');
                                        $concepto=$factura->detalle->values()->first();

                                    }

                                    else{
                                        $neto=0;
                                        $total=0;
                                        $pagos=0;

                                    }
                                    if ($factura->pagos != null){
                                        $pagos=$factura->pagos->sum('Monto');
                                        $saldo=$total-$pagos;
                                        $saldo=round( $saldo, 0, PHP_ROUND_HALF_UP);
                                        if ($saldo < 0){
                                            $afavor=$saldo;
                                                $saldo=0;
                                            $afavor=round( $afavor, 0, PHP_ROUND_HALF_UP);
                                            $afavor=$afavor*(-1);  // pff el valor es negativo, pero las titis se confunden con el signo (-) por eso se los dejo en (+) y en columna aparte.
                                        }

                                    }
                                    else{
                                        $pagos=0;
                                        $saldo=0;
                                    }

                                    if($factura->EstatusFacturacion==2){
                                        $estilo='text-danger';
                                        $saldo="";
                                        $pagos="";
                                        if($factura->devoluciones->DevolucionAmount < $total){
                                            $pagos=$factura->pagos->sum('Monto');
                                            $saldo=$total-$factura->devoluciones->DevolucionAmount-$pagos;

                                        }

                                    }

                                    else{
                                        $estilo='text-dark';
                                    }

                                    if($factura->EstatusFacturacion==3){
                                        $estilo='text-warning';


                                    }


                                    ?>
                                    <tr class="  {{$estilo}}">
                                       <td class="{{$estilo}}">
                                           <input type="checkbox" name="check{{$factura->Id}}" id="check{{$factura->Id}}" monto="{{round( $total, 0, PHP_ROUND_HALF_UP)}}" onclick="suma_resta(this)">
                                            <small> {{$factura->Id}}</small>
                                        </td>
                                        <td class="{{$estilo}}">
                                            <small>{{$fecha1}}</small>
                                        </td>
                                        <td class="{{$estilo}}">
                                            <small>{{$fecha2}}</small>
                                        </td>
                                        <td class="{{$estilo}}">
                                            <small>{{$factura->cliente->tipoClientes->nombre}}</small>
                                        </td>
                                        <td class="{{$estilo}}">
                                            <small>{{$factura->cliente->nombre}}</small>
                                        </td>

                                       <td>
                                            <small>{{$concepto['Concepto']}} </small>
                                        </td>
                                        <td class="{{$estilo}}">
                                            <small>{{number_format($total,0,',','.')}} </small>
                                        </td>


                                        <td class="{{$estilo}}">
                                            @if($pagos > 0)
                                               <a href="#" title="Ver Pagos"  data-toggle="modal" data-target="#myModal2" onclick="modal_pagos(this)" id="{{$index}}"> <small><i class="fa fa-eye"></i> {{number_format($pagos,0,',','.')}} </small></a>
                                                @else
                                                <small>{{$pagos}} </small>
                                                @endif
                                        </td>
                                        <td class="{{$estilo}}">
                                            @if($saldo != 0)
                                            <small>{{number_format($saldo,0,',','.')}} </small>
                                                @else
                                                <small>{{$saldo}}</small>
                                            @endif
                                        </td>
                                          <td class="{{$estilo}}">
                                              @if($afavor != 0)
                                                  <small>{{number_format($afavor,0,',','.')}} </small>
                                              @else
                                                  <small>{{$afavor}}</small>
                                              @endif
                                          </td>

                                         <td>
                                             <small> <a href="{{$factura->UrlPdfBsale}}" title="ver documento" target="_blank"><i class="fa fa-file-pdf-o"></i> {{$factura->NumeroDocumento}}</a></small>
                                             @if ($factura->devoluciones != null)
                                                 @if ($factura->devoluciones->DevolucionAnulada == '1')
                                                     |  <small class="text-danger"> <a href="{{$factura->devoluciones->UrlPdfBsale}}" title="ver NC" target="_blank"><i class="text-danger">NC{{$factura->devoluciones->NumeroDocumento}}</i></a></small>
                                                     | <a target="_blank" href="{{$factura->devoluciones->anulaciones->UrlPdfBsale}}">ND{{$factura->devoluciones->anulaciones->NumeroDocumento}}</a>

                                                 @else
                                                     @if($factura->devoluciones->priceAdjustment == 1)
                                                        |  <small class="text-danger"> <a href="{{$factura->devoluciones->UrlPdfBsale}}" title="ver NC" target="_blank"><i class="text-danger">NC{{$factura->devoluciones->NumeroDocumento}}</i></a></small><small class="text-muted">-${{ number_format($factura->devoluciones->DevolucionAmount,0,',','.')}}  </small>
                                                      @else
                                                         |  <small class="text-danger"> <a href="{{$factura->devoluciones->UrlPdfBsale}}" title="ver NC" target="_blank"><i class="text-danger">NC{{$factura->devoluciones->NumeroDocumento}}</i></a></small></small>
                                                     @endif
                                                 @endif


                                             @endif

                                         </td>
                                        <td>
                                            @if($factura->EstatusFacturacion==1) <!--  cuando no tiene NC-->
                                            <a href="#"  title="Enviar Documento al Cliente"  onclick="enviar_documento({{$factura->Id}})" class="badge badge-success" > <i class="fa fa-send-o"></i></a>

                                                @if($saldo >1)
                                                    <a href="#" class="badge badge-primary open-Modal"  onclick="" data-toggle="modal" data-target="#myModal" monto="{{$saldo}}" data-id="{{$factura->Id}}" title="Ingresar Pago" ><i class="fa fa-dollar"> </i></a>
                                                    <a href="#" class="badge badge-mint text-danger open-Modal"  id="{{$index}}"  title="Generar Nota de Crédito" data-toggle="modal" onclick="carga_nc(this)" data-target="#myModalnc" monto="{{$saldo}}" cliente="{{$factura->Rut}}" monto="{{$saldo}}" data-id="{{$factura->Id}}"><i class="fa fa-reply"> </i></a>
                                                @else

                                                @endif
                                            @else   <!--  cuando si tiene NC-->

                                                @if($factura->EstatusFacturacion==2) <!--//Cuando no tiende nd anulacion -->
                                                    @if ($saldo > 2)

                                                        <a href="#" class="badge badge-primary open-Modal"  onclick="" data-toggle="modal" data-target="#myModal" cliente="{{$factura->Rut}}" monto="{{$saldo}}" data-id="{{$factura->Id}}" title="Ingresar Pago" ><i class="fa fa-dollar"> </i> </a>
                                                        <a href="#" class="badge badge-mint text-info  "  title="Generar Nota de Dédito" onclick="emitir_nd({{$factura->devoluciones->Id}})"><i class="fa fa-mail-forward"> </i></a>
                                                    @else
                                                         <a href="#" class="badge badge-mint text-info  "  title="Generar Nota de Dédito" onclick="emitir_nd({{$factura->devoluciones->Id}})"><i class="fa fa-mail-forward"> </i></a>
                                                    @endif
                                                 @else  <!-- si estatus es 3, que tiene nd anulacion -->
                                                <a href="#"  title="Enviar Documento al Cliente"  onclick="enviar_documento({{$factura->Id}})" class="badge badge-success" > <i class="fa fa-send-o"></i></a>

                                                @if($saldo >1)
                                                    <a href="#" class="badge badge-primary open-Modal"  onclick="" data-toggle="modal" data-target="#myModal" monto="{{$saldo}}" data-id="{{$factura->Id}}" title="Ingresar Pago" ><i class="fa fa-dollar"> </i></a>
                                                    <a href="#" class="badge badge-mint text-danger open-Modal"  id="{{$index}}"  title="Generar Nota de Crédito" data-toggle="modal" onclick="carga_nc(this)" data-target="#myModalnc" monto="{{$saldo}}" cliente="{{$factura->Rut}}" monto="{{$saldo}}" data-id="{{$factura->Id}}"><i class="fa fa-reply"> </i></a>
                                                @else

                                                @endif


                                                 @endif
                                            @endif


                                        </td>


                                    </tr>
                                  <?php
                                  if (($saldo != "")&&($saldo !=null))
                                  $total_saldo=$total_saldo + $saldo;
                                    if($factura->EstatusFacturacion!=2)
                                    $total_facturado=$total_facturado+$total;

                                    $total_afavor=$total_afavor+$afavor;
                                  ?>
                                @endforeach

                            </tbody>
                            <tfoot>
                            <tr><td colspan="5"></td><td id="tot_neto">Total Facturado: </td><td id="tot_tot"><label>{{number_format($total_facturado,0,',','.')}}</label></td><td id="tot_pago">Total Saldo :</td><td id="tot_saldo"><label>{{number_format($total_saldo,0,',','.')}}</label></td><td><label>{{number_format($total_afavor,0,',','.')}}</label></td><td></td></tr>
                            </tfoot>

                        </table>

                        <!**********************MODALES ***********************************-->
                        <!******************************************************************-->

                        <!***********PAGOS****************************-->
                        <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">

                                    <div class="modal-header">
                                       <h2> <i class="fa fa-dollar"> Efectuar Pago</i></h2>
                                    </div>
                                    <div class="modal-body">
                                        <h3>Monto a pagar: $<span id="monto_a_pagar"> </span></h3>

                                        <div class="dropbox">
                                            <input type="hidden" name="factura_id" id="factura_id">
                                            <input type="hidden" name="rut_cliente" id="rut_cliente"  value="<?php echo $clienteold; ?>">
                                            <input type="number"  class="form-control" placeholder="Ingrese pago" name="montopago" id="montopago">
                                        </div>
                                        <div class="form-group">
                                            <label>Fecha de Pago</label>
                                            <input type="date"  class="form-control" value="<?php echo date('Y-m-d'); ?>" name="fechapago" id="fechapago">
                                        </div>

                                        <div class="form-group">
                                            <label>Tipo de Pago</label>
                                            <select name="tipo_pago" id="tipo_pago" class="form-control">
                                                @foreach($tipo_pago as $fpago)
                                                    <option value="{{$fpago->id}}">{{$fpago->nombre}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" name="btnguardar_pago" id="btnguardar_pago" onclick="guardar_pago()">Guardar Pago</button>
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <!******************VER PAGOS **************************************->
                        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content animated flipInY">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title">PAGOS</h4>
                                        <small class="font-bold" id="lbl_cliente"></small>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table table-responsive table-bordered ">
                                                <th><tr>
                                                    <td>ID</td>
                                                    <td>Fecha Pago</td>
                                                        <td>Fecha Doc</td>
                                                        <td>Num Doc</td>
                                                         <td>Monto</td>
                                                        <td>Forma Pago</td>
                                                    </tr>
                                                </th>
                                                <tbody id="tabla_pagos"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!****************** NOTA CREDITO ---DEVOLUCION-- ***************-->
                        <div class="modal inmodal" id="myModalnc" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                    <form name="frm_nc" id="frm_nc" method="post">
                                        {{csrf_field()}}
                                        <div class="modal-header">
                                            <h2><strong><i class="fa fa-reply text-danger"></i>  NOTA DE CRÉDITO</strong></h2>
                                            <h4 id="nc_subtitulo"></h4>
                                        </div>

                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Seleccione Tipo de NC</label>
                                                    <select name="tipoNotaCredito" id="tipoNotaCredito" class="form-control" onchange="cambia_nc(this)">
                                                       <option value="1">NC Total</option>
                                                        <option value="2">NC Parcial</option>
                                                        <option value="3">NC Texto</option>
                                                    </select>
                                                </div>
                                                <hr>

                                                <div class="dropbox">

                                                    <input type="hidden" name="referenceDocumentId" id="referenceDocumentId" value="">
                                                    <input type="hidden" name="rut_cliente" id="rut_cliente" value=""  >
                                                    <input type="hidden" name="fid" id="fid" value=""  >

                                                </div>
                                                <div class="form-group table-responsive"  id="valores_nc" name="valores_nc" style="display: none;">
                                                    <label></label>
                                                   <table class="table table-bordered">
                                                       <thead>
                                                       <th>Descripcion </th><th>Cantidad</th><th>Valor</th><th>Nuevo Valor</th>
                                                       </thead>
                                                       <tbody  name="tb_nc_detalle" id="tb_nc_detalle"></tbody>
                                                   </table>
                                                </div>
                                                <div class="form-group">
                                                    <label>Motivo</label>
                                                    <textarea class="form-control" id="motive" name="motive" cols="20" rows="5"></textarea>
                                                </div>

                                            </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger" name="btnguardar_nc" id="btnguardar_nc" >Generar NC</button>
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>

        var total_suma_check;

        $(document).ready(function() {


            $('.dataTables-example').DataTable({

                pageLength: 25,
                order: [[0, "desc"]],
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                sum: 5,
                buttons: [

                    {extend: 'excel', title: 'Documentos Emitidos'},
                    {extend: 'pdf', title: 'Documentos Emitidos'},


                ],

            });

            total_suma_check=0;

        })

        //*****************FILTRO NUM DOC*********************************

        $('#num_doc').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            //console.log('hola: '+keycode);
            if(keycode === 13){
                //console.log('le achuntaste: '+keycode);
                $('#toggleSpinners').click();
                $('#filtro').submit();

            }
        });



        function cambia_nc(e){
            //alert(e.value);
            var tipo=e.value;
            if (tipo==2){

                $('#valores_nc').css('display','block');
            }
            else{
                $('#valores_nc').css('display','none');
            }
        }

        //******************************************* LEVANTA MODAL NC ****************************
        function carga_nc(e){

            // MAGIA: atrapo collection desde blade que viene del bakend
            var tempArray = <?php echo json_encode($facturas); ?>;

            var cont=0;
           // console.log(tempArray[e.id].NumeroDocumento);
            var numdoc="Documento N°: "+tempArray[e.id].NumeroDocumento;
            var cliente= " "+tempArray[e.id].cliente.nombre;
           // var monto=", Monro:"+tempArray[e.id].detalle.sum('Valor');
            //console.log(monto);
            $('#nc_subtitulo').html(numdoc + ', '+ cliente);
            $("#tb_nc_detalle").empty();
            tempArray[e.id].detalle.forEach(function(entry) {

                $("#tb_nc_detalle").append('<tr><td>'+tempArray[e.id].detalle[[cont]].Concepto+'</td><td align="right">'+tempArray[e.id].detalle[[cont]].Cantidad+'</td>' +
                    '<td align="right">'+tempArray[e.id].detalle[[cont]].Valor+'</td><td><input type="text" name="nuevo_valor[]" id="nuevo_valor'+cont+'"><input type="hidden" name="id_item[]" id="id_item'+cont+'">' +
                    '<input type="hidden" name="documentDetailIdBsale[]" id="documentDetailIdBsale'+cont+'" value="'+tempArray[e.id].detalle[[cont]].documentDetailIdBsale+'">  ' +
                    '<input type="hidden" name="quantity[]" id="quantity'+cont+'" value="'+tempArray[e.id].detalle[[cont]].Cantidad+'"> </td>')

                cont=cont+1;
            }, this);
           // alert();
            var monto=$(e).attr('monto');
            var rut=$(e).attr('cliente');
            var fid=$(e).attr('data-id');

            $('input[name=referenceDocumentId]').val(fid);
            $('input[name=rut_cliente]').val(rut);
            $('input[name=fid]').val(fid);
            console.log(monto+' '+rut+' '+fid);

        }
        $('#frm_nc').on('submit', function (e) {

            e.preventDefault(e);
            if ($('#motive').val().trim().length < 1){
                swal('Error','Debes agregar un  motivo','error');
            }
            else{

                swal('OK, espere un momento','Se está generando una Nota de Crédito','warning')
                $.ajax({

                    url: '/facturacion/guardar_nc',
                    type: "POST",
                    dataType: "JSON",
                    data:  new FormData(this),
                    processData: false,
                    contentType: false,

                    success: function (data) {

                       console.log(data.response_array.status);
                        if(data.response_array.status ==1)
                        {
                            console.log('nc return:' +data.nc_documento)
                            swal('success','exito'+data.response_array.Message,'success');
                            window.location.href = "{{ url('facturacion/lista_filtro2/'.$clienteold) }}";
                        }
                        else{
                            console.log(data.response_array.Message);
                            swal('error',data.response_array.Message,'error');
                        }


                    },
                    error: function (data) {
                        //console.log(data)
                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");


                    }
                })

            } //fin else valida Motivo


        })





        // ************** pedido por <3 Francesca <3  chekbox en columa id que sume los totales
        function suma_resta(e){
            //alert(e.id);
            var monto=$('#'+e.id).attr('monto');
            if( $('#'+e.id).is(':checked') ) {

                total_suma_check=parseInt(total_suma_check)+parseInt(monto);
                $('#totales_check').html('<h2>Totales: $'+new Intl.NumberFormat("de-DE").format(total_suma_check)+'</h2>');
            }


        else{
                total_suma_check=parseInt(total_suma_check)-parseInt(monto);
                $('#totales_check').html('<h2>Totales: $'+new Intl.NumberFormat("de-DE").format(total_suma_check)+'</h2>');
            }
        }



        function guardar_pago(){
            var id_factura=$('#factura_id').val();;
            var monto=$('#montopago').val();
            var fecha_pago=$('#fechapago').val();
            var tipo_pago=$('select[id=tipo_pago]').val();
            var rut_cliente=$('#rut_cliente').val();
            var maxmonto=$('#monto_a_pagar').html();
            maxmonto=maxmonto.replace(".", "", "gi");

            if ((parseInt(monto) >= 1) ){ //&& (parseInt(monto) <= parseInt(maxmonto))
                $('#btnguardar_pago').attr('disabled','true');
              //  alert(monto +" "+fecha_pago+" "+tipo_pago+" "+maxmonto);

               $.get("/facturacion/guardar_pago/"+id_factura+"/"+monto+"/"+fecha_pago+"/"+tipo_pago, function (response) {
                    // console.log(response.success)
                   if (response.success===true) {
                       swal("Exito!", response.msg, "success");
                       if(rut_cliente==="")
                       window.location.href = "{{ url('/facturacion/facturas') }}";
                       else
                           {
                             //  var miurl="/"+rut_cliente;
                           window.location.href = "{{ url('facturacion/lista_filtro2/'.$clienteold) }}";
                           }
                   }
                   else{
                       $('#btnguardar_pago').attr('disabled','false');
                       swal("Error!", response.msg, "error");
                   }
                });

            }
            else{
                swal('Cometiste un error', 'El monto digitado debe ser entre 1 y '+new Intl.NumberFormat("de-DE").format(maxmonto), 'error');
            }

        }

        function elimina_pago(id) {
            // alert(id);
            var rvid=id;
            var rut_cliente=$('#rut_cliente').val();

            swal({
                    title: "Estas segura de Eliminar el pago?",
                    text: "no vayas a equivocarte.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/facturacion/eliminar_pago/"+rvid+"/", function (response) {

                            if (response.success===true) {
                                swal("Ok", response.msg , "success");
                                if(rut_cliente==="")
                                    window.location.href = "{{ url('/facturacion/facturas') }}";
                                else
                                {
                                    //  var miurl="/"+rut_cliente;
                                    window.location.href = "{{ url('facturacion/lista_filtro2/'.$clienteold) }}";
                                }
                            }
                            else{

                                swal("ERROR", response.msg, "error");
                            }


                        });


                    } else {
                        swal("Bueno, mejor así", "Se conserva el pago ;)", "success");
                    }
                });
        }
        function modal_pagos(e){  //funcion para ver los pagos efectuados
            // MAGIA: atrapo collection desde blade que viene del bakend
            var tempArray = <?php echo json_encode($facturas); ?>;
            var fecha1;
            var FechaPago;
            var fecha2;
            var FechaFacturacion;
            var cont=0;
            var total_pagos=0;
            var monto_facturado=0;



            $("#tabla_pagos").empty();
            tempArray[e.id].pagos.forEach(function(entry) {
                console.log(tempArray[e.id].pagos[[cont]]);
                 total_pagos=total_pagos+tempArray[e.id].pagos[[cont]].Monto;
                 fecha1= tempArray[e.id].pagos[[cont]].FechaPago;
                // monto_facturado= tempArray[e.id].total;
                 FechaPago=new Date(fecha1).toLocaleString();//doy vuelta las fecha pago
                fecha2=tempArray[e.id].FechaFacturacion;
                FechaFacturacion=new Date(fecha2).toLocaleDateString();//doy vuelta las fecha fact
            $("#tabla_pagos").append('<tr><td>'+tempArray[e.id].pagos[[cont]].Id+'</td><td>'+fecha1+'</td><td>'+fecha2+'</td><td>'+tempArray[e.id].NumeroDocumento+'</td>' +
                '<td>'+new Intl.NumberFormat("de-DE").format(tempArray[e.id].pagos[[cont]].Monto)+'</td><td>'+tempArray[e.id].pagos[cont].nombre_pago+'</td><td><a href="#" onclick="elimina_pago('+tempArray[e.id].pagos[[cont]].Id+')"><i class="fa fa-trash text-danger"></i></a> </td></tr>')

           cont=cont+1;
            }, this);
            $("#tabla_pagos").append('<tr><td colspan="4" align="right"><strong>Total Pagado: </strong></td><td><strong>'+new Intl.NumberFormat("de-DE").format(total_pagos)+'</strong></td></tr>')
                $("#lbl_cliente").html('CLIENTE '+tempArray[e.id].cliente.nombre)
            // console.log ('chauuu '+tempArray[e.id].pagos[[e.id]].User);

        }

        $(document).on("click", ".open-Modal", function () {  //pagar factura
            var factura_id = $(this).data('id');
            var monto= $(this).attr('monto');
            $('#monto_a_pagar').html(new Intl.NumberFormat("de-DE").format(monto) );
            $('#montopago').val(monto);
            $('#factura_id').val(factura_id);

           // alert('fid: '+factura_id+ 'monto: '+monto);

        });


        function genera_factura_previa(id){
            var rvid=id;
            swal('Espere','Se va a generar una vista previa del documento','warning')
            $.get("/facturacion/facturar_nota_venta/"+rvid+"/2", function (response) {
                //  console.log(response.url);
                // console.log(response.arr);
                var win = window.open(response.url, '_blank');
                win.focus();
                // $(dis).closest('tr').remove();
            });
        }

        function enviar_documento(id_factura) {

            var rvid=id_factura;

            swal({
                    title: "Estas seguro de Enviar?",
                    text: "Se Enviará un e-mail al cliente",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, Enviar Documento!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/facturacion/enviar_documento/"+rvid+"", function (response) {
                            // console.log(response.success)
                            swal("Exito!", "se ha enviado el documento exitosamente.", "success");

                        });


                    } else {
                        swal("Cancelado", "Se conserva estado de nota de venta", "error");
                    }
                });
        }

        function emitir_nd(id){

            var rvid=id;
                console.log('devolucionId: '+rvid);
            swal({
                    title: "¿Emitir Nota Débito?",
                    text: "Se hará un documento en SII",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Sí, Emitir ND!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {

                        swal('OK, espere un momento','Se está generando una Nota de Débito','warning')
                        $.ajax({

                            url: '/facturacion/guardar_nd/'+rvid,
                            type: "GET",
                            dataType: "JSON",

                            processData: false,
                            contentType: false,

                            success: function (data) {

                                console.log(data.response_array.status);
                                if(data.response_array.status ==1)
                                {
                                    console.log('nc return:' +data.response_array.Message)
                                    swal('success',data.response_array.Message,'success');
                                    window.location.href = "{{ url('facturacion/lista_filtro2/'.$clienteold) }}";
                                }
                                else{
                                    console.log(data.response_array.Message);
                                    swal('error',data.response_array.Message,'error');
                                }


                            },
                            error: function (data) {
                                //console.log(data)
                                swal("Ups!", "Hubo problema lógico con base de datos!", "error");


                            }
                        })


                    } else {
                        swal("Cancelado", "Se conserva estado de nota de venta", "error");
                    }
                });

        }



    </script>
@endsection