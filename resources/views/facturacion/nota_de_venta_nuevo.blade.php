@extends('layouts.app')
@section('title')
    Nota de Venta
@endsection
@section('content')
    <?php


    $fecha1=date('Y-m-d');

    ?>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Facturacion</a>
                </li>
                <li class="active">
                    <strong>Nueva Nota de Venta </strong>
                    ->Valor Uf Hoy: <text class="text-info fa-1x">$ {{$uf}}</text>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">


                    <form name="frmNota_venta" method="post" action="{{ url('mantenedores/bodegas_guardar') }}">
                        {{csrf_field()}}
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">

                                            <div class="col-lg-6">
                                                 <div class="form-group">
                                                    <label>Cliente: </label>
                                                     <input type="hidden" name="uf" id="uf" value="{{$uf}}">
                                                     <select class="selectpicker form-control" name="cliente" id="cliente" data-live-search="true" onchange="carga_servivio(this)" >
                                                        <option value="">-Digite y Seleccione-</option>
                                                        @foreach($clientes as $cliente)
                                                            <option value="{{$cliente->rut}}" rut="{{$cliente->rut}}">{{$cliente->nombre}} - {{$cliente->rut}}</option>
                                                            @endforeach
                                                    </select>

                                                 </div>

                                                <div class="form-group">
                                                    <label>Numero OC</label>

                                                    <input type="number" name="oc" id="oc" class="form-control" value="{{old('oc')}}">
                                                </div>

                                                <div class="form-group"><label class=" control-label">Cuántos HES?</label>
                                                    <div>
                                                        <select class="form-control" name="cont_hes" id="cont_hes" onchange="levanta_hes()">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>

                                                        </select>
                                                    </div>
                                                    <label class="text-success" id="label_hes"> </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Fecha Nota Venta </label>
                                                    <input type="date" name="fecha_emision" id="fecha_emision" class="form-control" value="{{$fecha1}}">

                                                </div>

                                                <div class="form-group">
                                                    <label>Fecha OC </label>
                                                    <input type="date" name="fecha_oc" id="fecha_oc" class="form-control" value="{{$fecha1}}">
                                                </div>

                                                <div class="form-group">
                                                    <label>Forma de Pago: </label>
                                                    <select  name="tipo_pago_bsale" id="tipo_pago_bsale" class="form-control" >

                                                        @foreach($tipo_pago_bsale as $tipo)
                                                            <option value="{{$tipo->id}}" >{{$tipo->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                </div>
                            </div>
                        </div>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5><i class="glyphicon glyphicon-list-alt"> </i>  Detalle <small> </small></h5>

                                <div class="ibox-tools"  >
                                                <span class="">
                                                    <button type="button" title="Agregar Servicios" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                                                        <i class="fa fa-podcast"> </i>
                                                    </button>
                                                </span>
                                                <span class="">
                                                     <button type="button" title="Agregar Item" class="btn btn-default" data-toggle="modal" data-target="#myModal2">
                                                        <i class="glyphicon glyphicon-list-alt"> </i>
                                                    </button>
                                                </span>
                                </div>

                            </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <th>Código</th>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Cantidad</th>
                                                        <th>SubMonto</th>
                                                        <th>Descuento</th>
                                                        <th>Monto</th>
                                                    </thead>
                                                    <tbody id="tabla_detalle">

                                                    </tbody>
                                                    <tfoot>
                                                        <tr><td align="right"  colspan="6"><strong>Neto:</strong></td><td align="right"><strong><span id="neto"></span></strong></td></tr>
                                                        <tr><td align="right"  colspan="6"><strong>Iva 19%:</strong></td><td align="right"><strong><span  id="iva"></span></strong></td></tr>
                                                        <tr><td  align="right" colspan="6"><strong>Total:</strong></td><td align="right"> <strong><span id="total"></span></strong></td></tr>
                                                    </tfoot>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                    <div style="padding-bottom: 20px;">
                                    <a href="#" onclick="guardar_nota()" id="btn_guardar" name="btn_guardar" class="btn btn-active-dark btn-success pull-right"><i class="fa fa-save"></i> Guardar</a>
                                    </div>
                                </div>
                        </div>
                        <!- ******************************MODALS***************************************************************************************************************************************-->
                        <div>


                            <div class="modal inmodal " id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated  bounceInRight">
                                        <div class="modal-header">
                                            <h3><i class="fa fa-podcast"> </i> Agregar Servicios a la Nota de Venta</h3>
                                        </div>
                                        <div class="panel-bordered-primary">

                                            <div class="form-group"><label class="col-sm-2 control-label"></label>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <table class="table table-hover table-bordered table-striped table-hover">
                                                            <thead>
                                                            <th class="col-lg-2">Seleccione</th>
                                                            <th class="col-lg-2">Codigo</th>
                                                            <th class="col-lg-4">Descripcion</th>
                                                            <th class="col-lg-2">Valor UF</th>
                                                            <th class="col-lg-2" width="30px">% Desc.</th>

                                                            </thead>
                                                            <tbody id="tabla_servicios">

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                           <!-- <button type="button" name="btn_tipo" id="btn_tipo" class="btn btn-primary"><i class="fa fa-save"></i> Agregar</button>-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- ***********************MODAL 2 AGREGAR ITEMS **********************************-->
                            <div class="modal inmodal " id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated bounceIn">
                                        <div class="modal-header">
                                            <h3> <i class="glyphicon glyphicon-list-alt"> </i> Agregar Item a la Nota de Venta</h3>
                                        </div>

                                            <div class="modal-body">
                                               <div class="col-lg-12" id="form_items" >
                                                   <div class="row">
                                                       <div class="panel-bordered-primary">
                                                                        <div class="form-group">
                                                                            <label id="lb_descripcion">Descripcion</label>
                                                                            <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{old('descripcion')}}">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label id="lb_precio">Precio</label>
                                                                            <input type="number" name="precio" id="precio" class="form-control" value="{{old('precio')}}" onkeyup="calcula_subtotal()">
                                                                            <input type="hidden" name="precio_pesos" id="precio_pesos" class="form-control" >
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label id="lb_cantidad">Cantidad</label>
                                                                            <input type="number" name="cantidad" id="cantidad" class="form-control" value="1">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Descuento %</label>
                                                                            <input type="number" name="descuento_item" id="descuento_item" class="form-control" value="0" min="1" max="100">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Moneda</label>

                                                                            <select class="form-control" name="moneda_item" id="moneda_item">
                                                                                <option value="1">Pesos</option>
                                                                                <option value="2">UF</option>
                                                                            </select>

                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Sub Total</label>
                                                                            <input type="text" name="sub_total_item" id="sub_total_item" class="form-control" value="{{old('sub_total_item')}}" readonly>
                                                                        </div>
                                                       </div>
                                                     </div>

                                                </div>
                                             </div>
                                        <div class="row">
                                            <div class=" modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                 <button type="button" name="btn_agregar_item" id="btn_agregar_item" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- ***********************MODAL 3  AGREGAR HES **********************************-->
                            <div class="modal inmodal " id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated  bounceInRight">
                                        <div class="modal-header">
                                            <h3>Agregar HES a la Nota de Venta</h3>
                                        </div>
                                        <div class="panel-bordered-primary">

                                            <div class="form-group"><label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered table-striped table-hover">
                                                            <thead>
                                                            <th>Numero HES</th>
                                                            <th>FECHA HES</th>


                                                            </thead>
                                                            <tbody id="tabla_hes">

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" onclick="cancela_hes()" >Cerrar</button>
                                             <button type="button" name="btn_tipo" id="btn_tipo" class="btn btn-primary" onclick="guarda_hes()"><i class="fa fa-save"></i> Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        var contador_item=0;
        var impuesto=0.19;
        var neto=0;
        var iva=0;
        var total=0;
        var fdescuento=0;
        var monto_descuento=0;
        var detalle=[];
        var hes=[];
        var fruits = ["Banana", "Orange", "Apple", "Mango"];
        // fruits.push("Kiwi");
        //var frutas = [{"secas":"arroz","rojas":"cerezas","verdes":"manzanas"},{"secas":"arroz","rojas":"cerezas","verdes":"manzanas"}];
        //frutas.push("Kiwi");
        $(document).ready(function(){

            $('#neto').html(neto);
            $('#iva').html(iva);
            $('#total').html(total);

        });



        function carga_servivio(e) {
            limpia_detalle();
            var id_;
            var rut;
            id_=e.value;
            rut=$('option:selected', e).attr('rut');

            // alert('cambio cliente'+id_  + "RUT: "+rut)
            $.ajax({

                url: '/inventarios/cargar_servicios/'+rut,
                //type: $(dis).attr("method"),
                dataType: "JSON",
                //data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    console.log('servicios: '+data.servicios.length);

                    for(var i=0; i<data.servicios.length; i++){
                        // console.log('servicios2: '+data.servicios);
                        $("#tabla_servicios").append("<tr><td><input type='checkbox' class='js-switch_3' name='check[]'  contador='' id='check-" + data.servicios[i].Id + "-" + data.servicios[i].Codigo + "-"+ data.servicios[i].mantenedor_servicios.servicio+ "' precio='"+ data.servicios[i].Valor + "' onchange='check_servicio(this)'  ></td><td>" + data.servicios[i].Codigo + " </td><td><textarea  cols='30' rows='4'  name='concepto[]'  id='concepto-" + data.servicios[i].Id + "' >"+data.servicios[i].mantenedor_servicios.servicio +" "+data.servicios[i].Descripcion+" "+data.servicios[i].Conexion+"</textarea></td><td>"+ data.servicios[i].Valor + "</td><td><input type='number' style='width: 50px;' size='5' value='0' name='descuento[]'  id='descuento-" + data.servicios[i].Id + "' > </td></tr>");
                        $("#tabla_servicios").append(" ");data.servicios[i].mantenedor_servicios.servicio
                    }

                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

                    elems.forEach(function(html) {
                        var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
                    });

                    $('#btn_entregar').attr("disabled", false);

                },
                error: function (data) {

                    swal("ERROR", "NO seleccionaste ningún cliente", "error");


                }
            })
        }



        function check_servicio(e){
            var checkid=$(e).attr('id');
            var contador_check_item=$(e).attr('contador');
            var value=$(e).val();
            var uf = $('#uf').val();
            var preciouf=$(e).attr('precio');
            var preciopesos=(parseFloat(preciouf) * parseFloat(uf));
            var montodescuento=0;
            var registro=checkid.split('-');
            var descuento=$('#descuento-'+registro[1]).val();
            var concepto=$('#concepto-'+registro[1]).val(); //el descriptor del servicio
            var servicio_id=registro[1];
            console.log('contador check item: '+contador_check_item);
             //****************************aqui seteo la global monto_descuento as interger
            //var factdescuento=1/descuento;
               if (descuento > 0)
               {
                    montodescuento=((preciopesos*descuento)/100);
                   monto_descuento=montodescuento;
                   fdescuento=descuento;
               }

               else
                    montodescuento=0;
            var submonto=preciopesos-montodescuento;
            if (descuento >=0 && descuento<=100)
                    {
                    //var cantidad=$(e).attr('cantidad');
                    //alert(cantidad);
                    console.log('Check: '+value);
                    console.log('id: '+checkid);
                    console.log('precio: '+preciouf+ 'precio pesos: '+preciopesos);
                    console.log('descuento: '+registro[1]);
                    var valor;
                    var i;


                    if (e.checked == true) {
                        $('#descuento-'+registro[1]).attr("readonly","readonly");
                        $('#concepto-'+registro[1]).attr("readonly","readonly");
                        neto=neto+Math.round(submonto);
                        iva=neto*impuesto;
                        iva=Math.round(iva);
                        total=parseFloat(neto)+parseFloat(iva);
                        total=Math.round(total);

                        var submonto2=preciouf*$('#uf').val();
                        var iva_item=submonto*impuesto; //submonto=preciopesos-montodescuento; esta declarado un poco mas arriba
                        var subtotal_item=submonto+iva_item;

                        $('#neto').html(new Intl.NumberFormat("de-DE").format(neto));
                        $('#iva').html(new Intl.NumberFormat("de-DE").format(iva));
                        $('#total').html(new Intl.NumberFormat("de-DE").format(total));

                        $("#tabla_detalle").append("<tr id='tr-"+registro[1]+"'><td>"+registro[2]+"-"+registro[3]+"</td><td>"+concepto+"</td><td align='right'>UF "+preciouf+"</td><td align='right'>"+1+"</td><td align='right'>"+new Intl.NumberFormat("de-DE").format(Math.round(submonto2))+"</td><td align='right'>"+new Intl.NumberFormat("de-DE").format(Math.round(montodescuento))+"</td><td id='td-"+registro[1]+"' align='right'>"+new Intl.NumberFormat("de-DE").format(Math.round(submonto))+"</td></tr>")
                        //detalle.push({"codigo":registro[2]+"-"+registro[3],"descripcion":checkid, "precio":preciouf});
                        detalle.push({"contador":registro[1],"codigo":registro[2]+"-"+registro[3],"descripcion":concepto, "valor":preciopesos, "precio":preciopesos, "cantidad":1,"fdescuento":descuento,"descuento":monto_descuento,"subtotal":submonto, "iva":iva_item,"total":subtotal_item, "servicio_id":servicio_id});
                        $(e).attr('contador',contador_item);
                        contador_item=contador_item+1;
                    }
                    else{

                        $('#descuento-'+registro[1]).removeAttr("readonly");
                        $('#concepto-'+registro[1]).removeAttr("readonly");
                        var montodescuento= monto_descuento;

                        console.log('desc:'+preciopesos);
                        neto=neto-Math.round(submonto);
                        if (neto<0)
                            neto=0;
                        iva=neto*impuesto;
                        iva=Math.round(iva);
                        total=parseFloat(neto)+parseFloat(iva);
                        total=Math.round(total);

                        $('#neto').html(new Intl.NumberFormat("de-DE").format(neto));
                        $('#iva').html(new Intl.NumberFormat("de-DE").format(iva));
                        $('#total').html(new Intl.NumberFormat("de-DE").format(total));
                        $('#tr-'+registro[1]).closest('tr').remove();
                        delete detalle[contador_check_item];
                    }
                    }
            else{

                if (e.checked == true)
                    $(e).prop("checked", false);
                else
                    $(e).prop("checked", true);

                swal("ERROR", "El Descuento debe ser un valor entre 0 y 100%", "error");
            }


        }

        //**************************** agregar item ****************************************

        $('#btn_agregar_item').on('click', function (e) {
            var label="$";
            //alert('iTEM')
            if ($('#descripcion').val()!=""){
                $('#descripcion').css('border-color','#000000');

                if ($('#precio').val()!=""){
                    $('#precio').css('border-color','#000000');
                    if ($('#cantidad').val()!=""){
                        $('#cantidad').css('border-color','#000000');

                        if (($('#cantidad').val()<=100)&&($('#cantidad').val()>=0)) {
                            $('#cantidad').css('border-color','#000000');

                               if ($('#descuento').val()=="")
                                   $('#descuento').val(0)


                            if ($('#moneda_item').val()==='1') {
                                var submonto = ($('#precio').val() * $('#cantidad').val());
                                var valor_unitario=$('#precio').val();
                                var subtotal_item=$('#sub_total_item').val();
                               // var redondeo=Math.round(subtotal_item);
                                 label="$";
                                var subtotal_item2 = subtotal_item.replace(".", "", "gi");
                                var subtotal_item2 = subtotal_item2.replace(",", ".", "gi"); //formato necesario para que entre en la bd
                            }
                            else{

                                var submonto = ($('#precio_pesos').val() * $('#cantidad').val());
                                var valor_unitario=$('#precio_pesos').val();
                                 label="UF";
                               //var subtotal_item=$('#sub_total_item').val();

                                var subtotal_item=parseFloat($('#sub_total_item').val());
                              //  var redondeo=Math.round(subtotal_item);
                             //   var subtotal_item2 = Math.round(subtotal_item);

                            }



                            console.log('subt_item= '+subtotal_item+ '-'+subtotal_item2+ ' neto: '+neto+' iva:'+iva+' total: '+total);

                            var desc=$('#descuento_item').val()*0.01;
                            console.log('desc: '+desc);
                            var descuento=submonto*desc;
                            descuento=Math.round(descuento);
                            console.log('descuento: '+descuento);
                            console.log('submonto: '+submonto);
                            var submonto2=submonto-descuento;
                            console.log('submonto2: '+submonto2);
                            submonto2=Math.round(submonto2);
                            console.log('submonto2 el que se va a la bd con su iva: '+submonto2);
                            console.log('subtotal_item2 el que va en el front y calcula neto y total: '+subtotal_item2);
                            var iva_item=submonto2*impuesto;
                            var total_item=submonto2+iva_item;


                            console.log('total_anterior_ '+total);
                            neto=neto+submonto2;
                            iva=neto*impuesto;
                            total=parseFloat(neto)+parseFloat(iva);
                            neto=Math.round(neto);
                            iva=Math.round(iva);
                            total=Math.round(total);

                            console.log('neto: '+neto);
                            console.log('iva: '+iva);
                            console.log('tot: '+total);

                            $('#neto').html(new Intl.NumberFormat("de-DE").format(neto));
                            $('#iva').html(new Intl.NumberFormat("de-DE").format(iva));
                            $('#total').html(new Intl.NumberFormat("de-DE").format(total));

                            $("#tabla_detalle").append("<tr id='tr-"+$('#descripcion').val()+"'><td>&nbsp;</td><td>"+$('#descripcion').val()+"</td><td align='right'>"+ label+" "+$('#precio').val()+"</td><td align='right'>"+$('#cantidad').val()+"</td><td align='right'>"+new Intl.NumberFormat("de-DE").format(Math.round(submonto))+"</td><td align='right'>"+new Intl.NumberFormat("de-DE").format(Math.round(descuento))+"</td><td align='right' >"+new Intl.NumberFormat("de-DE").format(Math.round(submonto2))+"</td><td><a href='#' id='"+contador_item+"'  name='"+submonto2+"' onclick='elimina_item(this)'><i class='fa fa-trash'></i></a></td></tr>")
                            detalle.push({"contador":contador_item,"codigo":$('#descripcion').val(),"descripcion":$('#descripcion').val(), "valor":valor_unitario, "precio":$('#precio').val(), "cantidad":$('#cantidad').val(),"fdescuento":$('#descuento_item').val(),"descuento":descuento,"subtotal":submonto2, "iva":iva_item,"total":total_item,"servicio_id":null});
                            contador_item=contador_item+1;

                            $('#myModal2').modal('toggle');
                        }
                        else {
                            alert($('#cantidad').val());
                            $('#lb_cantidad').html('<strong class="text-danger">El Campo Cantidad debe ser un valor entre 0 y 99 </strong>');
                            $('#cantidad').css('border-color','#ff0000');
                        }


                    }
                    else {
                        $('#lb_cantidad').html('<strong class="text-danger">El Campo Cantidad es obligatorio</strong>');
                        $('#cantidad').css('border-color','#ff0000');
                    }


                }
                else {
                    $('#lb_precio').html('<strong class="text-danger">El Campo Precio es obligatorio</strong>');
                    $('#precio').css('border-color','#ff0000');
                }

            }
            else {
                $('#lb_descripcion').html('<strong class="text-danger">El Campo Descripción es obligatorio</strong>');
                $('#descripcion').css('border-color','#ff0000');
            }
        })

        function limpia_detalle(){
            $('#tabla_servicios').empty();
            $('#tabla_detalle').empty();
            $('#precio_pesos').val();
            neto=0;
            iva=0;
            total=0;
            monto_descuento=0;
            contador_item=0;
            $('#neto').html('');
            $('#iva').html('');
            $('#total').html('');
            detalle=[];
        }

        function calcula_subtotal() {
            var desc
            var descuento
            // primero pregunto si el  campo descuento tiene algun valor
            if (($('#descuento_item').val() <= 100) && ($('#descuento_item').val() >= 0)){
                desc=$('#descuento_item').val()*0.01;

            }
            else{
                desc=0;
            }
          // ahora pregunto si el combo moneda esta seteado en pesos o UF
            var subtotal;
            if ($('#moneda_item').val()==='1'){
                subtotal=$('#precio').val()*$('#cantidad').val();
                descuento=subtotal*desc;
                subtotal=subtotal-descuento;
                $('#sub_total_item').val(subtotal);
            }
            else{
                subtotal=$('#precio').val()*$('#uf').val()*$('#cantidad').val();
                $('#precio_pesos').val($('#precio').val()*$('#uf').val());
                descuento=subtotal*desc;
                subtotal=subtotal-descuento;
                $('#sub_total_item').val(subtotal);
            }
        }  //fin de la funcion calcula_subtotal


        //***** cambio moneda UF/Pesos ********************************

        $('#moneda_item').on('change', function (e) {

            var desc
            var descuento
            // primero pregunto si el  campo descuento tiene algun valor
            if (($('#descuento_item').val() <= 100) && ($('#descuento_item').val() >= 0)){
                desc=$('#descuento_item').val()*0.01;

            }
            else{
                desc=0;
            }

           // ahora hago el calculo...

            if ($('#moneda_item').val()==='1'){
                subtotal=$('#precio').val()*$('#cantidad').val();
                descuento=subtotal*desc;
                subtotal=subtotal-descuento;
                $('#sub_total_item').val(subtotal);
            }
            else{
                subtotal=$('#precio').val()*$('#uf').val()*$('#cantidad').val();
                $('#precio_pesos').val($('#precio').val()*$('#uf').val()); //un precio 2 para guardar el valor en pesos en la bd y no en UF
                descuento=subtotal*desc;
                subtotal=subtotal-descuento;
                $('#sub_total_item').val(subtotal);
            }

        })

        $('#descuento_item').on('keyup', function (e) {
            if (($('#descuento_item').val() <= 100) && ($('#descuento_item').val() >= 0))
            {
                var desc=$('#descuento_item').val()*0.01;
                var descuento=$('#precio').val()*$('#cantidad').val()*desc;
                var subtotal;
                if ($('#moneda_item').val()==='1'){
                    subtotal=($('#precio').val()*$('#cantidad').val())-descuento;
                    $('#sub_total_item').val(subtotal);
                }
                else{
                    subtotal=($('#precio').val()*$('#uf').val()*$('#cantidad').val());
                    descuento=subtotal*desc;
                    subtotal=subtotal-descuento;
                    $('#sub_total_item').val(subtotal);
                }
            }
            else{
                swal("COMETISTE UN ERROR", "El Descuento debe ser un valor entre 0 y 100", "error");
            }

        })

        $('#cantidad').on('keyup', function (e) {
            var desc
            var descuento
            // primero pregunto si el  campo descuento tiene algun valor
            if (($('#descuento_item').val() <= 100) && ($('#descuento_item').val() >= 0)){
                desc=$('#descuento_item').val()*0.01;

            }
            else{
                desc=0;
            }
            if (($('#cantidad').val() <= 100) && ($('#cantidad').val() >= 0)) {
                 desc = $('#descuento_item').val() * 0.01;
                 descuento = $('#precio').val()*$('#cantidad').val() * desc;
                var subtotal;
                if (($('#cantidad').val()===0)||($('#cantidad').val()==""))
                    descuento=0;
                if ($('#moneda_item').val() === '1') {
                    subtotal = ($('#precio').val() * $('#cantidad').val()) - descuento;
                    $('#sub_total_item').val(subtotal);
                }
                else {
                    subtotal = ($('#precio').val() * $('#uf').val() * $('#cantidad').val());
                    descuento = subtotal * desc;
                    subtotal = subtotal - descuento;
                    $('#sub_total_item').val(subtotal);
                }

            }
            else{
                swal("COMETISTE UN ERROR", "La cantidad ingresada no es valida para este item", "error");
            }

        })

        function levanta_hes(){
            hes=[];
            var contador=$('select[id=cont_hes]').val();
            $('#tabla_hes').empty();
            for(var i=0; i<contador; i++){
                // console.log('servicios2: '+data.servicios);
                $("#tabla_hes").append("<tr><td><input type='number' class='form-control' name='num_hes-" + i +  "' id='num_hes-" + i +  "'   ></td><td><input type='date'   name='fecha_hes-" + i +  "'  id='fecha_hes-" + i + "' > </td></tr>");
                //$("#tabla_servicios").append(" ");
            }

            $('#myModal3').modal('toggle');

        }

        function guarda_hes(){

            var cont_hes=$('select[id=cont_hes]').val();
            var str_hes="Hes";
            var bandera_hes=0;
            for (var i=0;i<cont_hes;i++){
                if (($('#num_hes-'+i).val()=="")|| ($('#fecha_hes-'+i).val()==""))
                    bandera_hes=1;
            }
            if (bandera_hes===0)
            {
                for (var i=0;i<cont_hes;i++){
                    hes.push({"num_hes":$('#num_hes-'+i).val(),"fecha_hes":$('#fecha_hes-'+i).val()})
                    str_hes=str_hes + " - " + $('#num_hes-'+i).val();
                }
                $('#label_hes').html(str_hes);
                $('#myModal3').modal('toggle');
            }
            else{
                swal('Cometiste un Error','No dejes campos en blanco!', 'error');
            }
        }
        function cancela_hes(){
            hes=[];
            $('#cont_hes').val('0').prop('selected', true);
            $('#myModal3').modal('toggle');
        }

        function elimina_item(dis){
           var id=dis.id;
           var monto=dis.name
            var monto2=Math.round(monto);
           monto=parseInt(monto2);
            //var subtotal_neto = monto.replace(".", "", "gi");
            var subtotal_neto = monto;
           //alert('monto: '+subtotal_neto + 'neto: '+neto);
            neto=parseInt(neto)-parseInt(subtotal_neto);
           // alert('nuevo neto: '+neto);
            iva=neto*impuesto;
            //alert('iva: '+iva)
            total=parseFloat(neto)+parseFloat(iva);
            //alert('total: '+total);

            $('#neto').html(new Intl.NumberFormat("de-DE").format(Math.round(neto)));
            $('#iva').html(new Intl.NumberFormat("de-DE").format(Math.round(iva)));
            $('#total').html(new Intl.NumberFormat("de-DE").format(Math.round(total)));
            $(dis).closest('tr').remove();
            delete detalle[id];
        }

    function guardar_nota(){
            if ($('select[id=cliente]').val() != "")
                {
                    console.log('netoo: '+neto);
                    $('#btn_guardar').attr("disabled", true);
                    var formulario = {"cabecera":[
                        {"cliente":$('select[id=cliente]').val(),"fecha_emision":$('#fecha_emision').val(),"numero_oc":$('#oc').val(),"fecha_oc":$('#fecha_oc').val(),"cont_hes":$('select[id=cont_hes]').val(),"tipo_pago_bsale":$('select[id=tipo_pago_bsale]').val()}],
                        "detalle":[{detalle}],
                        "pie":[{"neto":neto,"iva":iva,"total":total}],
                        "hes":[{hes}],
                        "csrfToken":[$('meta[name="csrf-token"]').attr('content')]
                    };

                    console.log(detalle);
                    console.log(formulario);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/facturacion/nota_de_venta_guardar',
                    type: 'post',
                    dataType: 'json',
                    contentType: 'application/json',

                    success: function (data) {
                        if (data.Success===true){

                            swal('Ok',data.msg,'success');
                            window.location.href="{{ url('/facturacion/nota_de_venta') }}";

                        }else{

                            swal('Error',' NO se pudo guardar la nota de venta: '+data.msg +' - '+ data.success ,'error');
                            $('#btn_guardar').attr("disabled", true);
                        }

                    },
                    error: function (data) {
                        swal('Error',data.msg,'error');
                        $('#btn_guardar').attr("disabled", true);

                    },
                    data: JSON.stringify(formulario)
                });
            }
            else
                swal('Cometiste un Error!','No seleccionaste un cliente','error');
        }

    </script>

@endsection

