<form id="insertContactos" class="insertContactos">
    {{csrf_field()}}
    <input id="Method" type="hidden" name="_method" value="PUT">    
    <div class="col-md-6 form-group">
        <label>Nombre Contacto</label>
        <input id="NombreContacto" name="contacto" class="form-control" validate="not_null">
    </div>
    <div class="col-md-6 form-group">
        <label>Tipo de Contacto</label>
        <select class="form-control" name="tipo_contacto" id="TipoContacto"  data-live-search="true" data-container="body">
            @foreach ($tipo_contactos as $row )
                <option value="{{$row->id}}" {{ old('tipo_contacto') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
            @endforeach
        </select>
    </div>

    <div class="col-md-6 form-group">
        <label>Correo</label>
        <input name="correo" id="email"  class="form-control">
    </div>
    <div class="col-md-6 form-group">
        <label>Teléfono</label>
        <input name="telefono" id="fono" class="form-control">
        <input type="hidden" id="IdClienteOculto" name="rut" value="{{@$cliente->rut}}" class="form-control">
        <input type="hidden" id="IdContactoOculto" name="id" class="form-control">
    </div>
</form>

<div class="col-md-12">
    <br>
    <button id ="grabarContacto" type="button" tipo="graba" class="btn btn-primary grabarContacto">Guardar</button>
</div>