@extends('layouts.app')
@section('title', 'Clientes')
@section('content')

@include('clientes.modalContactos')

@include('clientes.modalGiros')

		
<div id="container" class="">
	
	<div class="boxed">
		<div id="content-container">
			<div id="page-title" style="padding-right: 25px;">
			</div>
			<br>
			<ol class="breadcrumb">
				<li><a href="#">Inicio</a></li>
				<li class="active">Clientes</li>
			</ol>
			<div id="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="panel ">
							<!--Panel heading-->
							<div class="panel-heading">
								<h3 class="panel-title">Módulo Clientes</h3>
							</div>
							<!--Panel body-->
							<div class="panel-body">
								<!--Tabs content-->
								<div class="tab-content">
									<div id="tabs-box-1" class="tab-pane fade in active form-cont1">
										<div class="row">
											<div class="col-md-12">
												<h3>Crear Nuevo Cliente</h3><br>
											</div>
										</div>

										 <div class="row">
		                                    @if (count($errors) > 0)
		                                        <div class="alert alert-danger">
		                                            <ul>
		                                                @foreach($errors->all() as $error)
		                                                    <li>{{$error}}
		                                                    </li>
		                                                @endforeach
		                                            </ul>
		                                        </div>
		                                    @endif
		                                </div>
										<form id="insertCliente" name="frmCliente" method="post" action="{{ route('clientes.store') }}">
											{{csrf_field()}}
											<div class="row">
												<div class="col-md-3 form-group">
													<label>Tipo de Cliente</label>
													<select name="tipo_cliente" class="form-control TipoCliente" data-live-search="true" validate="not_null">
														<option value="">-- Seleccione --</option>
														@foreach ($tipos_cliente as $row )
		                                                    <option value="{{$row->id}}" {{ old('tipo_cliente') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
		                                                @endforeach
													</select>
												</div>
												<div class="col-md-3 form-group">
													<label>Tipo de Pago</label>
													<select name="tipo_pago_bsale_id" class="form-control" data-live-search="true" validate="not_null">
														<option value="">-- Seleccione --</option>
														@foreach ($tipos_pago as $row )
		                                                    <option value="{{$row->id}}" {{ old('tipo_pago_bsale_id') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
		                                                @endforeach
													</select>
												</div>
												<div class="col-md-3 form-group">
													<div class="checkbox" style="margin: 18px auto">
														<input type="checkbox" name="posee_pac" value="1" {{ (old('posee_pac') !== null) ? 'checked' : '' }}>
														<label for="posee_pac">Posee PAC</label>
														<br>
														<input type="checkbox" name="posee_prefactura"  value="1" {{ (old('posee_prefactura') !== null) ? 'checked' : '' }}>
														<label for="posee_prefactura">Posee Prefactura</label>
													</div>
												</div>
												<div class="col-md-2 form-group">
													<label>Rut</label>
													<input id="rut" name="rut" class="form-control" validate="not_null" value="{{old('rut')}}">
												</div>
												<div class="col-md-1 form-group">
													<label>Dv</label>
													<input id="dv" name="dv" class="form-control" validate="not_null" value="{{old('dv')}}" readonly="readonly">
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 form-group">
													<label>Clase Cliente</label>
													<select name="clase_cliente" class="form-control" data-live-search="true" validate="not_null">
														<option value="">-- Seleccione --</option>
														@foreach ($clase_cliente as $row )
		                                                    <option value="{{$row->id}}" {{ old('clase_cliente') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
		                                                @endforeach
													</select>
												</div>
												<div class="col-md-4 form-group">
													<label> Razón social / Cliente</label>
													<input name="nombre" class="form-control" value="{{old('nombre')}}" validate="not_null">
												</div>
												<div class="col-md-4 form-group">
													<label>Alias</label>
													<input name="alias" class="form-control" value="{{old('alias')}}" >
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 form-group">
													<label>Dirección Comercial</label>
													<textarea name="direccion" class="form-control" validate="not_null">{{old('direccion')}}</textarea>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 form-group">
													<label>Giro</label>
													<div class="input-group">
														<select id="Giro" name="giro" class="form-control " data-live-search="true" validate="not_null">
															<option value="">-- Seleccione --</option>
															@foreach ($giros as $row )
			                                                    <option value="{{$row->nombre}}" {{ old('giro') == $row->nombre ? 'selected' : '' }}>{{$row->nombre}}</option>
			                                                @endforeach
														</select>
														<span class="input-group-btn">
															<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarGiro"><i class="fa fa-plus" aria-hidden="true"></i></button>
														</span>
													</div>
												</div>
												<div class="col-md-4 form-group">
													<label>Region</label>
													<select id="region" name="region" class="form-control" >
														<option value="">-- Seleccione --</option>
														@foreach ($regiones as $row )
		                                                    <option value="{{$row->id}}" {{ old('region') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
		                                                @endforeach
													</select>
												</div>
												<div class="col-md-4 form-group">
													<label>Ciudad</label>
													<select id="ciudad" name="ciudad" class="form-control" data-live-search="true" validate="not_null">
														<option value="">-- Ciudad -- </option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 form-group">
													<label>Contacto</label>
													<div class="input-group">
														<select id="contacto" name="contacto" class="form-control" >
														</select>
														<span class="input-group-btn">
															<button id="agregarContactos" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalContactos"><i class="fa fa-plus" aria-hidden="true"></i></button>
														</span>
													</div>
												</div>
												<div class="col-md-4 form-group">
													<label>Teléfono</label>
													<input name="telefono" id="telefono" class="form-control" value="{{old('telefono')}}" >
												</div>
												<div class="col-md-4 form-group">
													<label>Correo</label>
													<input name="correo" id="correo" class="form-control" value="{{old('correo')}}" validate="email">
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 form-group">
													<label>Notas</label>
													<textarea name="comentario" class="form-control" >{{old('comentario')}}</textarea>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12 form-group">
													<br>
													<button name ="guardarCliente" type="submit" class="btn btn-primary guardar1">Guardar</button>
													<!--<button name ="guardarClienteIrServicio" type="submit" class="btn btn-primary guardar2">Guardar y Crear Servicio</button>-->
												</div>
											</div>
										</form>
										
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('mijava')
	<script type="text/javascript">
		
	    function loadCiudades() {
	        Region = $('#region').val();
	        if (Number(Region) > 0) {
	            $('#ciudad').load( "{{ url('/clientes/ciudad') }}/"+Region , function(data) {
	            });
	        } else {
	            $('select[name="ciudad"]').empty();
	        }
	    }

	    $(document).on('change', '#region', function() {
	        loadCiudades();
	    });

	    function loadContactos() {
	        rut = $('#rut').val();
	        if (rut) {
	            $('#contacto').load( "{{ url('/contactos/select') }}/"+rut , function(data) {
	            });
	        } else {
	            $('select[name="contacto"]').empty();
	        }
	    }

	    function changeCity(city) {
	    	$('#ciudad').val(city);
	    	$('#ciudad').change();
	    }

	    $( document ).ready(function() {
	    	const city = "{{ old('ciudad') }}";
		    if ((city != null) && (city != "")) {
		    	$('#region').trigger("change");
		    	setTimeout('changeCity('+city+')', 2000);
        	}
	        loadContactos();
		});

	    $('#rut').on('blur', function() {

	        rut = $(this).val();
	        input = $(this);
	        if (rut) {
	            $.get("{{ url('/clientes/existe') }}/"+rut, function(data) {
	            	let resp = data[0];
	            	$('#rut').val(resp.rut)
	                if (resp.msge == "si") {
	                    swal({
	                        title: "Este rut ya esta registrado",
	                        text: "Desea modificarlo?",
	                        type: "warning",
	                        showCancelButton: true,
	                        confirmButtonColor: "#28a745",
	                        confirmButtonText: "Si",
	                        cancelButtonText: "No",
	                        closeOnConfirm: true,
	                        allowOutsideClick: false
	                    }, function(isConfirm) {
	                        if (isConfirm) {
	                            window.location = "{{ url('/clientes') }}/"+resp.id+"/edit";
	                        } else {
	                            $(input).val('')
	                        }
	                    });
	                } else {
                        $('#dv').val(resp.dv);
                        $('#IdClienteOculto').val(rut);
	            		loadContactos();
	                }
	            });
	        }
	    });

    $('body').on('click', '#grabarGiro', function() {
		var csrf_token = "{{ csrf_token() }}";
    	let url = "{{ url('giros') }}"
        var form = {
        	nombre: $('#el_giro').val(),
            _token: csrf_token
        };
        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            success: function(response) {
                if (response != 'error') {
	            	$('#agregarGiro').modal('hide');
                	$("#Giro").append(response);
                	bootbox.alert('Registro Guardado Exitosamente');
	            } else {
	                bootbox.alert('No se guardó el Giro, favor revisar información');
	            }
            }
        });
    });

    $('body').on('click', '#grabarContacto', function() {
    	let url = "{{ url('contactos') }}"
    	let metodo = 'POST'
    	if ($(this).attr("tipo") == "modifica") {
    		url = "{{ url('contactos') }}"+"/"+$('#IdContactoOculto').val()+"/edit";
    		$('#Method').val("PUT");
    		metodo = 'PUT';
    	} else {
    		$('#Method').val("POST")
    	}

    	var csrf_token = "{{ csrf_token() }}";
        var form = {
        	rut: $('#IdClienteOculto').val(),
            contacto: $('#NombreContacto').val(),
            tipo_contacto: $('#TipoContacto').val(),
            correo: $('#email').val(),
            telefono: $('#fono').val(),
            _token: csrf_token
        };
        $('#telefono').val($('#fono').val());
        $('#correo').val($('#email').val());
        $.ajax({
            url: url,
            type: metodo,
            data: form,
            success: function(response) {
                if (response != 'error') {
	            	loadContactos();
	                $('#modalContactos').modal('hide');
	                bootbox.alert('Registro Guardado Exitosamente');
	            } else {
	                bootbox.alert('No se guardó el contacto, favor revisar información');
	            }
            }
        });
    });

    $(document).on('submit', '.delete-contacto', function(e){
        var form = this;
        e.preventDefault();
        swal({ 
                title: "¿Seguro que deseas eliminar el contacto?", 
                text: "No podrás deshacer este paso...", 
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar", 
                cancelButtonColor: "#339933",
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "Continuar!", 
                closeOnConfirm: false
            },
            function(isConfirm){
                if(isConfirm){
                    form.submit();
                }
            }
        );
    });
    
    $('.TipoCliente').on('change', function() {
        if ($(this).val() == "1") {
            $('#Giro').val('SIN GIRO, PERSONA NATURAL');
            $('.guardar2').show();
        } else {
        	$('#Giro').val('');
            if ($(this).val() == 3) {
                $('.guardar2').hide();
            } else {
                $('.guardar2').show();
            }
            $('#Giro').attr('validate', 'not_null')
        }

    });

    </script>

@endsection