
@extends('layouts.app')
@section('title', 'Cliente')
@section('content')

<div id="container" class="effect aside-float aside-bright mainnav-sm">
	<div class="containerHeader"></div>
	<div class="boxed">
		<div id="content-container">
			<div id="page-title" style="padding-right: 25px;">
			</div>
			<br>
			<ol class="breadcrumb">
				<li><a href="#">Inicio</a></li>
				<li class="active">Información de Clientes</li>
			</ol>
			<div id="page-content">
				<div class="row">
					<div class="panel">
						<!--Panel heading-->
						<div class="panel-heading">
							<h1 class="text-center">Cliente: <small> {{$cliente->nombre}}   RUT: {{ $cliente->rut }}-{{ $cliente->dv }}</small></h1>
							<input type="hidden" id="id-c" value="{{$cliente->id}}" class="form-control">
						</div>
						<hr>
						<div class="col-md-5">
							<div class="callout callout-primary row">
								@include('clientes.resumen.info-comercial')
							</div>
							<div class="callout callout-success row">
								@include('clientes.resumen.info-contacto')
							</div>
							<div class="callout callout-warning row">
								
								@include('clientes.resumen.servicios')
							
								@include('clientes.resumen.saldo')
							</div>
						</div>

						<div class="col-md-7">
							@include('clientes.resumen.documentos-vencidos')
							
							@include('clientes.resumen.documentos-pagados')

							@include('clientes.resumen.documentos-emitidos')

							@include('clientes.resumen.documentos-extras')
							
						</div>
					</div>
				</div>

				<hr>
				<div class="row">
					<div class="panel">
						<div class="panel-body">
							<div class="data_contactos">
								<h4 class="text-center">Contactos del cliente</h4>
								<table class="table table-responsive table-striped" id="tabla_ver_contactos">
								    <thead>
								        <tr>
								            <th>Nombre</th>
								            <th>Tipo</th>
								            <th>Correo</th>
								            <th>Teléfono</th>
								            <th>Opciones</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('mijava')

	<script>
		//ver los contactos del cliente
    $(document).ready( function () {
        var id = $('#id-c').val();
        var url = "{{ url('contactos/lista') }}/"+id;
        tabla1 = $('#tabla_ver_contactos').DataTable({
        	destroy: true,
        	paging: false,
    		searching: false,
	        processing: true,
	        serverSide: false,
	        order: [[1, 'asc']],
	        ajax: url,
	        columns: [
	                    { data: 'contacto', name: 'contacto' },
	                    { data: 'tipo_contacto', name: 'tipo_contacto' },
	                    { data: 'correo', name: 'correo' },
	                    { data: 'telefono', name: 'telefono' },
	                    { data: 'action', name: 'action', orderable: false, searchable: false}
	                 ],
	        language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });


        tabla0 = $('.TableDocVencidos').DataTable({
        	destroy: true,
        	paging: false,
        	pageLength: 5,
        	lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
    		searching: false,
	        processing: true,
	        serverSide: false,
	        order: [[1, 'asc']],
            language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
        });


        var url = "{{ url('clientes/docs_extras') }}/"+id;
	    tabla2 = $('#FacturasExtrasTableCliente').DataTable({
        	destroy: true,
        	paging: false,
        	pageLength: 5,
        	lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
    		searching: false,
	        processing: true,
	        serverSide: false,
	        order: [[3, 'asc']],
	        ajax: url,
	        columns: [
	                    { data: 'NumeroDocumento', name: 'NumeroDocumento' },
	                    { data: 'TipoDocumento', name: 'TipoDocumento' },
	                    { data: 'FechaFacturacion', name: 'FechaFacturacion' },
	                    { data: 'FechaVencimiento', name: 'FechaVencimiento' },
	                    { data: 'TotalFactura', name: 'TotalFactura', className: 'dt-body-right' },
	                    { data: 'TotalSaldo', name: 'TotalSaldo', className: 'dt-body-right'  },
	                    { data: 'SaldoFavor', name: 'SaldoFavor', className: 'dt-body-right'  }
	                 ],
	        language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });

	    var url = "{{ url('clientes/docs_emitidos') }}/"+id;
	    tabla3 = $('#FacturasTableCliente').DataTable({
        	destroy: true,
        	pageLength: 5,
        	lengthMenu: [ 5, 10, 25, 50, 75, 100 ],        	
    		searching: false,
	        serverSide: false,
	        order: [[3, 'asc']],
	        ajax: url,
	        columns: [
	                    { data: 'NumeroDocumento', name: 'NumeroDocumento' },
	                    { data: 'TipoDocumento', name: 'TipoDocumento' },
	                    { data: 'FechaFacturacion', name: 'FechaFacturacion' },
	                    { data: 'FechaVencimiento', name: 'FechaVencimiento' },
	                    { data: 'TotalFactura', name: 'TotalFactura', className: 'dt-body-right'  },
	                    { data: 'TotalSaldo', name: 'TotalSaldo', className: 'dt-body-right'  },
	                    { data: 'SaldoFavor', name: 'SaldoFavor', className: 'dt-body-right'  }
	                 ],
	        language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });

        var url = "{{ url('clientes/docs_pagados') }}/"+id;
	    tabla4 = $('#TableDocPagados').DataTable({
        	destroy: true,
        	pageLength: 5,
        	lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
    		searching: false,
	        serverSide: false,
	        order: [[3, 'asc']],
	        ajax: url,
	        columns: [
	                    { data: 'NumeroDocumento', name: 'NumeroDocumento' },
	                    { data: 'TipoDocumento', name: 'TipoDocumento' },
	                    { data: 'FechaFacturacion', name: 'FechaFacturacion' },
	                    { data: 'FechaVencimiento', name: 'FechaVencimiento' },
	                    { data: 'pagos', name: 'pagos', className: 'dt-body-right' }
	                 ],
	        language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });

	    $(document).on('click', '.verDocVencidos', function() {
	        $('#ModalDocVencidos').modal('show');
	    });
	    $(document).ready(function() {
		    $('#TableSerActivos').DataTable();
		});
	    $(document).on('click', '.eye-activos', function() {
	        $('#ModalverSercActivos').modal('show');
	    }); 
	    $(document).ready(function() {
		    $('#TableSerSuspendidos').DataTable();
		});
	    $(document).on('click', '.eye-suspendidos', function() {
	        $('#ModalverSercSuspendidos').modal('show');
	    });
	    $(document).ready(function() {
		    $('#TableSerSuspendidos').DataTable();
		});
	    $(document).on('click', '.eye-corteComercial', function() {
	        $('#ModalverSercCorteComercial').modal('show');
	    }); 
	    $(document).ready(function() {
		    $('#TableSerCambioRazonSocial').DataTable();
		});
	    $(document).on('click', '.eye-cambioRazonSocial', function() {
	        $('#ModalverSercCambioRazonSocial').modal('show');
	    });
	    $(document).ready(function() {
		    $('#TableSerTemporal').DataTable();
		});
	    $(document).on('click', '.eye-Temporal', function() {
	        $('#ModalverSercTemporal').modal('show');
	    });
	    $(document).ready(function() {
		    $('#TableSerFinContrato').DataTable();
		});
	    $(document).on('click', '.eye-FinContrato', function() {
	        $('#ModalverSercFinContrato').modal('show');
	    });
    });
	</script>
@endsection