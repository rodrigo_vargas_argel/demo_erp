<div class="modal fade" role="dialog" id="agregarGiro">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Agregar Giro</h4>
			</div>
			<div class="modal-body">
				<form id="insertGiro">
					<div class="row">
						{{csrf_field()}}
						<div class="col-md-12 form-group">
						<label>Giro</label>
							<input name="nombre" id="el_giro" class="form-control" validate="not_null">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple" id="grabarGiro">Guardar</button>
			</div>
		</div>
	</div>
</div>