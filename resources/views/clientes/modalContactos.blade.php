<div class="modal" role="dialog" id="modalContactos">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title modal-title-accion">Agregar Contacto</h4>
				<h4><span class="modal-title-contacto"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					@include('clientes.formContactos')
				</div>
				<hr>
				<div class="row">
					<div class="panel">
						<!-- Panel body -->
						<div class="panel-body">
							<div class="data_contactos">
								<table class="table table-responsive table-striped" id="tabla_ver_contactos">
								    <thead>
								        <tr>
								        	<th>Id</th>
								            <th>Nombre</th>
								            <th>Tipo</th>
								            <th>Correo</th>
								            <th>Teléfono</th>
								            <th>Opciones</th>
								        </tr>
								    </thead>
								    <tbody id='detalle-tabla'>
								    	
								    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
