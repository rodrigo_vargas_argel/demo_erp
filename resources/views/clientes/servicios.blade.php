@extends('layouts.app')
@section('title', ' Clientes - Servicios')
@section('content')

<?php
	$id_usuario = @Auth::user()->id;
	require_once('../public/class/methods_global/Method.php'); 
?>
<div class="boxed">
	<div id="content-container">
		<div id="page-title" style="padding-right: 25px;">
		</div>
		<br>
		<ol class="breadcrumb">
			<li><a href="#">Inicio</a></li>
			<li class="active">Servicios</li>
		</ol>
		<div id="page-content">
			<div class="row">
				<div class="col-md-5">
					<div class="panel ">
						<!--Panel heading-->
						<div class="panel-heading">
							<h3 class="panel-title">Módulo crear servicios</h3>
						</div>
						<!--Panel body-->
						<div class="panel-body container-form">
							<div class="row">
								<form id="formServicio">
									<div class="col-md-12 form-group">
										<label class="campo-cliente">Cliente</label>
										<div class="input-group campo-cliente">
											<select id="Rut" name="Rut" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Cliente">
												<option value="">Seleccione...</option>
											</select>
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalCliente"><i class="fa fa-plus" aria-hidden="true"></i></button>
											</span>
											<input type="hidden" name="usuario" value="<?php echo $id_usuario?>"/>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label class="compo-grupo">Grupo</label>
										<div class="input-group compo-grupo">
											<select name="Grupo" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Grupo"> 
												<option value="">Seleccione...</option>
												<option value="1">Grupo 1</option>
												<option value="2">Grupo 2</option>
												<option value="3">Grupo 3</option>
											</select>
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#grupo"><i class="fa fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<!-- aqui -->
										<label class="campo-cobreServicio">Tipo de Cobro de servicio</label>
										<div class="input-group campo-cobreServicio">
											<select id="TipoFactura" name="TipoFactura" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Tipo de Cobro">
												<option value="">Seleccione...</option>
											</select>
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalTipoFacturacion"><i class="fa fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
									</div>

									<div id="divFechaActivacionTMP" style="display:none;">
										<div class="col-md-12 form-group">
											<label>Duración del servicio</label>
											<div class="form-group">
												<div id="date-range">
													<div class="input-daterange input-group" id="datepicker">
														<input type="text" class="form-control" id="FechaInicioDesactivacionTMP" name="FechaInicioDesactivacionTMP" data-nombre="Fecha de Activación"/>
														<span class="input-group-addon">a</span>
														<input type="text" class="form-control" id="FechaFinalDesactivacionTMP" name="FechaFinalDesactivacionTMP" data-nombre="Fecha de Activación"/>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="col-md-12 form-group">
										<div class="campo-servicio">
											<label>Tipo de Servicio</label>
											<select name="TipoServicio" id="TipoServicio" class="form-control selectpicker" data-live-search="true" validate="not_null" data-nombre="Servicio">
												<option value="">Seleccione...</option>
											</select>
										</div>
									</div>
									<div class="col-md-12 containerTipoServicioFormulario"></div>
									<div class="col-md-6 form-group">
										<label class="campo-Valor">Valor</label>
										<div class="form-group">
											<input type="text" id="Valor" name="Valor" class="form-control" validate="not_null" data-nombre="Valor">
										</div>
									</div>
									<div class="col-md-6 form-group">
										<label>Descuento %</label>
										<div class="input-group">
											<input type="text" name="Descuento" id="Descuento" class="form-control" placeholder="Ingrese el descuento en %" min="0" max="100" step="1" value="0">
											<span class="input-group-addon" id="DescuentoPesos">%</span>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<div class="text-center" style="padding-left:20px;padding-right:20px">
											<label class="control-label h5" for="selectDescuento">Descuento temporal</label>
										</div>
										<select class="selectpicker form-control" name="selectDescuento" id="selectDescuento" data-live-search="true" data-container="body" validate="not_null" data-nombre="selectDescuento">
											<option value="1">Si</option>
											<option value="0">No</option>
										</select>
									</div>
										
									<div id="otrosServicios" style="display:none">
										<div class="col-md-12 form-group">
											<label class="campo-Conexiones">Conexiones</label>
											<div class="form-group">
												<input type="text" name="Alias" class="form-control campo-Conexiones">
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-direccion">Dirección</label>
											<textarea name="Direccion" class="form-control campo-direccion" rows="3"></textarea>
											<br class="campo-direccion">
										</div>

										<div class="col-md-6 campo-cordenadas">
											<div class="form-group">
												<label class="control-label" for="Latitud">Coordenadas</label>
												<input id="Latitud" name="Latitud" type="text" placeholder="Ingrese la latitud" class="form-control input-sm coordenadas" value="-41.3214705">
											</div>
										</div>
										<div class="col-md-6 campo-cordenadas">
											<div class="form-group">
												<label class="control-label" for="name">&nbsp;</label>
												<input id="Longitud" name="Longitud" type="text" placeholder="Ingrese la longitud" class="form-control input-sm coordenadas" value="-73.0138898">
											</div>
										</div>

										<div class="col-md-12 form-group">
											<div id="Map" style="height:350px; width:100%;" class="campo-cordenadas"></div>
										</div>

										<div class="col-md-12 form-group">
											<label class="campo-referencia">Referencia</label>
											<div class="form-group campo-referencia">
												<input type="text" name="Referencia" class="form-control">
											</div>
											<!--<br class="campo-referencia"> -->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-contacto">Contacto</label>
											<div class="form-group campo-contacto">
												<input type="text" name="Contacto" class="form-control">
											</div>
											<!--<br class="campo-contacto"> -->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-telefonoContacto">Fono Contacto</label>
											<div class="form-group campo-telefonoContacto">
												<input type="text" name="Fono" class="form-control">
											</div>
											<!--<br class="campo-telefonoContacto"> -->
										</div>
										<div class="col-md-12 form-group">
											<label>Fecha Comprometida de Instalación</label>
											<div class="form-group campo-FechaComprometidaInstalacion">
												<input name="FechaComprometidaInstalacion" class="form-control date">
											</div>
											<!--<br class="campo-FechaComprometidaInstalacion">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-estacionReferencia">Estaciones de Referencia</label>
											<div class="form-group campo-estacionReferencia">
												<input type="text" name="PosibleEstacion" class="form-control">
											</div>
											<!--<br class="campo-estacionReferencia">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-usuarioPPPoE">Posible Usuario PPPoE</label>
											<div class="form-group campo-usuarioPPPoE">
												<input type="text" name="UsuarioPppoeTeorico" class="form-control">
											</div>
											<!--<br class="campo-usuarioPPPoE">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-equipamiento">Equipamiento</label>
											<div class="form-group campo-equipamiento">
												<input type="text" name="Equipamiento" class="form-control">
											</div>
											<!--<br class="campo-equipamiento">-->
										</div>
										<div class="col-md-12 form-group">
											<label class="campo-señalTeorica">Señal Teorica</label>
											<div class="form-group campo-señalTeorica">
												<input type="text" name="SenalTeorica" class="form-control">
											</div>
										</div>
									</div>

									<div class="col-md-12 form-group">
										<label class="campo-señalTeorica">Facturación Costo de instalación / Habilitación</label>
										<div class="form-group campo-señalTeorica">
											<select id="BooleanCostoInstalacion" name="BooleanCostoInstalacion" class="form-control selectpicker">
												<option value="1">Si</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>

									<div id="divCostoInstalacion">
										<div class="col-md-12 form-group">
											<label class="campo-CostoInstalacion">Costo de instalación / Habilitación</label>
											<div class="input-group">
												<input type="text" id="CostoInstalacion" name="CostoInstalacion" class="form-control number" validate="not_null" data-nombre="Costo de Instalacion">
												<input type="hidden" id="CostoInstalacionIva" name="CostoInstalacionIva" class="form-control number">
												<span class="input-group-addon" id="CostoInstalacionPesos">0</span>
											</div>
										</div>
										
										<div class="form-group col-md-6">
											<label class="control-label" for="moneda"><stong>Moneda</stong></label>
											<select class="selectpicker form-control" name="moneda" id="moneda" data-live-search="true" data-container="body" validate="not_null" data-nombre="Moneda">
												<option value="1">Pesos</option>
												<option value="2">UF</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label>Descuento Instalación</label>
											<div class="input-group">
												<input type="text" name="CostoInstalacionDescuento" class="form-control" min="0" max="100" step="1">
												<span class="input-group-addon">%</span>
											</div>
										</div>
									</div>

									<div class="col-md-12 form-group">
										<label> Descripción</label>
										<textarea name="Descripcion" class="form-control" rows="4" placeholder="Descripción del Servicio | Adjunto en el Doc enviado al sii"></textarea>
										<br>
										<button type="button" class="btn btn-primary guardarServ">Guardar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="panel ">
						<!--Panel heading-->
						<div class="panel-heading">
							<h3 class="panel-title">Servicios registrados</h3>
							
						</div>
						<!--Panel body-->
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12 form-group">
									<h3>
										<i title="Descargar servicios" style="cursor: pointer;" class="fa fa-file-excel-o" id="fa-file-excel-o"></i>
									</h3>
									<div class="dataServicios" id="tab-Servicios">
									
										<h4>No hay servicios</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modalEditar" class="modal fade" tabindex="-1" role="dialog" id="load">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gris-oscuro p-t-10 p-b-10">
				<h4 class="modal-title c-negro">Código: <span class="Codigo"></span> <button type="button" data-dismiss="modal" class="close c-negro f-25" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></h4>
			</div>
			<div class="modal-body">
				<form id="showServicio">
				    <input type="hidden" class="Id" name="Id" id="Id">
				    <div class="row">
			            <div class="form-group col-md-6">
			                <label class="compo-grupo">Grupo</label>
			                <div class="compo-grupo">
			                    <select id="Grupo" name="Grupo" class="form-control selectpicker" data-live-search="true">
			                        <option value="">Seleccione...</option>
			                        <option value="1">Grupo 1</option>
			                        <option value="2">Grupo 2</option>
			                        <option value="3">Grupo 3</option>
			                    </select>
			                </div>
			            </div>
			            <div class="form-group col-md-6">
			                <label class="campo-cobreServicio">Tipo de Cobro de servicio</label>
			                <div class="campo-cobreServicio">
			                    <select id="TipoFactura2" name="TipoFactura" class="selectpicker form-control" data-live-search="true">
			                        <option value="">Seleccione....</option>
			                    </select>
			                </div>
			            </div>
			            <div class="form-group col-md-4">
			                <label class="campo-Valor">Valor</label>
			                <div class="form-group">
			                    <input id="Valor" type="text" name="Valor" class="form-control ValorEdit">
			                </div>
			            </div>
			            <div class="form-group col-md-4">
			                <label>Descuento %</label>
			                <div class="input-group">
			                    <input type="text" id="Descuento" name="Descuento" class="form-control DescuentoEdit" placeholder="Ingrese el descuento en %">
			                    <span class="input-group-addon" id="DescuentoPesosEdit">%</span>
			                </div>
			            </div>
		                <div class="form-group col-md-4">
		                    <div class="text-center" style="padding-left:20px;padding-right:20px">
		                        <label class="control-label h5" for="selectDescuento">Descuento temporal</label>
		                    </div>
		                    <select class="selectpicker form-control" name="selectDescuento" id="descuentoTemporal" data-live-search="true" data-container="body" validate="not_null" data-nombre="selectDescuento">
		                        <option value="1">Si</option>
		                        <option value="0">No</option>
		                    </select>
		                </div>

		                <div id="otrosServiciosEditar" style="display:none">
		                    <label> Descripción</label>
		                    <textarea id="Descripcion" name="Descripcion" class="form-control" rows="5"></textarea>
		                    <br>

		                    <label class="campo-Conexiones">Conexiones</label>
		                    <div class="form-group">
		                        <input id="Conexion" type="text" name="Conexion" class="form-control campo-Conexiones">
		                    </div>

		                    <label class="campo-direccion">Dirección</label>
		                    <textarea id="Direccion" name="Direccion" class="form-control campo-direccion" rows="5"></textarea>
		                    <br class="campo-direccion">

		                    <div class="col-md-6 campo-cordenadas">
		                        <div class="form-group">
		                            <label class="control-label" for="LatitudEdit">Coordenadas</label>
		                            <input id="LatitudEdit" name="LatitudEdit" type="text" placeholder="Ingrese la latitud" class="form-control input-sm coordenadasEdit">
		                        </div>
		                    </div>
		                    <div class="col-md-6 campo-cordenadas">
		                        <div class="form-group">
		                            <label class="control-label" for="LongitudEdit">&nbsp;</label>
		                            <input id="LongitudEdit" name="LongitudEdit" type="text" placeholder="Ingrese la longitud" class="form-control input-sm coordenadasEdit">
		                        </div>
		                    </div>
		                    <br class="campo-cordenadas">

		                    <div id="MapEdit" style="height:350px; width:100%;" class="campo-cordenadas"></div>

		                    <br class="campo-cordenadas">
		                    <label class="campo-referencia">Referencia</label>
		                    <div class="form-group campo-referencia">
		                        <input id="Referencia" type="text" name="Referencia" class="form-control">
		                    </div>
		                    <br class="campo-referencia">
		                    <label class="campo-contacto">Contacto</label>
		                    <div class="form-group campo-contacto">
		                        <input id="Contacto" type="text" name="Contacto" class="form-control">
		                    </div>
		                    <br class="campo-contacto">
		                    <label class="campo-telefonoContacto">Fono Contacto</label>
		                    <div class="form-group campo-telefonoContacto">
		                        <input id="Fono" type="text" name="Fono" class="form-control">
		                    </div>
		                    <label>Fecha Comprometida de Instalación</label>
		                    <div class="form-group campo-FechaComprometidaInstalacion">
		                        <input id="FechaComprometidaInstalacion" name="FechaComprometidaInstalacion" class="form-control date">
		                    </div>
		                    <br class="campo-telefonoContacto">
		                    <label class="campo-estacionReferencia">Estaciones de Referencia</label>
		                    <div class="form-group campo-estacionReferencia">
		                        <input id="PosibleEstacion" type="text" name="PosibleEstacion" class="form-control">
		                    </div>
		                    <br class="campo-estacionReferencia">
		                    <label class="campo-usuarioPPPoE">Usuario PPPoE</label>
		                    <div class="form-group campo-usuarioPPPoE">
		                        <input id="UsuarioPppoeTeorico" type="text" name="UsuarioPppoeTeorico" class="form-control">
		                    </div>
		                    <br class="campo-usuarioPPPoE">
		                    <label class="campo-equipamiento">Equipamiento</label>
		                    <div class="form-group campo-equipamiento">
		                        <input id="Equipamiento" type="text" name="Equipamiento" class="form-control">
		                    </div>
		                    <label class="url-graficos" for="UrlGraficos">Url graficos</label>
		                    <div class="form-group url-graficos">
		                        <input id="UrlGraficos" type="url" name="UrlGraficos" class="form-control" placeholder="http://dominio.com">
		                    </div>
		                    <br class="campo-equipamiento">
		                    <label class="campo-señalTeorica">Señal Teorica</label>
		                    <div class="form-group campo-señalTeorica">
		                        <input id="SenalTeorica" type="text" name="SenalTeorica" class="form-control">
		                    </div>
		                </div>
		                <br class="campo-equipamiento">
		                <label class="campo-señalTeorica">Facturación Costo de instalación / Habilitación</label>
		                <div class="form-group campo-señalTeorica">
		                    <select id="BooleanCostoInstalacion" name="BooleanCostoInstalacion" class="form-control selectpicker">
		                        <option value="1">Si</option>
		                        <option value="0">No</option>
		                    </select>
		                </div>
		                <br>
		                <div id="divCostoInstalacionEditar">
		                    <label class="campo-CostoInstalacion">Costo de instalación / Habilitación</label>
		                    <div class="form-group">
		                        <input type="text" id="CostoInstalacion" name="CostoInstalacion" class="form-control" validate="not_null" data-nombre="Costo de Instalacion">
		                    </div>
		                    <br>
		                    <label>Descuento Instalación</label>
		                    <div class="input-group">
		                        <input type="text" id="CostoInstalacionDescuento" name="CostoInstalacionDescuento" class="form-control" min="0" max="100" step="1">
		                        <span class="input-group-addon">%</span>
		                    </div>
		                    <br>
		                </div>
				    </div><!-- /.row -->
				</form>			
			</div><!-- /.modal-body -->
			<div class="modal-footer p-b-20 m-b-20">
				<div class="col-sm-12">
					<button type="button" class="btn btn-purple" id="updateServ" name="updateServ">Guardar</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="modalEstatus" class="modal fade" tabindex="-1" role="dialog" id="load">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gris-oscuro p-t-10 p-b-10">
				<h4 class="modal-title c-negro">Código: <span class="Codigo"></span> <button type="button" data-dismiss="modal" class="close c-negro f-25" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></h4>
			</div>
			<div class="modal-body">
			<div id="loader_servicios"></div>
				<form id="formEstatus">
				    <input type="hidden" class="Id" name="Id" id="Id">
				    <input type="hidden" class="" name="servicio_rut_dv" id="servicio_rut_dv">
				    <input type="hidden" class="" name="servicio_nombre_cliente" id="servicio_nombre_cliente">
				    <input type="hidden" class="" name="servicio_codigo_cliente" id="servicio_codigo_cliente">
				    <div class="row" style="padding:20px">
				        <div class="col-md-12">
				            <div class="form-group">
				                <label class="compo-grupo">Estado del Servicio</label>
				                <div class="compo-grupo">
				                    <select id="Activo" name="Activo" class="form-control selectpicker" data-live-search="true">
				                        <option value="">Seleccione...</option>
				                        <option value="8">Pendiente Instalación</option>
                        				<option value="9">Sin Factibilidad</option>
				                        <option value="1">Activo</option>
				                        <option value="2">Suspendido</option>
				                        <option value="3">Corte Comercial</option>
				                        <option value="4">Cambio razón social</option>
				                        <option value="5">Activo temporal</option>
				                        <option value="0">Término de contrato</option>
				                        
				                    </select>
				                </div>
				                <br>
				                <div id="divFechaActivacion" style="display:none">
				                    <label for="FechaInicioDesactivacion">Fecha de suspensión</label>
				                    <div class="form-group">
				                        <div id="date-range">
				                            <div class="input-daterange input-group" id="datepicker">
				                                <input type="text" class="form-control" id="FechaInicioDesactivacion" name="FechaInicioDesactivacion" data-nombre="Fecha de Activación"/>
				                                <span class="input-group-addon">a</span>
				                                <input type="text" class="form-control" id="FechaFinalDesactivacion" name="FechaFinalDesactivacion" data-nombre="Fecha de Activación"/>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div id="divFechaSuspension" style="display:none">
				                    <label for="FechaInicioSuspension">Ingrese fecha de suspensión para cobrar proporcional</label>
				                    <div class="form-group">
				                            <div class="input-daterange " id="datepicker">
				                                <input type="text" class="form-control" id="FechaInicioSuspension" name="FechaInicioSuspension" data-nombre="Fecha de suspensión"/>
				                            </div>
				                    </div>
				                </div>

				            </div>
				            
				            <div class="form-group">
				                <label for="selectEnviaCorreo" class="compo-grupo">Enviar correo a Técnicos</label>
				                <div class="compo-grupo">
				                    <select id="selectEnviaCorreo" name="selectEnviaCorreo" class="form-control selectpicker" data-live-search="true">
				                        <option value="1">Si</option>
				                        <option value="2">No</option>
				                    </select>
				                </div>
				            </div>
				        </div>
				    </div>
				</form>	
			</div><!-- /.modal-body -->
			<div class="modal-footer p-b-20 m-b-20">
				<div class="col-sm-12">
					<button type="button" class="btn btn-purple" id="updateEstatus" name="updateEstatus">Guardar</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="verServicios" aria-labelledby="verServicios">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lista de Datos técnicos</h4>
			</div>
			<div class="modal-body containerListDatosTecnicos">
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="agregarDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Agregar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary guardarDatosTecnicos">Guardar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="ModalDatosTecnicos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Actualizar Datos Técnicos</h4>
			</div>
			<div class="modal-body containerTipoServicio">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary actualizarDatosTecnicos">Actualizar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="ModalCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Registro de Cliente</h4>
			</div>
			<div class="modal-body container-form2">
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Tipo de Cliente</label>
						<select name="TipoCliente" class="form-control TipoCliente" data-live-search="true" validate="not_null" data-nombre="Tipo de Cliente">
							<option value="">Seleccione...</option>
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label>Tipo de Pago</label>
						<select name="TipoPago" class="form-control TipoPago" data-live-search="true" validate="not_null">
							<option value="">Seleccione...</option>
						</select>
					</div>
					<div class="col-md-3 form-group">
						<label>Rut</label>
						<input name="Rut" class="form-control" validate="not_null" data-nombre="Rut">
					</div>
					<div class="col-md-1 form-group">
						<label>Dv</label>
						<input id="Dv" name="Dv" class="form-control" validate="not_null" disabled>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Clase Cliente</label>
						<select name="ClaseCliente" class="form-control ClaseCliente" data-live-search="true" validate="not_null" data-nombre="Clase Cliente">
							<option value="">Seleccione...</option>
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label> Razón social / Cliente</label>
						<input name="Nombre" class="form-control" validate="not_null" data-nombre="Razón social">
					</div>
					<div class="col-md-4 form-group">
						<label>Alias</label>
						<input name="Alias" class="form-control" data-nombre="Alias">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 form-group">
						<label>Dirección  Comercial</label>
						<textarea name="DireccionComercial" class="form-control" validate="not_null" data-nombre="Dirección"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Giro</label>
						<select id="Giro" name="Giro" class="form-control selectpicker Giro" data-live-search="true" validate="not_null" data-nombre="Giro">
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label>Region</label>
						<select id="Region" name="Region" class="form-control selectpicker Region" data-live-search="true" validate="not_null">
						</select>

					</div>
					<div class="col-md-4 form-group">
						<label>Ciudad</label>
						<select id="Ciudad" name="Ciudad" class="form-control selectpicker Ciudad" data-live-search="true" validate="not_null">
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Contacto</label>
						<input name="Contacto" class="form-control" validate="not_null" data-nombre="Contacto">
					</div>
					<div class="col-md-4 form-group">
						<label>Teléfono</label>
						<input name="Telefono" class="form-control" validate="not_null" data-nombre="Teléfono">
					</div>
					<div class="col-md-4 form-group">
						<label>Correo</label>
						<input name="Correo" class="form-control" validate="email" data-nombre="Correo">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 form-group">
						<label>Notas</label>
						<textarea name="Comentario" class="form-control" validate="not_null" data-nombre="Notas"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary guardarCliente">Guardar Cliente</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="ModalTipoFacturacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content containerTipoFactura">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Tipo de Facturación</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 form-group">
						<label>Código</label>
						<input name="TipoFacCodigo" class="form-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Descripción</label>
						<input name="TipoFacDescripcion" class="form-control">
						<input type="hidden" name="TipoCliente" id="getTipoDoc" class="form-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Tiempo de Facturación</label>
						<select id="TipoFacturacion" name="TipoFacturacion" class="form-control selectpicker" data-live-search="true">
							<option value="1">Mensual</option>
							<option value="2">Semestral</option>
							<option value="3">Anual</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary agregarTipoFacturacion">Guardar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="grupo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Grupo</h4>
			</div>
			<div class="modal-body containerGrupo">
				<div class="row">
					<div class="col-md-12 form-group">
						<label>Nombre del grupo</label>
						<input name="NomGrupo" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary agregarGrupo">Guardar</button>
			</div>
		</div>
	</div>
</div>



@endsection

@section('mijava')

	<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>

	<script src="{!! asset('js/methods_global/mapa.js?v=1389884346') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1247385395') !!}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8" type="text/javascript"></script>
	<script src="{!! asset('js/servicios/controller.js?v=19080736') !!}"></script>

<!-- 
	<script src="{!! asset('js/methods_global/mapa.js?v=1580805263') !!}"></script>
	<script src="{!! asset('js/methods_global/mapaEdit.js?v=1601188581') !!}"></script>
	<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap">
	</script>
	<script src="{!! asset('js/servicios/controller.js?v=2070837879') !!}"></script>
-->
	<script src="{!! asset('js/swalExtend.js') !!}"></script>

@endsection


