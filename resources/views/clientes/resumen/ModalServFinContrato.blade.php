<div class="modal fade" role="dialog" id="ModalverSercFinContrato">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Servicios con fin contrato</h4>
			</div>
			<div class="modal-body">
				<div class="row">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 style="margin-top: 0">Servicios con fin contrato</h3>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="TableSerFinContrato" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código de Servicio</th>
                                                <th class="text-center">Conexión</th>
                                                <th class="text-center">Valor</th>
                                                <th class="text-center">Fecha instalación</th>
                                                <th class="text-center">Tipo de Servicio</th>
                                                <!-- <th class="text-center">Acciones</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if ($finados[0]['error'] == '')
                                            @foreach ($finados as $dato)
                                                <tr>
                                                    <th class="text-center">{{ $dato['Codigo'] }}</th>
                                                    <th class="text-center">{{ $dato['Conexion'] }}</th>
                                                    <th class="text-center">{{ number_format($dato['Valor'] , 0, '.', '') }}</th>
                                                    <th class="text-center">{{ $dato['FechaInstalacion'] }}</th>
                                                    <th class="text-right">{{ $dato['Tipo'] }}</th>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td>{{$finados[0]['error']}}</td><td></td><td></td><td></td><td></td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>