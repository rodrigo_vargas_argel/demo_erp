<h4 class="text-center">Información de Contacto</h4>
<table class="view-show">
	<tbody>
		<tr>
			<td width="42%" class="text-right odd"> Región : &nbsp;</td>
			<td width="58%"> {{ @$cliente->regiones->nombre }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Ciudad : &nbsp;</td>
			<td> {{ @$cliente->ciudades->nombre }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Dirección Comerc.: &nbsp;</td>
			<td> {{ $cliente->direccion }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Contacto : &nbsp;</td>
			<td> {{ $cliente->contacto }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Teléfono : &nbsp;</td>
			<td> {{ $cliente->telefono }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Correo : &nbsp;</td>
			<td> {{ $cliente->correo }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Notas : &nbsp;</td>
			<td> {{ $cliente->comentario }} </td>
		</tr>
	</tbody>
</table>