<div class="col-md-12">
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="row">
                    <h3>
                    @if ($vencidos['totalDocumentos'] > 0)
                        <span style="color: red;">{{$vencidos['totalDocumentos']}} &nbsp; Documento(s) Vencido(s)</span>
                    <!--    <i class="fa fa-eye verDocVencidos" title="Ver Documentos Vencidos"></i>   -->
                    @else
                        <span style="color: green;"> NO hay Documento(s) Vencido(s)</span>
                    @endif
                    </h3>
                    <div class="col-md-12">
                        <h4>Total Adeudado $ {{ number_format($vencidos['totalDeuda'], 0, '.', '') }}</h4>
                    </div>
                </div>
                @include('clientes.resumen.ModalDocVencidos')
<!--        
                <div class="modal fade" role="dialog" id="ModalDocVencidos">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Documentos Vencidos</h4>
                            </div>
                            <div class="modal-body">
                                @include ('clientes.resumen.ModalDocVencidos') 
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>   
-->
            </div>
        </div>
    </div>
</div>