<div class="col-md-12">
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <h3 style="color:green">Servicios</h3>
               
                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color: green;" class="servicios-activos"></span>  
                    @if ($activos[0]['error'] == '')
                        {{count($activos)}} activos
                        <i class="fa fa-eye eye-activos" title="Ver Servicios activos"></i>
                    @else
                        0 activos
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServActivos')
                <br>

                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color:orange;" class="servicios-suspendidos"></span>
                    @if ($suspendidos[0]['error'] == '')
                        {{count($suspendidos)}} suspendidos
                        <i class="fa fa-eye eye-suspendidos" title="Ver Servicios suspendidos"></i>
                    @else
                        0 suspendidos
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServSuspendidos')

                <br>
                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color:red;" class="servicios-corteComercial"></span>  
                    @if ($cortados[0]['error'] == '')
                        {{count($cortados)}} por corte comercial 
                        <i class="fa fa-eye eye-corteComercial" title="Ver servicios por corte comercial"></i>
                    @else
                        0 por corte comercial 
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServCorteComercial')

                <br>
                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color:red;" class="servicios-cambioRazonSocial"></span>  
                    @if ($cambiados[0]['error'] == '')
                        {{count($cambiados)}} por cambio razón social
                        <i class="fa fa-eye eye-cambioRazonSocial" title="Ver servicios por cambio razón social"></i>
                    @else
                        0 por cambio razón social
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServCambioRazonSocial')

                <br>
                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color:orange;" class="servicios-Temporal"></span>  
                    @if ($temporales[0]['error'] == '')
                        {{count($temporales)}} temporales
                        <i class="fa fa-eye eye-Temporal" title="Ver servicios temporales"></i>
                    @else
                        0 temporales
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServTemporal')

                <br>
                <div style="padding-left: 0px;" class="col-md-12">
                    <h3>
                    <span style="color:red;" class="servicios-FinContrato"></span> 
                    @if ($finados[0]['error'] == '')
                        {{count($finados)}} Fin de contrato
                        <i class="fa fa-eye eye-FinContrato" title="Ver servicios por fin de contrato"></i>
                    @else
                        0 Fin de contrato
                    @endif
                    </h3>
                </div>
                @include ('clientes.resumen.ModalServFinContrato')

            </div>
        </div>
    </div>
</div>