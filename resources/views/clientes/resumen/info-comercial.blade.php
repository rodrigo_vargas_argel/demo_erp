<h4 class="text-center">Información Comercial</h4>
<table class="view-show" width="100%">
	<tbody>
		<tr>
			<td width="42%" class="text-right odd"> Alias : &nbsp;</td>
			<td width="58%"> {{ $cliente->alias }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Tipo de Cliente : &nbsp;</td>
			<td> {{ @$cliente->tipoClientes->nombre }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Tipo de Pago : &nbsp;</td>
			<td> {{ @$cliente->tipoPagos->nombre }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Posee PAC : &nbsp;</td>
			<td> {{ $cliente->posee_pac == 1 ? "SI" : "NO"  }}</td>
		</tr>
		<tr>
			<td class="text-right odd"> Posee Prefactura : &nbsp;</td>
			<td> {{ $cliente->posee_prefactura == 1 ? "SI" : "NO" }} </td>
		</tr>
		<tr>
			<td class="text-right odd"> Clase de Cliente : &nbsp;</td>
			<td> {{ @$cliente->claseClientes->nombre }} </td>
		</tr>
		<tr>
			<td class="text-right odd"> Giro : &nbsp;</td>
			<td> {{ $cliente->giro }}</td>
		</tr>
	</tbody>
</table>