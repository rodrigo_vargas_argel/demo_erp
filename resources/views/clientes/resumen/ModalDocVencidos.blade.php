
				<div class="row">
                    <div class="panel">
                        <div class="panel-body">
                           <!-- <h3 style="margin-top: 0; color:red;">Documentos Vencidos</h3>  -->
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped TableDocVencidos">
                                        <thead>
                                            <tr>
                                                <th class="text-center">N* de Documento</th>
                                                <th class="text-center">Tipo de Documento</th>
                                                <th class="text-center">Fecha Emisión</th>
                                                <th class="text-center">Fecha Vencimiento</th>
                                                <th class="text-center">Total Doc.</th>
                                                <th class="text-center">Saldo Doc.</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($vencidos['datos'] as $dato)
                                            <tr>
                                                <th class="text-center">{{ $dato['NumeroDocumento'] }}</th>
                                                <th class="text-center">{{ $dato['TipoDocumento'] }}</th>
                                                <th class="text-center">{{ $dato['FechaFacturacion'] }}</th>
                                                <th class="text-center">{{ $dato['FechaVencimiento'] }}</th>
                                                <th class="text-right">{{ number_format($dato['totalDoc'] , 0, '.', '') }}</th>
                                                <th class="text-right">{{ number_format($dato['saldo_doc'] , 0, '.', '') }}</th>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			