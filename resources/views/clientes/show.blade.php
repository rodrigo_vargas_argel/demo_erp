<div class="modal fade" role="dialog" id="modalCliente">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="active">Información de Cliente</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="panel">
						<!--Panel heading-->
						<div class="panel-heading">
							<h1 class="text-center">Cliente: <small> <span class="titulado"> </span> </small></h1>
						</div>
						<hr>

						<div class="callout callout-primary col-md-5">
							<h4 class="text-center">Información Comercial</h4>
								<table class="view-show" width="100%">
									<tbody>
										<tr>
											<td width="42%" class="text-right odd"> Rut : &nbsp;</td>
											<td width="58%" class="alias"></td>
										</tr>
										<tr>
											<td width="42%" class="text-right odd"> Nombre : &nbsp;</td>
											<td width="58%" class="nombresito"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Tipo de Cliete : &nbsp;</td>
											<td class="tipoCliente"></td>
										</tr>

										<tr>
											<td class="text-right odd"> Giro : &nbsp;</td>
											<td class="giro"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Direccion : &nbsp;</td>
											<td class="direccion"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Comuna : &nbsp;</td>
											<td class="ciudad"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Region : &nbsp;</td>
											<td class="region"></td>
										</tr>
									</tbody>
								</table>
						</div>
						<div class="callout callout-success col-md-6">
							<h4 class="text-center">Información de Contacto</h4>
								<table class="view-show">
									<tbody>
									<!--	<tr>
											<td width="42%" class="text-right odd"> Región : &nbsp;</td>
											<td width="58%" class="region"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Ciudad : &nbsp;</td>
											<td class="ciudad"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Dirección Comerc.: &nbsp;</td>
											<td class="direccion"></td>
										</tr>-->
										<tr>
											<td class="text-right odd"> Contacto : &nbsp;</td>
											<td class="contacto"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Teléfono : &nbsp;</td>
											<td class="telefono"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Correo : &nbsp;</td>
											<td class="correo"></td>
										</tr>
										<tr>
											<td class="text-right odd"> Notas : &nbsp;</td>
											<td class="comentario"></td>
										</tr>
									</tbody>
								</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
