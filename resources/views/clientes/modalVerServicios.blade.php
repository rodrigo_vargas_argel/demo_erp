<div class="modal fade" role="dialog" id="modalVerServicios">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Servicios</h4>
				<h4><span class="modal-title-servicio"></span></h4>
			</div>
			<div class="modal-body">
				@include('clientesServicios.tablaServicios');
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	

</script>