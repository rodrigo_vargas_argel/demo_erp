
@extends('layouts.app')
@section('title', 'Clientes')
@section('content')
@include('clientes.show')
@include('clientes.modalContactos')
@include('clientes.modalVerServicios')

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2><img src="{!! asset('img/logo_ico.png') !!}"> Demo ERP</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{ url('/') }}">Home</a>
			</li>
			<li>
				<a>Clientes</a>
			</li>

		</ol>
	</div>
	<div class="col-lg-2">
		<div class="head-list" style="padding: 10px;">
			<a href="{{route('clientes.create')}}" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Nuevo Cliente</a>

		</div>
	</div>
</div>

<div id="container" class="wrapper wrapper-content animated fadeInRight">

	<div class="row">
		<div id="col-lg-12">


				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5><i class="glyphicon glyphicon-list-alt"></i> Clientes Registrados en el ERP</h5>

					</div>
					<div class="ibox-content">

								<div class="listaCliente ">


									<table class="table table-responsive table-striped table-bordered table-hover" id="laravel_datatable" style="width:100%">

									    <thead>
									        <tr>
									            <th>Rut</th>
									            <th>Nombre</th>
									            <th>Correo</th>
									            <th>Telefono</th>
									            <th>Obras</th>
									            <th>Opciones</th>
									        </tr>
									    </thead>
									</table>
								</div>
					</div>
				</div>

		</div>
	</div>
</div>
	<!-- inicio modal editar cliente -->
	<div class="modal fade" role="dialog" id="editarCliente">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div type="button" class="close closeCliente" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="closeCliente" >&times;</span></div>
					<h4 class="modal-title">Editar Cliente</h4>
				</div>
				<div class="modal-body container-form-update">
					<div class="row">
						<div class="col-md-12">
							<h3>Datos del Cliente</h3><br>
						</div>
					</div>
					<input type="hidden" name="IdCliente">
					<div class="row">
						<div class="col-md-2 form-group">
							<label>Estado Cliente</label>
							<select name="stateCliente" class="form-control stateCliente" data-live-search="true" validate="not_null">
								<option class="seleccione" value="">Seleccione...</option>
							</select>
						</div>
						<div class="col-md-3 form-group">
							<label>Tipo de Cliente</label>
							<select name="TipoCliente_update" class="form-control TipoCliente" data-live-search="true">
								<option value="">Seleccione...</option>
							</select>
						</div>
						<div class="col-md-3 form-group">
							<label>Tipo de Pago</label>
							<select name="TipoPago_update" class="form-control TipoPago" data-live-search="true" validate="not_null">
								<option value="">Seleccione...</option>
							</select>
						</div>
						<div class="col-md-1 form-group">
							<div class="checkbox" style="margin: 18px auto">
								<input id="PoseePac_update" name="PoseePac_update" class="magic-checkbox" type="checkbox">
								<label for="PoseePac_update">Posee PAC</label>

								<input id="PoseePrefactura_update" name="PoseePrefactura_update" class="magic-checkbox" type="checkbox">
								<label for="PoseePrefactura_update">Posee Prefactura</label>
							</div>
						</div>
						<div class="col-md-2 form-group">
							<label>Rut</label>
							<input name="Rut_update" class="form-control" disabled>
						</div>
						<div class="col-md-1 form-group">
							<label>Dv</label>
							<input id="Dv_update" name="Dv_update" class="form-control" disabled>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label> Razón social / Cliente</label>
							<input name="Nombre_update" class="form-control">
							<input type="hidden" name="cliente_id_bsale" class="form-control">
							<input type="hidden" name="stateOculto" class="form-control">
						</div>
						<div class="col-md-6 form-group">
							<label>Alias</label>
							<input name="Alias_update" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 form-group">
							<label>Dirección  Comercial</label>
							<textarea name="DireccionComercial_update" class="form-control"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 form-group">
							<label>Giro</label>
							<input name="Giro_update" class="form-control" validate="not_null">
						</div>
						<div class="col-md-4 form-group">
							<label>Region</label>
							<select id="Region_update" name="Region_update" class="form-control Region_update" data-live-search="true" validate="not_null">
							</select>
						</div>
						<div class="col-md-4 form-group">
							<label>Ciudad</label>
							<select id="Ciudad_update" name="Ciudad_update" class="form-control Ciudad_update" data-live-search="true" validate="not_null">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 form-group">
							<label>Contacto</label>
							<input name="Contacto_update" class="form-control">
						</div>
						<div class="col-md-4 form-group">
							<label>Teléfono</label>
							<input name="Telefono_update" class="form-control">
						</div>
						<div class="col-md-4 form-group">
							<label>Correo</label>
							<input name="Correo_update" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 form-group">
							<label>Comentarios</label>
							<textarea name="Comentario_update" class="form-control"></textarea>
						</div>
					</div>
					
					<div class="panel">
					
						<div class="panel-body">
						<label for="">Otros Contactos</label>
							<div class="dataContactos2">
									<h4>No hay información</h4>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<br>
							<button type="button" class="btn btn-primary actualizarCliente" id="actualizarCliente">Actualizar</button -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin modal editar cliente -->
	<!--<div class="modal fade" tabindex="-1" role="dialog" id="verServicios" aria-labelledby="verServicios">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Lista de Datos técnicos</h4>
				</div>
				<div class="modal-body containerListDatosTecnicos">
				</div>
			</div>
		</div>
	</div>-->

	<div class="modal fade" id="agregarDatosTecnicos">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Agregar Datos Técnicos</h4>
				</div>
				<div class="modal-body containerTipoServicio">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary guardarDatosTecnicos">Guardar</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div id="modalEditar" class="modal fade" tabindex="-1" role="dialog" id="load">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-gris-oscuro p-t-10 p-b-10">
					<h4 class="modal-title c-negro">Código: <span class="Codigo"></span> <button type="button" data-dismiss="modal" class="close c-negro f-25" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></h4>
				</div>
				<div class="modal-body">
					@include('componentes.componentes_servicios.modal_EditarServicio')

				</div><!-- /.modal-body -->
				<div class="modal-footer p-b-20 m-b-20">
					<div class="col-sm-12">
						<button type="button" class="btn btn-purple" id="updateServ" name="updateServ">Guardar</button>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	@include('servicios.modales.modalStatusServicio')
	@include('servicios.show')
	<!--  <script src="{!! asset('js/jquery-2.2.1.min.js') !!}"></script>
	<script src="{!! asset('js/clientes/controller.js') !!}"></script> -->

@endsection
@section('mijava')
	<!--<script src="{--!! asset('plugins/datatables/dataTables.buttons.min.js') !!}"></script>
	<script src="{--!! asset('plugins/datatables/buttons.html5.min.js') !!}"></script>
	<script src="{--!! asset('plugins/datatables/buttons.print.min.js') !!}"></script>
	<script src="{--!! asset('plugins/datatables/buttons.flash.min.js') !!}"></script>-->
	<script src="{!! asset('plugins/datatables/jszip.min.js') !!}"></script>
	<script src="{!! asset('plugins/datatables/pdfmake.min.js') !!}"></script>
	<script src="{!! asset('plugins/datatables/vfs_fonts.js') !!}"></script>


	<script>
	var table = null;
	$(document).ready( function () {
	    table = $('#laravel_datatable').DataTable({
            pageLength: 50,
            order: [[ 1, "asc" ]],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'excel', title: 'Clientes  de Teledata'},
                {extend: 'pdf', title: 'Clientes de Teledata'},
            ],
			ajax: "{{ url('get_clientes') }}",
	           columns: [
	                    { data: 'rut', name: 'rut' },
	                    { data: 'nombre', name: 'nombre' },
	                    { data: 'correo', name: 'correo' },
	                    { data: 'telefono', name: 'telefono' },
	                    { data: 'servicios', name: 'servicios', orderable: false, searchable: false },
	                    { data: 'action', name: 'action', orderable: false, searchable: false}
	                 ],
	           language: {url: 'json/lenguaje_tata_table.json'},

	        });
	});

	$(document).on('submit', '.delete-contacto', function(e){
        var form = this;
        e.preventDefault();
        swal({ 
                title: "¿Seguro que deseas eliminar el contacto?", 
                text: "No podrás deshacer este paso...", 
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar", 
                cancelButtonColor: "#339933",
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "Continuar!", 
                closeOnConfirm: false
            },
            function(isConfirm){
                if(isConfirm){
                    form.submit();
                }
            }
        );
    });

	$(document).on('click', '.view-cliente', function(){
        $('#modalCliente').modal('show');
        $('.titulado').html($(this).attr('cliente')+" RUT: "+$(this).attr('rut'));
        var id = $(this).attr('id-cli');

        $.get("{{ url('/clientes') }}/"+id, function(data) {
        	$('.alias').html(data.rut);
            $('.nombresito').html(data.nombre);
        	$('.tipoCliente').html(data.tipo_clientes.nombre)
        	$('.giro').html(data.giro)
        	$('.pac').html(data.posee_pac == 1 ? "SI" : "NO")
        	$('.prefactura').html(data.posee_prefactura == 1 ? "SI" : "NO" )
        	$('.claseCLiente').html(data.clase_clientes.nombre)
        	$('.giro').html(data.giro)
        	$('.region').html(data.regiones.nombre)
        	$('.ciudad').html(data.ciudades.nombre)
        	$('.direccion').html(data.direccion)
        	$('.contacto').html(data.contacto)
        	$('.telefono').html(data.telefono)
        	$('.correo').html(data.correo)
        	$('.comentario').html(data.comentario)
        });
    });

	//ver los contactos del cliente
    $(document).on('click', '.abrir-modal-contactos', function() {
        $('#modalContactos').modal('show');
        // parametros para armar la data table
        var id = $(this).attr('id');
        var rut = $(this).attr('rut');
        var cliente = $(this).attr('nombre');
        $('#IdClienteOculto').val(rut);
        $('#IdContactoOculto').val(null);
        $("#grabarContacto").attr("tipo", "graba");
        $('.modal-title-contacto').html(cliente);
        var url = "{{ url('contactos/lista') }}/"+id;
        tabla2 = $('#tabla_ver_contactos').DataTable({
        	destroy: true,
        	paging: false,
    		searching: false,
	        processing: true,
	        serverSide: false,
	        order: [[1, 'asc']],
	        ajax: url,
	        columns: [
	        			{ data: 'id', name: 'id' },
	                    { data: 'contacto', name: 'contacto' },
	                    { data: 'tipo_contacto', name: 'tipo_contacto' },
	                    { data: 'correo', name: 'correo' },
	                    { data: 'telefono', name: 'telefono' },
	                    { data: 'action', name: 'action', orderable: false, searchable: false}
	                 ],
	        language: {url: 'json/lenguaje_tata_table.json'},
	    });
    });

    //ver servicios del modulo /clientes/listaCliente.blade.php
    $(document).on('click', '.verServiciosCliente', function() {
        $('#modalVerServicios').modal('show');
        // parametros para armar la data table
        var id = $(this).attr('attr');
        $("#servicio_rut_dv").val(id);
        var nombre_cliente = $(this).attr('data-nombre'); 
        $("#servicio_nombre_cliente").val(nombre_cliente);
        $('.modal-title-servicio').html(nombre_cliente);
        var url = "{{ url('clientes/servicios') }}/"+id;
        tablita = $('#tabla_ver_servicios').DataTable({
        	"autoWidth": false,
        	"destroy": true,
        	"paging": true,
    		"searching": true,
	        "processing": true,
	        "serverSide": false,
	        "order": [[1, 'asc']],
	        "dom": '<"html5buttons"B>lTfgitp',
	        "ajax": url,
	        "columns": [
	                    { data: 'Codigo', name: 'Codigo', width: '14% !important' },
	                    { data: 'Conexion', name: 'Conexion', width: '8% !important'},
	                    { data: 'Valor', name: 'Valor', width: '8% !important'},
	                    { data: 'Grupo', name: 'Grupo', width: '8% !important'},
	                    { data: 'Fecha', name: 'Fecha', width: '10% !important'},
	                    { data: 'Estado', name: 'Estado', width: '6% !important'},
	                    { data: 'Tipo', name: 'Tipo', width: '24% !important'},
	                    { data: 'Velocidad', name: 'Velocidad', width: '6% !important'},
	                    { data: 'Plan', name: 'Plan', width: '6% !important'},
	                    { data: 'action', name: 'action', orderable: false, searchable: false, width: '10% !important'}
	                 ],
	        "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });
    });

    $(document).on('click', '.edit-contacto', function() {
    	let contacto = $(this).attr('contacto');
    	let correo = $(this).attr('correo');
    	let fono = $(this).attr('fono');
    	let id = $(this).attr('cid');
    	let tc = $(this).attr('tc');

    	$("#NombreContacto").val(contacto);
    	$("#TipoContacto").val(tc);
    	$("#email").val(correo);
    	$("#fono").val(fono);
    	$("#IdContactoOculto").val(id);
    	$("#grabarContacto").attr("tipo", "modifica");    	

    });

    $('body').on('click', '#grabarContacto', function() {
    	let url = "{{ url('contactos') }}"
    	let msge = ' Guardado';
    	let metodo = 'POST';
    	$('#Method').val("POST");
    	if ($(this).attr("tipo") == "modifica") {
    		url = "{{ url('contactos') }}"+"/"+$('#IdContactoOculto').val();
    		$('#Method').val("PUT");
    		metodo = 'PUT';
    		msge = 'Modificado';
    	}

    	var csrf_token = "{{ csrf_token() }}";
        var form = {
        	_method: metodo,
        	rut: $('#IdClienteOculto').val(),
            contacto: $('#NombreContacto').val(),
            tipo_contacto: $('#TipoContacto').val(),
            correo: $('#email').val(),
            telefono: $('#fono').val(),
            _token: csrf_token
        };
        
        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            success: function(response) {
                if (response != 'error') {
	                $('#modalContactos').modal('hide');
	                bootbox.alert('Registro '+msge+' Exitosamente');
	            } else {
	                bootbox.alert('No se guardó el contacto, favor revisar información');
	            }
            }
        });
    });

    
    $(document).on('click', '.estatusServicio', function() {

        $('body').removeClass('loaded');

        $('#Activo').val($(this).attr('estado'));

        var ObjectMe = $(this);
        var ObjectTR = ObjectMe.closest("tr");
        var ObjectId = $(this).attr('attr');
        var ObjectCode = ObjectTR.find("td").eq(0).text();
        $('.Codigo').text(ObjectCode);
        $('.Id').val(ObjectId);
        $('#servicio_codigo_cliente').val(ObjectCode);

        $.ajax({
            type: "GET",
            url: "{{ url('servicios/estado') }}/"+id,
            data: "id=" + ObjectId,
            success: function(response) {
                response = JSON.parse(response)
                $('#FechaInicioDesactivacion').val('')
                $('#FechaFinalDesactivacion').val('')
                if(response.EstatusServicio == '5') {
                    if(response.FechaInicioDesactivacion == ''){
                        //si es 5 pero fechas null es porque ya se suspendio
                        $('#Activo').val(2);
                    }else{
                        //sigue activo temporalmente
                        $('#Activo').val(5)
                    }
                    $('#divFechaActivacion').show()
                    $('#FechaInicioDesactivacion').val(response.FechaInicioDesactivacion)
                    $('#FechaFinalDesactivacion').val(response.FechaFinalDesactivacion)
                    
                } else if (response.FechaFinalDesactivacion == '2999/01/31') {
                    $('#Activo').val(0)
                    $('#divFechaActivacion').hide()
                } else if ( response.EstatusServicio != '5' && response.FechaFinalDesactivacion) {
                    $('#Activo').val(2)
                    $('#FechaInicioDesactivacion').val(response.FechaInicioDesactivacion)
                    $('#FechaFinalDesactivacion').val(response.FechaFinalDesactivacion)
                    $('#divFechaActivacion').show()
                } else {
                    $('#Activo').val(1)
                    $('#divFechaActivacion').hide()
                }
              //  $('#Activo').selectpicker('refresh')
                $('body').addClass('loaded');
            },
            error: function(xhr, status, error) {
                setTimeout(function() {
                    var err = JSON.parse(xhr.responseText);
                    swal('Solicitud no procesada', err.Message, 'error');
                }, 1000);
            }
        });
    });

    $(document).on('click', '.estado-servicio', function() {
        $('#modalEstatus').modal('show');
        $('.Codigo').html($(this).attr('cod'));
        $('#Id').val($(this).attr('attr'));
        $("#Activo").val($(this).attr('status')).change();
        $('#FechaInicioDesactivacion').val($(this).attr('finid'));
        $('#FechaFinalDesactivacion').val($(this).attr('ffind'));
    });

    $('#Activo').on('change', function() {
    	n =  new Date(); y = n.getFullYear(); m = ("0" + (n.getMonth() + 1)).slice(-2); d = n.getDate();
        if (($(this).val() == "5" ) || ($(this).val() == "2")) {
        	$('#divFechaActivacion').show()
        	$('input[name="FechaInicioDesactivacion"]').attr('validate', 'not_null')
            $('#FechaFinalDesactivacion').attr('validate', 'not_null')
            document.getElementById('FechaFinalDesactivacion').disabled = false; 
        } else if (($(this).val() == "0" ) || ($(this).val() == "3")) {
            $('#divFechaActivacion').show();
            $('#FechaInicioDesactivacion').val(y + "-" + m + "-" + d);
            $('#FechaFinalDesactivacion').val(''); 
            document.getElementById('FechaFinalDesactivacion').disabled = true; 
        } else {
            $('#divFechaActivacion').hide()
            $('input[name="FechaInicioDesactivacion"]').val('')
            $('input[name="FechaFinalDesactivacion"]').val('')
        }
    });

    $('body').on('click', '#updateEstatus', function() {
        document.getElementById("updateEstatus").disabled = true;
        if( $('#enviar_mail').is(':checked') ) {
            var correo=1;
        }
        else{
            var correo=0;
		}

        var id = $('#Id').val();
        var url = "{{ url('/servicios') }}/"+id
        var csrf_token = "{{ csrf_token() }}";
        var form = {
            _method: "PATCH",
            _token: csrf_token,
            cambioEstado: $('#Activo').val(),
            fechaIniDes: $('#FechaInicioDesactivacion').val(),
            fechaFinDes: $('#FechaFinalDesactivacion').val(),
            correo: correo
        };

        $.ajax({
            url: url,
            type: 'PATCH',
            data: form,
            success: function(response) {
                var data = JSON.parse(response)
                if (( data.estado != null) && (data.estado == "OK")) {
                    $('#modalEstatus').modal('hide');
                    bootbox.alert('Estado cambiado correctamente . E-mail enviado a ' + data.email);
                    setTimeout('location.reload()', 3000);
                } else {
                    bootbox.alert('Ha ocurrido un error :-(  No se cambió estado');
                }
                document.getElementById("updateEstatus").disabled = false;
            },
            error:function (jqXHR, exception) {
                let errores = "";
                obj = jqXHR.responseJSON.errors;
                Object.keys(obj).forEach(function(key) {
                  errores += obj[key];
                })
                bootbox.alert('Ha ocurrido un error :-( '+ errores );
                document.getElementById("updateEstatus").disabled = false;
            }
        });
    });

    // Ver servicio
    $(document).on('click', '.ver-servicio', function() {
        $('#modalServicio').modal('show');
        let id = $(this).attr('attr');
        let tipoServ = $(this).attr('id-serv');
        $('.titulito').html("Código servicio: " + $(this).attr('cod'));
        let url = "{{ url('/servicios') }}/"+id;

        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                Object.keys(data).forEach(key => $('.'+key).html(data[key]))
                $('.tipos').hide();
                $('.serv-'+tipoServ).show();
            },
            error:function (jqXHR, exception) {
                bootbox.alert('Error al traer los datos' );
            }
        });
    });

	</script>
@endsection