
@extends('layouts.app')

@section('content')
        <?php
        $porc_activo=($servicios->count()/$tservicios)*100;
        $porc_nuevos=($servicios_nuevos->count()/$servicios->count())*100;
        $porc_cc=($servicios_corte->count()/$servicios->count())*100;
        $porc_fin=($servicios_fin->count()/$servicios->count())*100;
        ?>
<!DOCTYPE html>
<html>
<head>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card-header"></div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif


            </div>



        </div>
    </div>
</div>
<h3><img src="{!! asset('img/logo_ico.png') !!}">  DASHBOARD - {{@Auth::user()->nombre}}</h3>
<!--<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins collapsed">
            <div class="ibox-title">
                <h5><b> @php echo count($pendientes); @endphp Servicios pendientes de Activación: </b></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content" >
                <ul>
                    @php $hoy = date('Y-m-d'); @endphp
                    @foreach ($pendientes as $row)
                        @if ( $row->FechaComprometidaInstalacion < $hoy )
                            <li class="text-danger"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} - {{ $row->cliente?$row->cliente->nombre:''}} {{ $row->Codigo }}
                        @elseif ( $row->FechaComprometidaInstalacion == $hoy )
                            <li class="text-warning"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} - {{ $row->cliente?$row->cliente->nombre:''}} {{ $row->Codigo }}
                        @else
                            <li class="text-success"> Fecha Comprometida : {{ $row->FechaComprometidaInstalacion }} - {{ $row->cliente?$row->cliente->nombre:''}} {{ $row->Codigo }}
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>


    </div>
</div>-->

<hr>

<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">2020</span>
                <h5>Total Maquinaria en stock</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{$servicios->count()}}</h1>
                <div class="stat-percent font-bold text-success">{{number_format($porc_activo,0,',','.')}}% <i class="fa fa-bolt"></i></div>
                <small>Total Activos</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">

                <h5>Maquinas  Nuevas</h5>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <span class="label label-info pull-right">2020</span>
                    </a>

                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box ">
                                @foreach($servicios_nuevos as $nuevo)
                                    <div>
                                        <small class="pull-right">{{$nuevo->FechaInstalacion}}</small>
                                        <strong>-{{$nuevo->clientes?$nuevo->clientes->nombre:''}}</strong> <br>
                                        <small class="text-muted"></small>
                                    </div>
                                @endforeach
                            </div>
                        </li>

                    </ul>
                </li>

            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{$servicios_nuevos->count()}}</h1>
                <div class="stat-percent font-bold text-navy">{{number_format($porc_nuevos,0,',','.')}}% <i class="fa fa-level-up"></i></div>
                <small> 2020</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">

                <h5>Maquinas en Servicio Técnico</h5>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <span class="label label-warning pull-right">Hasta hoy</span>
                    </a>

                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box ">
                                @foreach($servicios_corte as $corte)
                                    <div>
                                        <small class="pull-right">{{$corte->update_at}}</small>
                                        <strong>-{{$corte->clientes?$corte->clientes->nombre:''}}</strong> <br>
                                        <small class="text-muted"></small>
                                    </div>
                                @endforeach
                            </div>
                        </li>

                    </ul>
                </li>

            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{$servicios_corte->count()}}</h1>
                <div class="stat-percent font-bold text-warning">{{number_format($porc_cc,0,',','.')}}% <i class="fa fa-level-down"></i></div>
                <small></small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Maquinas Arrendadas</h5>

                        <!-- *************** dropdown **********************-->
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                     <span class="label label-danger pull-right">2020</span>
                    </a>

                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box ">
                                @foreach($servicios_fin as $fin)
                                <div>
                                    <small class="pull-right">{{$fin->FechaInicioDesactivacion}}</small>
                                    <strong>{{$fin->clientes?$fin->clientes->nombre:''}}</strong><br>
                                    <small class="text-muted"></small>
                                </div>
                                 @endforeach
                            </div>
                        </li>

                    </ul>
                </li>


            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{$servicios_fin->count()}}</h1>
                <div class="stat-percent font-bold text-danger">{{number_format($porc_fin,0,',','.')}}% <i class="fa fa-level-down"></i></div>
                <small>Servicios con fin de contrato</small>
            </div>


        </div>
    </div>
</div>
<!--The div element for the map -->
<div id="map"></div>
<

<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {lat: -41.3214705, lng: -73.0138898};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 12, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8&callback=initMap">
</script>
</body>
</html>
@endsection
