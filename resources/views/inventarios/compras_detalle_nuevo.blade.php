 @extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Detalle Compra</strong>
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight " >
        <div class="row">
            <div class="col-lg-10">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5><i class="fa fa-shopping-cart"> </i>  Agregar Detalle a la  Compra </h5><br>
                        <div class="row">
                            <div class="col-lg-4">
                                <small>Proveedor:  {{$compra->proveedor->nombre}} </small><br>
                                <small>Nro Doc :  {{$compra->numero_documento}} </small><br>
                                <small class="" >Monto Neto :   <span class="badge badge-info">$ {{number_format($compra->total_documento,0,',','.')}}</span> </small><br>
                                <input type="hidden" name="total_factura" id="total_factura" value=" {{$compra->total_documento}}">
                            </div>
                            <div class="col-lg-4">
                                <small>Bodega Destino:  {{$compra->bodega->nombre}} </small><br>
                                <small>Fecha Compra :  {{$compra->fecha_emision}} </small><br>
                            </div>
                        </div>
                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="ibox table-responsive" id="rv_panel">
                      <div class="ibox-content" >
                        <div class="sk-spinner sk-spinner-double-bounce">
                            <div class="sk-double-bounce1"></div>
                            <div class="sk-double-bounce2"></div>
                        </div>
                        <div class="row">

                            <form name="frmComprad" id="frmComprad" method="post" >
                            {{csrf_field()}}
                                <input type="hidden" name="compra_id" id="compra_id" value="{{$compra->id}}">
                                <div class="col-lg-12">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Código ó Codigo de Barra</label>
                                            <div class="input-group">
                                                <input type="text" name="codigo" id="codigo" class="form-control" value="{{old('codigo')}}" onblur="busca_producto(this.value)">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Cantidad</label>
                                            <div class="input-group">
                                                <input type="number" name="cantidad" id="cantidad" class="form-control" value="{{old('cantidad')}}">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Precio Unit. Neto ($)</label>
                                            <div class="input-group">
                                                <input type="number" name="precio" id="precio" class="form-control" value="{{old('precio')}}">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    <div class="form-group" style="padding-top: 18px;">
                                        <button  class="btn btn-primary pull-left" id="btn_agregar" name="btn_agregar" type="submit"  >
                                            <i class="fa fa-shopping-cart"></i>  Agregar</button>
                                    </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 pull-left" >
                                            <span id="codigo_error"></span>
                                            <input type="hidden" id="producto_id" name="producto_id" value="">
                                            <input type="hidden" id="producto_unico" name="producto_unico" value="">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                            <!- ******************************MODALS***************************************************************************************************************************************-->
                            <div>
                                <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title">Producto Múltiple</h4>
                                                <small class="font-bold">Agrega las MAC o Nro de serie de cada producto.</small>
                                            </div>
                                            <form id="formMac" role="form" class="form-horizontal" method="post" action="{{ url('compra_detalle_mac_guardar') }}">
                                                {{csrf_field()}}
                                                <div class="modal-body">

                                                        <div id="bodymac" class="">

                                                        </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="btn_cerrar_modal" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                    <button type="submit" name="btn_mac" id="btn_mac" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" value="guardar"><i class="fa fa-save"></i> Guardar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg animated  rollIn">
                                        <div class="modal-content">

                                            <div class="modal-body">
                                                <div class="wrapper wrapper-content  ">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox float-e-margins">
                                                                <div class="ibox-title">
                                                                    <h5>Lista de Productos e Insumos Disponibles</h5>
                                                                    <a href="{{ url('/mantenedores/productos_nuevo') }}" class="btn btn-info pull-right" style="padding-top: 5px;"><i class="fa fa-plus"></i> Nuevo Registro </a>
                                                                </div>
                                                                <div class="ibox-content">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped table-bordered table-hover dataTables-example"   >
                                                                            <thead>
                                                                            <tr>
                                                                                <th> <small>Id</small></th>
                                                                                <th> <small>Codigo Barra</small></th>
                                                                                <th> <small>Tipo Producto</small></th>
                                                                                <th> <small>Marca</small></th>
                                                                                <th> <small>Modelo</small></th>
                                                                                <th> <small>Descripcion</small></th>
                                                                                <th> <small>Unico</small></th>


                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @foreach($productos as $producto)
                                                                                <tr>

                                                                                    <td>
                                                                                        <small> {{$producto->id}}</small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small>{{$producto->codigo_barra}} </small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small> {{$producto->tipo_producto->nombre}} </small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small>{{$producto->marca->nombre}} </small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small>{{$producto->modelo->nombre}} </small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small>{{$producto->descripcion}} </small>
                                                                                    </td>
                                                                                    <td>
                                                                                        <small> {{$producto->unico}} </small>
                                                                                    </td>

                                                                                    </small>
                                                                                </tr>
                                                                            @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
        </div>


        <!-- *****************************DETALLE **********************************************-->
        <div class="row">
           <div class="col-lg-10">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-list"></i>  Detalle de la Compra <small class="m-l-sm">Aún se puede editar la compra, una vez finalizado el proceso no se podrá editar.</small></h5>

                    </div>
                    <div class="ibox" id="rv_panel2">
                         <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-double-bounce">
                                        <div class="sk-double-bounce1"></div>
                                        <div class="sk-double-bounce2"></div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12 table-responsive">

                                        <table class="footable table table-stripped  table-bordered " id="tabla_detalle">
                                            <thead>
                                                <tr>

                                                    <th data-toggle="true">Cantidad</th>
                                                    <th>Codigo</th>
                                                    <th>Descripción</th>
                                                    <th>P. Unitario</th>
                                                    <th>Total</th>
                                                    <th>Accion</th>
                                                    <th data-hide="all"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <?php
                                                    $total_detalle=0;
                                                    ?>
                                                    @if($detalles != 'No hay Registros')
                                                        @foreach($detalles as $detalle)
                                                           <tr>


                                                                   @if($detalle->producto->unico == 0)
                                                                   <td  > <span  class="badge badge-primary">{{$detalle->cantidad}}</span></td>
                                                                   @else
                                                                    <td > {{$detalle->cantidad}}</td>
                                                                   @endif

                                                               <td>{{$detalle->producto->codigo_barra}}</td>
                                                               <td> <small>{{$detalle->producto->tipo_producto->nombre}} {{$detalle->producto->marca->nombre}} {{$detalle->producto->modelo->nombre}}</small></td>

                                                               <td>{{number_format($detalle->precio_neto,0,',','.')}}</td>
                                                               <td>{{number_format($detalle->total_neto,0,',','.')}}</td>
                                                               <td> <a class="del" id="{{$detalle->id}}" name="{{$detalle->total_neto}}"  href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a> </td>

                                                               @if($detalle->producto->unico == 0)
                                                                   <td>
                                                                    <table class="table table-bordered">

                                                                           @foreach($detalle->detalle_mac as $detallemac)
                                                                            <tr>
                                                                              <td><small>Serie/MAC:</small></td> <td><small>{{$detallemac->mac}}</small></td>
                                                                            </tr>
                                                                           @endforeach

                                                                    </table>
                                                                   </td>

                                                               @endif

                                                           </tr>
                                                         <?php $total_detalle=$total_detalle+$detalle->total_neto ?>
                                                       @endforeach
                                                    @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>

                                       </div>
                                    </div>
                          </div>
                    </div>
                    <div class="ibox-footer">
                            <span class="pull-right">
                             <a class="btn btn-primary" href="#" id="rv_finalzar" data-value="{{$compra->id}}"><i class="fa fa-check"></i> Finalizar</a>
                            </span>
                        <div> <h3> <i class="fa fa-money"></i> Total Neto:  <span id="total_sumatoria" data-value="{{$total_detalle}}" class="badge badge-info"> $ <?php echo number_format($total_detalle,0,',','.'); ?></span> </h3>  </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
        <!-- *****************************HASTA ACA EL DETALLE **********************************************-->


 @endsection
@section('mijava')
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();
            $( "#codigo" ).focus();


        });

        $("#codigo").keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
           // console.log(keycode)
            if(keycode == '13'){
               // alert('13')
                $('#cantidad').focus();
                event.preventDefault();
                return false;
            }
        });

        $("#cantidad").keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            // console.log(keycode)
            if(keycode == '13'){
                // alert('13')
                $('#precio').focus();
                event.preventDefault();
                return false;
            }
        });

        function valida_enter(e){

            if (e.keyCode === 13) {
                var form = e.target.form;
                var index = Array.prototype.indexOf.call(form, e.target);
                form.elements[index + 1].focus();
                e.preventDefault();

                return false;
            }
        }

        function busca_repetido(r) {
            $("#btn_mac").attr("disabled", false);
            $('#'+r.id).css('border-color','#ffffff');
            var i;
            var rvmac=$('#'+r.id).val();
            var cant=$("#cantidad2").val();
            if($('#'+r.id).val() !="")
            {
                 console.log(rvmac);
                console.log(cant);
                for(i=0; i<cant;i++){
                    if (r.value===$('#MAC'+i).val()){
                        if (r.name !=$('#MAC'+i).attr('name'))
                        {
                            //$('#mac_serie'+i).css('border-color','#ff0000');
                            $('#'+r.id).css('border-color','#ff0000');
                            $("#btn_mac").attr("disabled", true);
                            $('#'+r.id).focus();
                            swal("Cometiste un Error!", 'La MAC-SEIRE ingresada está repetida', "error");
                        }
                        else{
                            //  $("#btn_mac").attr("disabled", false);
                            //  $('#'+r.id).css('border-color','#ffffff');
                        }
                    }
                }
            }

        }

       /* document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)  {
                return false;
            }
        }*/

        function busca_producto(codigo){
            //alert('hola '+codigo);
            $.get("/inventarios/compras_detalle_busca_producto/"+codigo+"", function (response) {

                if (response.nomatch === undefined){
                    //console.log(response.nomatch);
                    $('#codigo_error').html('<div class="text text-info">'+response.coleccion.tipo_producto.nombre+' '+response.coleccion.marca.nombre+' '+response.coleccion.modelo.nombre+'</div>');
                    $('#producto_id').val(response.coleccion.id)
                    $('#producto_unico').val(response.coleccion.unico)
                    $('#btn_agregar').attr("disabled", false)
                    return false;
                }
                else {
                    alert(response.nomatch)
                    $('#codigo_error').html('<div class="text text-danger">'+response.nomatch+'</div>');
                    $('#btn_agregar').attr("disabled", true)
                    return false;
                }

               // $(dis).closest('tr').remove();
            });

        }

         //*****************************************************************************************
        //este datatable es para la modal buscar producto
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: []
            });

        });
        //****************************************cierre de moral buscar productos *************************************************
       /* $('#myModal').on('hide.bs.modal', function (e) {
           // alert('hide');
            var anim = 'rollIn';
            $('.modal .modal-dialog').attr('class', 'modal-dialog animated ' + anim + ' ');
        })*/





        $('#frmComprad').on('submit', function (e) {
            e.preventDefault(e);
            //alert('envio');
            if (($('#precio').val() === "")||($('#precio').val() < 0)) {
                swal("COMETISTE UN ERROR", "NO se puede agregar este ítem porque el precio es incorrecto ", "error");
            }
            else {

                var total = ($('#precio').val() * $('#cantidad').val() + parseInt($('#total_sumatoria').attr('data-value')));
                //alert(total);
                if (parseInt(total) > parseInt($('#total_factura').val()))
                    swal("COMETISTE UN ERROR", "NO se puede agregar este ítem porque su monto (o la sumatoria de ítems) sobrepasa el monto neto de la factura! ", "error");
                else {
                    if ($('#producto_unico').val() === '0') {
                        // alert('Prodcuto con MAC'+ $('#cantidad').val())
                        var html, i;
                        for (i = 0; i < $('#cantidad').val(); i++) {
                            //alert(i);
                            if (i === 0) {
                                html = '<div class="form-group">';
                                html = html + '<div class="col-lg-2">MAC ' + (parseInt(i) + 1) + '</div><div class="col-lg-10"> <input type="text" name="MAC' + i + '" id="MAC' + i + '" value="" class="form-control" onload="this.focus()" onkeypress="return  valida_enter(event)" onblur="busca_repetido(this)" required ></div>';
                                html = html + '</div>';
                            }
                            else {
                                html = html + '<div class="form-group">';
                                html = html + '<div class="col-lg-2">MAC ' + (parseInt(i) + 1) + '</div><div class="col-lg-10"> <input type="text" name="MAC' + i + '"  id="MAC' + i + '" value="" class="form-control" onkeypress="return  valida_enter(event)" onblur="busca_repetido(this)" required ></div>';
                                html = html + '</div>';
                            }
                        }
                        html = html + '<input type="hidden" name="compra_id" id="compra_id" value="' + $('#compra_id').val() + '"  >';
                        html = html + '<input type="hidden" name="producto_id" id="producto_id" value="' + $('#producto_id').val() + '"   >';
                        html = html + '<input type="hidden" name="cantidad2" id="cantidad2" value="' + $('#cantidad').val() + '"   >';
                        html = html + '<input type="hidden" name="precio" id="precio" value="' + $('#precio').val() + '"   >';

                        $('#bodymac').html(html);
                        $('#myModal2').modal('show');

                        $('#btn_agregar').attr("disabled", true);
                        $('#codigo').val('');
                        $('#cantidad').val('');
                        $('#precio').val('');

                        $('#formMac').validate({
                            rules: {}
                        });
                    }
                    else {

                        $('#btn_agregar').attr("disabled", true);
                        $.ajax({

                            url: '/inventarios/compras_detalle_nuevo/guardar',
                            type: "POST",
                            dataType: "JSON",
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            //dataType: 'json',
                            success: function (data) {

                                $("#tabla_detalle").append("<tr><td>" + data.coleccion.cantidad + "</td><td>" + data.coleccion.producto.codigo_barra + "</td><td><small>" + data.coleccion.producto.tipo_producto.nombre + " " + data.coleccion.producto.marca.nombre + " " + data.coleccion.producto.modelo.nombre + "</small></td>" +
                                    "<td>" + formatcurrency(data.coleccion.precio_neto) + "</td><td>" + formatcurrency(data.coleccion.total_neto) + "<td> <a class='del' name=" + data.coleccion.total_neto + " id=" + data.coleccion.id + " href='#' onclick='delete_item(this)' ><i class='text-danger fa fa-trash' title='Eliminar Registro'></i></a> </td></td></tr>")

                                var suma = parseInt($('#total_sumatoria').attr('data-value')) + parseInt(data.coleccion.total_neto);
                                $('#total_sumatoria').attr('data-value', suma);
                                $('#total_sumatoria').html('$ ' + formatcurrency(suma));
                                $('#btn_agregar').attr("disabled", true);
                                $('#codigo').val('');
                                $('#cantidad').val('');
                                $('#precio').val('');
                                $('#btn_agregar').attr("disabled", false);
                            },
                            error: function (data) {
                                console.log(data)
                                $('#codigo_error').html('<div class="text text-danger"> ' + data.responseJSON.errors.codigo + ' ' + data.responseJSON.errors.cantidad + ' ' + data.responseJSON.errors.precio + '</div>');
                                $('#btn_agregar').attr("disabled", false);
                                //swal("ERROR", data.responseJSON.errors.codigo, "error");
                            }

                        })
                    }
                } //else valida monto total factura
            } //else valida precio
        })

        $('.del').click(function () { //RV Dice: SWAL para eliminar

            var dis=this;
             // alert(dis.name); //en realidad este campo correponde al monto (value), pero value como atributo no aplica para un <a> por eso estoy usando el attr name para tener el monto que se va a restar del total
            //alert($('#total_sumatoria').attr('data-value'));
            var nuevo_total= parseInt($('#total_sumatoria').attr('data-value')) - parseInt(dis.name);
            //alert(nuevo_total);
            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {

                    if (isConfirm) {
                        /*console.log('holaa: '+ (dis.name)); */
                        $.get("/compras_detalle_eliminar/"+dis.id+"", function  (response) {

                            exito=response.Success;
                        });
                        $('#total_sumatoria').attr('data-value',nuevo_total);
                        $('#total_sumatoria').html('$ '+ formatcurrency(nuevo_total));

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

        function delete_item(dis)
        {
            var nuevo_total= parseInt($('#total_sumatoria').attr('data-value')) - parseInt(dis.name);
            //alert(nuevo_total);
            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {

                    if (isConfirm) {
                        /*console.log('holaa: '+ (dis.name)); */
                        $.get("/compras_detalle_eliminar/"+dis.id+"", function  (response) {
                            exito=response.Success;
                        });
                        $('#total_sumatoria').attr('data-value',nuevo_total);
                        $('#total_sumatoria').html('$ '+ formatcurrency(nuevo_total));

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });

        }


        $(function() {

            var l = $( '.ladda-button-demo' ).ladda();
                //Boton con spinner submit de las MAC

                l.click(function(){
                    // Start loading
                    $('#btn_cerrar_modal').attr("disabled", true);
                    $('#formMac').submit();
                    l.ladda( 'start' );

                });


            $('#rv_finalzar').on('click', function () {
                //spinner del panel completo, se depliega con el boton finalizar
                $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                $('#rv_panel2').children('.ibox-content').toggleClass('sk-loading');

                var total_sumatoria, total_factura
                total_factura=$('#total_factura').val();
                total_sumatoria= $('#total_sumatoria').attr('data-value');
               // alert(total_factura +' '+ total_sumatoria);
                if (parseInt(total_factura) === parseInt(total_sumatoria) )
                {
                    $.ajax({

                        url: '/inventarios/compras_finalizar/'+$('#compra_id').val(),
                        type: "GET",
                        dataType: "JSON",
                        // data:  new FormData(this),
                        processData: false,
                        contentType: false,
                        //dataType: 'json',
                        success: function (data) {
                           // alert('exito '+data.filas_afectadas);
                            swal("OK!", "Datos Almacenados exitosamente!", "success");
                            $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                            $('#rv_panel2').children('.ibox-content').toggleClass('sk-loading');
                            window.location.href="{{ url('/inventarios/compras') }}";

                        },
                        error: function (data) {
                            //alert('fail '+data);
                            swal("Ups!", "Un problema lógico con base de datos NO  ha podido Finalizar esta Compra!", "error");
                            $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                            $('#rv_panel2').children('.ibox-content').toggleClass('sk-loading');

                        }

                    })

                }
                else
                {
                    swal("ERROR", "No se puede finalzar la compra porque el Monto Neto de la factura no es igual al Total Neto (sumatoria del detalle)! ", "error");
                    $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                    $('#rv_panel2').children('.ibox-content').toggleClass('sk-loading');

                }

            })
        })
    </script>
@endsection