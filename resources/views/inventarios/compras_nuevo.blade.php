@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Nueva Compra</strong>
                </li>
            </ol>
        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-10">
                <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5><i class="fa fa-shopping-cart"> </i>  Nueva Compra <!--<small> Aquí también puedes crear nuevos Tipos de proveedores, CC y Bodegas [+]</small>--></h5>

                            </div>

                            <div>
                                @if (count($errors)>0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                    <div class="ibox-content">

                        <div class="row">

                            <form name="frmCompra" method="post" action="{{ url('inventarios/compras_guardar') }}">
                                {{csrf_field()}}

                                                    <div class="col-lg-6">

                                                                <div class="form-group">
                                                                    <label>Tipo de Documento</label>
                                                                    <select name="tipo_doc" id="tipo_doc" class="form-control">
                                                                        <option value="">-Seleccione-</option>
                                                                        @foreach($tipos_doc as $tipo )
                                                                            <option value="{{$tipo->id}}" {{ old('tipo_doc') == $tipo->id ? 'selected' : '' }}>{{$tipo->nombre}}</option>
                                                                        @endforeach
                                                                    </select>


                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Número Documento</label>
                                                                    <input type="number" name="num_doc" id="num_doc" class="form-control" value="{{old('num_doc')}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Fecha Emision o Fecha de Compra</label>
                                                                    <input type="date" name="fecha_emision" id="fecha_emision" class="form-control" value="{{old('fecha_emision')}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Fecha Vencimiento (de la factura)</label>
                                                                    <input type="date" name="fecha_venc" id="fecha_venc" class="form-control" value="{{old('fecha_venc')}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Total Neto ($)</label>
                                                                    <input type="number" name="total_neto" id="total_neto" class="form-control" value="{{old('total_neto')}}">
                                                                </div>


                                                    </div>

                                                    <div class="col-lg-6">

                                                            <div class="form-group">
                                                                <label >Proveedor </label>
                                                                <div class="input-group">
                                                                    <select name="proveedor" class="form-control">
                                                                        <option value="">-Seleccione-</option>
                                                                        @foreach($proveedores as $proveedor )
                                                                            <option value="{{$proveedor->id}}" {{ old('proveedor') == $proveedor->id ? 'selected' : '' }}>{{$proveedor->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                                                <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                                                <i class="fa fa-plus"></i>
                                                                                </button>-->
                                                                    </span>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label >Centro de Costo</label>
                                                                <div class="input-group">
                                                                    <select name="costo" id="costo" class="form-control">
                                                                        <option value="">-Seleccione-</option>
                                                                        @foreach($costos as $costo )
                                                                            <option value="{{$costo->id}}" {{ old('costo') == $costo->id ? 'selected' : '' }}>{{$costo->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                                                  <!--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    </button>-->
                                                                    </span>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">

                                                                <label >Bodega Destino</label>
                                                                <div class="input-group">
                                                                    <select name="bodega" id="bodega" class="form-control">
                                                                        <option value="">-Seleccione-</option>
                                                                        @foreach($bodegas as $bodega )
                                                                            <option value="{{$bodega->id}}" {{ old('bodega') == $bodega->id ? 'selected' : '' }}>{{$bodega->nombre}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                    <!--<span class="input-group-btn">
                                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3">
                                                                                <i class="fa fa-plus"></i>
                                                                            </button>
                                                                        </span>-->
                                                                </div>
                                                            </div>

                                                        <div class="form-group">
                                                            <label>Observaciones</label>
                                                            <textarea  cols="10" rows="5" name="obs" id="obs" class="form-control">{{old('obs')}}</textarea>
                                                        </div>
                                                        <br>
                                                        <div class="form-group">
                                                            <button class="btn btn-primary pull-right" type="submit" onclick="frmCompra.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                                        </div>
                                                    </div>

                            </form>
                         </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

                        <!- ******************************MODALS***************************************************************************************************************************************-->
                        <div>


                            <div class="modal inmodal " id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated  bounceInRight">

                                        <div class="modal-body">

                                            <div class="form-group"><i class="fa fa-shopping-bag"></i> <label> Ingresa un nuevo tipo de producto</label>
                                                <input type="text" name="txt_nuevo_tipo" id="txt_nuevo_tipo" placeholder="Escribe aqui..." class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="button" name="btn_tipo" id="btn_tipo" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ****************************** -->
                        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">

                                    <div class="modal-body">

                                        <div class="form-group"><i class="fa fa-map-marker"></i> <label> Ingresa una nueva Marca</label>
                                            <input type="text" name="txt_nueva_marca" id="txt_nueva_marca" placeholder="Escribe aqui..." class="form-control"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                        <button type="button" name="btn_marca" id="btn_marca" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************************** -->

                    <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">

                                <div class="modal-body">

                                    <div class="form-group"><i class="fa fa-mobile"></i> <label> Ingresa un nuevo Modelo</label>
                                        <input type="email" name="txt_nuevo_modelo" id="txt_nuevo_modelo" placeholder="Escribe aqui..." class="form-control"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                    <button type="button" name="btn_modelo" id="btn_modelo" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                <!-- ****************************** -->

        </div>
    </div>
    </div>
    </div>

@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
                return false;
            }
        }

        //*********************************** cambio combo marca
        $('#marca').on('change',function() {

            //alert('cambio marca');
            var id_
            id_=this.value;
            //alert('cambio marca'+id_)
            $.ajax({

                url: '/cargar_marcas_modelos/'+id_,
                //type: $(dis).attr("method"),
                dataType: "JSON",
                //data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    console.log('modelos: '+data.data.length);
                    $("#modelo").empty();
                    for(var i=0; i<data.data.length; i++){
                        $("#modelo").append("<option value='"+data.data[i].id+"'>"+data.data[i].nombre+"</option>")
                    }

                },
                error: function (data) {

                    swal("ERROR", "NO seleccionaste ninguna marca!", "error");


                }
            })
        })

        //***************************************************************** NUEVO TIPO

        $('#btn_tipo').on('click',function() {

            var nombre_tipo
            nombre_tipo=document.getElementById("txt_nuevo_tipo").value;

            $.ajax({

                url: '/nuevo_tipo_producto/'+nombre_tipo,
                dataType: "JSON",
                processData: false,
                contentType: false,

                success: function (data) {
                    $("#tipo_producto").append("<option value='"+data.sql+"'>"+nombre_tipo+"</option>")
                    swal("Listo!", nombre_tipo + " ha sido almacenado con éxito! id:"+data.sql, "success");
                    $('#myModal').modal('hide');

                },
                error: function (data) {
                    nombre_tipo==""? nombre_tipo=', No dejes campos en blanco':nombre_tipo=nombre_tipo;
                    swal("ERROR", "NO se ha podido guardar "+nombre_tipo+" - "+data.sql, "error");

                }
            })
        })

        //***************************************************************** NUEVA MARCA

        $('#btn_marca').on('click',function() {

            var nombre_marca
            nombre_marca=document.getElementById("txt_nueva_marca").value;

            $.ajax({

                url: '/nueva_marca_producto/'+nombre_marca,
                dataType: "JSON",
                processData: false,
                contentType: false,

                success: function (data) {
                    $("#marca").append("<option value='"+data.sql+"'>"+nombre_marca+"</option>")
                    swal("Listo!", "La marca "+nombre_marca + " ha sido almacenada con éxito! id:"+data.sql, "success");
                    $('#myModal2').modal('hide');

                },
                error: function (data) {
                    nombre_marca==""? nombre_marca=', No dejes campos en blanco':nombre_marca=nombre_marca;
                    swal("ERROR", "NO se ha podido guardar la marca "+nombre_marca+" - "+data.sql, "error");

                }
            })
        })

        //***************************************************************** NUEVO MODELO

        $('#btn_modelo').on('click',function() {

            var nombre_modelo
            var marca_id = document.getElementById('marca').value;
            // alert(marca_id)
            if (marca_id ===""){
                swal("COMENTISTE UN ERROR", "Primero debes seleccionar una marca y luego crear el modelo", "error");
            }
            else{

                nombre_modelo=document.getElementById("txt_nuevo_modelo").value;

                $.ajax({

                    url: '/nuevo_modelo_producto/'+marca_id+'/'+nombre_modelo,
                    dataType: "JSON",
                    processData: false,

                    contentType: false,

                    success: function (data) {
                        $("#modelo").append("<option value='"+data.sql+"'>"+nombre_modelo+"</option>")
                        swal("Listo!", "El Modelo "+nombre_modelo + " ha sido almacenado con éxito! id:"+data.sql, "success");
                        $('#myModal3').modal('hide');

                    },
                    error: function (data) {
                        nombre_modelo==""? nombre_modelo=', No dejes campos en blanco':nombre_modelo=nombre_modelo;
                        swal("ERROR", "NO se ha podido guardar "+nombre_modelo+" - "+data.sql, "error");

                    }
                })

            }

        })
    </script>
@endsection
