@extends('layouts.app')
@section('title')
    Compras
@endsection
@section('content')
    <?php
    use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Compras</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/inventarios/compras_nuevo') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Compras Efectuadas</h5>

                    </div>
                    <div class="ibox-content">


                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nro. Documento</th>
                                    <th>Tipo Documento</th>
                                    <th>Fecha Compra</th>
                                    <th>Fecha Venc.</th>
                                    <th>Proveedor</th>
                                    <!--<th>Centro Costo</th>-->
                                    <th>Bodega Destino</th>
                                    <th>Monto Neto</th>
                                    <th>Estado</th>
                                    <th>Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($consulta as $compra)
                                    <tr>
                                        <?php
                                        $fecha1=Carbon::parse($compra->fecha_emision);
                                        $fecha1=$fecha1->format('d-m-Y');
                                        $fecha2=Carbon::parse($compra->fecha_vencimiento);
                                        $fecha2=$fecha2->format('d-m-Y');
                                        ?>
                                        <td>
                                            {{$compra->id}}
                                        </td>
                                        <td>
                                            {{$compra->numero_documento}}
                                        </td>
                                        <td>
                                            {{$compra->tipo_documento->nombre}}
                                        </td>
                                        <td>
                                            {{$fecha1}} <!--emision-->
                                        </td>
                                        <td>
                                            {{$fecha2}} <!--vencimiento-->
                                        </td>
                                        <td>
                                            {{$compra->proveedor->nombre}}
                                        </td>
                                      <!--  <td>
                                            @if($compra->centro_costo != null)
                                            {{--$compra->centro_costo->nombre--}}
                                            @endif

                                        </td>-->
                                        <td>
                                            {{$compra->bodega->nombre}}
                                        </td>
                                        <td>
                                            {{ number_format($compra->total_documento,0,',','.')}}
                                        </td>
                                        <td>
                                            @if($compra->estado_id != null)
                                                <?php
                                                    $badge="";
                                                switch ($compra->estado_id){
                                                    case 1:
                                                        $badge ='<label class="badge badge-danger">'.$compra->estado->nombre.'</label>';
                                                        break;
                                                    case 2:
                                                        $badge ='<label class="badge badge-info">'.$compra->estado->nombre.'</label>';
                                                        break;
                                                    case 3:
                                                        $badge ='<label class="badge badge-warning">'.$compra->estado->nombre.'</label>';
                                                        break;
                                                    case 4:
                                                        $badge ='<label class="badge badge-success">'.$compra->estado->nombre.'</label>';
                                                        break;
                                                }

                                                echo $badge;
                                                ?>

                                            @endif

                                        </td>
                                        <td>
                                            @if($compra->estado_id ==1)
                                            <a href="{{ url('/inventarios/compras_editar/'.$compra->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                             <!-- <a class="del" id="{{$compra->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a> ||-->

                                                <a href="{{ url('/inventarios/compras_detalle_nuevo/'.$compra->id) }}" ><i class="fa fa-fast-forward" title="Editar Registro"></i></a>
                                            @else
                                                <a data-toggle="modal" data-target="#myModal2"  data-id="{{$compra->id}}" class="open-Modal2"> <i class="fa fa-upload text-primary " title="Adjuntar Archivo"></i></a>

                                                    @if($compra->archivos->count() !=0)
                                                        @foreach($compra->archivos as $file)

                                                            <?php
                                                            $archivo=explode('.',$file->path);
                                                            // echo $archivo[1];
                                                            if(($archivo[1]=='png') ||($archivo[1]=='jpg')||($archivo[1]=='jpeg')||($archivo[1]=='gif'))
                                                            {
                                                            ?>
                                                            <a data-toggle="modal" data-target="#myModal"  data-id="{{$file->path}}" class="open-Modal"> <i class="text-warning fa fa-file-image-o "></i></a>
                                                            <?php
                                                            }
                                                            if ($archivo[1]=='pdf')
                                                            {
                                                                ?>


                                                            <a  target="_blank"  href="{{'/storage/'}}{{$file->path}}" > <i class="text-danger fa fa-file-pdf-o "></i></a>
                                                            <?php  }
                                                                if(($archivo[1]=='doc')||($archivo[1]=='docx')||($archivo[1]=='xls')||($archivo[1]=='xlsx'))
                                                                {
                                                                ?>

                                                                <a  target="_blank"  href="{{'/storage/'}}{{$file->path}}" > <i class="text-info fa fa-file-archive-o "></i></a>
                                                                <?php  } ?>
                                                        @endforeach

                                                    @endif
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                    <!--  modal -->
                    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">

                                <div class="modal-body">

                                    <div class="dropbox">
                                        <img  id="imageBox"  class="img-responsive img-thumbnail" >
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal file -->

                    <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <form name="form_archivos" id="form_archivos" enctype="multipart/form-data" class="form-horizontal" method="post">
                                {{csrf_field()}}
                                <div class="modal-content animated bounceInRight">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Subir Archivos Adjuntos a la compra <i class="fa fa-upload"></i></h4>
                                        <div>
                                            @if (count($errors)>0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach($errors->all() as $error)
                                                            <li>{{$error}}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="modal-body">

                                        <input type="hidden" name="id_report" id="id_report" value="">
                                        <div class="ibox-content">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="fileinput fileinput-new " data-provides="fileinput">
                                                                                             <span class="btn btn-default btn-file "><text class="text-success" >Puede Seleccionar varios a la vez</text>
                                                                                             <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple/>
                                                                                             </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success" >Subir Archivo(s)</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 50,
                responsive: true,
                order: [[ 0, "desc" ]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Compras de Productos e Insumos  de Teledata'},
                    {extend: 'pdf', title: 'Compras Productos e Insumos de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

        $('.del').click(function () { //RV Dice: SWAL para eliminar compras... solo si no estan finalizadas y que no tengan detalles

            var dis=this;

            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/mantenedores/compras_eliminar/"+dis.id+"", function  (response) {

                            exito=response.Success;
                        });

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

        // abrir modal para ver cargar archivos en compra finalizada.
        $(document).on("click", ".open-Modal2", function () {
            var myDNI = $(this).data('id');
            document.getElementById('id_report').value =  myDNI ;
        });

        //********************************************************** subir y guardar archivos desde la modal ********************************

        $('#form_archivos').on('submit',function(e) {
            e.preventDefault(e);
            var dis=this;
            // console.log( id_report);
            console.log(dis);
            $.ajax({

                url: '/inventarios/compras_agregar_archivos',
                type: $(dis).attr("method"),
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    $('#form_archivos').trigger("reset");
                    swal("OK", "Registro Guardado", "success");
                    $('#myModal2').hide(); //cierre de modal
                    location.reload();

                },
                error: function (data) {
                    $('#form_archivos').trigger("reset");
                    swal("ERROR", "NO seleccionaste ningun archivo!", "error");
                    $('.modal-backdrop').remove();//eliminamos el backdrop del modal

                }
            })

        });
        // abrir modal para ver archivos adjuntos
        $(document).on("click", ".open-Modal", function () {
            var myDNI = $(this).data('id');
            document.getElementById('imageBox').src = '{{'../storage/'}}' + myDNI ;
        });

    </script>
@endsection
