@extends('layouts.app')
@section('title')
    Traslados
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Nuevo Traslado</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-exchange"></i>  Nuevo Traslado</h5>

                    </div>
                    <div>

                            <div id="ftcodigo_error">
                            </div>

                    </div>
                        <form name="frmTraslado" id="frmTraslado" method="post"  >
                            {{csrf_field()}}
                                <div class="ibox-content">
                                     <div class="row">
                                         <div class="col-lg-6">

                                                <div class="form-group">
                                                    <label><strong>Bodega Origen</strong></label>
                                                    <select name="bodega_origen" id="bodega_origen" class="form-control" onchange="carga_combo2(this)">
                                                        <option value="">- Seleccione -</option>
                                                        @foreach($bodegas as $bodega)
                                                        <option value="{{$bodega->id}}" principal="{{$bodega->principal}}">{{$bodega->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                         </div>
                                         <div class="col-lg-6">

                                                <div class="form-group">
                                                    <label><strong>Bodega Destino</strong></label>
                                                    <select name="bodega_destino" id="bodega_destino" class="form-control">

                                                    </select>
                                                </div>
                                         </div>
                                     </div>
                                        <div class="row">

                                                <div class="form-group">
                                                    <label>Motivo del traslado </label>
                                                    <textarea cols="10" rows="2" name="motivo" id="motivo" class="form-control">{{old('obs')}}</textarea>

                                                </div>

                                                <div class="form-group">
                                                    <div class="form-actions pull-right">
                                                        <button type="submit" class="btn btn-primary" id="btnAgregar" name="btnAgregar" ><i class="fa fa-plus"></i> Agregar Productos</button>
                                                        <button type="button" class="btn btn-primary" id="btnAgregar2" name="btnAgregar2" style="visibility: hidden;  " onclick="abre_modal()" ><i class="fa fa-plus"></i> Agregar Productos</button>
                                                    </div>
                                                </div>
                                        </div>
                                </div>
                        </form>
                </div>
            </div>
        </div>
        <!--  detalle ********************    DETALLE *************    DETALLE **************************    DETALLE **************************************** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-list-ul"></i> Detalle</h5>
                        <button type="button" class="btn btn-warning pull-right" name="btnFinalizar" id="btnFinalizar" style="visibility: hidden"><i class="fa fa-check"></i> Finalizar Proceso</button>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="table-responsive">
                                <table id="tabla_detalle" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr><td>ID</td><td>Codigo</td><td>Descripcion</td><td>Mac/Serie</td><td>Cantidad</td></tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  MODAL ***********************  MODAL AGREGAR***********************  MODAL AGREGAR*******************************  MODAL AGREGAR********************* -->
        <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <form name="frmDetalleTraslado" id="frmDetalleTraslado" method="post">
                        {{csrf_field()}}
                        <div class="ibox" id="rv_panel">
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-double-bounce">
                                    <div class="sk-double-bounce1"></div>
                                    <div class="sk-double-bounce2"></div>
                                </div>
                                <div class="modal-body">
                                    <div>

                                        <div id="fdtcodigo_error">
                                        </div>

                                    </div>
                                    <span id="titulo_form"></span>
                                    <input type="hidden" name="traslado_id" id="traslado_id">
                                    <input type="hidden" name="fdbodega_origen" id="fdbodega_origen">
                                    <input type="hidden" name="fdcantidad" id="fdcantidad">
                                    <input type="hidden" id="producto_id" name="producto_id" value="">
                                    <input type="hidden" id="producto_unico" name="producto_unico" value="">

                                    <div class="form-group">
                                        <label>Codigo Producto</label>
                                        <input type="text" name="codigo_producto" id="codigo_producto" class="form-control" onblur="busca_producto(this.value)">
                                    </div>
                                    <div class="form-group" id="div_mac" style="visibility: hidden">
                                        <label>Mac/Serie</label>
                                        <input type="text" name="mac_serie" id="mac_serie" class="form-control" value="0">
                                    </div>
                                    <div class="form-group" id="div_cantidad">
                                        <label>Cantidad</label>
                                        <input type="number" name="cantidad" id="cantidad" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" name="btnAceptar" id="btnAceptar" class="btn btn-primary" >Aceptar</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>





   @endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){

            var keyCode = evt.keyCode;
            if(keyCode === 13)
            {
               // alert(evt.keyCode);
                return false

            }
        }

        function abre_modal() {
            $('#myModal').modal('show');
        }

        function busca_producto(codigo){
            //alert('hola '+codigo);
            $.get("/inventarios/compras_detalle_busca_producto/"+codigo+"", function (response) {

                if (response.nomatch === undefined){
                    //console.log(response.nomatch);
                    $('#fdtcodigo_error').html('<div class="text text-info">'+response.coleccion.tipo_producto.nombre+' '+response.coleccion.marca.nombre+' '+response.coleccion.modelo.nombre+'</div>');
                    $('#producto_id').val(response.coleccion.id)
                    $('#producto_unico').val(response.coleccion.unico)
                        if(response.coleccion.unico===0){
                            $('#mac_serie').val('');
                            $('#cantidad').val('1');
                            $('#div_mac').css('visibility','visible')
                            $('#div_cantidad').css('visibility','hidden')
                            $('#mac_serie').focus();

                        }
                        else{
                            $('#mac_serie').val('0');
                            $('#div_mac').css('visibility','hidden')
                            $('#div_cantidad').css('visibility','visible')
                            $('#cantidad').val('');
                            $('#cantidad').focus();
                        }
                    $('#btnAceptar').attr("disabled", false);
                    return false;
                }
                else {
                    alert(response.nomatch)
                    $('#fdtcodigo_error').html('<div class="text text-danger">'+response.nomatch+'</div>');
                    $('#btnAceptar').attr("disabled", true);
                    return false;
                }

                // $(dis).closest('tr').remove();
            });

        }

        //************************************ submit traslado cabeza

        $('#frmTraslado').on('submit', function (e) {
            $('#btnagregar').attr("disabled", true);
            e.preventDefault(e);
            // alert('submit');

            $.ajax({

                url: '/inventarios/traslado_guardar',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data.traslado_id)
                    //swal("Listo!", "El traslado de Stock ha sido registrado con éxito! ", "success");
                    $('#bodega_origen').attr("disabled", true);
                    $('#bodega_destino').attr("disabled", true);
                    $('#motivo').attr("disabled", true);
                    $('#btnAgregar').css("visibility", "hidden");
                    $('#btnAgregar2').css("visibility", "visible");
                    $('#traslado_id').val(data.traslado_id);
                    $('#fdbodega_origen').val($('#bodega_origen').val());
                    $('#titulo_form').html(' Trasaldo ID: '+data.traslado_id);
                    $('#ftcodigo_error').html('');
                    $('#myModal').modal('show');

                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#ftcodigo_error').html('<div class="text text-danger"><h3> '+data.responseJSON.errors.bodega_destino+',  '+data.responseJSON.errors.motivo+'</h3></div>');
                    $('#btnAceptar').attr("disabled", false);

                }
            })
        })


        //************************************ submit detalle

        $('#frmDetalleTraslado').on('submit', function (e) {
            $('#btnAceptar').attr("disabled", true);
            e.preventDefault(e);
            // alert('submit');
            $.ajax({

                url: '/inventarios/traslado_detalle_guardar',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    if (data.error===false){
                        $('#fdtcodigo_error').html('<div class="text text-danger"><h3> '+data.msg+'</h3></div>');
                        $('#btnAceptar').attr("disabled", false);
                        $("#tabla_detalle").append("<tr><td>"+data.registro.id+"</td><td>"+data.registro.producto.codigo_barra+"</td><td><small>"+data.registro.producto.marca.nombre+" "+data.registro.producto.modelo.nombre+" </td><td><small>"+data.registro.mac_serie+" </td><td><small>"+data.registro.cantidad+" </small></td></tr>")
                        $("#btnFinalizar").css('visibility','visible');
                        $('#btnAceptar').attr("disabled", false);
                        $('#mac_serie').val('');
                        $('#cantidad').val('');
                        $('#codigo_producto').val('');
                        $('#codigo_producto').focus();
                       // swal("Listo!", "El traslado de Stock ha sido registrado con éxito! ", "success");
                    }
                    else{
                        $('#fdtcodigo_error').html('<div class="text text-danger"><h3> '+data.msg+'</h3></div>');
                        $('#btnAceptar').attr("disabled", false);
                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    }
                   // console.log(data.registro.producto.marca.nombre)


                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#fdtcodigo_error').html('<div class="text text-danger"><h3> '+data.responseJSON.errors.codigo_producto+',  '+data.responseJSON.errors.cantidad+' '+data.responseJSON.errors.mac_serie+'</h3></div>');
                    $('#btnAceptar').attr("disabled", false);

                }
            })
        })


        $('#btnFinalizar').on('click', function (e) {
            $('#btnagregar').attr("disabled", true);
            $('#btnFinalizar').attr("disabled", true);
            var traslado_id=$('#traslado_id').val();

            $.ajax({

                url: '/inventarios/traslado_finalizar/'+traslado_id,
                    type: "GET",
                dataType: "JSON",
                //data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    if (data.Success===true){

                        swal("Listo!", "El traslado de Stock ha sido registrado con éxito! ", "success");
                        window.location.href="{{ url('/inventarios/traslados') }}";
                    }
                    else{

                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    }
                    // console.log(data.registro.producto.marca.nombre)

                    $("#btnFinalizar").css('visibility','visible');
                    $('#btnFinalizar').attr("disabled", false);
                    $('#btnAgregar').attr("disabled", false);

                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $("#btnFinalizar").css('visibility','visible');
                    $('#btnFinalizar').attr("disabled", false);
                    $('#btnAgregar').attr("disabled", false);

                }
            })

        })


        function carga_combo2(e) {
           var principal=$('option:selected', e).attr('principal');
          // alert(principal);

            $.ajax({

                url: '/inventarios/traslado_combo/'+principal,
                type: "GET",
                dataType: "JSON",
                //data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data.bodegas2)
                    if (data.Success===true){

                        //swal("Listo!", "la bodega is ok ", "success");
                        $("#bodega_destino").empty();
                        for(var i=0; i<data.bodegas2.length; i++){
                            $("#bodega_destino").append("<option value='"+data.bodegas2[i].id+"'>"+data.bodegas2[i].nombre+"</option>")
                        }
                      //  window.location.href="{{ url('/inventarios/traslados') }}";
                    }
                    else{

                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    }
                    // console.log(data.registro.producto.marca.nombre)


                    $('#btnAgregar').attr("disabled", false);

                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");

                    $('#btnAgregar').attr("disabled", false);

                }
            })
        }
    </script>
@endsection