@extends('layouts.app')
@section('title')
    RMA
@endsection
@section('content')
    <?php
            use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Demo ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Servicio Tecnico</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <button type="button" class="btn btn-primary" id="btnAgregar2" name="btnAgregar2" onclick="abre_modal()" ><i class="fa fa-plus"></i> Nuevo Registro</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-retweet"></i>  Lista de Envios a Servicio Tecnico</h5>
                        <hr>
                        <div class="table-responsive">
                            <table class="footable table table-bordered table-striped data-table toggle-arrow-tiny">
                                <thead>
                                <th>ID</th><th> Numero RMA</th><th>Proveedor </th><th>Producto Descripcion</th><th>Pantente / Serie</th><th>Fecha Salida</th><th>Fecha Regreso</th><th>Estado</th> <th data-hide="all"></th>
                                </thead>
                                <tbody>
                                @foreach($consulta as $registro)
                                <tr>
                                    <?php
                                    $fecha1=Carbon::parse($registro['fecha_salida']);
                                    $fecha1=$fecha1->format('d-m-Y');
                                        if($registro['fecha_llegada'] != ""){
                                        $fecha2=Carbon::parse($registro['fecha_llegada']);
                                        $fecha2=$fecha2->format('d-m-Y');
                                        }else{
                                            $fecha2="--.--.--";
                                        }
                                    ?>
                                    <td>{{$registro['id']}}</td>
                                    <td>{{$registro['num_rma']}}</td>
                                    <td>{{$registro['proveedor']['nombre']}}</td>
                                    <td>{{$registro['producto']['tipo_producto']['nombre']}} {{$registro['producto']['marca']['nombre']}} {{$registro['producto']['modelo']['nombre']}} ({{$registro['producto']['id']}})</td>
                                    <td>{{$registro['mac_serie']}}</td>
                                    <td>{{$fecha1}}</td>
                                    <td>{{$fecha2}}</td>
                                    @if($registro['estado'] ==0)
                                        <td><span class="badge badge-danger">Pendiente</span>
                                        <a href="#" onclick="editar_rma(this)" data-value="{{$registro['id']}}"><i class="fa fa-edit"></i></a>
                                        </td>
                                        @elseif($registro['estado'] ==1)
                                        <td><span class="badge badge-primary">Retornado</span></td>
                                        @else
                                            <td><span class="badge badge-success">No Retornado</span></td>
                                        @endif
                                    <td>
                                        <ul class="notes">
                                            <li>
                                                <div>
                                                    <h3>SALIDA DE EQUIPO:</h3>
                                                    {{$registro['usuario']['nombre']}} Dice: <br>
                                                    {{$registro['obs_salida']}}
                                                </div>
                                            </li>
                                            @if($registro['obs_llegada'] != "")
                                            <li>
                                                <div>
                                                    <h3>REGRESO DE EQUIPO:</h3>
                                                    {{$registro['usuario2']['nombre']}} Dice: <br>
                                                    {{$registro['obs_llegada']}}
                                                </div>
                                            </li>
                                             @endif
                                        </ul>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="ibox-content">


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ********************** MODAL NUEVO ********************************************************** -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <form name="frmRma" id="frmRma" method="post">
                    {{csrf_field()}}
                    <div class="ibox" id="rv_panel">
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-double-bounce">
                                <div class="sk-double-bounce1"></div>
                                <div class="sk-double-bounce2"></div>
                            </div>
                            <div class="modal-title">
                                <h2><i class="fa fa-retweet"></i> Nuevo Registro RMA</h2>
                            </div>
                            <div class="modal-body">
                                <div>

                                    <div id="fdtcodigo_error">
                                    </div>

                                </div>
                                <span id="titulo_form"></span>
                                <input type="hidden" name="traslado_id" id="traslado_id">
                                <input type="hidden" name="fdbodega_origen" id="fdbodega_origen">
                                <input type="hidden" name="fdcantidad" id="fdcantidad">
                                <input type="hidden" id="producto_id" name="producto_id" value="">
                                <input type="hidden" id="producto_unico" name="producto_unico" value="">

                                <div class="form-group">
                                    <label>Codigo Producto</label>
                                    <input type="text" name="codigo_producto" id="codigo_producto" class="form-control" onblur="busca_producto(this.value)">
                                </div>
                                <div class="form-group" id="div_mac" >
                                    <label>Patente / Num Serie</label>
                                    <input type="text" name="mac_serie" id="mac_serie" class="form-control" value="0">
                                </div>
                                <div class="form-group" id="div_proveedor">
                                    <label>Proveedor Destino</label>
                                    <select name="proveedor" id="proveedor" class="form-control">
                                        @foreach($proveedores as $proveedor)
                                        <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label>Numero OT /Guia /Documento</label>
                                    <input type="number" name="numero_rma" id="numero_rma" class="form-control">
                                </div>
                                <div class="form-group" id="div_obs">
                                    <label>Observaciones</label>
                                    <textarea name="observaciones" id="observaciones" cols="10" rows="5" class="form-control"></textarea>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="btnAceptar" id="btnAceptar" class="btn btn-primary" >Aceptar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--  *********** Modal editar -->

    <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <form name="frmRma2" id="frmRma2" method="post">
                    {{csrf_field()}}
                    <div class="ibox" id="rv_panel">
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-double-bounce">
                                <div class="sk-double-bounce1"></div>
                                <div class="sk-double-bounce2"></div>
                            </div>
                            <div class="modal-title">
                                <h2><i class="fa fa-retweet"></i> RMA, Retorno de equipo</h2>
                            </div>
                            <div class="modal-body">
                                <div>

                                    <div id="fdtcodigo_error2">
                                    </div>

                                </div>
                                <span id="titulo_form"></span>
                                <input type="hidden" name="rma_id" id="rma_id">

                                <div class="form-group" id="div_estados">
                                    <label>Estado</label>
                                   <select name="estado" id="estado" class="form-control">
                                       <option value="1">Retornado</option>
                                       <option value="2">No Retornado</option>
                                   </select>

                                </div>

                                <div class="form-group" id="div_obs">
                                    <label>Observaciones</label>
                                    <textarea name="observaciones2" id="observaciones2" cols="10" rows="5" class="form-control"></textarea>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="btnAceptar2" id="btnAceptar2" class="btn btn-primary" >Aceptar</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
  @endsection
@section('mijava')
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });
        document.onkeydown=function(evt){

            var keyCode = evt.keyCode;
            if(keyCode === 13)
            {
                // alert(evt.keyCode);
                return false

            }
        }

        function abre_modal() {

            $('#myModal').modal('show');
            $('#codigo_producto').focus();
        }

        function busca_producto(codigo){
            //alert('hola '+codigo);
            $.get("/inventarios/compras_detalle_busca_producto/"+codigo+"", function (response) {

                if (response.nomatch === undefined){
                    //console.log(response.nomatch);
                    $('#fdtcodigo_error').html('<div class="text text-info">'+response.coleccion.tipo_producto.nombre+' '+response.coleccion.marca.nombre+' '+response.coleccion.modelo.nombre+'</div>');
                    $('#producto_id').val(response.coleccion.id)
                    $('#producto_unico').val(response.coleccion.unico)
                    if(response.coleccion.unico===0){
                        $('#mac_serie').val('');
                        $('#numero_rma').val('1');
                        $('#mac_serie').focus();
                        $('#codigo_producto').css('border-color', 'green');

                    }
                    else{
                        $('#mac_serie').val('0');
                        $('#numero_rma').val('');
                        $('#numero_rma').focus();
                        $('#codigo_producto').css('border-color', 'green');
                    }
                    $('#btnAceptar').attr("disabled", false);
                    return false;
                }
                else {
                    //alert(response.nomatch)
                    $('#fdtcodigo_error').html('<div class="text text-danger">'+response.nomatch+'</div>');
                    $('#codigo_producto').css('border-color', 'red');
                    //$('#codigo_producto').focus();
                    $('#btnAceptar').attr("disabled", true);
                    return false;
                }

                // $(dis).closest('tr').remove();
            });

        }


        $('#frmRma').on('submit', function (e) {
            $('#btnAceptar2').attr("disabled", true);
            e.preventDefault(e);
            // alert('submit');
            $.ajax({

                url: '/inventarios/rma_guardar',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    if (data.error===false){

                        $('#myModal').modal('hide');
                        swal("Listo!", "El RMA se ha creado con éxito! ", "success");
                        window.location.href="{{ url('/inventarios/rma') }}";
                    }
                    else{
                        $('#fdtcodigo_error').html('<div class="text text-danger"><h3> '+data.msg+'</h3></div>');
                        $('#btnAceptar').attr("disabled", false);
                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    }
                    // console.log(data.registro.producto.marca.nombre)
                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#btnAceptar').attr("disabled", false);
                    $('#fdtcodigo_error').html('<div class="text text-danger"><h3> '+data.responseJSON.errors.codigo_producto+',  '+data.responseJSON.errors.numero_rma+' '+data.responseJSON.errors.mac_serie+' '+data.responseJSON.errors.observaciones+'</h3></div>');


                }
            })
        })

        function editar_rma(e){
           // alert($(e).attr('data-value'));
            var rma_id=$(e).attr('data-value');
            $('#rma_id').val(rma_id);
            $('#myModal2').modal('show');
            $('#observaciones2').focus();
        }

        $('#frmRma2').on('submit', function (e) {
            $('#btnAceptar2').attr("disabled", true);
            e.preventDefault(e);
            // alert('submit');
            $.ajax({

                url: '/inventarios/rma_actualizar',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    if (data.error===false){

                        $('#myModal2').modal('hide');
                        swal("Listo!", "El RMA se ha modificado con éxito! ", "success");
                        window.location.href="{{ url('/inventarios/rma') }}";
                    }
                    else{
                        $('#fdtcodigo_error2').html('<div class="text text-danger"><h3> '+data.msg+'</h3></div>');
                        $('#btnAceptar2').attr("disabled", false);
                        swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    }
                    // console.log(data.registro.producto.marca.nombre)
                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#btnAceptar2').attr("disabled", false);
                    $('#fdtcodigo_error2').html('<div class="text text-danger"><h3> '+data.responseJSON.errors.observaciones2+'</h3></div>');

                }
            })
        })

    </script>
@endsection
