@extends('layouts.app')
@section('title')
    Compras
@endsection
@section('content')
    <?php
    session_start();
    use Carbon\Carbon;
    ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Demo ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Existencias</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Listado de Existencias</h5>

                    </div>
                    <div class="ibox-content">


                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>

                                    <th>Código Producto</th>
                                    <th>Descripción Producto</th>
                                    <th>Bodega</th>
                                    <th>Stock</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($consulta != "No hay Registros")
                                    @foreach($consulta as $compra)
                                        <tr>

                                            <td>
                                                <small>{{$compra->producto->codigo_barra}}</small>
                                            </td>
                                            <td>
                                                <small>{{$compra->producto->tipo_producto->nombre}} {{$compra->producto->marca->nombre}} {{$compra->producto->modelo->nombre}}</small>
                                            </td>
                                            <td>
                                                <small>{{$compra->bodega->nombre}}</small>
                                            </td>
                                            <td>
                                                @if($compra->producto->unico==0)
                                                    <a data-toggle="modal" data-target="#myModal" nombre_producto="{{$compra->producto->marca->nombre}} {{$compra->producto->modelo->nombre}}"  data-id="{{$compra->producto->id}}" bodega-id="{{$compra->bodega->id}}" class="open-Modal"> <span class="label label-default"> {{$compra->total}}   <i class="fa fa-list-ul"></i></span></a>
                                                @else
                                                    <a class="rv_movimiento" id="{{$compra->producto->marca->nombre}} {{$compra->producto->modelo->nombre}}" name="ver_movimiento"   href="#"   data-id="{{$compra->producto->id}}" bodega-id="{{$compra->bodega->id}}" bodega-nombre="{{$compra->bodega->nombre}}" existencia="{{$compra->total}}" > <small>{{$compra->total}}</small></a>
                                                @endif
                                            </td>


                                        </tr>
                                    @endforeach
                                 @else
                                    <tr><td colspan="4">{{$consulta}}</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                    <!--  modal -->
                    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">
                                <div class="ibox" id="rv_panel">
                                    <div class="ibox-content">
                                        <div class="sk-spinner sk-spinner-double-bounce">
                                            <div class="sk-double-bounce1"></div>
                                            <div class="sk-double-bounce2"></div>
                                        </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive" id="mac">
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Ajuste Inventario -->
                <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content animated bounceInRight">


                                    <div class="ibox-content">

                                                <div class="text-danger" id="codigo_error">

                                                </div>

                                        <div class="modal-body">
                                            <form name="frmAjuste" id="frmAjuste" method="post">
                                                {{csrf_field()}}
                                                <h3><i class="text-danger fa fa-wrench xl"> Ajuste Manual de Stock </i> </h3>
                                                <span id="fnombre"></span>
                                                <input type="hidden" id="fproducto_id" name="fproducto_id" value="">
                                                <input type="hidden" id="fbodega_id" name="fbodega_id" value="">
                                                <input type="hidden" id="fexistencia" name="fexistencia" value="">

                                                <div class="form-group">
                                                    <label>Nuevo Stock</label>
                                                    <input type="number" name="stock" id="stock" class="form-control" value="{{old('stock')}}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Motivo del ajuste (obs)</label>
                                                    <textarea cols="10" rows="5" name="obs" id="obs" class="form-control">{{old('obs')}}</textarea>

                                                </div>
                                                <div class="pull-right">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button> ||
                                                <input class="btn btn-primary " id="btn_guardar" name="btn_guardar" type="submit" value="Guardar">
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                        </div>
                    </div>
                </div>

                <!--MODAL 3 traslado inventarios entre bodegas -->

                <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content animated bounceInRight">


                            <div class="ibox-content">

                                <div class="text-danger" id="ftcodigo_error">

                                </div>

                                <div class="modal-body">
                                    <form name="frmTraslado" id="frmTraslado" method="post">
                                        {{csrf_field()}}
                                        <h3><i class="text-warning xl fa fa-exchange"> Traslado Entre Bodegas</i></h3>
                                        <span id="ftnombre"></span>
                                        <input type="hidden" id="ftproducto_id" name="ftproducto_id" value="">
                                        <input type="hidden" id="ftbodega_origen" name="ftbodega_origen" value="">
                                        <input type="hidden" id="ftexistencia" name="ftexistencia" value="">

                                        <div class="form-group">
                                            <label>Bodega Destino</label>
                                            <select name="bodega_destino" id="bodega_destino" class="form-control" >
                                                <option value="">-Seleccione Bodega</option>
                                                @foreach($bodegas as $bodega)

                                                    <option value="{{$bodega->id}}">{{$bodega->nombre}}</option>

                                                  @endforeach
                                            </select>

                                        </div>

                                        <div class="form-group">
                                            <label>Cantidad a Trasladar (stock)</label>
                                            <input type="number" name="stock" id="stock" class="form-control" value="{{old('stock')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Motivo del Traslado</label>
                                            <textarea cols="10" rows="5" name="motivo" id="motivo" class="form-control">{{old('obs')}}</textarea>

                                        </div>
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button> ||
                                            <input class="btn btn-primary " id="btn_guardar" name="btn_guardar" type="submit" value="Guardar">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- movimiento historico -->

            <div class="col-sm-4" id="historial" style="visibility: hidden;">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5 id="titulo_movimiento" producto_id="" bodega_id="" bodega_nombre="" existencia="" producto_nombre="" >Movimientos Históricos </h5>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="tab-content">
                            <div id="contact-1" class="tab-pane active">
                                <div class="row m-b-lg">
                                    <div class=" text-center">

                                        <div class="m-b-sm pull-left">
                                            <img alt="image" class="img-container" id="imagen_producto" src=""
                                                 style="width: 130px; padding-top: 0px">
                                        </div>
                                    </div>
                                    <!--<div class="col-lg-3">
                                        <a id="btn_traslado" data-toggle="modal" data-target="#myModal3"  data-id="" bodega-id="" class="open-Modal3 btn btn-primary btn-sm btn-block " title="Traslado"><i class="fa fa-exchange"></i></a>
                                        <hr>-->
                                   <!-- <div class="col-lg-3">
                                        <a id="btn_ajuste" data-toggle="modal" data-target="#myModal2"  data-id="" bodega-id="" class="open-Modal2 btn btn-primary btn-sm btn-block " title="Ajuste Stock"> <i class="fa fa-wrench"></i> </a>
                                    </div>-->
                                </div>
                                <div class="full-height-scroll" style="padding: 1px;">
                                    <div id="client-details">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- hasta aca el movimiento hisorico -->

        </div>
    </div>
@endsection
@section('mijava')
    <script>


        //DATATABLE DEL FRONT
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Stock Productos e Insumos  de Teledata'},
                    {extend: 'pdf', title: 'Stock Productos e Insumos de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });



        // abrir modal para ver mac
        $(document).on("click", ".open-Modal", function () {
            $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
            var producto_id = $(this).data('id');
            var bodega_id=$(this).attr('bodega-id');
            var nombre_producto=$(this).attr('nombre_producto');
            //console.log('bidega: '+bodega);
            var html='hola';


            $.ajax({
                url: '/inventarios/get_mac/'+producto_id+'/'+bodega_id,
                type: "GET",
                dataType: "JSON",
                // data:  new FormData(this),
                processData: false,
                contentType: false,
                //dataType: 'json',
                success: function (data) {
                    //alert('exito '+data);
                    //swal("OK!", "Datos Almacenados exitosamente!", "success");
                    $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                    //console.log(data.mac.length);
                    var rv_html="";
                    var servicio="";

                    for (var i=0; i<data.mac.length; i++){
                        var mac_serie=data.mac[i].mac_serie;
                        var mac_serie_con_formato="";

                            for(var x=0; x<mac_serie.length; x++  )
                            {
                                mac_serie_con_formato=mac_serie_con_formato+ mac_serie[x];
                                if ((x%2!=0)&&(x!=(mac_serie.length-1))){
                                   mac_serie[x]
                                    mac_serie_con_formato=mac_serie_con_formato+':';
                                    // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                               }
                            }

                         if (data.mac[i].bodega.nombre == "CLIENTE"){
                             servicio= " - Obra /Sucursal:  "+data.mac[i].servicio.Codigo;
                         }
                         else{
                             servicio="";
                         }

                        if (i===0)
                            rv_html='  <strong>Patentes / Serie disponibles de  '+nombre_producto+'</strong><ul class="list-group clear-list"><li class="list-group-item fist-item"><span>'+parseInt(i+1)+' </span>)' +
                                ' <a class="" onclick="carga_historia(this)" id="'+data.mac[i].mac_serie+'" mac_serie="'+data.mac[i].mac_serie+'" name="ver_movimiento"   href="#" nombre_producto="'+nombre_producto+'"   data-id="'+data.mac[i].producto_id+'" bodega-id="'+data.mac[i].bodega_id+'" bodega-nombre="'+data.mac[i].bodega.nombre+'" existencia="1" class="open-Modal"> <small>' +mac_serie_con_formato+ ' ' + servicio +'</small></a> </li>';
                        else
                            rv_html=rv_html + '<ul class="list-group clear-list"><li class="list-group-item fist-item"><span>'+parseInt(i+1)+' </span>)' +
                                ' <a class="" onclick="carga_historia(this)" id="'+data.mac[i].mac_serie+'" mac_serie="'+data.mac[i].mac_serie+'" name="ver_movimiento"   href="#"  nombre_producto="'+nombre_producto+'"  data-id="'+data.mac[i].producto_id+'" bodega-id="'+data.mac[i].bodega_id+'" bodega-nombre="'+data.mac[i].bodega.nombre+'" existencia="1" class="open-Modal"> <small>' +mac_serie_con_formato+ ' ' + servicio +'</small></a> </li>';
                    }

                    $('#mac').html(rv_html);
                },
                error: function (data) {
                    //alert('fail '+data);
                    console.log(data)
                    swal("Ups!", "Un problema lógico con base de datos NO se  ha podido cargar las MAC!", "error");
                    $('#rv_panel').children('.ibox-content').toggleClass('sk-loading');
                }

            })

        });

        // Ajuste stock:  cargando los hidden del form  de la modal para ajuste de stock
        $(document).on("click", ".open-Modal2", function () {
            $('#codigo_error').html(''); //limpìo los errores que se hayan mostrado en una ejecucion anterior.
            $('#fproducto_id').val( $('#titulo_movimiento').attr('producto_id'));
            $('#fbodega_id').val($('#titulo_movimiento').attr('bodega_id'));
            $('#fexistencia').val($('#titulo_movimiento').attr('existencia'));
            $('#fnombre').html('<h3>'+ $('#titulo_movimiento').attr('producto_nombre')+ '<br> Stock: <text class="text-success"> ' + $('#titulo_movimiento').attr('existencia') + ' Unidades  </text> en bodega '+$('#titulo_movimiento').attr('bodega_nombre')+'</h3><hr>');
        });

        //SUBMIT Ajustar Stock *****************************************************************

        $('#frmAjuste').on('submit', function (e) {
            $('#btn_guardar').attr("disabled", true);
            e.preventDefault(e);
           // alert('submit');
            $.ajax({

                url: '/ajuste_stock',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    swal("Listo!", "El ajuste de Stock ha sido almacenado con éxito! ", "success");
                    $('#myModal2').modal('hide');
                    window.location.href="{{ url('/inventarios/existencias') }}";


                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#codigo_error').html('<div class="text text-danger"><h1> '+data.responseJSON.errors.stock+' '+data.responseJSON.errors.obs+'</h1></div>');
                    $('#btn_guardar').attr("disabled", false);

                }
            })
        })

        // Traslados: cargando los hidden del form  de la modal para traslados *********************************
        $(document).on("click", ".open-Modal3", function () {
            $('#ftcodigo_error').html(''); //limpìo los errores que se hayan mostrado en una ejecucion anterior.
            $('#ftproducto_id').val( $('#titulo_movimiento').attr('producto_id'));
            $('#ftbodega_origen').val($('#titulo_movimiento').attr('bodega_id'));
            $('#ftexistencia').val($('#titulo_movimiento').attr('existencia'));
            $('#ftnombre').html('<h3>'+ $('#titulo_movimiento').attr('producto_nombre')+ '<br> Stock ' + $('#titulo_movimiento').attr('existencia') + ' Unidades  en bodega '+$('#titulo_movimiento').attr('bodega_nombre')+'</h3><hr>');
        });

        //SUBMIT Traslado Stock *****************************************************************

        $('#frmTraslado').on('submit', function (e) {
            $('#btn_guardar').attr("disabled", true);
            e.preventDefault(e);
            // alert('submit');
            $.ajax({

                url: '/traslado_stock',
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    console.log(data)
                    swal("Listo!", "El traslado de Stock ha sido registrado con éxito! ", "success");
                    $('#myModal3').modal('hide');
                    window.location.href="{{ url('/inventarios/existencias') }}";


                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    $('#ftcodigo_error').html('<div class="text text-danger"><h1> '+data.responseJSON.errors.bodega_destino+' '+data.responseJSON.errors.stock+''+data.responseJSON.errors.motivo+'</h1></div>');
                    $('#btn_guardar').attr("disabled", false);

                }
            })
        })

        //CARGAR HISTORIAL MOVIMIENTOS
        $('.rv_movimiento').click(function () {

            var dis=this;
            var producto_id = $(this).data('id');
            var bodega_id=$(this).attr('bodega-id');
            var bodega_nombre=$(this).attr('bodega-nombre');
            var existencia=$(this).attr('existencia');
            //console.log('hola'+dis.id);
            $('#titulo_movimiento').html(dis.id + ': <strong class="text-success">'+ existencia+'  Unid.</strong> <br><small>'+bodega_nombre+'</small>');
            $('#titulo_movimiento').attr('producto_nombre',dis.id);
            $('#titulo_movimiento').attr('producto_id',producto_id);
            $('#titulo_movimiento').attr('bodega_id',bodega_id);
            $('#titulo_movimiento').attr('bodega_nombre',bodega_nombre);
            $('#titulo_movimiento').attr('existencia',existencia);
            $('#mov_stock').html('holaaa');
            $('#btn_ajuste').attr('disabled',false);
            $('#btn_traslado').attr('disabled',false);


            $.ajax({

                url: '/inventarios/get_movimientos/'+producto_id+'/'+bodega_id,
                type: "GET",
                dataType: "JSON",
                // data:  new FormData(this),
                processData: false,
                contentType: false,
                //dataType: 'json',
                success: function (data) {



                    var rv_html="";
                    for (var i=0; i<data.mac.length; i++){
                        var badge="";
                        if (data.mac[i].e_s === 'e')
                            badge="<span class='badge badge-primary pull-left' title='"+data.mac[i].obs+"'><i class='fa fa-mail-forward'></i> Entrada</span> ";
                        if (data.mac[i].e_s === 's')
                            badge="<span class='badge badge-success pull-left' title='"+data.mac[i].obs+"'><i class='fa fa-mail-reply'></i> Salida</span> ";
                        if (data.mac[i].e_s === 'a')
                            badge="<span class='badge badge-danger pull-left' title='"+data.mac[i].obs+"'><i class='fa fa-wrench'></i> Ajuste</span> ";
                        if (i===0) {
                            rv_html = '<strong>Movimiento Histórico en '+bodega_nombre+'</strong><ul class="list-group-item"><li class="list-group-item fist-item">'+badge+ ' <small> ' +data.mac[i].cantidad+ ' Unid. ' +data.mac[i].tipo_movimiento+ '<span class="pull-right">'+ data.mac[i].fecha.toLocaleString()+  '</small></span></li>';
                        }

                        else {
                            rv_html = rv_html + ' <li class="list-group-item">'+badge+ '  <small>' +
                                ' '  +data.mac[i].cantidad+' Unid. ' +data.mac[i].tipo_movimiento+ '<span class="pull-right ">'+data.mac[i].fecha+' </small></span></li>';
                        }


                    }
                    rv_html=rv_html + '</ul>';

                    $('#client-details').html(rv_html);
                    $('#historial').css("visibility", "visible");

                },
                error: function (data) {
                    //alert('fail '+data);
                    console.log(data)
                    swal("Ups!", "Un problema lógico con base de datos NO se  ha podido cargar las MAC!", "error");
                }

            })
        });

        function carga_historia(e) {
           //alert('hihih');
            var dis=e;
            var rvmac=dis.id;
            var producto_id = $(e).data('id');
            var mac_serie_con_formato="";
            //alert('popo' + producto_id);
            var mac_serie=$(e).attr('mac_serie');
            var bodega_id=$(e).attr('bodega-id');
            var bodega_nombre=$(e).attr('bodega-nombre');
            var existencia=$(e).attr('existencia');
            var nombre_producto=$(e).attr('nombre_producto');

            for(var x=0; x<rvmac.length; x++  )
            {
                mac_serie_con_formato=mac_serie_con_formato+ rvmac[x];
                if ((x%2!=0)&&(x!=(rvmac.length-1))){
                    rvmac[x]
                    mac_serie_con_formato=mac_serie_con_formato+':';
                    // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                }
            }

            //console.log('hola'+dis.id);
            $('#titulo_movimiento').html('<strong class="text-success">'+ nombre_producto+'  </strong> <br>'+mac_serie_con_formato+'<br><small>Ubicacion: '+bodega_nombre+'</small>');
            $('#titulo_movimiento').attr('producto_nombre',dis.id);
            $('#titulo_movimiento').attr('producto_id',producto_id);
            $('#titulo_movimiento').attr('bodega_id',bodega_id);
            $('#titulo_movimiento').attr('bodega_nombre',bodega_nombre);
            $('#titulo_movimiento').attr('existencia',existencia);
            $('#mov_stock').html('holaaa');
            $('#btn_ajuste').attr('disabled',true);
            $('#btn_traslado').attr('disabled',true);


            $.ajax({

                url: '/inventarios/get_movimientos_mac/'+mac_serie+'/'+producto_id,
                type: "GET",
                dataType: "JSON",
                // data:  new FormData(this),
                processData: false,
                contentType: false,
                //dataType: 'json',
                success: function (data) {


                    console.log(data.mac.length);
                    if (data.patch_foto != "NO")
                        $('#imagen_producto').attr("src","../storage/"+data.patch_foto);
                    else
                        $('#imagen_producto').attr("src","");

                    var rv_html="";
                    for (var i=0; i<data.mac.length; i++){
                        var badge="";
                        if (data.mac[i].e_s === 'e')
                            badge="<span class='badge badge-primary pull-left' title='"+data.mac[i].obs+"'><i class='fa fa-mail-forward'></i> Entrada</span> ";
                        if (data.mac[i].e_s === 's')
                            badge="<span class='badge badge-success pull-left'  title='"+data.mac[i].obs+"'><i class='fa fa-mail-reply'></i> Salida</span> ";
                        if (data.mac[i].e_s === 'a')
                            badge="<span class='badge badge-danger pull-left'  title='"+data.mac[i].obs+"'><i class='fa fa-wrench'></i> Ajuste</span> ";
                        if (i===0) {
                            rv_html = '<strong>Movimiento Histórico</strong><ul class="list-group-item"><li class="list-group-item fist-item">'+badge+ ' <small> ' +data.mac[i].bodega_destino.nombre+ ' '+data.mac[i].tipo_movimiento+'</small><span class="pull-right "><small>'+data.mac[i].fecha+'</small></span></li>';
                        }

                        else {
                            rv_html = rv_html + ' <li class="list-group-item">'+badge+ ' <small>'  +data.mac[i].bodega_destino.nombre+ ' '+data.mac[i].tipo_movimiento+'</small><span class="pull-right "><small>'+data.mac[i].fecha+'</small></span></li>';
                        }


                    }
                    rv_html=rv_html + '</ul>';

                    $('#client-details').html(rv_html);
                    $('#historial').css("visibility", "visible");
                    $('#myModal').modal('hide');

                },
                error: function (data) {
                    //alert('fail '+data);
                    console.log(data)
                    swal("Ups!", "Un problema lógico con base de datos NO se  ha podido cargar las MAC!", "error");
                }

            })
        }

    </script>
@endsection
