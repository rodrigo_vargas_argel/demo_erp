@extends('layouts.app')
@section('title')
    Traslados
@endsection
@section('content')
    <?php
    use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Traslados</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/inventarios/traslado_nuevo') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-exchange"></i>  Lista de Traslados Efectuados</h5>

                    </div>
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
                            <div class="table-responsive">
                                 <table class="footable table table-bordered table-striped data-table toggle-arrow-tiny"  >
                                <thead>
                                <tr>
                                    <th data-toggle="true">Id</th>
                                    <th >Fecha</th>
                                    <th >Bodega Origen</th>
                                    <th >Bodega Destino</th>
                                    <th >Responsable</th>
                                    <th>Observaciones y Comentarios</th>
                                    <th>Estado</th>
                                    <th data-hide="all"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($consulta != "No hay registros")
                                @foreach($consulta as $compra)
                                <?php
                                $fecha1=Carbon::parse($compra->fecha);
                                $fecha1=$fecha1->format('d-m-Y');
                                ?>
                                    <tr>
                                        <td>
                                            <small> {{$compra->id}}</small>
                                        </td>
                                        <td>
                                            <small>{{$fecha1}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->bodega_origen->nombre}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->bodega_destino->nombre}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->usuario->nombre}}</small>
                                        </td>
                                        <td>
                                            <small>{{$compra->motivo}}</small>
                                        </td>
                                        <td>
                                            <small>
                                                @if($compra->estado ==1)
                                                    <!--  <a href="{--{ url('/inventarios/compras_editar/'.$compra->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                                 <a class="del" id="{--{$compra->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a> ||-->
                                                    <span class="badge badge-danger">Pendiente</span>
                                                    <a href="{{ url('/inventarios/traslado_continuar/'.$compra->id) }}" ><i class="fa fa-fast-forward" title="Continuar con Traslado"></i></a>
                                                @else

                                                        <span class="badge badge-success"><i class="fa fa-check"></i> Realizado</span>
                                                    @endif


                                            </small>
                                        </td>

                                        <td>

                                            <table class="table able-striped table-bordered table-hover table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <td>&nbsp;</td> <td>&nbsp;</td>
                                                                <td><small class="text-success"> Producto</small></td>
                                                                <td><small  class="text-success">Cantidad / Unidades</small></td>
                                                                <td><small  class="text-success">Mac / Serie</small></td>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($compra->detalle as $detalle)
                                            <tr>
                                                <td >&nbsp;</td> <td>&nbsp;</td><td><small class="text-muted">{{$detalle->producto->marca->nombre}} {{$detalle->producto->modelo->nombre}} </small></td>
                                                <td><small class="text-muted">{{$detalle->cantidad}} Unidades</small></td><td><small class="text-muted">{{$detalle->mac_serie}}</small></td>

                                            </tr>
                                                            @endforeach
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>
                                @endforeach
                                    @else
                                    <tr><td colspan="5">{{$consulta}}</td></tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

  @endsection
    @section('mijava')
        <script>
            $(document).ready(function() {

                $('.footable').footable();
                $('.footable2').footable();

            });
        </script>
    @endsection