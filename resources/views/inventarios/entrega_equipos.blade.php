@extends('layouts.app')
@section('title')
    Entrega Equipos
@endsection
@section('content')
    <?php
    use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> <i class="fa fa-forward text-info"></i> Entrega de Maquinaria</h2>
            <div class="hr-line-dashed"  style="border: 1px solid;  border-color: #5cb85c;"></div>
            <!--<ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Entrega de Equipos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">

            </div>
        </div>-->
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="col-lg-6">
                        <form class="form-horizontal" name="frmEntrega" id="frmEntrega" method="post">
                            {{csrf_field()}}

                            <input type="hidden" name="equipos_para_entrega" id="equipos_para_entrega" value="0">
                            <div class="form-group"><label class="col-sm-2 control-label">Origen</label>

                                <div class="col-sm-10">
                                    <select class="col-sm-10 form-control" name="bodega_origen" id="bodega_origen" onchange="carga_stock(this)">
                                        @foreach($bodega_origen as $bodega1)
                                            <option value="{{$bodega1->id}}">{{$bodega1->nombre}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Destino</label>
                                <div class="col-sm-10">
                                    <select class="col-sm-10 form-control" name="bodega_destino" id="bodega_destino" onchange="muestra_cliente(this)">
                                       <option value="">-Seleccione-</option>
                                        @foreach($bodega_destino as $bodega2)
                                            <option value="{{$bodega2->id}}">{{$bodega2->nombre}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                            <div class="cliente_servicio" id="cliente_servicio" style="display: none;">
                                <div class="form-group"><label class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <select class="selectpicker form-control col-sm-10 form-control" style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="carga_servivio(this)">
                                            <option value="">Seleccione Cliente</option>
                                            @foreach($clientes as $cliente)
                                                <option value="{{$cliente->id}}" rut="{{$cliente->rut}}">{{$cliente->nombre}}-{{$cliente->rut}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-10 form-control" name="servicio" id="servicio">
                                          <option value="">-Seleccione Obra o sucursal-</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="destaciones" id="destaciones" style="display: none;">
                                <div class="form-group"><label class="col-sm-2 control-label">Estación Teledata</label>

                                    <div class="col-sm-10">
                                        <select class="selectpicker form-control col-sm-10 form-control" style="border-color: green;" name="estacion" id="estacion" data-live-search="true" >
                                            <option value="">Seleccione Estación Teledata</option>
                                            @foreach($estaciones as $estacion)
                                                <option value="{{$estacion->id}}" >{{$estacion->nombre}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="tabla_entrega">
                                <div class="hr-line-dashed"  style="border: 1px solid;  border-color: #5cb85c;"></div>

                                <h3><label class="form-actions text-primary">Maquinas Disponibles para Entrega en Arriendo: </label> <label id="lbl_num_equipos">0</label></h3>
                                @if($existencias != 'No hay Productos en esta bodega')

                                    <div class="col-lg-12 table-responsive">

                                        <table class=" table table-stripped  table-bordered " >
                                            <thead>
                                                <tr>
                                                    <th data-toggle="true">Elije</th>
                                                    <th>Codigo</th>
                                                    <th>MAC-Patente /serie</th>
                                                    <th>Descripción</th>
                                                    <!--<th data-hide="all"></th>-->
                                                </tr>
                                            </thead>
                                            <tbody id="tabla_detalle">
                                                <?php
                                                $total_detalle=0;
                                                $contador=0;
                                                ?>

                                                @foreach($existencias as $detalle)
                                                    <?php
                                                    $mac_serie=  $detalle->mac_serie;
                                                    $mac_serie_con_formato="";
                                                    for($i=0;$i<strlen($mac_serie);$i++){
                                                        $mac_serie_con_formato=$mac_serie_con_formato."".$mac_serie[$i];
                                                        if (($i%2!=0)&&($i!=(strlen($mac_serie)-1))){
                                                            //$detalle->mac_serie[$i];
                                                            $mac_serie_con_formato=$mac_serie_con_formato.':';
                                                            // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                                                        }
                                                    }

                                                    ?>
                                                    <tr>
                                                        <!--
                                                        @if($detalle->producto->unico == 0)
                                                            <td  > <span  class="badge badge-primary">{{$detalle->cantidad}}</span></td>
                                                        @else
                                                            <td ><span  class="badge badge-success"> {{$detalle->cantidad}}</span></td>
                                                        @endif
                                                         -->
                                                        <td ><input type="checkbox" class="js-switch_3" name="check[]" id="check{{$detalle->producto_id}}-{{$detalle->mac_serie}}" value="{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" mac_serie="{{$detalle->mac_serie}}" producto_id="{{$detalle->producto_id}}" cantidad="{{$detalle->cantidad}}" onchange="check_equipo(this)">
                                                            @if ($detalle->mac_serie == 0)
                                                                <div id="div_cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" style="visibility: hidden">
                                                                    <select class="form-control" name="cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" id="cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}"></select>
                                                                </div>
                                                            @endif
                                                        </td>
                                                            <td>{{$detalle->producto->codigo_barra}}</td>
                                                        <td><small>{{$mac_serie_con_formato}}</small></td>
                                                        <td> <small>{{@$detalle->producto->tipo_producto->nombre}} {{@$detalle->producto->marca->nombre}} {{@$detalle->producto->modelo->nombre}}</small></td>

                                                    </tr>
                                                <?php $contador=$contador+1; ?>
                                                @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <ul class="pagination pull-right"></ul>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <div class="form-group">
                                            <textarea class="form-control" name="observaciones" id="observaciones" placeholder="Observaciones: Escribe algo aqui..."  rows="5"></textarea>
                                        </div>
                                        <div class="hr-line-dashed"  style="border: 1px solid;  border-color: #5cb85c;"></div>
                                        <div class="form-group">
                                            <button class="ladda-button btn btn-primary" data-style="expand-right" name="btn_entregar" id="btn_entregar" >Entregar</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 @endsection
@section('mijava')
    <script>

        function check_equipo(e){
            var value=$(e).val();
            var mac_serie=$(e).attr('mac_serie');
            var cantidad=$(e).attr('cantidad');
            //alert(cantidad);
            var valor;
            var i;
            valor=$('#equipos_para_entrega').val();

            if (e.checked == true) {
                //alert(mac_serie);
                //alert(value);
                valor=parseInt(valor)+1;
                $('#equipos_para_entrega').val(valor);
                $('#lbl_num_equipos').html(valor);
                if (mac_serie==='0'){
                    $('#div_cantidad'+value+'').css('visibility','visible');
                    for(i=1;i<=cantidad;i++){
                        $('#cantidad'+value+'').append("<option value='"+i+"'>"+i+"</option>");
                    }

                }
                else{
                console.log(mac_serie);
                }

            }
            else{
               // alert('deseleccionado');
                valor=parseInt(valor)-1;
                $('#equipos_para_entrega').val(valor);
                $('#lbl_num_equipos').html(valor);

                if (mac_serie==='0'){
                    $('#cantidad'+value+'').empty();
                    $('#div_cantidad'+value+'').css('visibility','hidden');

                }
            }
        }
        //var elem_3 = document.querySelector('.js-switch_3');
        //var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

        elems.forEach(function(html) {
            var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
        });

        $(document).ready(function (){

            var l = $( '.ladda-button' ).ladda();

           /* l.click(function(){

                // Start loading



            });*/

        });

        function carga_stock(e) {
            var principal=$('option:selected', e).attr('value');
            // alert(principal);


            $.ajax({

                url: '/inventarios/carga_stock/'+principal,
                type: "GET",
                dataType: "JSON",
                //data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    //console.log(data.msg)
                    if (data.Success===true){
                        $('#tabla_detalle').empty();
                        var i;
                        var estilo="visibility: hidden";
                        for (i=0;i<=data.coleccion.length-1; i++) {
                            var mac_serie = data.coleccion[i].mac_serie;
                            var mac_serie_con_formato = "";

                            for (var x = 0; x < mac_serie.length; x++) {
                                mac_serie_con_formato = mac_serie_con_formato + mac_serie[x];
                                if ((x % 2 != 0) && (x != (mac_serie.length - 1))) {
                                    mac_serie[x]
                                    mac_serie_con_formato = mac_serie_con_formato + ':';
                                    // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                                }
                            }

                            $("#tabla_detalle").append("<tr><div class='form-group'><td><div id='div_cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' style='"+estilo+"'><select class='form-control' name='cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' id='cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "'></select></div>")

                            $("#tabla_detalle").append("<input type='checkbox' class='js-switch_3' name='check[]' id='check" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "'   value='" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' cantidad='" + data.coleccion[i].cantidad + "' mac_serie='" + data.coleccion[i].mac_serie + "'  onchange='check_equipo(this)'>");

                            $("#tabla_detalle").append("</td></div>" +
                                "<td>" + data.coleccion[i].producto.codigo_barra + "</td>" +
                                "<td>" + mac_serie_con_formato + "</td><td><small>" + data.coleccion[i].producto.tipo_producto.nombre + " " + data.coleccion[i].producto.marca.nombre + " " + data.coleccion[i].producto.modelo.nombre + "</small></td></tr>")

                        }

                        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

                        elems.forEach(function(html) {
                            var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
                        });

                        $('#btn_entregar').attr("disabled", false);


                    }
                    else{
                        $('#tabla_detalle').empty();
                        $('#tabla_detalle').html("<h5 class='text-danger'>"+data.coleccion+"</h5>");
                        $('#btn_entregar').attr("disabled", true);

                    }


                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                    swal(7
                    )
                }
            })
            var valor;
            valor=parseInt(0);
            $('#equipos_para_entrega').val(valor);
            $('#lbl_num_equipos').html(valor);
        }

        function muestra_cliente(r){
            //alert(r.text);
            var texto;
            texto=$( "#bodega_destino option:selected" ).text();
            //alert(texto);
            if (texto==="CLIENTE"){
               $("#cliente_servicio").css('display','block');
                $("#destaciones").css('display','none');
            }
            else{
                $("#cliente_servicio").css('display','none');
                $("#destaciones").css('display','block');
               // console.log('jkasjhd');
            }
        }

        function carga_servivio(e) {
            var id_;
            var rut;
            id_=e.value;
            rut=$('option:selected', e).attr('rut');

           // alert('cambio cliente'+id_  + "RUT: "+rut)
            $.ajax({

                url: '/inventarios/cargar_servicios/'+rut,
                //type: $(dis).attr("method"),
                dataType: "JSON",
                //data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    console.log('servicios: '+data.servicios.length);
                    $("#servicio").empty();
                    for(var i=0; i<data.servicios.length; i++){
                       // console.log('servicios2: '+data.servicios);
                        $("#servicio").append("<option value='"+data.servicios[i].Id+"'>"+data.servicios[i].Codigo+" "+data.servicios[i].mantenedor_servicios.servicio +" "+data.servicios[i].Descripcion+" "+data.servicios[i].Conexion+"</option>")
                    }

                },
                error: function (data) {

                    swal("ERROR", "NO seleccionaste ningún cliente", "error");


                }
            })
        }


        $('#frmEntrega').on('submit', function (e) {
            //$('#btn_entregar').attr("disabled", true);
            var l = $( '.ladda-button' ).ladda();
            e.preventDefault(e);
            l.ladda( 'start' );
                    $.ajax({

                        url: '/inventarios/entrega_equipos_guardar',
                        type: "POST",
                        dataType: "JSON",
                        data:  new FormData(this),
                        processData: false,
                        contentType: false,

                        success: function (data) {
                          //  console.log(data)
                            if (data.Success===true){

                                swal("Listo!", data.msg, "success");
                                window.location.href="{{ url('/inventarios/entrega_equipo') }}";
                            }
                            else{

                                swal("Ups!", "Hubo problema lógico con base de datos!"+data.msg , "error");
                            }
                            // console.log(data.registro.producto.marca.nombre)


                        },
                        error: function (data) {
                            var msg="";
                            //   console.log(data.responseJSON.errors.key);
                            var length=0;
                            Object.keys(data.responseJSON.errors).forEach(function(key) {
                                //var myJSON = JSON.stringify(data.responseJSON.errors);
                                var keys = Object.keys(data.responseJSON.errors);
                                var campo=keys[length];
                                $('#'+campo).css('border-color','#ff0000');
                                $('#label_'+campo).html("<label class='text-danger'>"+data.responseJSON.errors[key]+"</label>");
                                console.log(keys[length]);
                                // alert(myJSON + '-'+keys[0]);
                                //  console.log(data.responseJSON.errors);
                                msg=msg+ data.responseJSON.errors[key];
                                //$('#mac_serie'+length).css('border-color','#909');
                                //alert($('#mac_serie'+key).val());
                                length++;
                            });
                            //alert(length);


                            var i;
                            // alert(data.responseJSON);
                            //  for (i=0;i<=data.message.errors.length;i++)
                            swal("Cometiste un Error!", msg, "error");
                            l.ladda('stop');

                        }
                    })
           /* setTimeout(function(){
                l.ladda('stop');
            },12000)*/
            // alert('submit');

        })



    </script>
 @endsection