@extends('layouts.app')
@section('title')
    Stock Inicial
@endsection
@section('content')
    <?php
    use Carbon\Carbon;
    //$carbon = new \Carbon\Carbon();
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Demo ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Inventarios</a>
                </li>
                <li class="active">
                    <strong>Stock Inicial</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">

            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="col-lg-6">
                        <form class="form-horizontal" name="frmInicial" id="frmInicial" method="post">
                            {{csrf_field()}}
                            <div class="ibox-content">

                                <div class="row">

                                    <div class="form-group">
                                        <label class="form-actions">Seleccione Bodega</label>
                                        <select class="form-control" name="bodega" id="bodega" onchange="carga_inventario(this)">
                                            @foreach($bodegas as $bodega)
                                                <option value="{{$bodega->id}}">{{$bodega->nombre}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="form-group input-group">
                                        <label class="form-actions" >Ingrese Codigo Producto <span id="mensaje" class="text-danger"></span></label>
                                        <select class="selectpicker form-control" name="codigo_producto" id="codigo_producto" data-live-search="true" onchange="busca_producto(this.value)" >
                                            <option value="">-Digite y Seleccione-</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->codigo_barra}}" descripcion="{{$producto->descripcion}}">{{$producto->codigo_barra}} - {{$producto->marca->nombre}}- {{$producto->modelo->nombre}}</option>
                                            @endforeach
                                        </select>

                                        <!--<input type="text" name="codigo_producto" id="codigo_producto" class="form-control" value="{{old('codigo')}}" onblur="busca_producto(this.value)">-->
                                        <span class="input-group-btn">
                                                <a type="button" class="btn btn-primary"  href="{{ url('/mantenedores/productos_nuevo') }}" onclick="window.open(this.href, 'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;">
                                                <i class="fa fa-plus"></i>
                                                </a>
                                            </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-actions">Ingrese Cantidad</label>
                                        <input type="number" name="cantidad" id="cantidad" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-actions">Estado</label>
                                        <input type="hidden" name="estado" id="estado" value="1">
                                        <!--<select class="form-control" name="estado" id="estado" >
                                            <option value="1">Reserva Nuevo</option>
                                            <option value="2">Reserva Usado</option>
                                            <option value="3">En Uso</option>-->

                                        </select>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-lg-6 pull-left" >
                                        <span id="codigo_error"></span>
                                        <input type="hidden" id="id_producto" name="id_producto" value="">
                                        <input type="hidden" id="producto_unico" name="producto_unico" value="">
                                    </div>
                                    <div class="form-group">
                                        <button  class="btn btn-primary pull-right" id="btn_agregar" name="btn_agregar" type="submit"  >
                                            <i class="fa fa-shopping-cart"></i>  Agregar</button>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>

        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-list"></i>  Detalle de la Carga <small class="m-l-sm"></small></h5>

                    </div>
                    <div class="ibox" id="rv_panel2">
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-double-bounce">
                                <div class="sk-double-bounce1"></div>
                                <div class="sk-double-bounce2"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 table-responsive">

                                    <table class=" table table-stripped  table-bordered " >
                                        <thead>
                                        <tr>

                                            <th data-toggle="true">Cantidad</th>
                                            <th>Codigo</th>
                                            <th>Descripción</th>
                                            <th>MAC-Patente-Serie</th>
                                            <th >Estado</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabla_detalle">
                                        <?php
                                        $total_detalle=0;
                                        ?>
                                        @if($detalles != 'No hay Registros')
                                            @foreach($detalles as $detalle)
                                                <?php
                                                $mac_serie=  $detalle->mac_serie;
                                                $mac_serie_con_formato="";
                                                        for($i=0;$i<strlen($mac_serie);$i++){
                                                                $mac_serie_con_formato=$mac_serie_con_formato."".$mac_serie[$i];
                                                                if (($i%2!=0)&&($i!=(strlen($mac_serie)-1))){
                                                                    //$detalle->mac_serie[$i];
                                                                    $mac_serie_con_formato=$mac_serie_con_formato.':';
                                                                   // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                                                                  }
                                                        }

                                                        ?>
                                                <tr>


                                                    @if($detalle->producto->unico == 0)
                                                        <td  > <span  class="badge badge-primary">{{$detalle->cantidad}}</span></td>
                                                    @else
                                                        <td ><span  class="badge badge-success"> {{$detalle->cantidad}}</span></td>
                                                    @endif

                                                    <td>{{$detalle->producto->codigo_barra}}</td>
                                                    <td> <small>{{@$detalle->producto->tipo_producto->nombre}} {{@$detalle->producto->marca->nombre}} {{@$detalle->producto->modelo->nombre}}</small></td>
                                                    <td><small>{{$mac_serie_con_formato}}</small></td>
                                                        <td><small>{{@$detalle->estadoo->nombre}}</small></td>

                                                </tr>

                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- *****************************HASTA ACA EL DETALLE **********************************************-->
        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Producto Múltiple</h4>
                        <small class="font-bold">Agrega las Patente o Nro de serie de cada producto.</small>
                    </div>
                    <form id="formMac" role="form" class="form-horizontal" method="post" >
                        {{csrf_field()}}
                        <div class="modal-body">

                            <div id="bodymac" class="">

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn_cerrar_modal" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            <button type="submit" name="btn_mac" id="btn_mac" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" value="guardar"><i class="fa fa-save"></i> Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("mijava")
    <script>
        $('#codigo_producto').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                $('#cantidad').focus();
                return false;
            }
        });

        function valida_enter(e){

            if (e.keyCode === 13) {
                var form = e.target.form;
                var index = Array.prototype.indexOf.call(form, e.target);
                form.elements[index + 1].focus();
                e.preventDefault();

                return false;
            }
        }


        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

        function busca_producto(codigo){

            var id_bodega=$('#bodega').val();
            //alert('hola '+id_bodega);
            $.get("/inventarios/busca_producto/"+codigo+"/"+id_bodega+"", function (response) {

                if (response.nomatch === undefined){
                    //console.log(response.nomatch);
                    $('#mensaje').html('<div class="text text-success">'+response.coleccion.tipo_producto.nombre+' '+response.coleccion.marca.nombre+' '+response.coleccion.modelo.nombre+'</div>');
                    $('#id_producto').val(response.coleccion.id)
                    $('#producto_unico').val(response.coleccion.unico)
                    $('#btn_agregar').attr("disabled", false)

                    if(response.mensaje != 0){
                        $('#mensaje').html('<div class="text text-danger">['+response.coleccion.tipo_producto.nombre+' '+response.coleccion.marca.nombre+' '+response.coleccion.modelo.nombre+'] OJO, ya existen '+response.mensaje+' Unidades en esta Bodega </div>');
                    }
                    else
                        return false;
                }
                else {
                    //alert(response.nomatch)
                    $('#mensaje').html('<div class="text text-danger"><strong>'+response.nomatch+'</strong></div>');
                    $('#btn_agregar').attr("disabled", true)

                    return false;
                }

                // $(dis).closest('tr').remove();
            });

        }
        $('#frmInicial').on('submit', function (e) {
            e.preventDefault(e);
            //alert('envio');
            if ($('#codigo_producto').val() === "") {
                swal("COMETISTE UN ERROR", "Debes digitar un codigo de producto válido ", "error");
            }
            else {


                if (($('#cantidad').val() === '') ||($('#cantidad').val() < 1))
                    swal("COMETISTE UN ERROR", "NO se puede agregar este ítem porque la cantidad es incorrecta! ", "error");
                else {
                    if ($('#producto_unico').val() === '0') {
                        // alert('Prodcuto con MAC'+ $('#cantidad').val())
                        var html, i;
                        for (i = 0; i < $('#cantidad').val(); i++) {
                            //alert(i);
                            if (i === 0) {
                                html = '<div class="form-group">';
                                html = html + '<div class="col-lg-2" id="label_mac_serie' + i + '">MAC-PATENTE-SERIE ' + (parseInt(i) ) + '</div><div class="col-lg-10"> <input type="text" name="mac_serie' + i + '" id="mac_serie' + i + '" value="" onkeypress="return  valida_enter(event)" class="form-control" required onblur="busca_repetido(this)" ></div>';
                                html = html + '</div>';
                            }
                            else {
                                html = html + '<div class="form-group">';
                                html = html + '<div class="col-lg-2" id="label_mac_serie' + i + '">MAC-PATENTE-SERIE ' + (parseInt(i) ) + '</div><div class="col-lg-10"> <input type="text" name="mac_serie' + i + '"  id="mac_serie' + i + '" value="" onkeypress="return  valida_enter(event)" class="form-control" required onblur="busca_repetido(this)" ></div>';
                                html = html + '</div>';
                            }
                        }

                        html = html + '<input type="hidden" name="producto_id" id="producto_id" value="' + $('#id_producto').val() + '"   >';
                        html = html + '<input type="hidden" name="bodega_id" id="bodega_id" value="' + $('#bodega').val() + '"   >';
                        html = html + '<input type="hidden" name="cantidad2" id="cantidad2" value="' + $('#cantidad').val() + '"   >';
                        html = html + '<input type="hidden" name="estado2" id="estado2" value="' + $('#estado').val() + '"   >';


                        $('#bodymac').html(html);
                        $('#myModal2').modal('show');

                        $('#btn_agregar').attr("disabled", true);
                        $('#codigo_producto').val('');
                        $('#cantidad').val('');
                        $('#btn_mac').attr("disabled", false);


                        $('#formMac').validate({
                            rules: {}
                        });
                    }
                    else {

                        $('#btn_agregar').attr("disabled", true);
                        $.ajax({

                            url: '/inventarios/stock_inicial/guardar',
                            type: "POST",
                            dataType: "JSON",
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            //dataType: 'json',
                            success: function (data) {

                                $("#tabla_detalle").append("<tr><td>" + data.coleccion.cantidad + "</td><td>" + data.coleccion.producto.codigo_barra + "</td><td><small>" + data.coleccion.producto.tipo_producto.nombre + " " + data.coleccion.producto.marca.nombre + " " + data.coleccion.producto.modelo.nombre + "</small></td>" +
                                     "<td> <a class='del'  id=" + data.coleccion.id + " href='#' onclick='delete_item(this)' ><i class='text-danger fa fa-trash' title='Eliminar Registro'></i></a> </td></td></tr>")


                                $('#btn_agregar').attr("disabled", true);
                                $('#codigo_producto').val('');
                                $('#cantidad').val('');

                                $('#btn_agregar').attr("disabled", false);
                            },
                            error: function (data) {
                                console.log(data)
                                $('#codigo_error').html('<div class="text text-danger"> ' + data.responseJSON.errors.codigo_producto + ' ' + data.responseJSON.errors.cantidad +  '</div>');
                                $('#btn_agregar').attr("disabled", false);

                                //swal("ERROR", data.responseJSON.errors.codigo, "error");
                            }

                        })
                    }
                } //else valida codigo
            } //else valida cantidad
        })


        function delete_item(dis)
        {

            //alert(nuevo_total);
            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {

                    if (isConfirm) {
                        /*console.log('holaa: '+ (dis.name)); */
                        //$.get("/compras_detalle_eliminar/"+dis.id+"", function  (response) {
                        $.get("/stock_inicial_eliminar/"+dis.id+"", function  (response) {
                                exito=response.Success;
                            });



                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });


        }


        $('#formMac').on('submit', function (e) {
            e.preventDefault(e);
            $('#btn_mac').attr("disabled", true);

            $.ajax({

                url: '/inventarios/stock_inicial_mac_guardar',
                type: "POST",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                //dataType: 'json',
                success: function (data) {
                    //console.log(data.coleccion)
                    $('#tabla_detalle').empty()
                            var i;
                    for (i=0;i<=data.coleccion.length-1; i++)
                    {
                         console.log('elo: '+ data.coleccion[i].estadoo.nombre);
                        var mac_serie=data.coleccion[i].mac_serie;
                        var mac_serie_con_formato="";

                        for(var x=0; x<mac_serie.length; x++  )
                        {
                            mac_serie_con_formato=mac_serie_con_formato+ mac_serie[x];
                            if ((x%2!=0)&&(x!=(mac_serie.length-1))){
                                mac_serie[x]
                                mac_serie_con_formato=mac_serie_con_formato+':';
                                // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                            }
                        }

                    $("#tabla_detalle").append("<tr><td>" + data.coleccion[i].cantidad + "</td><td>" + data.coleccion[i].producto.codigo_barra + "</td><td><small>" + data.coleccion[i].producto.tipo_producto.nombre + " " + data.coleccion[i].producto.marca.nombre + " " + data.coleccion[i].producto.modelo.nombre + "</small></td>" +
                        "<td>"+mac_serie_con_formato +"</td><td>"+data.coleccion[i].estadoo.nombre+"</td></tr>")
                    $('#btn_cerrar_modal').attr("disabled", true);
                        $('#myModal2').modal('hide');
                    }
                },
                error: function (data) {
                    var msg="";
                 //   console.log(data.responseJSON.errors.key);
                    var length=0;
                    Object.keys(data.responseJSON.errors).forEach(function(key) {
                        //var myJSON = JSON.stringify(data.responseJSON.errors);
                        var keys = Object.keys(data.responseJSON.errors);
                        var campo=keys[length];
                        $('#'+campo).css('border-color','#ff0000');
                        $('#label_'+campo).html("<label class='text-danger'>"+data.responseJSON.errors[key]+"</label>");
                        console.log(keys[length]);
                       // alert(myJSON + '-'+keys[0]);
                      //  console.log(data.responseJSON.errors);
                        msg=msg+ data.responseJSON.errors[key];
                        //$('#mac_serie'+length).css('border-color','#909');
                        //alert($('#mac_serie'+key).val());
                        length++;
                    });
                    //alert(length);


                    var i;
                   // alert(data.responseJSON);
                  //  for (i=0;i<=data.message.errors.length;i++)
                   swal("Cometiste un Error!", msg, "error");


                }
            })
        })


        function carga_inventario(e) {
            var principal=$('option:selected', e).attr('value');
          //  alert(principal);


            $.ajax({

                url: '/inventarios/carga_stock/'+principal,
                type: "GET",
                dataType: "JSON",
                //data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                    //console.log(data.msg)
                    if (data.Success===true){
                        $('#tabla_detalle').empty()
                        var i;
                        for (i=0;i<=data.coleccion.length-1; i++)
                        {
                            var mac_serie=data.coleccion[i].mac_serie;
                            var mac_serie_con_formato="";

                            for(var x=0; x<mac_serie.length; x++  )
                            {
                                mac_serie_con_formato=mac_serie_con_formato+ mac_serie[x];
                                if ((x%2!=0)&&(x!=(mac_serie.length-1))){
                                    mac_serie[x]
                                    mac_serie_con_formato=mac_serie_con_formato+':';
                                    // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                                }
                            }

                            $("#tabla_detalle").append("<tr><td>" + data.coleccion[i].cantidad + "</td><td>" + data.coleccion[i].producto.codigo_barra + "</td><td><small>" + data.coleccion[i].producto.tipo_producto.nombre + " " + data.coleccion[i].producto.marca.nombre + " " + data.coleccion[i].producto.modelo.nombre + "</small></td>" +
                                "<td>"+mac_serie_con_formato +"</td></tr>")

                        }
                        $('#codigo_producto').val('');
                        $('#cantidad').val('');
                        $('#mensaje').html('');

                    }
                    else{

                        $('#tabla_detalle').empty()
                        $("#tabla_detalle").append("<tr><td colspan='4'><h3 class='text-danger' >" + data.coleccion+ "</h3></td>")
                        $('#codigo_producto').val('');
                        $('#cantidad').val('');
                        $('#mensaje').html('');

                    }


                },
                error: function (data) {
                    console.log(data)
                    swal("Ups!", "Hubo un problema en la Base de Datos", "error");
                    $('#codigo_producto').val('');
                    $('#cantidad').val('');
                    $('#mensaje').html('');

                }
            })
        }
        function busca_repetido(r) {
            $("#btn_mac").attr("disabled", false);
            $('#'+r.id).css('border-color','#ffffff');
           var i;
           var cant=$("#cantidad2").val();
           if($('#'+r.id).val() !="")
           {
           // alert(r);
            //alert(cant);
                for(i=0; i<cant;i++){
                    if (r.value===$('#mac_serie'+i).val()){
                        if (r.name !=$('#mac_serie'+i).attr('name'))
                        {
                        //$('#mac_serie'+i).css('border-color','#ff0000');
                        $('#'+r.id).css('border-color','#ff0000');
                        $("#btn_mac").attr("disabled", true);
                        swal("Cometiste un Error!", 'La MAC-SEIRE ingresada está repetida', "error");
                        }
                        else{
                          //  $("#btn_mac").attr("disabled", false);
                          //  $('#'+r.id).css('border-color','#ffffff');
                        }
                    }
                }
           }

        }
    </script>


    @endsection