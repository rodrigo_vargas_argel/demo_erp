

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
<link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
{{-- <link rel="stylesheet" href="{!! asset('css/app.css') !!}" /> --}}
<link href="{!! asset('css/animate.css') !!}" rel="stylesheet">
<link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
{{-- <link rel="stylesheet" href="{!! asset('css/app.css') !!}" /> --}}
<style>
@import url(http://fonts.googleapis.com/css?family=Open+Sans);

html,body{
color:white;
font-family:'Opens Sans',helvetica;
height:100%;
width:100%;
margin: 0px;
}

.portada{
background: url({!! asset('img/gears.jpg') !!}) no-repeat fixed center;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
height: 100%;
width: 100% ;
text-align: center;
opacity: 1;

}
.text{
margin: 30px 0px 30px 0px;
padding: 10px;
background: rgba(0,0,0,0.5);
display: inline-block;
}
</style>
<body class="text-center justify-content-center">
<div class="portada text-center justify-content-center">
    <div class="loginscreen animated fadeInRight text-center justify-content-center" >
        <div class="row text-center justify-content-center">
            <div class=" text-center col-lg-6 col-lg-offset-2 justify-content-center" style="opacity: 0.9; padding-top: 100px;">
                <div class="ibox float-e-margins text-center justify-content-center">
                    <div class="ibox-title">
                        <span><img  src="{!! asset('img/logo_blanco.png')  !!}"> <h3 style="color: #0a0a0a;"><strong>DEMO ERP</strong> </h3></span>
                    </div>
                    <div class= "ibox-content text-center justify-content-center"  >
                       <!-- <div class="text-center loginscreen animated fadeInDown" >
                            <div class="row justify-content-center">
                                <div class="col-lg-10" style="padding-top: 100px;">-->
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label style="color: #0a0a0a;" for="usuario" class="col-md-4 col-form-label text-md-right"><strong>{{ __('usuario') }}</strong></label>

                                    <div class="col-md-8">
                                        <input id="usuario" style="color: #0a0a0a;" type="text" class="form-control @error('usuario') is-invalid @enderror" name="usuario" value="{{ old('usuario') }}" required autocomplete="usuario" autofocus>
                                        @error('usuario')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <label style="color: #0a0a0a;" for="password" class="col-md-4 col-form-label text-md-right"><strong>{{ __('Password') }}</strong></label>

                                    <div class="col-md-8">
                                        <input id="password"  style="color: #1c00cf; "  type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                               <!-- <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {--{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>-->

                                <div class="form-group row mb-0 " >
                                    <div class="col-md-8 offset-md-4 pull-right">
                                        <button type="submit" class="btn btn-success ">
                                            {{ __('Login') }}
                                        </button>

                                        @if (Route::has('password.request'))
                                           <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {--{ __('Forgot Your Password?') }}
                                            </a>-->
                                        @endif
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>


                </div>
            </div>
        </div>
</div>
</body>

<script type="text/javascript">
    var path_base = "{{env('MIX_URL')}}";
</script>
<script src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables/media/js/jquery.dataTables.js') !!}"></script>
<script src="{!! asset('plugins/bootbox/bootbox.min.js') !!}"></script>
<script src="{!! asset('js/methods_global/methods.js') !!}"></script>
<script src="{!! asset('js/login/login.js') !!}"></script>

