@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9">
			<h2 class="text-center">Listado de Tickets Pendientes de Visita Terreno</h2>
		</div>
		<div class="col-lg-3">

		</div>
	</div>

	<hr class="hr-sin-top">

	@include('layouts.message')

	<table class="table table-bordered">
		<tr>
			<th> Nro </th>
			<th> Origen </th>
			<th> Tipo </th>
			<th> Area </th>
			<th> Nivel </th>
			<th> Prioridad </th>
			<th> Cliente </th>
			<th> Servicio </th>
			<th> Estación </th>
			<th> Técnico </th>
			<th> Estado </th>
			<th> Fecha Evento </th>
			<th> Descripción </th>
			<th> Opciones </th>
		</tr>
		@foreach ($data as $row)
			<tr>
				<td> {{ $row->id }} </td>
				<td> {{ @$row->ticket_origen->nombre }} </td>
				<td> {{ @$row->ticket_tipo->nombre }} </td>
				<td> {{ @$row->ticket_area->nombre }} </td>
				<td> {{ @$row->ticket_nivel->nombre }} </td>
				<td> {{ @$row->prioridad_id }} </td>
				<td> {{ @$row->clientes->nombre }} </td>
				<td> {{ @$row->servicios->Codigo }} </td>
				<td> {{ @$row->servicios->estaciones->nombre }} </td>
				<td> {{ @$row->tecnico->nombre }} </td>
				<td> {{ @$row->ticket_estado->nombre  }} </td>
				<td> {{ $row->fecha_evento }} {{ $row->hora_evento }} </td>
				<td> {{ $row->descripcion }} </td>				
				<td>
					@if ($row->ticket_ot->count() > 0)
					@php ($fila = $row->ticket_ot->first())
						<a class="btn btn-info btn-sm" href="{{ route('ots.show', $fila->id) }}">Ver</a>
					@else
						<a class="btn btn-primary btn-sm" href="{{ route('ots.crear',$row->id) }}">Creat OT</a>
					@endif
				</td>
			</tr>
		@endforeach
	</table>
	{!! $data->links() !!}
	<p class="text-center text-primary"><small> -- . -- </small></p>
</div>
@endsection