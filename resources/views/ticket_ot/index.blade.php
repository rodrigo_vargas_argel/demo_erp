@extends('layouts.app')
@section('title', 'Listado de OTs')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9">
			<h2 class="text-center">Listado de Órdenes de Trabajooooo!!</h2>
		</div>
		<div class="col-lg-3">

		</div>
	</div>

	<hr class="hr-sin-top">

	@include('layouts.message')
	@include('ticket_ot.modal_trabajos')
	@include('ticket_ot.modal_equipos')
	@include('ticket_ot.modal_comentarios')
	@include('ticket_ot.modal_cierre_ot')
    @include('ticket_ot.entregas.form_equipos')

	<table class="table table-bordered dataTabless">
        <thead>
    		<tr>
    			<th> Ticket </th>
    			<th> Cliente </th>
    			<th> Servicio </th>
    			<th> Fecha Visita </th>
    			<th> Técnico </th>
    			<th> Trabajos </th>
    			<th> Equipos </th>
    			<th> Estado </th>
    			<th> Opciones </th>
    		</tr>
        </thead>
        <tbody>
		@foreach ($data as $row)
    		<tr>
    			<td> {{ @$row->tickets->id }} </td>
    			<td> {{ @$row->tickets->clientes->nombre }} </td>
    			<td> {{ @$row->tickets->servicios->Codigo }} </td>
    			<td> {{ @$row->tickets->fecha_visita }} </td>
    			<td> {{ @$row->tickets->tecnico->nombre }} </td>
    			<td> {{ @$row->ticket_ot_detalle->count() }} <i class="detalle fa fa-eye" tkt="{{@$row->tickets->id}}" attr="{{ $row->id }}"></i> </td>
    			<td> {{ @$row->ticket_ot_equipo->count() }} <i class="equipos fa fa-eye" tkt="{{@$row->tickets->id}}" attr="{{ $row->id }}"></i> &nbsp; &nbsp; <span class="btn btn-success btn-xs fa fa-forward text-info" tecnico="{{ @$row->tickets->tecnico_id }}" cliente="{{ @$row->tickets->cliente_id }}" serv="{{ @$row->tickets->servicios_id }}" title="Entrega de Equipos"></span></td>
    			<td class="ot-{{ $row->id }}"> {{ @$row->ticket_estado->nombre }} </td>
    			<td>
    				<a class="btn btn-info btn-sm fa fa-eye" href="{{ route('ots.show', $row->id) }}" target=”_blank” title="VER"></a>
    				<a class="btn btn-success btn-sm fa fa-comments" attr="{{ $row->tickets->id }}" title="COMENTARIOS"></a>
    				<a class="btn btn-warning btn-sm fa fa-edit" href="{{ route('ots.edit', $row->id) }}" title="EDITAR"></a>
    				@if ($row->estado_id != 9)
    					<a class="btn btn-warning btn-sm fa fa-cog cierre{{ $row->id }}" attr="{{ @$row->tickets->id }}" ot="{{ $row->id }}"  title="CERRAR OT"></a>
    				@endif
    			</td>
    		</tr>
		@endforeach
        </tbody>
	</table>
	{!! $data->links() !!}
	<p class="text-center text-primary"><small> -- . -- </small></p>

</div>
@endsection

@section('mijava')
<script type="text/javascript">
//ver servicios del modulo /clientes/listaCliente.blade.php
    $(document).on('click', '.detalle', function() {
        $('#modal-trabajos').modal('show');
        // parametros para armar la data table
        var id = $(this).attr('attr');
        var tkt = $(this).attr('tkt');
        $(".nro_ot").html(tkt);

        var url = "{{ url('ots/trabajos') }}/"+id;
        try{
            tablita = $('#tabla_ver_trabajos').DataTable({
                "autoWidth": false,
                "destroy": true,
                "paging": true,
                "searching": true,
                "processing": true,
                "serverSide": false,
                "order": [[1, 'asc']],
                "ajax": url,
                "columns": [
                            { data: 'nombre', name: 'nombre', width: '80% !important' },
                            { data: 'estado', name: 'estado', width: '20% !important'},
                         ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });
        } catch (e){
            console.error(e);
        }
            
    });


    $(document).ready(function(){
        $('.dataTabless').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'excel', title: 'Ordenes de Trabajo Teledata'},
                {extend: 'pdf', title: 'Ordenes de Trabajo Teledata'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            language: {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
        });

        var l = $( '.ladda-button' ).ladda();
    });


    $(document).on('click', '.equipos', function() {
        $('#modal-equipos').modal('show');
        // parametros para armar la data table
        var id = $(this).attr('attr');
        var tkt = $(this).attr('tkt');
        $(".nro_ot").html(tkt);

        var url = "{{ url('ots/equipos') }}/"+id;
        try{
            tablita = $('#tabla_ver_equipos').DataTable({
            	"autoWidth": false,
            	"destroy": true,
            	"paging": true,
        		"searching": true,
    	        "processing": true,
    	        "serverSide": false,
    	        "order": [[1, 'asc']],
    	        "ajax": url,
    	        "columns": [
    	                    { data: 'cantidad', name: 'cantidad', width: '10% !important' },
    	                    { data: 'equipo_id', name: 'equipo_id', width: '75% !important'},
                            { data: 'entregado', name: 'entregado', width: '15% !important'},
    	                 ],
    	        "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
    	    });
        } catch (e){
            console.error(e);
        }
    });

    $(document).on('click', '.fa-comments', function() {
        $('#modal-comentarios').modal('show');
        // parametros para armar la data table
        var id = $(this).attr('attr');
        $(".nro_tkt").html(id);

        var url = "{{ url('ots/comentarios') }}/"+id;
        tablita = $('#tabla_ver_comentarios').DataTable({
        	"autoWidth": false,
        	"destroy": true,
        	"paging": true,
    		"searching": true,
	        "processing": true,
	        "serverSide": false,
	        "order": [[0, 'asc']],
	        "ajax": url,
	        "columns": [
	                    { data: 'fecha', name: 'fecha', width: '15% !important' },
	                    { data: 'usuarios_id', name: 'usuarios_id', width: '20% !important'},
	                    { data: 'comentario', name: 'comentario', width: '65% !important'},
	                 ],
	        "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
	    });
    });

    $(document).on('click', '.fa-cog', function() {
    	$(".id_tkt").html($(this).attr('attr'));
        $("#nro_ot").val($(this).attr('ot'));
        $("#id_tkt").val($(this).attr('attr'));
        $('#modal-cierre-ot').modal('show');
    });

    $(document).on('click', '.toggle-check', function() {
        var url = "{{ url('ots/entregas') }}"
        var form = {
            _method: "POST",
            _token: "{{ csrf_token() }}",
            usuario: "{{ @Auth::user()->id }}",
            fila: $(this).attr('attr'),
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            success: function(response) {
                var data = JSON.parse(response)
                if (( data.estado != null) && (data.estado == "OK")) {
                    
                    bootbox.alert(data.msge);
                } else {
                    bootbox.alert('Ha ocurrido un error :-(  No se cambió estado');
                }
            },
            error:function (jqXHR, exception) {
                let errores = "";
                obj = jqXHR.responseJSON.errors;
                Object.keys(obj).forEach(function(key) {
                  errores += obj[key];
                })
                bootbox.alert('Ha ocurrido un error :-( '+ errores );
            }
        });
    });

    $('body').on('click', '#graba-cierre', function() {
        var id = $('#nro_ot').val();
        var url = "{{ url('ots/cierre') }}/"+id
        var csrf_token = "{{ csrf_token() }}";
        var form = {
            _method: "POST",
            _token: csrf_token,
            ticket_id: $('#id_tkt').val(),
            fecha_salida: $('#fecha_salida').val(),
            hora_salida: $('#hora_salida').val(),
            fecha_llegada: $('#fecha_llegada').val(),
            hora_llegada: $('#hora_llegada').val(),
            observaciones: $('#observaciones').val(),
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            success: function(response) {
                var data = JSON.parse(response)
                if (( data.estado != null) && (data.estado == "OK")) {
                    $('#modal-cierre-ot').modal('hide');
                    $('.ot-'+id).html('Cerrado');
                    $('.cierre'+id).remove();
                    bootbox.alert(data.msge);
                } else {
                    bootbox.alert('Ha ocurrido un error :-(  No se cambió estado');
                }
            },
            error:function (jqXHR, exception) {
                let errores = "";
                obj = jqXHR.responseJSON.errors;
                Object.keys(obj).forEach(function(key) {
                  errores += obj[key];
                })
                bootbox.alert('Ha ocurrido un error :-( '+ errores );
            }
        });
    });

    $(document).on('click', '.fa-forward', function() {
        $('#modal-entrega-eq').modal('show');
        var tecnico = {{ @Auth::user()->id }};
        var cliente = $(this).attr("cliente");
        var servicio = $(this).attr("serv");
        var estacion = $(this).attr("estacion");
        $("#bodega_origen option[value="+ tecnico +"]").attr("selected",true);
        $('#bodega_origen').trigger("change");
        if (cliente != null) {
            $("#bodega_destino option[value=13]").attr("selected",true);
            $('#bodega_destino').trigger("change");
            $("#cliente option[value="+ cliente +"]").attr("selected",true);
            $('#cliente').trigger("change");
        } else if (estacion != null) {
            $("#bodega_destino option[value=18]").attr("selected",true);
            $('#bodega_destino').trigger("change");
            $("#estacion option[value="+ estacion +"]").attr("selected",true);
            $('#estacion').trigger("change");
        }
        if (servicio != null) {
            $("#servicio option[value="+ servicio +"]").attr("selected",true);
        }
        console.log("clicked");
        // parametros para armar la data table
       /* var id = $(this).attr('attr');
        var tkt = $(this).attr('tkt');
        $(".nro_ot").html(tkt);

        var url = "{{ url('ots/trabajos') }}/"+id;
        try{
            tablita = $('#tabla_ver_trabajos').DataTable({
                "autoWidth": false,
                "destroy": true,
                "paging": true,
                "searching": true,
                "processing": true,
                "serverSide": false,
                "order": [[1, 'asc']],
                "ajax": url,
                "columns": [
                            { data: 'nombre', name: 'nombre', width: '80% !important' },
                            { data: 'estado', name: 'estado', width: '20% !important'},
                         ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });
        } catch (e){
            console.error(e);
        }*/
            
    });

</script>
@include('ticket_ot.entregas.jss')
@stop