@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9">
			<h2 class="text-center"> Ver Recorrido</h2>
		</div>
		<div class="col-lg-3">
			<a class="btn btn-link" href="{{ route('recorridos.index') }}"> Volver</a>
		</div>
	</div>

	<hr class="sin-margin-top">
	
	<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3">
			<div class="form-group">
				<strong>Número Recorrido: </strong>{{ @$data->numero }}
			</div>
		</div>
		
		@php($guion = "")
		<div class="col-xs-12 col-sm-5 col-md-5">
			<div class="form-group">
				<strong>Sectores: </strong>
					@foreach ($data->recorrido_sectores as $value)
						{{ $guion.$value->sectores->nombre }} 
						@php($guion = "- ")
					@endforeach
			</div>
		</div>

		<div class="col-xs-12 col-sm-4 col-md-4">
			<div class="form-group">
				<strong>Nombre: </strong>{{ @$data->nombre }}
			</div>
		</div>

		<div class="col-xs-12 col-sm-4 col-md-3">
			<div class="form-group">
				<strong>Recorridos: </strong>
					@foreach ($data->recorridos as $value)
						<br>
						{{ @$value->tipo_recorridos->nombre." ".@date("H:i", @strtotime($value->hora_inicio))." (".$value->turno }})
					@endforeach
			</div>
		</div>

		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="form-group">
				<strong>Descripción: </strong>{{ $data->descripcion }}
			</div>
		</div>
	</div>
</div>
<p class="text-center text-primary"><small> -- . -- </small></p>

@endsection
