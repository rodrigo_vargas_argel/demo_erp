<div class="modal fade" role="dialog" id="modal-comentarios" >
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Comentarios del Ticket N° <span class="nro_tkt"> </span> </h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="dataComentarios">
						<table class="table table-responsive table-striped" id="tabla_ver_comentarios" width="100%">
						    <thead>
						        <tr>
						            <th style="width:15% !important">Fecha</th>
						            <th style="width:20% !important">Usuario</th>
						            <th style="width:65% !important">Comentario</th>
						        </tr>
						    </thead>
						    <tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>