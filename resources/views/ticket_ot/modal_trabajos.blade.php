<div class="modal fade" role="dialog" id="modal-trabajos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Trabajos OT <span class="nro_ot"></span> </h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="dataTrabajos">
						<table class="table table-responsive table-striped" id="tabla_ver_trabajos" width="100%">
						    <thead>
						        <tr>
						            <th style="width:80% !important">Trabajos</th>
						            <th style="width:20% !important">Estado</th>
						        </tr>
						    </thead>
						    <tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>