@extends('layouts.app')
@section('title', 'Creación de OTs')
@section('content')
<link href="{!! asset('css/plugins/select2/select2.min.css') !!}" rel="stylesheet" />

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">
        <h2>Teledata ERP</h2>
        <div class="col-xs-9 col-sm-10 col-md-11">
	        <ol class="breadcrumb">
	            <li>
	                <a href="">Home</a>
	            </li>
	            <li>
	                <a>Mantenedores</a>
	            </li>
	            <li class="active">
	                <strong>Editar Orden de Trabajo N° {{$ticket->id}}</strong>
	            </li>
	        </ol>
	    </div>
	    <div class="col-xs-3 col-sm-2 col-md-1">
			<a class="btn btn-link" href="{{ route('ots.index') }}"> Volver</a>
		</div>
    </div>
</div>

<hr>

<div class="row wrapper wrapper-content animated fadeInRight" style="margin-top:-15px;">
	@include('layouts.message')

	<form action="{{ route('ots.update',$data->id) }}" method="POST">
		@csrf
		@method('PUT')
		<div class="row">
			<div class="col-xs-4 col-sm-2 col-md-2">
				<strong> Ticket de origen:</strong>
				<p> {{ $ticket->id }} </p>
				<input type="hidden" name="ticket_id" value="{{$ticket->id }}"/>
			</div>
			<div class="col-xs-8 col-sm-3 col-md-3">
				<strong> Estación:</strong>
				<p> {{ @$ticket->servicios->estaciones->nombre }} </p>
			</div>
			<div class="col-xs-4 col-sm-2 col-md-2">
				<strong> Servicio:</strong>
				<p> {{ @$ticket->servicios->Codigo }} </p>
			</div>
			<div class="col-xs-8 col-sm-5 col-md-5">
				<strong> Cliente:</strong>
				<p> {{ @$ticket->clientes->nombre}} </p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-2 col-md-2">
				<strong> Tipo de Ticket:</strong>
				<p> {{ @$ticket->ticket_tipo->nombre }} </p>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3">
				<strong> Area:</strong>
				<p> {{ @$ticket->ticket_area->nombre}} </p>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2">
				<strong> * Fecha Visita:</strong>
				<div class="form-group ">
					<input type="date" name="fecha_visita" class="form-control" value="{{ $ticket->fecha_visita }}">
				</div>
			</div>
			<div class="col-xs-7 col-sm-3 col-md-3">
				<strong> * Técnico:</strong>
				<select name="tecnico" class="form-control tecnico_id">
	                @foreach($tecnicos as $fila )
	                    <option value="{{$fila->id}}" {{ ($ticket->tecnico_id == $fila->id) ? 'selected' : '' }}> {{ $fila->nombre }} </option>
	                @endforeach
	            </select>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2">
				<strong> * Estado OT:</strong>
				<select name="estado_id" class="form-control estado_id">
	                @foreach($estados as $fila )
	                	@if ((($fila->id != 9) && ($data->estado_id != 9)) || (($fila->id == 9) && ($data->estado_id == 9)))
	                    	<option value="{{$fila->id}}" {{ ($data->estado_id == $fila->id) ? 'selected' : '' }}> {{ $fila->nombre }} </option>
	                    @endif
	                @endforeach
	            </select>
			</div>
		</div>
		<div class="row hidden comentario">
			<div class="col-xs-1 col-sm-5 col-md-7" >

			</div>
			<div class="col-xs-6 col-sm-7 col-md-5" >
				<strong> * Comentario de estado OT <span class="lbl-comments"> </span>:</strong>
				<div class="form-group ">
					<input type="text" name="comentario" class="form-control" value="{{ old('comentario') }}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-7 col-sm-12 col-md-12">
				<strong> Observaciones:</strong>
				<div class="form-group ">
					<input type="text" name="descripcion" class="form-control" value="{{ $data->descripcion }}">
				</div>
			</div>
		</div>

		<hr>

		@csrf
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p> <b> * Trabajos : </b> </p>
				@php ( $filas_visibles = $ot_detalle->count() )
				@php ( $x = 0 )
				@foreach ($ot_detalle as $value)
					<div class="row show" id="filita{{ $x }}">
						<div class="col-xs-10 col-sm-9 col-md-8">
							<div class="form-group sin-bottom">
								<select style="width:100%" name="nombre[{{ $x }}]" class="form-control nombre">
			                        <option value=""> -- Seleccione o Ingrese uno nuevo -- </option>
			                        @foreach($trabajos_ot as $fila )
			                            <option value="{{$fila->nombre}}" {{ (strtoupper($value->nombre) == strtoupper($fila->nombre)) ? 'selected' : '' }}>{{$fila->nombre}}</option>
			                        @endforeach
			                    </select>
							</div>
							<input type="hidden" name="ot_detalle[{{ $x }}]" value="{{ $value->id }}" class="form-control">
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1">
							@if ( ( $x + 1 ) >= $filas_visibles )
								<div class="btn btn-success mas" id="mas-{{$x}}" fila="{{$x}}">
									+
								</div>
							@endif
						</div>
					</div>
					@php ( $x++ )
				@endforeach

				@for ($i = $x; $i < 10; $i++)
					<div class="row hidden" id="filita{{ $i }}">
						<div class="col-xs-10 col-sm-9 col-md-8">
							<div class="form-group sin-bottom">
								<select style="width:100%" name="nombre[{{ $i }}]" class="form-control nombre">
			                        <option value=""> -- Seleccione o Ingrese uno nuevo -- </option>
			                        @foreach($trabajos_ot as $fila )
			                            <option value="{{$fila->nombre}}" {{ (strtoupper(old('nombre.'.$i)) == strtoupper($fila->nombre)) ? 'selected' : '' }}>{{$fila->nombre}}</option>
			                        @endforeach
			                    </select>
							</div>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1">
							<div class="btn btn-success mas" id="mas-{{$i}}" fila="{{$i}}">
								+
							</div>
						</div>
					</div>
				@endfor

			</div>


			<div class="col-xs-12 col-sm-6 col-md-6 bordecitoh">
				<p> <span class="col-xs-8 col-sm-7 col-md-8"><b> Equipos a utilizar : </b> </span><span class="col-xs-2 col-sm-3 col-md-3"><b> Cantidad </b> </span> </p>
				@php ( $filas_visibles = $ot_equipos->count() )
				@php ( $x = 0 )
				@foreach ($ot_equipos as $value)
					<div class="row show" id="filita_{{ $x }}">
						<div class="col-xs-10 col-sm-9 col-md-8">
							<div class="form-group sin-bottom">
								<select style="width:100%" name="equipo[{{ $x }}]" class="form-control equipo">
			                        <option value=""> -- Seleccione o Ingrese uno nuevo -- </option>
			                        @foreach($equipos as $fila )
			                            <option value="{{$fila->id}}" {{ ($value->equipo_id == $fila->id) ? 'selected' : '' }}> {{ @$fila->marca->nombre}} {{@$fila->modelo->nombre}} </option>
			                        @endforeach
			                    </select>
							</div>
							<input type="hidden" name="ot_equipos[{{ $x }}]" value="{{ $value->id }}" class="form-control">
						</div>
						<div class="col-xs-2 col-sm-3 col-md-3">
							<div class="form-group sin-bottom">
								<input type="text" name="cantidad[{{ $x }}]" value="{{ $value->cantidad }}" class="form-control" placeholder="00">
							</div>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1">
						@if ( ( $x + 1 ) >= $filas_visibles )
							<div class="btn btn-success mas2" id="mas_{{$x}}" fila="{{$x}}">
								+
							</div>
						@endif
						</div>
					</div>
					@php ( $x++ )
				@endforeach

				@for ($i = $x; $i < 15; $i++)
					<div class="row hidden" id="filita_{{ $i }}">
						<div class="col-xs-8 col-sm-7 col-md-8">

							<div class="form-group sin-bottom">
								<select style="width:100%" name="equipo[{{ $i }}]" class="form-control equipo">
			                        <option value=""> -- Escriba y/o seleccione equipo -- </option>
			                        @foreach($equipos as $fila )
			                            <option value="{{$fila->id}}" {{ old('equipo.'.$i) == $fila->id ? 'selected' : '' }}>{{ @$fila->marca->nombre}} {{@$fila->modelo->nombre}}</option>
			                        @endforeach
			                    </select>
							</div>
						</div>
						<div class="col-xs-2 col-sm-3 col-md-3">
							<div class="form-group sin-bottom">
								<input type="text" name="cantidad[{{ $i }}]" value="{{ old('cantidad.'.$i) }}" class="form-control" placeholder="00">
							</div>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1">
							<div class="btn btn-success mas2" id="mas_{{$i}}" fila="{{$i}}">
								+
							</div>
						</div>
					</div>
				@endfor
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Grabar</button>
			</div>
		</div>
	</form>
</div>
<p class="text-center text-primary"><small> -- . -- </small></p>
@endsection
   

@section('mijava')
	<style type="text/css">
		.bordecitoh{ border-left: solid 2px rgb(26, 179, 148);}

	</style>

	<script type="text/javascript">
	    $(document).ready(function() {
	    	$(window).keydown(function(event){
			    if(event.keyCode == 13) {
			      	event.preventDefault();
			      	return false;
			    }
			});

	    	$('.nombre').select2({
				tags: true,
				selectOnClose: true,
				createSearchChoice: function (term, data) {
					if ($(data).filter(function () {
						return this.text.localeCompare(term) === 0;
					}).length === 0) {
						return { id: term, text: term };
					}
				},
	    	});

	    	$('.equipo').select2({
				tags: false,
				selectOnClose: true,
	    	});


	    	$('.mas').click(function(e){
	    		let fila = $(e.target).attr('fila');
	    		let sigue = parseInt(fila) + 1;
	    		console.log("sigue: " + sigue + " fila: " + fila);
	    		let campo1 = document.getElementById('filita'+fila);
	    		let campo2 = document.getElementById('filita'+sigue);
	    		let campo3 = document.getElementById('mas-'+sigue);
	    		$(this).removeClass("show").addClass("hidden");
	    		campo2.classList.remove("hidden");
		    	campo2.classList.add("show");
		    	if (sigue > 8){
	    			campo3.classList.remove("show");
		    		campo3.classList.add("hidden");
	    		}
	    	});

	    	$('.mas2').click(function(e){
	    		let fila = $(e.target).attr('fila');
	    		let sigue = parseInt(fila) + 1;
	    		let campo1 = document.getElementById('filita_'+fila);
	    		let campo2 = document.getElementById('filita_'+sigue);
	    		let campo3 = document.getElementById('mas_'+sigue);
	    		$(this).removeClass("show").addClass("hidden");
	    		campo2.classList.remove("hidden");
		    	campo2.classList.add("show");
		    	console.log(sigue);
		    	if (sigue > 13){
	    			campo3.classList.remove("show");
		    		campo3.classList.add("hidden");
	    		}
	    	});

	    	$('.estado_id').change(function(e){
	    		if (($(this).val() == "5") || ($(this).val() == "9")) {
	    			$('.lbl-comments').html($('.estado_id option:selected').text().replace('o', 'a'));
		            $('.comentario').removeClass("hidden").addClass("show");
		        } else {
		        	$('.comentario').val("");
		        	$('.comentario').removeClass("show").addClass("hidden");
		        }
	    	});
	    });
	</script>
@stop