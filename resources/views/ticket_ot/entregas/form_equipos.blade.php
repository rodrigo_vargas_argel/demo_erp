<div class="modal fade" role="dialog" id="modal-entrega-eq">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Entrega de Equipos </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal" name="frmEntrega" id="frmEntrega" method="post">
                            {{csrf_field()}}
                            <div class="col-lg-6">
                                <input type="hidden" name="equipos_para_entrega" id="equipos_para_entrega" value="0">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Origen</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-10 form-control" name="bodega_origen" id="bodega_origen" onchange="carga_stock(this)">
                                            @foreach($bodega_origen as $bodega1)
                                                <option value="{{$bodega1->id}}">{{$bodega1->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Destino</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-10 form-control" name="bodega_destino" id="bodega_destino" onchange="muestra_cliente(this)">
                                           <option value="">-Seleccione-</option>
                                            @foreach($bodega_destino as $bodega2)
                                                <option value="{{$bodega2->id}}">{{$bodega2->nombre}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>                                
                            <div class="cliente_servicio col-lg-12" id="cliente_servicio" style="display: none;">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <select class="selectpicker form-control col-sm-10 form-control" style="border-color: green;" name="cliente" id="cliente" data-live-search="true" onchange="carga_servivio(this)">
                                            <option value="">Seleccione Cliente</option>
                                            @foreach($clientes as $cliente)
                                                <option value="{{$cliente->id}}" rut="{{$cliente->rut}}">{{$cliente->nombre}}-{{$cliente->rut}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-10 form-control" name="servicio" id="servicio">
                                          <option value="">-Seleccione Servicio-</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="destaciones col-lg-12" id="destaciones" style="display: none;">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estación Teledata</label>
                                    <div class="col-sm-10">
                                        <select class="selectpicker form-control col-sm-10 form-control" style="border-color: green;" name="estacion" id="estacion" data-live-search="true" >
                                            <option value="">Seleccione Estación Teledata</option>
                                            @foreach($estaciones as $estacion)
                                                <option value="{{$estacion->id}}" >{{$estacion->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="tabla_entrega" class="col-lg-12">
                                <div class="hr-line-dashed"  style="border: 1px solid;  border-color: #5cb85c;"></div>
                                <h3>
                                    <label class="form-actions text-primary">Equipos para Entrega: </label>
                                    <label id="lbl_num_equipos">0</label>
                                </h3>
                                @if($existencias != 'No hay Productos en esta bodega')

                                    <div class="col-lg-12 table-responsive">

                                        <table class=" table table-stripped  table-bordered " >
                                            <thead>
                                                <tr>
                                                    <th data-toggle="true">Elije</th>
                                                    <th>Codigo</th>
                                                    <th>MAC-Serie</th>
                                                    <th>Descripción</th>
                                                    <!--<th data-hide="all"></th>-->
                                                </tr>
                                            </thead>
                                            <tbody id="tabla_detalle">
                                                <?php
                                                $total_detalle=0;
                                                $contador=0;
                                                ?>

                                                @foreach($existencias as $detalle)
                                                    <?php
                                                    $mac_serie=  $detalle->mac_serie;
                                                    $mac_serie_con_formato="";
                                                    for($i=0;$i<strlen($mac_serie);$i++){
                                                        $mac_serie_con_formato=$mac_serie_con_formato."".$mac_serie[$i];
                                                        if (($i%2!=0)&&($i!=(strlen($mac_serie)-1))){
                                                            //$detalle->mac_serie[$i];
                                                            $mac_serie_con_formato=$mac_serie_con_formato.':';
                                                            // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                                                        }
                                                    }

                                                    ?>
                                                    <tr>
                                                        <td ><input type="checkbox" class="js-switch_3" name="check[]" id="check{{$detalle->producto_id}}-{{$detalle->mac_serie}}" value="{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" mac_serie="{{$detalle->mac_serie}}" producto_id="{{$detalle->producto_id}}" cantidad="{{$detalle->cantidad}}" onchange="check_equipo(this)">
                                                            @if ($detalle->mac_serie == 0)
                                                                <div id="div_cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" style="visibility: hidden">
                                                                    <select class="form-control" name="cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}" id="cantidad{{$detalle->id}}-{{$detalle->producto_id}}-{{$detalle->mac_serie}}"></select>
                                                                </div>
                                                            @endif
                                                        </td>
                                                            <td>{{$detalle->producto->codigo_barra}}</td>
                                                        <td><small>{{$mac_serie_con_formato}}</small></td>
                                                        <td> <small>{{@$detalle->producto->tipo_producto->nombre}} {{@$detalle->producto->marca->nombre}} {{@$detalle->producto->modelo->nombre}}</small></td>

                                                    </tr>
                                                <?php $contador=$contador+1; ?>
                                                @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <ul class="pagination pull-right"></ul>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <div class="form-group">
                                            <textarea class="form-control" name="observaciones" id="observaciones" placeholder="Observaciones: Escribe algo aqui..."  rows="5"></textarea>
                                        </div>
                                        <div class="hr-line-dashed"  style="border: 1px solid;  border-color: #5cb85c;"></div>
                                        <div class="form-group">
                                            <button class="ladda-button btn btn-primary" data-style="expand-right" name="btn_entregar" id="btn_entregar" >Entregar</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>