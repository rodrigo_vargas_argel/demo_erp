<script>
    function check_equipo(e){
        var value=$(e).val();
        var mac_serie=$(e).attr('mac_serie');
        var cantidad=$(e).attr('cantidad');
        //alert(cantidad);
        var valor;
        var i;
        valor=$('#equipos_para_entrega').val();

        if (e.checked == true) {
            //alert(mac_serie);
            //alert(value);
            valor=parseInt(valor)+1;
            $('#equipos_para_entrega').val(valor);
            $('#lbl_num_equipos').html(valor);
            if (mac_serie==='0'){
                $('#div_cantidad'+value+'').css('visibility','visible');
                for(i=1;i<=cantidad;i++){
                    $('#cantidad'+value+'').append("<option value='"+i+"'>"+i+"</option>");
                }

            } else {
                console.log(mac_serie);
            }

        }
        else{
           // alert('deseleccionado');
            valor=parseInt(valor)-1;
            $('#equipos_para_entrega').val(valor);
            $('#lbl_num_equipos').html(valor);

            if (mac_serie==='0'){
                $('#cantidad'+value+'').empty();
                $('#div_cantidad'+value+'').css('visibility','hidden');
            }
        }
    }
    //var elem_3 = document.querySelector('.js-switch_3');
    //var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
    });

    function carga_stock(e) {
        var principal=$('option:selected', e).attr('value');
        // alert(principal);

        $.ajax({
            url: "{{url('/inventarios/carga_stock')}}/"+principal,
            type: "GET",
            dataType: "JSON",
            //data:  new FormData(this),
            processData: false,
            contentType: false,

            success: function (data) {
                //console.log(data.msg)
                if (data.Success===true){
                    $('#tabla_detalle').empty();
                    var i;
                    var estilo="visibility: hidden";
                    for (i=0;i<=data.coleccion.length-1; i++) {
                        var mac_serie = data.coleccion[i].mac_serie;
                        var mac_serie_con_formato = "";

                        for (var x = 0; x < mac_serie.length; x++) {
                            mac_serie_con_formato = mac_serie_con_formato + mac_serie[x];
                            if ((x % 2 != 0) && (x != (mac_serie.length - 1))) {
                                mac_serie[x]
                                mac_serie_con_formato = mac_serie_con_formato + ':';
                                // la condicion x!=(mac_serie.length-1 es para que no dibuje los dos puntitos al final de la cadena.
                            }
                        }

                        $("#tabla_detalle").append("<tr><div class='form-group'><td><div id='div_cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' style='"+estilo+"'><select class='form-control' name='cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' id='cantidad" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "'></select></div>");

                        $("#tabla_detalle").append("<input type='checkbox' class='js-switch_3' name='check[]' id='check" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "'   value='" + data.coleccion[i].id + "-" + data.coleccion[i].producto_id + "-" + data.coleccion[i].mac_serie + "' cantidad='" + data.coleccion[i].cantidad + "' mac_serie='" + data.coleccion[i].mac_serie + "'  onchange='check_equipo(this)'>");

                        $("#tabla_detalle").append("</td></div>" +
                            "<td>" + data.coleccion[i].producto.codigo_barra + "</td>" +
                            "<td>" + mac_serie_con_formato + "</td><td><small>" + data.coleccion[i].producto.tipo_producto.nombre + " " + data.coleccion[i].producto.marca.nombre + " " + data.coleccion[i].producto.modelo.nombre + "</small></td></tr>");

                    }

                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch_3'));

                    elems.forEach(function(html) {
                        var switchery = new Switchery(html, { color: '#7c8bc7', jackColor: '#1ab394', size:'small' });
                    });

                    $('#btn_entregar').attr("disabled", false);

                }
                else{
                    $('#tabla_detalle').empty();
                    $('#tabla_detalle').html("<h5 class='text-danger'>"+data.coleccion+"</h5>");
                    $('#btn_entregar').attr("disabled", true);

                }

            },
            error: function (data) {
                console.log(data);
                swal("Ups!", "Hubo problema lógico con base de datos!", "error");
                swal(7);
            }
        })
        var valor;
        valor=parseInt(0);
        $('#equipos_para_entrega').val(valor);
        $('#lbl_num_equipos').html(valor);
    }

    function muestra_cliente(r){
        //alert(r.text);
        var texto;
        texto=$( "#bodega_destino option:selected" ).text();
        //alert(texto);
        if (texto==="CLIENTE"){
           $("#cliente_servicio").css('display','block');
            $("#destaciones").css('display','none');
        }
        else{
            $("#cliente_servicio").css('display','none');
            $("#destaciones").css('display','block');
           // console.log('jkasjhd');
        }
    }

    function carga_servivio(e) {
        var id_;
        var rut;
        id_=e.value;
        rut=$('option:selected', e).attr('rut');

       // alert('cambio cliente'+id_  + "RUT: "+rut)
        $.ajax({

            url: "{{url('/inventarios/cargar_servicios')}}/"+rut,
            //type: $(dis).attr("method"),
            dataType: "JSON",
            //data: new FormData(this),
            processData: false,
            contentType: false,

            //dataType: 'json',
            success: function (data) {
                console.log('servicios: '+data.servicios.length);
                $("#servicio").empty();
                for(var i=0; i<data.servicios.length; i++){
                   // console.log('servicios2: '+data.servicios);
                    $("#servicio").append("<option value='"+data.servicios[i].Id+"'>"+data.servicios[i].Codigo+" "+data.servicios[i].mantenedor_servicios.servicio +" "+data.servicios[i].Descripcion+" "+data.servicios[i].Conexion+"</option>");
                }

            },
            error: function (data) {
                swal("ERROR", "NO seleccionaste ningún cliente, o no tiene servicios asociados", "error");
            }
        })
    }


    $('#frmEntrega').on('submit', function (e) {
        //$('#btn_entregar').attr("disabled", true);
        var l = $( '.ladda-button' ).ladda();
        e.preventDefault(e);
        l.ladda( 'start' );
            $.ajax({

                url: "{{url('/inventarios/entrega_equipos_guardar')}}",
                type: "POST",
                dataType: "JSON",
                data:  new FormData(this),
                processData: false,
                contentType: false,

                success: function (data) {
                  //  console.log(data)
                    if (data.Success===true){
                        swal("Listo!", data.msg, "success");
                        window.location.href="{{ url('/ots') }}";
                    }
                    else{
                        swal("Ups!", "Hubo problema lógico con base de datos!"+data.msg , "error");
                    }
                    // console.log(data.registro.producto.marca.nombre)

                },
                error: function (data) {
                    var msg="";
                    //   console.log(data.responseJSON.errors.key);
                    var length=0;
                    Object.keys(data.responseJSON.errors).forEach(function(key) {
                        //var myJSON = JSON.stringify(data.responseJSON.errors);
                        var keys = Object.keys(data.responseJSON.errors);
                        var campo=keys[length];
                        $('#'+campo).css('border-color','#ff0000');
                        $('#label_'+campo).html("<label class='text-danger'>"+data.responseJSON.errors[key]+"</label>");
                        console.log(keys[length]);
                        // alert(myJSON + '-'+keys[0]);
                        //  console.log(data.responseJSON.errors);
                        msg=msg+ data.responseJSON.errors[key];
                        //$('#mac_serie'+length).css('border-color','#909');
                        //alert($('#mac_serie'+key).val());
                        length++;
                    });
                    //alert(length);

                    var i;
                    // alert(data.responseJSON);
                    //  for (i=0;i<=data.message.errors.length;i++)
                    swal("Cometiste un Error!", msg, "error");
                    l.ladda('stop');

                }
            });
    });
</script>