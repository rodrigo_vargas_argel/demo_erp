<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE-edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>OT Nro . {{ @$data->tickets->id }}</title>
		<style type="text/css">
			body {
			    font-family: DejaVu Serif, sans-serif;
			    font-size: 0.7em;
			}
			.borde-top{border-top: 1px solid green;}
			.borde-abajo{border-bottom: 1px solid green;}
			.bord3s{border: 1px solid green;}
			.text-left{text-align: left;}
			.text-right{text-align: right;}
			.text-center{text-align: center;}
			.sin-top{margin-top: -10px;}
		</style>
	</head>
	<body>
		<table width="100%">
			<thead>
				<tr>
					<th width="20%">
						<img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjg4RTBCQzlBOTE3NjExRTZBNzg2RjgwMzZBQzg4MDM1IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjg4RTBCQzk5OTE3NjExRTZBNzg2RjgwMzZBQzg4MDM1IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkRENUM0MDkzMUY0MzExRTFBNzA2OEMyMEJFQkE0NDAzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkRENUM0MDk0MUY0MzExRTFBNzA2OEMyMEJFQkE0NDAzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgANgB4AwERAAIRAQMRAf/EAJgAAQACAwEBAAAAAAAAAAAAAAAFBwMEBggBAQEAAgMBAQAAAAAAAAAAAAAAAQMCBAUGBxAAAgIBBAICAgECBgMBAAAAAQIDBAUAERIGIQcxEyIUUUFhcUJSYnIjkTIVCBEAAgECAwUDCQcFAQAAAAAAAAECEQMhEgQxQVFhBXEiE/CBscHRMlJykpGhQmIjUxThstIzFQb/2gAMAwEAAhEDEQA/APVOgGgGgGgGgGgOa9jd3qdJ6ff7DYiNhqwVK1UHiZp5WCRR8vPEFj+R28Dc+dbOk0zvXFBbyHy2nnEd79+5eOTOxZx6kQPMVq1aEVox8heLpIxH/JmP8nXpv4Wjh3GqvjUv/jx3t1Lo9Ie1LfeMRdrZiKODsWGkWO+sIKxyxyAmKdFO/HlwZWXc+Rv8EDXD6noFp5rLjCWwpcXF0fkiy9cwDQDQDQDQDQDQDQDQDQDQDQFfe9umZPtvrm7QxSfdk6ssV6pX32+1oG3aMf7mjLBf922uh0zUqzfUpe7sZFaNPg6lS9Ot5+X1tevLE0ElQTpBj5BwaQQDa39ittxddiE5fBU/zrm9a/8ARWtP1mzpa1tSp4j4O5/r+nBy+bkbilB4nTf/AJj6jl68Wa7hkq8lOPN/VDjK8qlHavFyYzlT54yMwCbj4Xf4Ya9B1zURlKNqOOTb2mtcmpyqtheuuCYjQDQDQDQDQDQDQDQDQDQDQEd2HNVsJhrWTsEBK6bqp/zOx4xoP7s5A1XduKEHJ7IqpjKSiqspCe1JbsNcnYCyzczC43ic7libCjw7MSf+HwN9fL7+rzTlm72dtyktqr+38Md35ltpu5v87y9hbPr7saZnCcHY/uUG+izG5BcDbeMkj53Txy/qQdfROl6l3rEZN1awb403+fbTc8DesXc8anT66BcNANANANANANANANANANANAUz7w7WhvVMJDKQtJhYs8D5M7r/1JsN9yiNz22/zKf6a4XWtRJpWY07yrKuzLXCr3Y+rieW6/wBSlCSswo98q8Ny24cfsK+gr9rngE9enNJARusiwSPuP5DoDH/5OuIulxar4dx80nl+x95+ahyY+LJJ+HcfNKWX78X5mqk96x7acT26L9p2EdsClcRztx5P/wBb7Dx+Eh2338Atrf6Tcenu+G0lbm91fe3bcU9zTpu4G10nqc7d9W5pKE8N/vbtuPKjoeidepPajQDQDQDQDQDQDQDQDQDQGhn8zXwuGt5SdHlSrGXWCFS8srnxHDEo8tJI5CIo+SRrO3BydEQ3RHF9L9ZhLMnZO2ol7sd5jYes20kFQueX1ICWV2Unbl8DYBfjc6kNNGMnJ4zbrX0U7Fgt/ZVnL0vTYxk7tzvXJOvZ2bdn3budh62DqnJ949cYLtdVmkRamWVSK+SjUc1O3/rIBt9iH+qk/wCBGqb2nhdVJLz7/JGjren2tRGkljx3m10bLZK3iWo5lfrz+Jf9PJpvuHZQDHYRiF5RzxlXU7fO6+GUgbDtyik3jVbfL0GzZU1BKfvb+fPznRaxLRoBoBoBoDmew99o4PJfoT4rMXJDGsgmoY6zbh2YkcfshRl5Dj5GtqzpXONVKK7ZJAhqfunrV6BbFHFZ63XZmjE8GIuyR8kYo68ljI3RgVb+CNXS6bOLo5QT+ZE0JfN+wKWIycuPlw+btvEFJsUcXbtV25qG/GWJGRtt9jsfB8aptaRzjXNBdskmDSX2rjWdVHX+yAsQu5wt4Abnbckx+Bqx6CXxW/riDpcdnsbkchk6FWQvZxEyV7ylSAskkKTqAT4b8JV+Nas7UopN7JbPQQbskMUvD7ED/WwdOQ32YfBH9xrBOgOWyvsajjcjYoSYTO2HrtwM9XFXLED+Ad45Y0ZGHnbwdbVvRuUU80F2ySZJhq+z8dYsw11wPYUaaRYxJLh7scal2C8ndowqqN/JPwNZS0Mkq5ofXEGTK+y8bjcjYoS4bOzvXbgZq2KuTwv4B3jlRCrr/cai3opSinmhjxkkDXpe0erT5arBZoZTFT33SpXuZLG2qcLysSYoTPKipyZieAJ8nwPJ1lLQ3FFtOMqY4ST89ASvc+8YrqlStLbgtX7t6Uw4/E46E2btl1Uu/wBUIK7iONSzsSAB/cgGnTaaV1ujSS2t4JEGGT2L19utY/smPjuZjF5MgVnxdWa5INwxP2RxKzpxKFW5D8W/E+dZLSTzuDpGUfidPSERCe6OuPbkppic+12FElnqrh7pljjkJEbun17qrlTxJ+djq7/mzpXNCnzRJJTI+x8Fjuqw9juV70EVqUVqOMlqyR5GxZdzHHXhqyBJGkkKkqP4/L486pho5yuZE1hi3Xupca8CCT6r2nFdnw8eVxpkWJmeGetOhisV54mKSwTxN5jkjYbMp/xG42Oq79iVqWWX9GuKBL6pBx3qStDW6VHFDbjuxi/k2FiFZUQlsjYYrtMsb7oTxbxtuPBI2J3eoSbu4qmEf7USysfZEcdX2HmZbLPk4rS1XgrzS9mqCoVgCPHE2Lrz1po32D8gdwxYHXU0TrZil3aV/adcfzNNEohsbcpnJ0/18VELH7EP0F8h3Mr9n2DjuJKYj+f9Xj+fGrrkXldZYU+Gz/kS6llVo+51e79vl6e+IyVezaqvkoMjJcqz1ba0okMYaOGaOVGhWNwy/BJB+Ncxu07UPEzRonSlHVVfNUxqY4E1Wte5TahFrHdeSqZE/YaO7daQRchzKA1VBbjvtufnVEo6amDnXsXtGBVXc0r0+654Wfsy332/uiaxP2io1dWiQGui42tLWeNWUsjq3wdv6a6+mbdqNO7hwtOvPvOpKqY+r26rdmxIq4yJLJtwiF2v9wcA8xueNmokB8b+JSE/1eNTfi/DlV4Ue6z6pV+zEl1O/wAOntOpl+zDCDG5PHTZiaWFstPka0sPKGIGCNWqsjRKRurRMUJJ2O++udcencYZs0XlXuqLrtx27e3ExMXa4+/XaNSLuTYXEdZTIUJb9ijLdt2ZGjtxNWhiRoIgv22RGrP52UnU2HZTfhZ5TyulcqWx1e3hUOhL4KlBF7LyVrsWQjsdotQP/wDAx6JIsFXDRy8doHdFWSeV+L2SGJH4jwgG9F2VbKUF3F7z3uXs4e0g+9Pp14+65611m/DN1meWVM1jvrlX9fNxlPtkqycRE6yox/YVT+Mi777lgF+X6UVNd/c+Mefq5EG5ia8K+0+x2FtRvLJisUr0wsgkjCy3OLsxURlX5HjxYnwdwPG+NxvwIKn4pY/SSaT06w9qwXOzX4GtvDLF0fFrHLwSNY1a9O0rr9bW35ceKndYh433bbLM/ApBOn436F2esHyjTjHs7IXurX4zFtHB3XEyLKIfvMXKtZryiMx/tBAElQP5QqW2YLulL9BKa+R+lPlwB//Z">
					</th>
					<th width="60%">
						<h2 class="text-center"> Orden de Trabajo N° {{ @$data->tickets->id }}</h2>
					</th>
					<td width="20%">
						Estado: {{ @$data->ticket_estado->nombre }}
					</td>
				</tr>
			</thead>
		</table>
		
		<hr style="color:green">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-right" width="15%"> Tkt Origen : </th>
					<td class="borde-abajo" width="25%"> {{ @$data->tickets->id }} </td>
					<th class="text-right" width="15%"> Servicio : </th>
					<td class="borde-abajo" width="45%"> {{ @$data->tickets->servicios->Codigo }} </td>
				</tr>
				<tr>
					<th class="text-right"> Tipo Ticket : </th>
					<td class="borde-abajo"> {{ @$data->tickets->ticket_tipo->nombre }} </td>
					<th class="text-right"> Cliente : </th>
					<td class="borde-abajo"> {{ @$data->tickets->clientes->nombre }} </td>
				</tr>
				<tr>
					<th class="text-right"> Area : </th>
					<td class="borde-abajo"> {{ @$data->tickets->ticket_area->nombre }} </td>
					<th class="text-right"> Estación : </th>
					<td class="borde-abajo"> {{ @$data->tickets->servicios->estaciones->nombre }} </td>
				</tr>
				<tr>
					<th class="text-right"> Fecha Visita : </th>
					<td class="borde-abajo"> {{ @$data->tickets->fecha_visita }} </td>
					<th class="text-right"> Técnico : </th>
					<td class="borde-abajo"> {{ @$data->tickets->tecnico->nombre }} </td>
				</tr>
				<tr>
					<th class="text-right"> Observaciones : </th>
					<td class="borde-abajo" colspan="3"> {{ @$data->descripcion }} </td>
				</tr>
			</thead>
		</table>
		@php ($cont = 0)
		<br>

		<h4 class="text-left"> Trabajos a realizar:</h4>

		<table class="bord3s sin-top" width="100%">
			<tbody>
				@foreach($ot_detalle as $fila)
				<tr>
					<td>
						- {{$fila->nombre}}
					</td>
				</tr>
				@php ($cont++)
				@endforeach
			</tbody>
		</table>

		<h4 class="text-left"> Equipos a utilizar:</h4>

		<table class="bord3s sin-top" width="100%">
			<tbody>
				@foreach($ot_equipos as $fila)
				<tr>
					<td>
						-{{ @$fila->cantidad}} {{ @$fila->equipos->marca->nombre}} {{@$fila->equipos->modelo->nombre}}
					</td>
				</tr>
				@php ($cont++)
				@endforeach
			</tbody>
		</table>

		@for ($x = $cont; $x < 20; $x++)
			<br> &nbsp;
		@endfor
		<table width="100%">
			<tbody>
				<tr>
					<td width="30%"> </td> 
					<td width="20%" class="borde-top"> Firma Persona 01 </td>
					<td width="30%"> </td> 
					<td width="20%" class="borde-top"> Firma Persona 02 </td></tr>
			</tbody>
		</table>
		<br> &nbsp;
		<br> &nbsp;
		<table>
			<tbody>
				<tr>
					<td> <small> Fecha impresión : {{ date("d-m-Y") }} </small> </td>
					<td> <small> - Fecha creación OT: {{ @date("d-m-Y", @strtotime($data->created_at)) }} </small> </td>
					<td> <small> - Fecha ult. modificación : {{ @date("d-m-Y", @strtotime($data->updated_at)) }} </small> </td>
				</tr>
			</tbody>
		</table>



	</body>
</html>