<div class="modal fade" role="dialog" id="modal-cierre-ot">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Cerrar OT <span class="id_tkt"></span> -> Ticket <span class="id_tkt"></span> </h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="form-group ">
							<strong> * Fecha LLegada:</strong>
							<input type="date" name="fecha_llegada" id="fecha_llegada" class="form-control" value="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-2 col-md-2">
						<div class="form-group ">
							<strong> * Hora LL.:</strong>
							<input type="time" name="hora_llegada" id="hora_llegada" class="form-control" value="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="form-group ">
							<strong> * Fecha Salida:</strong>
							<input type="date" name="fecha_salida" id="fecha_salida" class="form-control" value="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-2 col-md-2">
						<div class="form-group ">
							<strong> * Hora S.:</strong>
							<input type="time" name="hora_salida" id="hora_salida" class="form-control" value="">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group ">
							<strong>* Observaciones:</strong>
							<input type="text" name="observaciones" id="observaciones" class="form-control" value="">
							<input type="hidden" name="ot_id" id="nro_ot" value="">
							<input type="hidden" name="ticket_id" id="id_tkt" value="">
						</div>
					</div>

					<div class="col-xs-4 col-sm-3 col-md-2 text-center">
						<button type="button" id="graba-cierre" class="btn btn-primary">Grabar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>