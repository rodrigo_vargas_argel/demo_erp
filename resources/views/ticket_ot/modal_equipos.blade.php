<div class="modal fade" role="dialog" id="modal-equipos">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Equipos incluídos en OT <span class="nro_ot"></span> </h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="dataEquipos">
						<table class="table table-responsive table-striped" id="tabla_ver_equipos" width="100%">
						    <thead>
						        <tr>
						            <th style="width:10% !important">Cant</th>
						            <th style="width:75% !important">Equipo</th>
						            <th style="width:15% !important">Recibido</th>
						        </tr>
						    </thead>
						    <tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>