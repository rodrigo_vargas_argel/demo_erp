<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header navbar navbar-dark bg-dar">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class=" dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold"><i class="fa fa-user-circle-o"></i> {{@Auth::user()->nombre}} {{@Auth::user()->email}}</strong>
                            </span> <span class="text-muted text-xs block"><i class="fa fa-arrow-circle-o-down"></i> <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li> <a href="{{ route('logout') }}"> <i class="fa fa-sign-out"></i> Log out</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img src="{!! asset('img/logo_blanco.png') !!}">
                </div>
           <!-- <li>
                @if ((@Auth::user()->tipo_usuario ==1)|| (@Auth::user()->tipo_usuario ==0))
                    <a href="#" title="Factibilidades"><i class="fa fa-wifi"></i> <span class="nav-label">FACTIBILIDADES</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="{{ url('factibilidades') }}"><i class="glyphicon glyphicon-list-alt"> </i>Ver Listado  <span class="nav-label"></span> </a>
                        </li>

                        <li>
                            <a href="{{ url('dashboard/factibilidades') }}"><i class="fa fa-dashboard"> </i>Dashboard  <span class="nav-label"></span> </a>
                        </li>
                    </ul>
                @endif
            </li>-->
           <!-- <li>

                    <a href="#" title="Tickets"><i class="fa fa-ticket"></i> <span class="nav-label">TICKETS</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="{{ route('tickets.create') }}"><i class="fa fa-plus-circle"> </i>Crear Ticket <span class="nav-label"></span> </a>
                        </li>
                        <li>
                            <a href="{{ route('tickets.index') }}"><i class="glyphicon glyphicon-list-alt"> </i>Lista <span class="nav-label"></span> </a>
                        </li>

                        <li>
                            <a href="{{ route('tickets.calendario') }}"><i class="glyphicon glyphicon-calendar"> </i>Calendario <span class="nav-label"></span> </a>
                        </li>
                        <li>
                            <a href="{{ url('/ots') }}"><i class="fa fa-th"> OTs</i> <span class="nav-label"></span> </a>
                        </li>

                    </ul>

            </li>-->
           <!-- <li>

                <a href="{--{ url('/') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Main view</span></a>
            </li>
            <li>

                <a href="{--{ url('/minor') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Minor view</span> </a>
            </li>-->
            @if ((@Auth::user()->tipo_usuario ==1)|| (@Auth::user()->tipo_usuario ==0)|| (@Auth::user()->tipo_usuario ==2)|| (@Auth::user()->tipo_usuario ==4))
                <li>
                    <a href="#" title="Clientes"><i class="fa fa-group"></i> <span  class="nav-label">CLIENTES </span> <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="{{ url('/clientes') }}" title="Ver Clientes"><i class="fa fa-eye"> Ver Clientes</i> <span class="nav-label"> </span> </a>
                        </li>

                        @if ((@Auth::user()->tipo_usuario ==1)|| (@Auth::user()->tipo_usuario ==0))
                             <li>
                                 <a href="{{ route('clientes.create') }}"><i class="fa fa-group"> Crear Cliente</i> <span class="nav-label"></span> </a>
                             </li>
                         @endif

                         <li>
                             <a href="{{ url('/facturacion/nota_de_venta') }}" id="toggleSpinners"><i class="glyphicon glyphicon-list-alt"> </i> Facturar Cliente <span class="nav-label"></span> </a>
                         </li>

                        <!--

                         <li>
                             <a href="{{ route('servicios.index') }}"><i class="fa fa-eye"> Ver Servicios</i> <span class="nav-label"></span> </a>
                         </li>
                         @if ((@Auth::user()->tipo_usuario ==1)|| (@Auth::user()->tipo_usuario ==0))
                             <li>
                                 <a href="{{ route('servicios.create') }}"><i class="fa fa-share"> Crear Servicio</i> <span class="nav-label"></span> </a>
                             </li>
                         @endif
                         -->

                       </ul>
                 </li>
            @endif

            <!--
            <li>
                @if ((@Auth::user()->tipo_usuario ==1)|| (@Auth::user()->tipo_usuario ==0) || (@Auth::user()->tipo_usuario ==4) )
                    <a href="#" title="Facturacion"><i class="fa fa-dollar"></i> <span class="nav-label">FACTURACION</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level collapse">
                        @if (@Auth::user()->tipo_usuario !=4)
                            <li>
                                <a href="{{ url('/facturacion/nota_de_venta_nuevo') }}" id="toggleSpinners"><i class="fa fa-plus-circle"> </i>DTE Manual (NV) <span class="nav-label"></span> </a>
                            </li>
                            <li>
                                <a href="{{ url('/facturacion/nota_de_venta') }}" id="toggleSpinners"><i class="glyphicon glyphicon-list-alt"> </i> Emision/Lista DTE Manual <span class="nav-label"></span> </a>
                            </li>
                            <li>
                                <a href="{{ url('/facturacion/masiva') }}" class="text-info"><i class="fa fa-dollar text-info"> </i> DTE Automatica (Masiva) </a>
                            </li>
                        @endif
                        <li>

                            <a href="{{ url('/facturacion/facturas') }}" id="toggleSpinners2" class="text-success"><i class="fa fa-files-o text-success"> </i> Gestión de DTE Emitidos</span> </a>
                        </li>

                                @if (@Auth::user()->tipo_usuario !=4 )
                                     <li>

                                        <a href="{{ url('/facturacion/deudas') }}" onclick="$('#toggleSpinners').click();"><i class="fa fa-bell"> </i>Cobranzas <span class="nav-label"></span> </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/facturacion/rpt_pagos') }}"><i class="fa fa-money"> </i>Ver Pagos </a>
                                    </li>
                                @endif

                    </ul>

                @endif
            </li>
            -->
            @if ((@Auth::user()->tipo_usuario ==1) || (@Auth::user()->tipo_usuario ==2)|| (@Auth::user()->tipo_usuario ==0)|| (@Auth::user()->tipo_usuario ==3))
<li>
<a href="#" title="Inventarios"><i class="fa fa-barcode"></i> <span class="nav-label">INVENTARIOS</span> <span class="fa arrow"></span></a>
<ul class="nav nav-second-level collapse">
@if (@Auth::user()->tipo_usuario !=3)
<!--<li>
   <a href="{{ url('/inventarios/compras') }}"><i class="fa fa-shopping-cart"> Compras</i> <span class="nav-label"> </span> </a>
<li>-->
<li>
   <a href="{{ url('/inventarios/existencias') }}"><i class="fa fa-binoculars"> STOCK</i> <span class="nav-label"> </span> </a>
<li>
<!--
<li>
   <a href="{{ url('/inventarios/traslados') }}"><i class="fa fa-exchange"> Traslados</i> <span class="nav-label"> </span> </a>
<li>
-->
<li>
   <a href="{{ url('/inventarios/rma') }}"><i class="fa fa-retweet">A Servicio Técnico</i> <span class="nav-label"> </span> </a>
<li>

        <li>
            <a href="{{ url('/inventarios/stock_inicial') }}"><i class="fa fa-check-square-o"> Stock Inicial</i> <span class="nav-label"> </span> </a>
        <li>
        <li>
            <a href="{{ url('/inventarios/devolucion_equipo') }}"><i class="fa fa-backward text-danger"></i><text class="text-primary" > Retorno de Maquinaria</text> <span class="nav-label"> </span> </a>
        <li>
@endif
      <li>
<a href="{{ url('/inventarios/entrega_equipo') }}"><i class="fa fa-forward text-info"></i> Entrega de Maquinaria <span class="nav-label"> </span> </a>
<li>


</ul>
</li>
           @endif


<li>
<a href="#" title="Mantenedores"><i class="fa fa-gears"></i> <span class="nav-label">MANTENEDORES</span> <span class="fa arrow"></span></a>
<ul class="nav nav-second-level collapse">
@if ((@Auth::user()->tipo_usuario ==1) || (@Auth::user()->tipo_usuario ==2)|| (@Auth::user()->tipo_usuario ==0))
   @if(@Auth::user()->tipo_usuario ==1)
   <li>
       <a href="{{ url('/mantenedores/bodegas') }}"><i class="fa fa-archive"> Bodegas</i> <span class="nav-label"></span> </a>
   </li>
   <!--
   <li>
       <a href="{{ url('/mantenedores/costos') }}"><i class="fa fa-dollar"> Costos</i> <span class="nav-label"></span> </a>
   </li>
   <li>
       <a href="{{ url('/mantenedores/sites') }}"><i class="fa fa-map-marker"> Estaciones</i> <span class="nav-label"> </span> </a>
   </li>
-->

   <li>
       <a href="{{ url('/mantenedores/proveedores') }}"><i class="fa fa-dropbox"> Proveedores</i> <span class="nav-label"></span> </a>
   </li>
    <!--
   <li>
       <a href="{{ url('/correos/create') }}"><i class="fa fa-envelope"> Envío de Correos</i> <span class="nav-label"></span> </a>
   </li>
   <li>
       <a href="{{ url('/planes') }}"><i class="fa fa-dropbox"> Planes</i> <span class="nav-label"></span> </a>
   </li>
   <li>
       <a href="{{ url('/logs/eventos') }}"><i class="fa fa-eye"> Log de Eventos</i> <span class="nav-label"></span> </a>
   </li> -->
   @endif


   <li>
       <a href="{{ url('/mantenedores/marcas') }}"><i class="fa fa-th-list"> Marcas</i> <span class="nav-label"></span> </a>
   </li>
   <li>
       <a href="{{ url('/mantenedores/modelos') }}"><i class="fa fa-share-alt"> Modelos</i> <span class="nav-label"></span> </a>
   </li>

   <li>
       <a href="{{ url('/mantenedores/productos') }}"><i class="fa fa-shopping-bag"> Maquinaria</i> <span class="nav-label"></span> </a>
   </li>
 @endif


</ul>
</li>


</ul>

</div>
</nav>
