
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{!! asset('img/logoico.png') !!}"/>
    <title>TELEDATA ERP - @yield('title') </title>

    <link href="{!! asset('css/normaliza_css.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/bootstrap.css') !!}" rel="stylesheet">

  <!-- <link href="{--!! asset('css/nifty.min.css') !!}" rel="stylesheet">
    <link href="{--!! asset('css/demo/nifty-demo-icons.min.css') !!}" rel="stylesheet">-->
    <link href="{!! asset('plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
    <!--<link href="{--!! asset('css/themes/type-a/theme-dark.min.css') !!}" rel="stylesheet">
    <link href="{--!! asset('plugins/datatables/media/css/dataTables.bootstrap.css') !!}" rel="stylesheet">-->
    <link href="{!! asset('css/plugins/datatables/datatables.min.css') !!}" rel="stylesheet">

    <link href="{!! asset('plugins/bootstrap-select/bootstrap-select.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('plugins/pace/pace.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet">
    <link href="{!! asset('plugins/magic-check/css/magic-check.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/teledata.css') !!}" rel="stylesheet">

    <link href="{!! asset('css/plugins/footable/footable.core.css') !!}" rel="stylesheet"> <!-- tabla con despliegue de detalle se usa en modulo de compras para ver las MAC de los routers q se compran, tambie esta en traslados.-->
    <link href="{!! asset('css/plugins/ladda/ladda-themeless.min.css') !!}" rel="stylesheet"> <!--boton spinner-->
    <link href="{!! asset('css/plugins/toastr/toastr.min.css') !!}" rel="stylesheet"> <!-- spinners-->
    <link href="{!! asset('css/plugins/switchery/switchery.css') !!}" rel="stylesheet">


    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/textSpinners/spinners.css') !!}">
    <link href="{!! asset('css/animate.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">


    @yield('scriptshead')
</head>
<body class="md-skin body-small">

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" >

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Main view  -->
            <div class="ibox" id="ibox1">

                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave sk-loading">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                     @yield('content')
                </div>
            </div>


            <!-- Footer -->


        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

  <!--<script src="{--!! asset('js/app.js') !!}" type="text/javascript"></script>-->


  <!-- Mainly scripts -->
  <script type="text/javascript">
    var path_base = "{{env('MIX_URL')}}";
    var center
    var mapOptions
    var map
    var mapCenter
  </script>
  <script src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
  <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
  <script src="{!! asset('js/plugins/metisMenu/jquery.metisMenu.js') !!}"></script>
  <script src="{!! asset('js/plugins/slimscroll/jquery.slimscroll.min.js') !!}"></script>

  <script src="{!! asset('js/nifty.min.js') !!}"></script>
  <script src="{!! asset('plugins/bootstrap-select/bootstrap-select.min.js') !!}"></script>
  <script src="{!! asset('plugins/bootbox/bootbox.min.js') !!}"></script>
  <!--<script src="{--!! asset('plugins/datatables/media/js/jquery.dataTables.js') !!}"></script>-->
  <script src="{!! asset('js/plugins/datatables/datatables.min.js') !!}"></script>


  <!--<script src="{!! asset('js/methods_global/methods.js') !!}"></script>-->


  <script src="{!! asset('plugins/jquery-mask/jquery.mask.min.js') !!}"></script>
  <script src="{!! asset('plugins/numbers/jquery.number.js') !!}"></script>
  <!--<script src="{--!! asset('/plugins/pace/pace.min.js') !!}"></script>-->
  <script src="{!! asset('plugins/sweetalert/sweetalert.min.js') !!}"></script>

  <script src="{!! asset('js/plugins/footable/footable.all.min.js') !!}"></script> <!-- Tabla con despliegue de detalles-->
  <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}"></script> <!-- Validador de form directo-->

  <!-- Ladda   para botones con spinners-->
  <script src="{!! asset('js/plugins/ladda/spin.min.js') !!}"></script>
  <script src="{!! asset('js/plugins/ladda/ladda.min.js') !!}"></script>
  <script src="{!! asset('js/plugins/ladda/ladda.jquery.min.js') !!}"></script>

  <!-- Idle Timer plugin -->
  <script src="{!! asset('js/plugins/idle-timer/idle-timer.min.js') !!}"></script> <!-- timeout -->

  <!-- Custom and plugin javascript -->
  <script src="{!! asset('js/inspinia.js') !!}"></script>
  <script src="{!! asset('js/plugins/pace/pace.min.js') !!}"></script>

  <!-- switchery -->
  <script src="{!! asset('js/plugins/switchery/switchery.js') !!}"></script>

  <script src="{!! asset('js/plugins/select2/select2.full.min.js') !!}"></script>




@section('scripts')
    <script>
        /*function formatcurrency(n) {
            return n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        }*/

        //************ TIMEOUT DE SESION DE USUARIO, 60 MINUTOS, IGUAL QUE config/session.php ..lifetime

        $(document).ready(function () {

            // Set idle time
            $( document ).idleTimer( 3600000 ); //una hora

            $('#toggleSpinners').on('click', function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');


            })

            $('#toggleSpinners2').on('click', function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');


            })

            // esto hace que el menú lateral inicialice minimizado
             $('.navbar-minimalize').trigger("click");

        });

        $( document ).on( "idle.idleTimer", function(event, elem, obj){
            swal({
                    title: "¡TU SESIÓN HA CADUCADO!",
                    text: "Ya ha pasado mas de una hora, tendrás que volver al logIn!",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK!",
                    cancelButtonText: "Ir al LogIn",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "{{ url('logout') }}";
                    }



                });

        });

        /*$( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
            // function you want to fire when the user becomes active again
            alert('Bravo! te moviste al fin !!!!');


        });*/

        $(function(){



        })
    </script>
@yield('mijava')

@show

</body>
</html>
