@extends('layouts.app')
@section('scriptshead')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/vue-material/dist/vue-material.min.css">
    <link rel="stylesheet" href="https://unpkg.com/vue-material/dist/theme/default.css">
@endsection
@section('content')
<div id="app">
    <tabla-factibilidad></tabla-factibilidad>
</div>
<script src="{{ asset('js/app.js') }}"></script>
@endsection()