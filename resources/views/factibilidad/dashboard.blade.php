@extends('layouts.app')

@section('content')

        <!DOCTYPE html>
<html>
<head>

</head>
<body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="card-header"></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                    </div>



                </div>
            </div>
        </div>
        <h3><img src="{!! asset('img/logo_ico.png') !!}">  DASHBOARD - Tabla Resumen</h3>


        <hr>
        <div class="modal inmodal " id="modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <h2> <i class="fa fa-eye"> Detalle de Servicios</i></h2>
                    </div>
                    <div class="modal-body">
                        <div> <strong>Tabla de servicios activados 2020</strong><span class="pull-right"><button onclick="exportTableToExcel('tblData2')" class="text-info"><i class="fa fa-file-excel-o"></i> Excel</button>  </span></div>
                        <table border="1" class="table table-bordered" id="tblData2">
                            <thead>
                                <th>Contador</th>
                                <th>Cliente</th>
                                <th>Codigo</th>
                                <th>Tipo</th>
                                <th>Plan</th>
                                <th>Renta Mensual</th>
                                <th>Creado</th>
                                <th>Instalado</th>
                                <th>Estado</th>
                            </thead>
                            <tbody>
                            @foreach($sabana as $index=>$activados)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{$activados->clientes->nombre}}</td>
                                    <td>{{$activados->Codigo}}</td>
                                    <td>{{$activados->tipo_servicios->servicio}}</td>
                                    <td>{{$activados->plan_nombre}}</td>
                                    <td>{{$activados->Valor}}</td>
                                    <td>{{$activados->create_at}}</td>
                                    <td>{{$activados->FechaInstalacion}}</td>
                                    <td>{{$activados->estado_servicios->nombre}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-lg-7">
            <div class="table-responsive">
                <div><strong>Cantidad Mensual de Factibilidades según estado</strong><span class="pull-right"><button onclick="exportTableToExcel('tblnum')" class="text-info"><i class="fa fa-file-excel-o"></i> Excel</button></div>
                <table border="1" id="tblnum" class="table table-striped table-bordered">

                    <thead>
                    <th>Estado/MES</th>
                    <th>Ene</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Abr</th>
                    <th>May</th>
                    <th>Jun</th>
                    <th>Jul</th>
                    <th>Ago</th>
                    <th>Sep</th>
                    <th>Oct</th>
                    <th>Nov</th>
                    <th>Dic</th>
                    </thead>
                    <tbody>
                    @foreach($estados as $estado)
                        <?php $bandera=0;
                        $contador_columna=1;
                        $ene=0;
                        $feb=0;
                        $mar=0;
                        $abr=0;
                        $may=0;
                        $jun=0;
                        $jul=0;
                        $ago=0;
                        $sep=0;
                        $oct=0;
                        $nov=0;
                        $dic=0;

                        ?>
                        <tr>
                            <td>{{$estado->estado}}</td>
                        @foreach($sql as $index=>$consulta)

                               @if($estado->id==$consulta->estados_id)
                                    <?php
                                    if ($bandera==0){
                                        for ($i=1; $i<$consulta->mes; $i++){
                                            echo '<td></td>';
                                            $contador_columna++;
                                        }
                                        $bandera=1;
                                    }
                                    ?>

                                 <td >{{$consulta->total}} </td>
                                 <?php $contador_columna++;?>
                               @endif


                        @endforeach
                            <?php

                            if ($contador_columna<12){
                                for($i=$contador_columna;$i<=12;$i++){
                                    echo '<td></td>';
                                }
                            }
                            ?>

                        </tr>
                    @endforeach
                    <tr>
                        @foreach($sql as $index=>$consulta)
                            <?php
                            if ($consulta->mes == 1)
                                $ene=$ene+$consulta->total;
                            if ($consulta->mes == 2)
                                $feb=$feb+$consulta->total;
                            if ($consulta->mes == 3)
                                $mar=$mar+$consulta->total;
                            if ($consulta->mes == 4)
                               $abr=$abr+$consulta->total;

                            if ($consulta->mes == 5)
                                $may=$may+$consulta->total;
                            if ($consulta->mes == 6)
                                $jun=$jun+$consulta->total;
                            if ($consulta->mes == 7)
                                $jul=$jul+$consulta->total;
                            if ($consulta->mes == 8)
                                $ago=$ago+$consulta->total;

                            if ($consulta->mes == 9)
                                $sep=$sep+$consulta->total;
                            if ($consulta->mes == 10)
                                $oct=$oct+$consulta->total;
                            if ($consulta->mes == 11)
                                $nov=$nov+$consulta->total;
                            if ($consulta->mes == 12)
                                $dic=$dic+$consulta->total;
                            ?>
                        @endforeach
                        <td>Total </td><td>{{$ene}}</td><td>{{$feb}}</td><td>{{$mar}}</td><td>{{$abr}}</td><td>{{$may}}</td><td>{{$jun}}</td><td>{{$jul}}</td><td>{{$ago}}</td><td>{{$sep}}</td><td>{{$oct}}</td><td>{{$nov}}</td><td>{{$dic}}</td>
                    </tr>
                    </tbody>


                </table>
             </div>
           </div>
            <div class="col-lg-5">
                <div class="table-responsive">
                    <div><strong>Cantidad Mensual de Factibilidades a Servicio Activo</strong><span class="pull-right"><button onclick="exportTableToExcel('tblsrv')" class="text-info"><i class="fa fa-file-excel-o"></i> Excel</button>
                    <a href="#" class="open-Modal"  title="Ver Detalle" data-toggle="modal" data-target="#modal" ><i class="fa fa-eye"> </i></a></div>
                    <table id="tblsrv" border="1" title="Cantidad Mensual de Factibilidades a Servicio Activo" class="table table-bordered">
                        <thead>
                        <th>Mes</th>
                        <th>Cantidad</th>
                        </thead>
                        <tbody>
                        <?php
                        $sumCantidad=0;
                        ?>
                        @foreach($cont_servicios as $cont)
                            <?php
                            if ($cont->mes == 1)
                                $nomes="Enero";
                            if ($cont->mes == 2)
                                $nomes="Febrero";
                            if ($cont->mes == 3)
                                $nomes="Marzo";
                            if ($cont->mes == 4)
                                $nomes="Abril";

                            if ($cont->mes == 5)
                                $nomes="Mayo";
                            if ($cont->mes == 6)
                                $nomes="Junio";
                            if ($cont->mes == 7)
                                $nomes="Julio";
                            if ($cont->mes == 8)
                                $nomes="Agosto";

                            if ($cont->mes == 9)
                                $nomes="Septiembre";
                            if ($cont->mes == 10)
                                $nomes="Octubre";
                            if ($cont->mes == 11)
                                $nomes="Noviembre";
                            if ($cont->mes == 12)
                                $nomes="Diciembre";
                            ?>

                            <tr>
                                <td>{{$nomes}}</td>
                                <td>{{$cont->cantidad}}</td>
                            </tr>

                            <?php
                            $sumCantidad=$sumCantidad+$cont->cantidad;
                            ?>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <th>
                            Total
                        </th>
                        <th>
                            {{$sumCantidad}}
                        </th>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-7">
                <div class="table-responsive">
                    <div> <strong>Proporción Mensual de Factibilidades según estado</strong><span class="pull-right"><button onclick="exportTableToExcel('tblData')" class="text-info"><i class="fa fa-file-excel-o"></i> Excel</button>  </span></div>
                    <table  id="tblData" class="table table-striped table-bordered " border="1">
                        <thead>
                        <th>Estado/MES</th>
                        <th>Ene</th>
                        <th>Feb</th>
                        <th>Mar</th>
                        <th>Abr</th>
                        <th>May</th>
                        <th>Jun</th>
                        <th>Jul</th>
                        <th>Ago</th>
                        <th>Sep</th>
                        <th>Oct</th>
                        <th>Nov</th>
                        <th>Dic</th>
                        </thead>
                        <tbody>
                        @foreach($estados as $estado)
                            <?php $bandera=0;
                            $contador_columna=1;
                            $porc=0;


                            ?>
                            <tr>
                                <td>{{$estado->estado}}</td>
                                @foreach($sql as $index=>$consulta)


                                    @if($estado->id==$consulta->estados_id)
                                        <?php
                                        if ($bandera==0){
                                            for ($i=1; $i<$consulta->mes; $i++){
                                                echo '<td></td>';
                                                $contador_columna++;
                                            }
                                            $bandera=1;
                                        }
                                        ?>
                                        <?php
                                        if ($consulta->mes == 1)
                                            $porc=($consulta->total/$ene)*100;

                                        if ($consulta->mes == 2)
                                            $porc=($consulta->total/$feb)*100;
                                        if ($consulta->mes == 3)
                                            $porc=($consulta->total/$mar)*100;
                                        if ($consulta->mes == 4)
                                            $porc=($consulta->total/$abr)*100;

                                        if ($consulta->mes == 5)
                                            $porc=($consulta->total/$may)*100;
                                        if ($consulta->mes == 6)
                                            $porc=($consulta->total/$jun)*100;
                                        if ($consulta->mes == 7)
                                            $porc=($consulta->total/$jul)*100;
                                        if ($consulta->mes == 8)
                                            $porc=($consulta->total/$ago)*100;

                                        if ($consulta->mes == 9)
                                            $porc=($consulta->total/$sep)*100;
                                        if ($consulta->mes == 10)
                                            $porc=($consulta->total/$oct)*100;
                                        if ($consulta->mes == 11)
                                            $porc=($consulta->total/$nov)*100;
                                        if ($consulta->mes == 12)
                                            $porc=($consulta->total/$dic)*100;
                                        ?>
                                        <td >{{number_format($porc,0,',','.')}}%</td>
                                        <?php $contador_columna++;?>
                                    @endif


                                @endforeach
                                <?php

                                if ($contador_columna<12){
                                    for($i=$contador_columna;$i<=12;$i++){
                                        echo '<td></td>';
                                    }
                                }
                                ?>

                            </tr>
                        @endforeach
                        <tr>
                            @foreach($sql as $index=>$consulta)

                            @endforeach
                            <td> </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                        </tr>
                        </tbody>


                    </table>
                </div>

            </div>
            <div class="col-lg-5">
                <div class="table-responsive">
                    <div> <strong>Tasa de Conversión Mensual de Factibilidades</strong><span class="pull-right"><button onclick="exportTableToExcel('tblconversion')" class="text-info"><i class="fa fa-file-excel-o"></i> Excel</button>  </span></div>
                    <table border="1" class="table table-bordered" id="tblconversion">
                        <thead>
                        <th>Mes</th>
                        <th>Indicador</th>
                        </thead>
                        <tbody>
                        <tr><td>Enero</td><td>@if ($ene > 0) {{number_format((@$cont_servicios[0]->cantidad/$ene) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Febrero</td><td>@if ($feb > 0) {{number_format((@$cont_servicios[1]->cantidad/$feb) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Marzo</td><td>@if ($mar > 0) {{number_format((@$cont_servicios[2]->cantidad/$mar) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Abril</td><td>@if ($abr > 0) {{number_format((@$cont_servicios[3]->cantidad/$abr) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Mayo</td><td>@if ($may > 0) {{number_format((@$cont_servicios[4]->cantidad/$may) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Junio</td><td>@if ($jun > 0) {{number_format((@$cont_servicios[5]->cantidad/$jun) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Julio</td><td>@if ($jul> 0) {{number_format((@$cont_servicios[6]->cantidad/$jul) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Agosto</td><td>@if ($ago > 0) {{number_format((@$cont_servicios[7]->cantidad/$ago) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Septiembre</td><td>@if ($sep > 0) {{number_format((@$cont_servicios[8]->cantidad/$sep) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Octubre</td><td>@if ($oct > 0) {{number_format((@$cont_servicios[9]->cantidad/$oct) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Noviembre</td><td>@if ($nov > 0) {{number_format((@$cont_servicios[10]->cantidad/$nov) * 100,0,',','.')}}% @endif</td></tr>
                        <tr><td>Diciembre</td><td>@if ($dic > 0) {{number_format((@$cont_servicios[11]->cantidad/$dic) * 100,0,',','.')}}% @endif</td></tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>




</body>
</html>
@endsection
@section('mijava')
    <script>

        $(document).ready(function(){



        });




        function exportTableToExcel(tableID){

            var  filename = 'tabla';
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

            // Specify file name
            filename = filename?filename+'.xls':'excel_data.xls';

            // Create download link element
            downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                // Setting the file name
                downloadLink.download = filename;

                //triggering the function
                downloadLink.click();
            }
        }
     </script>
@endsection
