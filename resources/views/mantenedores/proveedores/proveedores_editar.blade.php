@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Proveedor</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Proveedor <small> {{$consulta[0]->nombre}} </small></h5>
                       
                    </div>

                    <div>
                            @if (count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmProveedor" method="post" action="{{ url('mantenedores/proveedores_modificar') }}">

                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label >Nombre</label>
                                        <input type="hidden" name="id" value="{{$consulta[0]->id}}">
                                        <input type="text" name="nombre" class="form-control" value="{{$consulta[0]->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Dirección</label>
                                        <input type="text" name="direccion" class="form-control" value="{{$consulta[0]->direccion}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Télefono</label>
                                        <input type="text" name="telefono" class="form-control" value="{{$consulta[0]->telefono}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Contacto</label>
                                        <input type="text" name="contacto" class="form-control" value="{{$consulta[0]->contacto}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Correo</label>
                                        <input type="text" name="correo" class="form-control" value="{{$consulta[0]->correo}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Rut</label>
                                        <input type="text" name="rut" class="form-control" value="{{$consulta[0]->rut}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Dv</label>
                                        <input type="text" name="dv" class="form-control" value="{{$consulta[0]->dv}}">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick="frmProveedor.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>   

                    
                </div>
            </div>
        </div>
    </div>
@endsection
