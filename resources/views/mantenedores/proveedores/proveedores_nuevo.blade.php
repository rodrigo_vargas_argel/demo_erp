@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nuevo Proveedor</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Crear Nuevo Proveedor <small> </small></h5>

                        </div>

                        <div>
                            @if (count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>

                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <form name="frmProveedor" method="post" action="{{ url('mantenedores/proveedores_guardar') }}">

                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label for="">Nombre</label>
                                                <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Dirección</label>
                                                <input type="text" name="direccion" class="form-control" value="{{old('direccion')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Télefono</label>
                                                <input type="text" name="telefono" class="form-control" value="{{old('telefono')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Contacto</label>
                                                <input type="text" name="contacto" class="form-control" value="{{old('contacto')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Correo</label>
                                                <input type="text" name="correo" class="form-control" value="{{old('correo')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Rut</label>
                                                <input type="number" name="rut" class="form-control" value="{{old('rut')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Dv</label>
                                                <input type="text" name="dv" class="form-control" value="{{old('dv')}}">
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit" onclick="frmProveedor.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                </div>
            </div>
        </div>
    </div>
@endsection
