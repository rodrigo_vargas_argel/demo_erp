@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nueva Ubicacion</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5>Crear Nueva Ubicacion <small> Bodegas, CC, Unidades de almacenaje</small></h5>

                            </div>

                                <div>
                                    @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                        <div class="ibox-content">
                            <div class="row">
                               <div class="col-lg-7">

                                    <form name="frmBodega" method="post" action="{{ url('mantenedores/bodegas_guardar') }}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Dirección</label>
                                            <input type="text" name="direccion" class="form-control" value="{{old('direccion')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Télefono</label>
                                            <input type="text" name="telefono" class="form-control" value="{{old('telefono')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Responsable</label>
                                            <select name="personal_id" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($responsables as $responsable )
                                                    <option value="{{$responsable->id}}" {{ old('personal_id') == $responsable->id ? 'selected' : '' }}>{{$responsable->nombre}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label >Correo</label>
                                            <input type="text" name="correo" class="form-control" value="{{old('correo')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tipo</label>
                                            <select name="principal" class="form-control">
                                                <option value="0" {{ old('principal') == 0 ? 'selected' : '' }}>Principal</option>
                                                <option value="1" {{ old('principal') == 1 ? 'selected' : '' }}>Estacion</option>
                                                <option value="2" {{ old('principal') == 2 ? 'selected' : '' }}>Movil (tecnico)</option>
                                            </select>

                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit" onclick="frmBodega.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </form>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }
    </script>
@endsection