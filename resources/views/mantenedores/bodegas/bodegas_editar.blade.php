@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Ubicacion</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Ubicaciones <small> Bodegas, CC, Unidades de almacenaje</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>


                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmBodega" method="post" action="{{ url('mantenedores/bodegas_modificar') }}">

                                    {{csrf_field()}}
                                    <div class="form-data">
                                        <label for="">Nombre</label>
                                        <input type="hidden" name="id" value="{{$consulta[0]->id}}">
                                        <input type="text" name="nombre" class="form-control" value="{{$consulta[0]->nombre}}">
                                    </div>
                                    <div class="form-data">
                                        <label for="">Dirección</label>
                                        <input type="text" name="direccion" class="form-control" value="{{$consulta[0]->direccion}}">
                                    </div>
                                    <div class="form-data">
                                        <label for="">Télefono</label>
                                        <input type="text" name="telefono" class="form-control" value="{{$consulta[0]->telefono}}">
                                    </div>
                                    <div class="form-data">
                                        <label for="">Persona Id</label>
                                        <select name="personal_id" class="form-control">
                                            <option value="">-Seleccione-</option>
                                            @foreach($responsables as $responsable )
                                                <option value="{{$responsable->id}}" {{ $consulta[0]->personal_id == $responsable->id ? 'selected' : '' }}>{{$responsable->nombre}}</option>
                                            @endforeach
                                        </select>
                                        <!--<input type="text" name="personal_id" class="form-control" value="{{--$consulta[0]->personal_id--}}">-->
                                    </div>
                                    <div class="form-data">
                                        <label for="">Correo</label>
                                        <input type="text" name="correo" class="form-control" value="{{$consulta[0]->correo}}">
                                    </div>
                                    <div class="form-data">
                                        <label for="">Principal</label>
                                        <select name="principal" class="form-control">
                                            <option value="0" {{ $consulta[0]->principal == 0 ? 'selected' : '' }}>Principal</option>
                                            <option value="1" {{ $consulta[0]->principal == 1 ? 'selected' : '' }}>Estacion</option>
                                            <option value="1" {{ $consulta[0]->principal == 2 ? 'selected' : '' }}>Movil (tecnico)</option>
                                        </select>
                                        <!--<input type="text" name="principal" class="form-control" value="{{--$consulta[0]->principal--}}">-->
                                    </div>
                                    <br>
                                    <div class="form-data">
                                        <button class="btn btn-primary" type="submit" onclick="frmBodega.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
