@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Envío de Correos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Envío de Correos <small> </small></h5>
                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmEditar" method="POST" action="{{ route('correos.update', $correo->id) }}">
                                    {{csrf_field()}}
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="">Motivo del correo</label>
                                        <select name="motivo_correos_id" class="form-control">
                                            <option value="">Seleccione...</option>
                                            @foreach ($motivos as $row )
                                                <option value="{{$row->id}}" {{ $correo->motivo_correos_id == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="">Usuario</label>
                                        <select name="usuarios_id" class="form-control">
                                            <option value="">Seleccione...</option>
                                            @foreach ($usuarios as $row )
                                                <option value="{{$row->id}}" {{ $correo->usuarios_id == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick="frmEditar.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }
    </script>
@endsection