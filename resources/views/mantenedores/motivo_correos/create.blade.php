@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Envío de Correos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="col-md-4 wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> Agregar Envío de Correos</h5>
                </div>
                <div>
                    @if (count($errors)>0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-11">

                            <form name="frmCrear" method="POST" action="{{ route('correos.store') }}">
                                {{csrf_field()}}
                                
                                <div class="form-group">
                                    <label for="">Motivo del correo</label>
                                    <select name="motivo_correos_id" class="form-control">
                                        <option value="">Seleccione...</option>
                                        @foreach ($motivos as $row )
                                            <option value="{{$row->id}}" {{ old('motivo_correos_id') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="">Usuario</label>
                                    <select name="usuarios_id" class="form-control">
                                        <option value="">Seleccione...</option>
                                        @foreach ($usuarios as $row )
                                            <option value="{{$row->id}}" {{ old('usuarios_id') == $row->id ? 'selected' : '' }}>{{$row->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit" onclick="frmCrear.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de envío de correo existentes </h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th width="25%">Motivo</th>
                                <th width="50%">Usuario</th>
                                <th width="15%">Creado</th>
                                <th width="4%" class="text-rigth"> Opc</th>
                                <th width="6%" class="text-left">iones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($correos as $row)
                                <tr>
                                    <td>
                                        {{@$row->motivo_correos->nombre}}
                                    </td>
                                    <td>
                                        {{@$row->usuarios->nombre}} - {{@$row->usuarios->email}}
                                    </td>
                                    <td>
                                        {{ (is_null($row->created_at) || ($row->created_at == "") ) ? '' : @date_format(date_create($row->created_at), 'd-m-Y') }}
                                    </td>
                                    <td>
                                        <a href="{{ url('/correos/'.$row->id.'/edit') }}" ><i class="fa fa-edit" title="Editar Registro"></i></a>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{route('correos.destroy',$row->id)}}" accept-charset="UTF-8" class="delete-row">
                                            {{csrf_field()}}
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-xs" title="Eliminar"><i class="text danger fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                autoWidth: false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Envío de Correos'},
                    {extend: 'pdf', title: 'Envío de Correos'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });

        });

        $(document).on('submit', '.delete-row', function(e){
            var form = this;
            e.preventDefault();
            swal({ 
                    title: "¿Seguro que deseas eliminar el envío de correo?", 
                    text: "No podrás deshacer este paso...", 
                    type: "warning", 
                    showCancelButton: true,
                    cancelButtonText: "Cancelar", 
                    cancelButtonColor: "#339933",
                    confirmButtonColor: "#DD6B55", 
                    confirmButtonText: "Continuar!", 
                    closeOnConfirm: false
                },
                function(isConfirm){
                    if(isConfirm){
                        form.submit();
                    }
                }
            );
        });
    </script>
@endsection