@extends('layouts.app')
@section('title')
    Planes
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Planes</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ route('planes.create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Planes Disponibles</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Velocidad Subida</th>
                                    <th>Velocidad Bajada</th>
                                    <th>Valor Referencial</th>
                                    <th>Tipo de Moneda</th>
                                    <th>Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($planes as $row)
                                    <tr>
                                        <td>
                                            {{$row->nombre}}
                                        </td>
                                        <td>
                                            {{$row->velocidad_subida}}
                                        </td>
                                        <td>
                                            {{$row->velocidad_bajada}}
                                        </td>
                                        <td>
                                            {{$row->valor_referencia}}
                                        </td>
                                        <td>
                                            {{$row->tipo_moneda}}
                                        </td>
                                        <td>
                                            <span class="col-md-4"><a href="{{ url('/planes/'.$row->id.'/edit') }}" ><i class="fa fa-edit" title="Editar Registro"></i></a></span>
                                            <span class="col-md-2"><bold>||</bold></span>
                                            <span class="col-md-4"><form method="POST" action="{{route('planes.destroy',$row->id)}}" accept-charset="UTF-8" class="delete-row">
                                                {{csrf_field()}}
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-xs" title="Eliminar"><i class="text danger fa fa-trash"></i></button>
                                            </form></span> 
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Planes de Teledata'},
                    {extend: 'pdf', title: 'Planes de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });

        });

        $(document).on('submit', '.delete-row', function(e){
        var form = this;
        e.preventDefault();
        swal({ 
                title: "¿Seguro que deseas eliminar el plan?", 
                text: "No podrás deshacer este paso...", 
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar", 
                cancelButtonColor: "#339933",
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "Continuar!", 
                closeOnConfirm: false
            },
            function(isConfirm){
                if(isConfirm){
                    form.submit();
                }
            }
        );
    });

    </script>
@endsection

