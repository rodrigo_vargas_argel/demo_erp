@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nuevo Plan</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5>Crear Nuevo Plan de datos</h5>

                            </div>

                                <div>
                                    @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                        <div class="ibox-content">
                            <div class="row">
                               <div class="col-lg-7">

                                    <form name="frmCrear" method="POST" action="{{ route('planes.store') }}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Velocidad Subida</label>
                                            <input type="text" name="velocidad_subida" class="form-control" value="{{old('velocidad_subida')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Velocidad Bajada</label>
                                            <input type="text" name="velocidad_bajada" class="form-control" value="{{old('velocidad_bajada')}}">
                                        </div>
                                        <div class="form-group">
                                            <label >Valor Referencial</label>
                                            <input type="text" name="valor_referencia" class="form-control" value="{{old('valor_referencia')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tipo de Moneda</label>
                                            <select name="tipo_moneda" class="form-control">
                                                <option value="UF" {{ old('tipo_moneda') == "UF" ? 'selected' : '' }}>UF</option>
                                                <option value="peso" {{ old('tipo_moneda') == "peso" ? 'selected' : '' }}>Peso</option>
                                            </select>

                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit" onclick="frmCrear.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </form>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }
    </script>
@endsection