@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Plan</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Plan <small> </small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmEditar" method="POST" action="{{ route('planes.update', $planes->id) }}">
                                    {{csrf_field()}}
                                    @method('PUT')
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="nombre" class="form-control" value="{{$planes->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Velocidad Subida</label>
                                        <input type="text" name="velocidad_subida" class="form-control" value="{{$planes->velocidad_subida}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Velocidad Bajada</label>
                                        <input type="text" name="velocidad_bajada" class="form-control" value="{{$planes->velocidad_bajada}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Valor Referencial</label>
                                        <input type="text" name="valor_referencia" class="form-control" value="{{$planes->valor_referencia}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tipo de Moneda</label>
                                        <select name="tipo_moneda" class="form-control">
                                            <option value="UF" {{ $planes->tipo_moneda == "UF" ? 'selected' : '' }}>UF</option>
                                            <option value="peso" {{ $planes->tipo_moneda == "peso" ? 'selected' : '' }}>Peso</option>
                                        </select>

                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick="frmEditar.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }
    </script>
@endsection