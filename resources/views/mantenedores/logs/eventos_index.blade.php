@extends('layouts.app')
@section('title')
    Log de Eventos
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Logs / Eventos</a>
                </li>
                <li class="active">
                    <strong>Log de Eventos</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Log de Eventos</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>TimeStamp_del_Evento</th>
                                    <th>Evento</th>
                                    <th>Usuario</th>
                                    <th>Cliente</th>
                                    <th>Descripción</th>
                                    <th>Servicio Detalle</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $row)
                                    <tr>
                                        <td>
                                            {{$row->id}}
                                        </td>
                                        <td>
                                            {{$row->created_at}}
                                        </td>

                                        <td>
                                            {{$row->tipo_evento}}
                                        </td>
                                        <td>
                                            {{@$row->usuarios->nombre}}
                                        </td>
                                        <td>
                                            {{@$row->servicio->clientes->nombre}}
                                        </td>
                                        <td>
                                            {{@$row->servicio->tipo_servicios->servicio}} {{@$row->servicio->Descripcion}}  {{@$row->servicio->Conexion}}
                                        </td>
                                        <td>
                                            {{@$row->detalle}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('mijava')
    <script>

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                "order": [[ 1, "desc" ]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Log de Eventos'},
                    {extend: 'pdf', title: 'Log de Eventos'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "language": {url: '{{url("/")}}/json/lenguaje_tata_table.json'},
            });

        });

    </script>
@endsection

