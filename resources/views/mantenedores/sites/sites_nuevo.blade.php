@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Radio Planing</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Agregar Estación <small> </small></h5>

                            </div>

                                <div>
                                    @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                        <div class="ibox-content">
                            <div class="row">
                               <div class="col-lg-12">

                                   <form name="frmSite"  id="storeEstacion" name="frmSite"  method="post" action="{{ url('mantenedores/sites_guardar') }}">
                                        {{csrf_field()}}
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nombre estación</label>
                                                <input type="text" name="nombre" class="form-control" placeholder="Nombre estación" value="{{old('nombre')}}">
                                            </div>
                                            <div class="form-group">
                                                <label >Dirección estación</label>
                                                <input type="text" name="direccion" class="form-control" placeholder="Dirección de la estación" value="{{old('direccion')}}">
                                            </div>
                                            <div class="form-group">
                                                <label >Télefono estación</label>
                                                <input type="text" name="telefono" class="form-control" placeholder="Télefono estación" value="{{old('telefono')}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Contacto estación</label>
                                                <input type="text" name="contacto" class="form-control" placeholder="Nombre contacto" value="{{old('contacto')}}">
                                            </div>
                                            <div class="form-group">
                                                <label >Correo</label>
                                                <input type="text" name="correo" class="form-control" placeholder="Email" value="{{old('correo')}}">
                                            </div>

                                            <div class="form-group">
                                                <label >Persona Id</label>
                                                <select name="personal_id" class="form-control">
                                                    <option value="">-Seleccione-</option>
                                                    @foreach($responsables as $responsable )
                                                        <option value="{{$responsable->id}}" {{ old('personal_id') == $responsable->id ? 'selected' : '' }}>{{$responsable->nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label >Dueño Cerro</label>
                                                <input type="text" name="dueno_cerro" class="form-control" placeholder="Dueño de cerro" value="{{old('dueno_cerro')}}">
                                            </div>
                                        </div>
                                       <div class="col-lg-12" style="padding-top:20px;"></div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label >Coordenadas</label>
                                                <input type="text" id="latitud_coordenada" name="latitud_coordenada" class="form-control input-sm coordenadas insert" placeholder="Ingrese latitud" value="{{old('latitud_coordenada')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="longitud_coordenada" name="longitud_coordenada" class="form-control coordenadas insert" placeholder="Ingrese longitud" value="{{old('longitud_coordenada')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div id="EstacionFormMap" style="height:350px; width:100%;"></div>
                                        </div>

                                       <div class="col-lg-12" style="padding-top:20px;"></div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="latitud_coordenada_site">Ingrese la latitud site</label>
                                                <input id="latitud_coordenada_site" name="latitud_coordenada_site" type="text" placeholder="Ingrese la latitud site" class="form-control input-sm coordenadas_site insert" data-nombre="Latitud" value="{{old('latitud_coordenada_site')}}" >
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="longitud_coordenada_site">&nbsp;Ingrese la longitud site</label>
                                                <input id="longitud_coordenada_site" name="longitud_coordenada_site" type="text" placeholder="Ingrese la longitud site" class="form-control input-sm coordenadas_site insert" data-nombre="Longitud" value="{{old('longitud_coordenada_site')}}">
                                            </div>
                                        </div>

                                        <div class="col-lg-8">
                                            <div id="EstacionFormSiteMap" style="height:350px; width:100%;"></div>
                                        </div>

                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label class="control-label" for="datos_proveedor_electrico">Datos Proveedor Eléctrico</label>
                                               <textarea id="datos_proveedor_electrico" name="datos_proveedor_electrico" rows="4" class="form-control" placeholder="Ingrese los datos del proveedor eléctrico" data-nombre="Datos Proveedor Eléctrico" value="{{old('datos_proveedor_electrico')}}"></textarea>
                                           </div>
                                       </div>

                                        <br>

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <button class="btn btn-primary pull-left" type="submit" onclick="frmSite.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
               return false;
            }
        }

        $(document).ready(function() {
            var mapOptions;
            var mapOptionsSite;
            google.maps.event.addDomListener(window, 'load', initialize)
            function initialize() {
                //mapa 1
                var latitudEdit = $('#latitud_coordenada').val()
                var longitudEdit = $('#longitud_coordenada').val();
                if(latitudEdit == ''){
                    $('#latitud_coordenada').val(-41.31398663408464)
                    latitudEdit = -41.31398663408464;
                }
                if(longitudEdit == ''){
                    $('#longitud_coordenada').val(-72.98765243141634);
                    longitudEdit = -72.98765243141634;
                }
                ResizeEdit(latitudEdit, longitudEdit)
                var centerEdit = new google.maps.LatLng(latitudEdit, longitudEdit);

                mapOptions = {
                    zoom: 19,
                    center: centerEdit,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                MapaEdit = new google.maps.Map(document.getElementById("EstacionFormMap"), mapOptions)
                markerEdit = new google.maps.Marker({
                    map: MapaEdit,
                    draggable: true,
                    position: centerEdit

                });

                google.maps.event.addListener(markerEdit, 'dragend', function(evt) {
                    $('#latitud_coordenada').val(evt.latLng.lat())
                    $('#longitud_coordenada').val(evt.latLng.lng())
                });


                //map 2
                var latitudEditSite = $('#latitud_coordenada_site').val()
                var longitudEditSite = $('#longitud_coordenada_site').val();

                if(latitudEditSite == ''){
                    $('#latitud_coordenada_site').val(-41.31398663408464)
                    latitudEditSite = -41.31398663408464;
                }
                if(longitudEditSite == ''){
                    $('#longitud_coordenada_site').val(-72.98765243141634);
                    longitudEditSite = -72.98765243141634;
                }

                ResizeEditSite(latitudEditSite, longitudEditSite)
                var centerEditSite = new google.maps.LatLng(latitudEditSite, longitudEditSite);

                mapOptionsSite = {
                    zoom: 19,
                    center: centerEditSite,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                MapaEditSite = new google.maps.Map(document.getElementById("EstacionFormSiteMap"), mapOptionsSite)
                markerEditSite = new google.maps.Marker({
                    map: MapaEditSite,
                    draggable: true,
                    position: centerEditSite

                });

                google.maps.event.addListener(markerEditSite, 'dragend', function(evt) {
                    $('#latitud_coordenada_site').val(evt.latLng.lat())
                    $('#longitud_coordenada_site').val(evt.latLng.lng())
                });
                //fin map 2
            }

            function validLatitude(lat) {
                return isFinite(lat) && Math.abs(lat) <= 90;
            }
            function validLongitude(lng) {
                return isFinite(lng) && Math.abs(lng) <= 180;
            }
            $(".coordenadas").on('blur', function() {

                var latitudEdit = $('#latitud_coordenada').val();
                var longitudEdit = $('#longitud_coordenada').val();

                if ($(this).attr('id') == 'latitud_coordenada' && latitudEdit) {
                    if (latitudEdit) {
                        if (!validLatitude(latitudEdit)) {
                            bootbox.alert("Ups! Debe ingresar una latitud valida");
                            $(this).val('')
                        }
                    }
                } else if ($(this).attr('id') == 'longitud_coordenada' && longitudEdit) {
                    if (!validLongitude(longitudEdit)) {
                        bootbox.alert("Ups! Debe ingresar una longitud valida");
                        $(this).val('')
                    }
                }

                if (latitudEdit && longitudEdit) {

                    ResizeEdit(latitudEdit, longitudEdit)
                }
            })
            //map 2
            $(".coordenadas_site").on('blur', function() {

                var latitudEditSite = $('#latitud_coordenada_site').val();
                var longitudEditSite = $('#longitud_coordenada_site').val();

                if ($(this).attr('id') == 'latitud_coordenada_site' && latitudEditSite) {
                    if (latitudEditSite) {
                        if (!validLatitude(latitudEditSite)) {
                            bootbox.alert("Ups! Debe ingresar una latitud valida");
                            $(this).val('')
                        }
                    }
                } else if ($(this).attr('id') == 'longitud_coordenada_site' && longitudEditSite) {
                    if (!validLongitude(longitudEditSite)) {
                        bootbox.alert("Ups! Debe ingresar una longitud valida");
                        $(this).val('')
                    }
                }

                if (latitudEditSite && longitudEditSite) {

                    ResizeEditSite(latitudEditSite, longitudEditSite)
                }
            })
            //fin map 2

            ResizeEdit = function (latitudEdit, longitudEdit){
                MapaEdit = new google.maps.Map(document.getElementById("EstacionFormMap"), mapOptions)
                mapCenterEdit = new google.maps.LatLng(latitudEdit, longitudEdit);

                markerEdit = new google.maps.Marker({
                    map: MapaEdit,
                    draggable: true,
                    position: mapCenterEdit

                });
                google.maps.event.addListener(markerEdit, 'dragend', function(evt) {
                    $('#latitud_coordenada').val(evt.latLng.lat())
                    $('#longitud_coordenada').val(evt.latLng.lng())
                });
                setTimeout(function() {
                    google.maps.event.trigger(MapaEdit, "resize");
                    MapaEdit.setCenter(mapCenterEdit);
                    MapaEdit.setZoom(MapaEdit.getZoom());

                    markerEdit.setOptions({position: mapCenterEdit});

                }, 1000)
            }

            //map 2
            ResizeEditSite = function (latitudEditSite, longitudEditSite){
                MapaEditSite = new google.maps.Map(document.getElementById("EstacionFormSiteMap"), mapOptionsSite)
                mapCenterEditSite = new google.maps.LatLng(latitudEditSite, longitudEditSite);

                markerEditSite = new google.maps.Marker({
                    map: MapaEditSite,
                    draggable: true,
                    position: mapCenterEditSite

                });
                google.maps.event.addListener(markerEditSite, 'dragend', function(evt) {
                    $('#longitud_coordenada_site').val(evt.latLng.lat())
                    $('#latitud_coordenada_site').val(evt.latLng.lng())
                });
                setTimeout(function() {
                    google.maps.event.trigger(MapaEditSite, "resize");
                    MapaEditSite.setCenter(mapCenterEditSite);
                    MapaEditSite.setZoom(MapaEditSite.getZoom());

                    markerEditSite.setOptions({position: mapCenterEditSite});

                }, 1000)
            }
            //fin map 2
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7_zeAQWpASmr8DYdsCq1PsLxLr5Ig0_8" type="text/javascript"></script>
@endsection