@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nuevo Modelo</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-shopping-bag"> </i>  Nuevo Modelo <small> </small></h5>
                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">

                                <form name="frmModelo" method="post" action="{{ url('mantenedores/modelos_guardar') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label >Marca Producto</label>
                                        <div class="input-group">
                                            <select name="marca" id="marca" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($marcas as $marca )
                                                    <option value="{{$marca->id}}" {{ old('marca') == $marca->id ? 'selected' : '' }}>{{$marca->nombre}}</option>
                                                @endforeach
                                            </select>

                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">
                                                    <i class="fa fa-shopping-bag"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Modelo</label>
                                        <input type="text" name="modelo" class="form-control" placeholder="Ingrese nombre del modelo" value="{{old('modelo')}}">
                                    </div>

                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <input type="text" name="descripcion" class="form-control" placeholder="Descripción del modelo" value="{{old('descripcion')}}">
                                    </div>

                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick="frmModelo.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- ****************************** -->
                </div>
                <!-- ****************************** -->
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
                return false;
            }
        }
    </script>
@endsection
