@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Modelo</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Modelo <small> </small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>


                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmModelo"  method="post" action="{{ url('mantenedores/modelos_modificar') }}">
                                    {{csrf_field()}}
                                    <div class="form-data">
                                        <label for="">Nombre Modelo</label>
                                        <input type="hidden" name="id" value="{{$consulta[0]->id}}">
                                        <input type="text" name="nombre" class="form-control" placeholder="Nombre modelo" value="{{$consulta[0]->nombre}}">
                                    </div>
                                    <div class="form-data">
                                        <label for="descripcion">Descripción</label>
                                        <input type="text" name="descripcion" class="form-control" placeholder="Descripción del modelo" value="{{$consulta[0]->descripcion}}">
                                    </div>
                                    <br>
                                    <div class="form-data">
                                        <button class="btn btn-primary pull-right" type="submit" onclick="frmModelo.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
                return false;
            }
        }
@endsection
