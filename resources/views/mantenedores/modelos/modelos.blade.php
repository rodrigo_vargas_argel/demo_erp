@extends('layouts.app')
@section('title')
    Modelos de Productos
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Modelos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/mantenedores/modelos_nuevo') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Modelos Disponibles</h5>

                    </div>
                    <div class="ibox-content">


                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Descripcion</th>
                                    <th>Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($consulta as $modelo)
                                    <tr>
                                        <td>
                                            {{$modelo->id}}
                                        </td>
                                        <td>
                                            @if($modelo->marca != null)
                                            {{$modelo->marca->nombre}}
                                            @endif
                                        </td>
                                        <td>
                                            {{$modelo->nombre}}
                                        </td>
                                        <td>
                                            {{$modelo->descripcion}}
                                        </td>
                                        <td>
                                            <a href="{{ url('/mantenedores/modelos_editar/'.$modelo->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                            <a class="del" id="{{$modelo->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        var exito
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Modelos de Teledata'},
                    {extend: 'pdf', title: 'Modelos de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

        $('.del').click(function () { //RV Dice: SWAL para eliminar informes

            var dis=this;

            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/mantenedores/modelos_eliminar/"+dis.id+"", function  (response) {

                            exito=response.Success;
                        });

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

    </script>
@endsection
