@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nueva Marca</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5>Crear Nueva Marca <small> </small></h5>

                            </div>

                                <div>
                                    @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                        <div class="ibox-content">
                            <div class="row">
                               <div class="col-lg-7">

                                    <form name="frmMarca" method="post" action="{{ url('mantenedores/marcas_guardar') }}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Nombre marca</label>
                                            <input type="text" name="nombre" class="form-control" placeholder="Nombre marca" value="{{old('nombre')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Descripción de la marca</label>
                                            <input type="text" name="descripcion" class="form-control" placeholder="Descripción de la marca" value="{{old('descripcion')}}">
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <button class="btn btn-primary pull-right" type="submit" onclick="frmMarca.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </form>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
