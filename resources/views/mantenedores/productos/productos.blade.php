@extends('layouts.app')
@section('title')
    Productos
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><i class=""></i> Demo ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Maquinarias </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/mantenedores/productos_nuevo') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Maquinarias Disponibles</h5>

                    </div>
                    <div class="ibox-content">


                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Codigo o Nombre</th>
                                    <th>Tipo Maquinaria</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Descripcion</th>
                                    <!--<th>Unico</th>-->
                                    <th>Precio</th>
                                    <th>Foto /Ficha</th>
                                    <th>Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($consulta as $producto)
                                    <tr>
                                        <td><small>
                                            {{@$producto->id}}
                                            </small>
                                        </td>
                                        <td><small>
                                            {{@$producto->codigo_barra}}
                                            </small>
                                        </td>
                                        <td><small>
                                            {{@$producto->tipo_producto->nombre}}
                                            </small>
                                        </td>
                                        <td><small>
                                            {{@$producto->marca->nombre}}
                                            </small>
                                        </td>
                                        <td><small>
                                            {{@$producto->modelo->nombre}}
                                            </small>
                                        </td>
                                        <td><small>
                                            {{@$producto->descripcion}}
                                            </small>
                                        </td>
                                       <!-- <td><small>
                                            {{@$producto->unico}}
                                            </small>
                                        </td>-->
                                        <td><small>
                                            {{@$producto->mantenedor_proveedores_id}}
                                            </small>
                                        </td>
                                        <td>
                                            @if(@$producto->archivos->count() !=0)
                                                @foreach($producto->archivos as $file)

                                                    <?php
                                                    $archivo=explode('.',$file->path);
                                                    // echo $archivo[1];
                                                    if(($archivo[1]=='png') ||($archivo[1]=='jpg')||($archivo[1]=='jpeg')||($archivo[1]=='gif'))
                                                    {
                                                    ?>
                                                    <a data-toggle="modal" data-target="#myModal"  data-id="{{$file->path}}" class="open-Modal"> <i class="text-warning fa fa-file-image-o "></i></a>
                                                    <?php
                                                    }
                                                    if ($archivo[1]=='pdf')
                                                    {
                                                    ?>


                                                    <a  target="_blank"  href="{{'/storage/'}}{{$file->path}}" > <i class="text-danger fa fa-file-pdf-o "></i></a>
                                                    <?php  }
                                                    if(($archivo[1]=='doc')||($archivo[1]=='docx')||($archivo[1]=='xls')||($archivo[1]=='xlsx'))
                                                    {
                                                    ?>

                                                    <a  target="_blank"  href="{{'/storage/'}}{{$file->path}}" > <i class="text-info fa fa-file-archive-o "></i></a>
                                                    <?php  } ?>
                                                @endforeach

                                            @endif
                                        </td>
                                        <td><small>
                                            <a href="{{ url('/mantenedores/productos_editar/'.$producto->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                            <a class="del" id="{{$producto->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a>
                                            || <a data-toggle="modal" data-target="#myModal2"  data-id="{{$producto->id}}" class="open-Modal2"> <i class="fa fa-upload text-primary " title="Adjuntar Imagen o Ficha"></i></a>
                                            </small>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <!--  modal -->
                    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">
                                <!-- <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                     <i class="fa fa-laptop modal-icon"></i>
                                     <h4 class="modal-title">Modal title</h4>
                                     <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                 </div>-->
                                <div class="modal-body">

                                    <div class="dropbox">
                                        <img  id="imageBox"  class="img-responsive img-thumbnail" >
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal file -->

                    <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <form name="form_archivos" id="form_archivos" enctype="multipart/form-data" class="form-horizontal" method="post">
                                {{csrf_field()}}
                                <div class="modal-content animated bounceInRight">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Subir Imagen o Ficha del Producto  <i class="fa fa-upload"></i></h4>
                                        <div>
                                            @if (count($errors)>0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach($errors->all() as $error)
                                                            <li>{{$error}}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="modal-body">

                                        <input type="hidden" name="id_report" id="id_report" value="">
                                        <div class="ibox-content">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="fileinput fileinput-new " data-provides="fileinput">
                                                                                             <span class="btn btn-default btn-file "><text class="text-success" >Puede Seleccionar varios a la vez</text>
                                                                                             <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple/>
                                                                                             </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success" >Subir Archivo(s)</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        var exito
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "ordering": false,
               // "order": [[ 1, "desc" ]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Productos e Insumos de Teledata'},
                    {extend: 'pdf', title: 'Productos e Insumos de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

        $('.del').click(function () { //RV Dice: SWAL para eliminar informes

            var dis=this;

            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/mantenedores/bodegas_eliminar/"+dis.id+"", function  (response) {

                            exito=response.Success;
                        });

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                        $(dis).closest('tr').remove();

                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

        // abrir modal para ver cargar archivos en compra finalizada.
        $(document).on("click", ".open-Modal2", function () {
            var myDNI = $(this).data('id');
            document.getElementById('id_report').value =  myDNI ;
        });

        //********************************************************** subir y guardar archivos desde la modal ********************************

        $('#form_archivos').on('submit',function(e) {
            e.preventDefault(e);
            var dis=this;
            // console.log( id_report);
            console.log(dis);
            $.ajax({

                url: '/mantenedores/productos_agregar_archivos',
                type: $(dis).attr("method"),
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    $('#form_archivos').trigger("reset");
                    swal("OK", "Registro Guardado", "success");
                    $('#myModal2').hide(); //cierre de modal
                    location.reload();

                },
                error: function (data) {
                    $('#form_archivos').trigger("reset");
                    swal("ERROR", "NO seleccionaste ningun archivo!", "error");
                    $('.modal-backdrop').remove();//eliminamos el backdrop del modal

                }
            })

        });
        // abrir modal para ver archivos adjuntos
        $(document).on("click", ".open-Modal", function () {
            var myDNI = $(this).data('id');
            document.getElementById('imageBox').src = '{{'../storage/'}}' + myDNI ;
        });


    </script>
@endsection
