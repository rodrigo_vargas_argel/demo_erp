@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Maquinaria</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5><i class="fa fa-shopping-bag"> </i>  Editar Maquinaria <small> Aquí también puedes crear nuevos Tipos de maquinarias, Marcas y Modelos [+]</small></h5>

                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-6">

                                <form name="frmProducto" method="post" action="{{ url('mantenedores/productos_modificar') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Tipo de Maquinaria</label>
                                        <input type="hidden" name="id_" value="{{$consulta->id}}">
                                        <div class="input-group">
                                            <select name="tipo_producto" id="tipo_producto" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($tipos as $tipo )
                                                    <option value="{{$tipo->id}}" {{ $consulta->mantenedor_tipo_producto_id == $tipo->id ? 'selected' : '' }}>{{$tipo->nombre}}</option>
                                                @endforeach
                                            </select>

                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus"></i>
                                                </button>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <label >Marca Maquinaria</label>
                                        <div class="input-group">
                                            <select name="marca" id="marca" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($marcas as $marca )
                                                    <option value="{{$marca->id}}" {{ $consulta->mantenedor_marca_id == $marca->id ? 'selected' : '' }}>{{$marca->nombre}}</option>
                                                @endforeach
                                            </select>

                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label >Modelo</label>
                                        <div class="input-group">
                                            <select name="modelo" id="modelo" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($modelos as $modelo )
                                                    <option value="{{$modelo->id}}" {{ $consulta->mantenedor_modelo_id == $modelo->id ? 'selected' : '' }}>{{$modelo->nombre}}</option>
                                                @endforeach
                                            </select>

                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Código o Nombre para futuras búsquedas</label>
                                        <input type="text" name="codigo_barra" class="form-control" value="{{ $consulta->codigo_barra}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Descripcion</label>
                                        <input type="text" name="descripcion" class="form-control" value="{{$consulta->descripcion}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Precio Referencial Arriendo</label>
                                        <input type="number" value="" class="form-control" name="proveedor" id="proveedor" value="{{$consulta->mantenedor_proveedores_id}}">


                                    </div>

                                    <div class="form-group">
                                    <!-- <label for="">Unico?</label>
                                        <select name="unico" class="form-control">
                                            <option value="0" {{ old('unico') == 0 ? 'selected' : '' }}>NO</option>
                                            <option value="1" {{ old('unico') == 1 ? 'selected' : '' }}>SÍ</option>
                                        </select>-->
                                        <input type="hidden" name="unico" value="0">


                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary pull-right" type="submit" onclick="frmProducto.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!- ******************************MODALS***************************************************************************************************************************************-->
                        <div>


                            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated bounceInRight">

                                        <div class="modal-body">

                                            <div class="form-group"><i class="fa fa-shopping-bag"></i> <label> Ingresa un nuevo tipo de producto</label>
                                                <input type="text" name="txt_nuevo_tipo" id="txt_nuevo_tipo" placeholder="Escribe aqui..." class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="button" name="btn_tipo" id="btn_tipo" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ****************************** -->
                        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">

                                    <div class="modal-body">

                                        <div class="form-group"><i class="fa fa-map-marker"></i> <label> Ingresa una nueva Marca</label>
                                            <input type="text" name="txt_nueva_marca" id="txt_nueva_marca" placeholder="Escribe aqui..." class="form-control">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                        <button type="button"  name="btn_marca" id="btn_marca" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************************** -->

                    <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">

                                <div class="modal-body">

                                    <div class="form-group"><i class="fa fa-mobile"></i> <label> Ingresa un nuevo Modelo</label>
                                        <input type="text" name="txt_nuevo_modelo" id="txt_nuevo_modelo" placeholder="Escribe aqui..." class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                    <button type="button" name="btn_modelo" id="btn_modelo" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ****************************** -->
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
@section('mijava')
    <script>
        document.onkeydown=function(evt){
            var keyCode = evt.keyCode;
            if(keyCode == 13)
            {
                return false;
            }
        }

        //*********************************** cambio combo marca
        $('#marca').on('change',function() {

            //alert('cambio marca');
            var id_
            id_=this.value;
            // alert('cambio marca'+id_)
            $.ajax({

                url: '/cargar_marcas_modelos/'+id_,
                //type: $(dis).attr("method"),
                dataType: "JSON",
                //data: new FormData(this),
                processData: false,
                contentType: false,

                //dataType: 'json',
                success: function (data) {
                    console.log(data.data.length);
                    $("#modelo").empty();
                    for(var i=0; i<data.data.length; i++){
                        $("#modelo").append("<option value='"+data.data[i].id+"'>"+data.data[i].nombre+"</option>")
                    }

                },
                error: function (data) {

                    swal("ERROR", "NO seleccionaste ninguna marca!", "error");


                }
            })
        })

        //***************************************************************** NUEVO TIPO

        $('#btn_tipo').on('click',function() {

            var nombre_tipo
            nombre_tipo=document.getElementById("txt_nuevo_tipo").value;

            $.ajax({

                url: '/nuevo_tipo_producto/'+nombre_tipo,
                dataType: "JSON",
                processData: false,
                contentType: false,

                success: function (data) {
                    $("#tipo_producto").append("<option value='"+data.sql+"'>"+nombre_tipo+"</option>")
                    swal("Listo!", nombre_tipo + " ha sido almacenado con éxito! id:"+data.sql, "success");
                    $('#myModal').modal('hide');

                },
                error: function (data) {
                    nombre_tipo==""? nombre_tipo=', No dejes campos en blanco':nombre_tipo=nombre_tipo;
                    swal("ERROR", "NO se ha podido guardar "+nombre_tipo+" - "+data.sql, "error");

                }
            })
        })

        //***************************************************************** NUEVA MARCA

        $('#btn_marca').on('click',function() {

            var nombre_marca
            nombre_marca=document.getElementById("txt_nueva_marca").value;

            $.ajax({

                url: '/nueva_marca_producto/'+nombre_marca,
                dataType: "JSON",
                processData: false,
                contentType: false,

                success: function (data) {
                    $("#marca").append("<option value='"+data.sql+"'>"+nombre_marca+"</option>")
                    swal("Listo!", "La marca "+nombre_marca + " ha sido almacenada con éxito! id:"+data.sql, "success");
                    $('#myModal2').modal('hide');

                },
                error: function (data) {
                    nombre_marca==""? nombre_marca=', No dejes campos en blanco':nombre_marca=nombre_marca;
                    swal("ERROR", "NO se ha podido guardar la marca "+nombre_marca+" - "+data.sql, "error");

                }
            })
        })

        //***************************************************************** NUEVO MODELO

        $('#btn_modelo').on('click',function() {

            var nombre_modelo
            var marca_id = document.getElementById('marca').value;
            // alert(marca_id)
            if (marca_id ===""){
                swal("COMENTISTE UN ERROR", "Primero debes seleccionar una marca y luego crear el modelo", "error");
            }
            else{

                nombre_modelo=document.getElementById("txt_nuevo_modelo").value;

                $.ajax({

                    url: '/nuevo_modelo_producto/'+marca_id+'/'+nombre_modelo,
                    dataType: "JSON",
                    processData: false,

                    contentType: false,

                    success: function (data) {
                        $("#modelo").append("<option value='"+data.sql+"'>"+nombre_modelo+"</option>")
                        swal("Listo!", "El Modelo "+nombre_modelo + " ha sido almacenado con éxito! id:"+data.sql, "success");
                        $('#myModal3').modal('hide');

                    },
                    error: function (data) {
                        nombre_modelo==""? nombre_modelo=', No dejes campos en blanco':nombre_modelo=nombre_modelo;
                        swal("ERROR", "NO se ha podido guardar "+nombre_modelo+" - "+data.sql, "error");

                    }
                })

            }

        })
    </script>
@endsection
