@extends('layouts.app')
@section('title')
    Costos
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Costos</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/mantenedores/costos_nuevo') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Costos Almacendados</h5>

                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Codigo Cuenta</th>
                                    <th>Responsable</th>
                                    <th>Correo</th>
                                    <th>Accion(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($consulta as $cont=>$costo)
                                    <tr>
                                        <td>
                                            {{$costo->id}}
                                        </td>
                                        <td>
                                            {{$costo->nombre}}
                                        </td>
                                        <td>
                                            {{$costo->codigo_cuenta}}
                                        </td>
                                        <td>
                                            {{$costo->responsable->nombre}}
                                        </td>
                                        <td>
                                            {{$costo->responsable->email}}
                                        </td>
                                        <td>
                                            <a href="{{ url('/mantenedores/costos_editar/'.$costo->id) }}" ><i class="fa fa-edit" title="Editar Registro"></i></a> ||
                                            <a class="delbodega" id="{{$costo->id}}" href="#" ><i class=" text-danger fa fa-trash" title="Eliminar Registro "></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('mijava')
    <script>
        var exito
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Lista de Costos de Teledata'},
                    {extend: 'pdf', title: 'Lista Costos de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

        $('.delbodega').click(function () { //RV Dice: SWAL para eliminar informes

            var dis=this;

            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/mantenedores/costos_eliminar/"+dis.id+"", function  (response) {
                            exito=response.Success;
                            $(dis).closest('tr').remove();
                        });

                            swal("Eliminado!", "Este registro ha sido eliminado.", "success");


                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

    </script>
@endsection
