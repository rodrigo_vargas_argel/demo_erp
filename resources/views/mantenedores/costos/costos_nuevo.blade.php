@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Nuevo Costo</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5>Crear Nuevo Costo <small> </small></h5>

                            </div>

                                <div>
                                    @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                        <div class="ibox-content">
                            <div class="row">
                               <div class="col-lg-6">

                                    <form name="frmBodega" method="post" action="{{ url('mantenedores/costos_guardar') }}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Codigo Cuenta</label>
                                            <input type="text" name="codigo_cuenta" class="form-control" value="{{old('codigo_cuenta')}}">
                                        </div>

                                        <div class="form-group">
                                            <label >Responsable</label>
                                            <select name="personal_id" class="form-control">
                                                <option value="">-Seleccione-</option>
                                                @foreach($responsables as $responsable )
                                                    <option value="{{$responsable->id}}" {{ old('personal_id') == $responsable->id ? 'selected' : '' }}>{{$responsable->nombre}}</option>
                                                @endforeach
                                            </select>

                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <button class="btn btn-primary pull-right" type="submit" onclick="frmBodega.submit();this.disabled=true"><i class="fa fa-save"></i> Guardar</button>
                                        </div>
                                    </form>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
