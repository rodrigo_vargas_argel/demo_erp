@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a>Mantenedores</a>
                </li>
                <li class="active">
                    <strong>Editar Costo</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Editar Costo <small> </small></h5>

                    </div>


                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-7">
                                <form name="frmBodega" method="post" action="{{ url('mantenedores/costos_modificar') }}">

                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="">Nombre</label>
                                        <input type="hidden" name="id" value="{{$consulta[0]->id}}">
                                        <input type="text" name="nombre" class="form-control" value="{{$consulta[0]->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Codigo Cuenta</label>
                                        <input type="text" name="codigo_cuenta" class="form-control" value="{{$consulta[0]->codigo_cuenta}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Persona Id</label>
                                        <select name="personal_id" class="form-control">
                                            <option value="">-Seleccione-</option>
                                            @foreach($responsables as $responsable )
                                                <option value="{{$responsable->id}}" {{ $consulta[0]->personal_id == $responsable->id ? 'selected' : '' }}>{{$responsable->nombre}}</option>
                                            @endforeach
                                        </select>
                                        <!--<input type="text" name="personal_id" class="form-control" value="{{--$consulta[0]->personal_id--}}">-->
                                    </div>

                                    <br>
                                    <div class="form-data">
                                        <button class="btn btn-primary" type="submit" onclick="frmBodega.submit();this.disabled=true"><i class="fa fa-save"></i> Modificar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
