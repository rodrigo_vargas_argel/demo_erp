@extends('layouts.app')
@section('title')
    Tickets
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Tickets</a>
                </li>
                <li class="active">
                    <strong>Lista</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="head-list" style="padding: 10px;">
                <a href="{{ url('/tickets/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Registro </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Lista de Tickets</h5>

                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Origen</th>
                                    <th>Tipo</th>
                                    <th>Estado</th>
                                    <th>Cliente</th>
                                    <th>Servicio</th>
                                    <th>Estacion</th>
                                    <th>Area</th>
                                    <th>Accion(s)</th>
                                    <th>OT</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tickets as $key=>$ticket)
                                    <tr>
                                        <td>
                                            {{$ticket->id}}
                                        </td>
                                        <td>
                                            {{$ticket->ticket_origen->nombre}}
                                        </td>
                                        <td>
                                            {{$ticket->ticket_tipo->nombre}}
                                        </td>
                                        <td>
                                            {{$ticket->ticket_estado->nombre}}
                                        </td>
                                        <td>
                                            {{$ticket->clientes?$ticket->clientes->nombre:''}}
                                        </td>
                                        <td>
                                            {{$ticket->servicios?$ticket->servicios->Codigo:''}}
                                        </td>
                                        <td>
                                            {{$ticket->estacion?$ticket->estacion->nombre:''}}
                                        </td>
                                        <td>
                                            {{$ticket->ticket_area?$ticket->ticket_area->nombre:''}}
                                        </td>
                                        <td>
                                            <a href="{{ url('/tickets/'.$ticket->id.'/edit/') }}" ><i class="fa fa-edit" title="Editar Ticket"></i></a>
                                            <button class="btn btn-success" onclick="cargarComentarios({{$ticket->id}})"><i class="fa fa-comment" title="Comentarios"></i></button>
                                            
                                        </td>
                                        <td>
                                            @if (($ticket->ticket_ot->count() > 0) && ($ticket->nivel_id == 3))
                                                @php ($fila = $ticket->ticket_ot->first())
                                                <a target="_blank" class="fa fa-eye" title="VER OT" href="{{ route('ots.show', $fila->id) }}"></a>
                                            @elseif ($ticket->nivel_id == 3)
                                                <a class="fa fa-copy" title="CREAR OT" href="{{ route('ots.crear',$ticket->id) }}"></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Historial Ticket</h4>
        </div>
        <div class="modal-body">
        <h3 style="width: 100%;background-color:#4CAF50;color:#fff;padding:0.1em;">Comentarios</h3>
          <div class="row">
              <div class="col-md-12">
                  <div id="tabla_comentarios"></div>
              </div>
          </div>
          <h3 style="width: 100%;background-color:#03A9F4;color:#fff;padding:0.1em;">Eventos</h3>
          <div class="row">
            <div class="col-md-12">
                <div id="tabla_auditoria"></div>
            </div>
        </div>
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </div>
  </div>
  
  
@endsection
@section('mijava')
    <script>

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                order: [[0, "desc"]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'excel', title: 'Tickets de Teledata'},
                    {extend: 'pdf', title: 'Tickets de Teledata'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                language:{
                        processing:     "Cargando...",
                        zeroRecords:    "No se Encontraron Coincidencias",
                        info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        infoEmpty:      "Mostrando 0 a 0 de 0 registros",
                        lengthMenu:     "Mostrar _MENU_ Registros",
                        search:         "Buscar:",
                        searchPlaceholder: "Buscar registros...",
                        infoFiltered:   "(Filtrados de _MAX_ Registros Totales)",
                        paginate: {
                        first:      "Primero",
                        last:       "Ultimo",
                        next:       "Siguiente",
                        previous:   "Anterior"
                    }
                }
                

            });

        });

        $('.delbodega').click(function () { //RV Dice: SWAL para eliminar informes

            var dis=this;

            swal({
                    title: "Estas seguro de eliminar?",
                    text: "no podrás recuperar este registro en el futuro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, Eliminar no mas!",
                    cancelButtonText: "No, me arrepentí!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log('holaa: '+ (dis.name));
                        $.get("/mantenedores/bodegas_eliminar/"+dis.id+"", function (response) {
                            // console.log(response.success)
                            $(dis).closest('tr').remove();
                        });

                        swal("Eliminado!", "Este registro ha sido eliminado.", "success");
                    } else {
                        swal("Cancelado", "Este registro aun sigue vigente :)", "error");
                    }
                });
        });

        function cargarComentarios(id){
            console.log('id',id);
            $.ajax({
                type:'GET',
                dataType:'json',
                url:`/ticket/${id}/resumen_historico`,

                success:function(data){

                    let comentarios = data.comentarios;
                    let auditorias = data.auditorias;

                    let tabla_comentarios = `
                    <div class="table-responsive">
                    <table class="table table-bordered">
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Usuario
                                </th>
                                <th>
                                    Comentario
                                </th>
                            </tr>
                        </thead>
                        <tbody>`;
                            comentarios.forEach(c => {
                                tabla_comentarios += `<tr><td>${c.created_at}</td><td>${c.usuario.nombre}</td><td>${c.comentario}</td></tr>`;
                            });
                    tabla_comentarios += `</tbody></table></div>`;

                    $('#tabla_comentarios').html(tabla_comentarios);


                    /*tabla auditoria*/
                    let tabla_auditoria = `
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Evento
                                </th>
                                <th>
                                    Usuario
                                </th>
                                <th>
                                    Comentario
                                </th>
                            </tr>
                        </thead>
                        <tbody>`;
                            auditorias.forEach(c => {
                                tabla_auditoria += `<tr><td>${c.created_at}</td><td>${c.ticket_auditoria_evento.nombre}</td><td>${c.usuario.nombre}</td><td>${c.comentario}</td></tr>`;
                            });
                            tabla_auditoria += `</tbody></table></div>`;

                    $('#tabla_auditoria').html(tabla_auditoria);

                     $('#myModal').modal('show');


                }

            });
        };

    </script>
@endsection

