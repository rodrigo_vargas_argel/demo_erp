@extends('layouts.app')
@section('title')
   Crear Ticket
@endsection
@section('scriptshead')
<style>
    .hidden{
        display:none;
    }
</style>
@endsection
@section('content')



    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><img src="{!! asset('img/logo_ico.png') !!}"> Teledata ERP</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a>Ticket</a>
                </li>
                <li class="active">
                    <strong>Crear Ticket</strong>
                </li>
            </ol>
        </div>

    </div>
    @if(old('msg'))
    <div class="alert alert-success">
        {{old('msg')}}
    </div>
    @endif
    <div class="wrapper wrapper-content animated fadeInRight">

        <!-- row formulario ejemplo -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5>Crear Ticket <small> </small></h5>

                    </div>

                    <div>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        <form name="frmTicket" method="post" action="{{ route('tickets.store') }}">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Origen</label>
                                        <select value="{{old('origen_id')}}" name="origen_id" id="origen_id" class="form-control">
                                            <option value="">Seleccione Origen</option>
                                            @foreach ($origenes as $o)
                                                <option value="{{$o->id}}" {{old('origen_id')==$o->id?'selected':''}}>{{$o->nombre}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-4 hidden" id="tipo">
                                    <div class="form-group">
                                        <label for="">Tipo</label>
                                        <select name="tipo_id" id="tipo_id" value="{{old('tipo_id')}}" class="form-control">
                                            <option value="">Seleccione Tipo</option>
                                            @foreach ($tipos as $t)
                                                <option value="{{$t->id}}" {{old('tipo_id')==$t->id?'selected':''}}>{{$t->nombre}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Area</label>
                                        <select name="area_id" id="area_id" value="{{old('area_id')}}" class="form-control">
                                            <option value="">Seleccione Area</option>
                                            @foreach ($areas as $a)
                                                <option value="{{$a->id}}" {{old('area_id')==$a->id?'selected':''}}>{{$a->nombre}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>   
                            </div>

                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        

                                        <label for="">Nivel</label>
                                        <select name="nivel_id" id="nivel_id" value="{{old('nivel_id')}}" class="form-control">
                                            <option value="">Seleccione Nivel</option>
                                            @foreach ($niveles as $n)
                                                <option value="{{$n->id}}" {{old('nivel_id')==$n->id?'selected':''}}>{{$n->nombre}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                                
                               <div class="col-lg-4" style="visibility: hidden;">
                                    <div class="form-group">
                                        <label for="">Prioridad</label>
                                        <select name="prioridad_id" class="form-control" value="{{old('prioridad_id')}}">

                                            @foreach ($prioridades as $p)
                                                <option {{old('prioridad_id')==$p->id?'selected':''}} value="{{$p->id}}">{{$p->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 hidden" id="tecnico">
                                    <div class="form-group">
                                            <label for="">Técnico</label>
                                            <select value="{{old('tecnico_id')}}" name="tecnico_id" id="select_tecnicos" class="form-control">
                                                <option value="">Seleccione Técnico</option>
                                                @foreach ($tecnicos as $t)
                                                    <option value="{{$t->id}}" {{old('tecnico_id')==$t->id?'selected':''}}>{{$t->nombre}}</option>
                                                @endforeach
                                            </select>

                                    </div>
                                </div>
                                <div class="col-lg-4 hidden" id="estacion">
                                    <div class="form-group">
                                            <label for="">Estacion</label>
                                            <select value="{{old('estacion_id')}}" name="estacion_id" class="form-control">
                                                <option value="">Seleccione Estación</option>
                                                @foreach ($estaciones as $e)
                                                    <option value="{{$e->id}}" {{old('estacion_id')==$e->id?'selected':''}}>{{$e->nombre}}</option>
                                                @endforeach
                                            </select>

                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Cliente</label>
                                        <select value="{{old('cliente_id')}}" class="col-sm-10 form-control" name="cliente_id" id="select_cliente">
                                            <option value="">Seleccione Cliente</option>
                                            @foreach($clientes as $c)
                                                <option value="{{$c->id}}" rut="{{$c->rut}}" {{old('cliente_id')==$c->id?'selected':''}}>{{$c->nombre}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Servicio</label>
                                    <select name="servicios_id" value="{{old('servicios_id')}}" class="form-control" id="select_servicios">
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 hidden" id="fecha">
                                    <div class="form-group">
                                        <label for="">Fecha Visita</label>
                                        <input type="date" class="form-control" name="fecha_visita" value="{{old('fecha_visita')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 hidden" id="hora">
                                    <div class="form-group">
                                        <label for="">Hora Visita</label>
                                        <input type="time" class="form-control" name="hora_visita" value="{{old('hora_visita')}}">
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="row">
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Estado Ticket</label>
                                        <select name="estado_id" id="select_estado" class="form-control" value="{{old('estado_id')}}">
                                            <option value="">Seleccione Estado</option>
                                            @foreach ($estados as $e)
                                                <option value="{{$e->id}}" {{ old('estado_id') == $e->id ? 'selected' : '' }}>{{$e->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> --}}


                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Descripción</label>
                                        <textarea name="descripcion" value="{{old('descripcion')}}" id="" cols="30" rows="5" class="form-control">{{old('descripcion')}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" onclick=""><i class="fa fa-save"></i> Crear</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>

        

    </div>
        @endsection
<!-- tus funciones javascript locales -->
        @section('mijava')
            <script>
                $(document).ready(function() {

                    /*marcando por defecto estado creado*/
                    $("#select_estado option[value='1']").prop('selected', true);
                    /*marcando por defecto estado creado*/

                    /*marcando por defecto origen llamado telefónico*/
                    $("#origen_id option[value='1']").prop('selected', true);
                    /*marcando por defecto origen llamado telefónico*/

                    /*marcando por defecto el area Area Tecnica*/
                    $("#area_id option[value='1']").prop('selected', true);
                    /*marcando por defecto el area Area Tecnica*/

                    /*marcando por defecto el tipo Solicitud Cliente*/
                    $("#tipo_id option[value='1']").prop('selected', true);
                    /*marcando por defecto el tipo Solicitud Cliente*/

                    /*marcando por defecto el nivel Nivel 1*/
                    $("#nivel_id option[value='1']").prop('selected',true);
                    /*marcando por defecto el nivel Nivel 1*/



                    var servicios_id = {!! json_encode(old('servicios_id')) !!};
                    var cliente_id = {!! json_encode(old('cliente_id')) !!};
                    var nivel_usuario = {!! json_encode(Auth::user()->nivel)!!};

                    
                    console.log('nivel usuario',nivel_usuario);

                    if(cliente_id){
                        var opcion = $('#select_cliente').find('option:selected');
                        var rut = opcion.attr("rut"); 
                        console.log('servicios_old',servicios_id,'rut',rut);
                        $.ajax({

                            url: '/inventarios/cargar_servicios/'+rut,
                            //type: $(dis).attr("method"),
                            dataType: "JSON",
                            //data: new FormData(this),
                            processData: false,
                            contentType: false,

                            //dataType: 'json',
                            success: function (data) {
                                console.log('servicios: '+data.servicios.length);
                                $("#select_servicios").empty();
                                for(var i=0; i<data.servicios.length; i++){
                                // console.log('servicios2: '+data.servicios);
                                    $("#select_servicios").append("<option value='"+data.servicios[i].Id+"'>"+data.servicios[i].Codigo+"</option>")
                                }

                                $("#select_servicios option[value='"+servicios_id+"']").prop('selected', true);


                            },
                            error: function (data) {

                                swal("ERROR", "NO ha seleccionado ningun cliente", "error");


                            }
                        });
                    }

                    $("#area_id").change(function(){
                        var id = $(this).children("option:selected").val();
                        if(id == 3){
                            /*seleccionando por defecto nivel Terreno cuando el area sea Torre Control*/
                            $("#nivel_id option[value='3']").prop('selected', true);
                            if(nivel_usuario != 1){
                                $("#fecha").removeClass("hidden");
                                $("#hora").removeClass("hidden");

                                $('#tipo').removeClass('hidden');
                                $('#tecnico').removeClass('hidden');
                                $('#estacion').removeClass('hidden');
                            }
                        }else{
                            $("#nivel_id option[value='1']").prop('selected', true);
                            $( "#fecha" ).addClass( "hidden" );
                            $( "#hora" ).addClass( "hidden" );

                            $('#tipo').addClass('hidden');
                            $('#tecnico').addClass('hidden');
                            $('#estacion').addClass('hidden');
                        }
                    })

                    $("#nivel_id").change(function(){
                        /*cargar tecnicos*/
                        var id = $(this).children("option:selected").val();
                        if(id == 3){
                            

                            /*seteando el area   Torre control cuando el nivel sea 3 (Terreno)*/
                            $("#area_id option[value='3']").prop('selected', true);
                            if(nivel_usuario != 1){
                                $("#fecha").removeClass("hidden");
                                $("#hora").removeClass("hidden");

                                $('#tipo').removeClass('hidden');
                                $('#tecnico').removeClass('hidden');
                                $('#estacion').removeClass('hidden');
                            }
                            

                        }/*else if(id == 1 || id == 2){*/
                        else{
                            $("#area_id option[value='1']").prop('selected', true);
                            $( "#fecha" ).addClass( "hidden" );
                            $( "#hora" ).addClass( "hidden" );

                            $('#tipo').addClass('hidden');
                            $('#tecnico').addClass('hidden');
                            $('#estacion').addClass('hidden');

                        }

                        if(id == 2 || id == 3){
                            
                            $.ajax({

                                url: `/usuarios/nivel_usuario/${id}`,
                                dataType: "JSON",
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    console.log('tecnicos: '+data.tecnicos.length);
                                    $("#select_tecnicos").empty();
                                    data.tecnicos.forEach(t => {
                                        $("#select_tecnicos").append(`<option value="${t.id}">${t.nombre}</option>`);
                                    });

                                },
                                error: function (data) {

                                    //swal("ERROR", "NO ha seleccionado ningun cliente", "error");


                                }
                            });
                        }
                        //alert(id);
                    });
                    
                    $("#select_cliente").change(function(){

                        var rut = $(this).children("option:selected").attr('rut');

                        $.ajax({

                            url: "{{url('/inventarios/cargar_servicios')}}/"+rut,
                            //type: $(dis).attr("method"),
                            dataType: "JSON",
                            //data: new FormData(this),
                            processData: false,
                            contentType: false,

                            //dataType: 'json',
                            success: function (data) {
                                console.log('servicios: '+data.servicios.length);
                                $("#select_servicios").empty();
                                for(var i=0; i<data.servicios.length; i++){
                                // console.log('servicios2: '+data.servicios);
                                    $("#select_servicios").append("<option value='"+data.servicios[i].Id+"'>"+data.servicios[i].Codigo+"</option>")
                                }

                            },
                            error: function (data) {

                                swal("ERROR", "NO ha seleccionado ningun cliente", "error");


                            }
                        });

                    });



                    // $('.footable').footable();
                    // $('.footable2').footable();

                    /*$('.dataTables-example').DataTable({

                        pageLength: 25,
                        order: [[0, "desc"]],
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        sum: 5,
                        buttons: [

                            {extend: 'excel', title: 'Documentos Pagados'},
                            {extend: 'pdf', title: 'Documentos Pagados'},


                        ],


                    });*/


                });


            </script>
@endsection