/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


// moment
import moment from 'moment'
Vue.prototype.moment = moment;
window.moment = require('moment');

//vue-multiselect
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)
 
import VueMaterial from 'vue-material'
// import 'vue-material/dist/vue-material.min.css'
// import 'vue-material/dist/theme/default.css'
import 'vue-material-design-icons/styles.css';
Vue.use(VueMaterial)

/**filtros */
Vue.filter('formatearFecha', function (valor) {
    if (!valor) return ''
    return moment(valor).format("DD-MM-YYYY");
})

Vue.filter('formatearHora', function (valor) {
    if (!valor) return ''
    return moment(valor).format("HH:mm");
})

Vue.filter('fechaCompleta', function (valor) {
    if (!valor) return ''
    return moment(valor).format("DD-MM-YYYY HH:mm");
})

/**sweet alert 2 */
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('modal-comentario', require('./components/ModalComentario.vue').default);
Vue.component('modal-factibilidad', require('./components/Modal_Factibilidad.vue').default);
Vue.component('calendario', require('./vistas/calendario.vue').default);




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 import Tabla_Factibilidad from './components/Tabla_Factibilidad.vue';
 import Detalle_Factibilidad from './components/Detalle_Factibilidad.vue';


const app = new Vue({
    el: '#app',
    components:{
        'tabla-factibilidad':Tabla_Factibilidad,
        'detalle-factibilidad':Detalle_Factibilidad
    }
});
